(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('three'), require('gsap'), require('currencyformatter.js')) :
    typeof define === 'function' && define.amd ? define(['exports', 'three', 'gsap', 'currencyformatter.js'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.configuratorSDK = {}, global.THREEJS.THREE, global.GSAP, global.OSREC.CurrencyFormatter));
})(this, (function (exports, THREE, gsap, OSREC) { 'use strict';

    function _interopNamespaceDefault(e) {
        var n = Object.create(null);
        if (e) {
            Object.keys(e).forEach(function (k) {
                if (k !== 'default') {
                    var d = Object.getOwnPropertyDescriptor(e, k);
                    Object.defineProperty(n, k, d.get ? d : {
                        enumerable: true,
                        get: function () { return e[k]; }
                    });
                }
            });
        }
        n.default = e;
        return Object.freeze(n);
    }

    var THREE__namespace = /*#__PURE__*/_interopNamespaceDefault(THREE);
    var OSREC__namespace = /*#__PURE__*/_interopNamespaceDefault(OSREC);

    /******************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise, SuppressedError, Symbol */


    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    typeof SuppressedError === "function" ? SuppressedError : function (error, suppressed, message) {
        var e = new Error(message);
        return e.name = "SuppressedError", e.error = error, e.suppressed = suppressed, e;
    };

    /** @returns {void} */
    function noop() {}

    function run(fn) {
    	return fn();
    }

    function blank_object() {
    	return Object.create(null);
    }

    /**
     * @param {Function[]} fns
     * @returns {void}
     */
    function run_all(fns) {
    	fns.forEach(run);
    }

    /**
     * @param {any} thing
     * @returns {thing is Function}
     */
    function is_function(thing) {
    	return typeof thing === 'function';
    }

    /** @returns {boolean} */
    function safe_not_equal(a, b) {
    	return a != a ? b == b : a !== b || (a && typeof a === 'object') || typeof a === 'function';
    }

    let src_url_equal_anchor;

    /**
     * @param {string} element_src
     * @param {string} url
     * @returns {boolean}
     */
    function src_url_equal(element_src, url) {
    	if (element_src === url) return true;
    	if (!src_url_equal_anchor) {
    		src_url_equal_anchor = document.createElement('a');
    	}
    	// This is actually faster than doing URL(..).href
    	src_url_equal_anchor.href = url;
    	return element_src === src_url_equal_anchor.href;
    }

    /** @returns {boolean} */
    function is_empty(obj) {
    	return Object.keys(obj).length === 0;
    }

    function subscribe(store, ...callbacks) {
    	if (store == null) {
    		for (const callback of callbacks) {
    			callback(undefined);
    		}
    		return noop;
    	}
    	const unsub = store.subscribe(...callbacks);
    	return unsub.unsubscribe ? () => unsub.unsubscribe() : unsub;
    }

    /**
     * Get the current value from a store by subscribing and immediately unsubscribing.
     *
     * https://svelte.dev/docs/svelte-store#get
     * @template T
     * @param {import('../store/public.js').Readable<T>} store
     * @returns {T}
     */
    function get_store_value(store) {
    	let value;
    	subscribe(store, (_) => (value = _))();
    	return value;
    }

    /** @returns {void} */
    function component_subscribe(component, store, callback) {
    	component.$$.on_destroy.push(subscribe(store, callback));
    }

    /**
     * @param {Node} target
     * @param {Node} node
     * @returns {void}
     */
    function append(target, node) {
    	target.appendChild(node);
    }

    /**
     * @param {Node} target
     * @param {string} style_sheet_id
     * @param {string} styles
     * @returns {void}
     */
    function append_styles(target, style_sheet_id, styles) {
    	const append_styles_to = get_root_for_style(target);
    	if (!append_styles_to.getElementById(style_sheet_id)) {
    		const style = element('style');
    		style.id = style_sheet_id;
    		style.textContent = styles;
    		append_stylesheet(append_styles_to, style);
    	}
    }

    /**
     * @param {Node} node
     * @returns {ShadowRoot | Document}
     */
    function get_root_for_style(node) {
    	if (!node) return document;
    	const root = node.getRootNode ? node.getRootNode() : node.ownerDocument;
    	if (root && /** @type {ShadowRoot} */ (root).host) {
    		return /** @type {ShadowRoot} */ (root);
    	}
    	return node.ownerDocument;
    }

    /**
     * @param {ShadowRoot | Document} node
     * @param {HTMLStyleElement} style
     * @returns {CSSStyleSheet}
     */
    function append_stylesheet(node, style) {
    	append(/** @type {Document} */ (node).head || node, style);
    	return style.sheet;
    }

    /**
     * @param {Node} target
     * @param {Node} node
     * @param {Node} [anchor]
     * @returns {void}
     */
    function insert(target, node, anchor) {
    	target.insertBefore(node, anchor || null);
    }

    /**
     * @param {Node} node
     * @returns {void}
     */
    function detach(node) {
    	if (node.parentNode) {
    		node.parentNode.removeChild(node);
    	}
    }

    /**
     * @template {keyof HTMLElementTagNameMap} K
     * @param {K} name
     * @returns {HTMLElementTagNameMap[K]}
     */
    function element(name) {
    	return document.createElement(name);
    }

    /**
     * @param {string} data
     * @returns {Text}
     */
    function text(data) {
    	return document.createTextNode(data);
    }

    /**
     * @returns {Text} */
    function space() {
    	return text(' ');
    }

    /**
     * @returns {Text} */
    function empty() {
    	return text('');
    }

    /**
     * @param {EventTarget} node
     * @param {string} event
     * @param {EventListenerOrEventListenerObject} handler
     * @param {boolean | AddEventListenerOptions | EventListenerOptions} [options]
     * @returns {() => void}
     */
    function listen(node, event, handler, options) {
    	node.addEventListener(event, handler, options);
    	return () => node.removeEventListener(event, handler, options);
    }

    /**
     * @param {Element} node
     * @param {string} attribute
     * @param {string} [value]
     * @returns {void}
     */
    function attr(node, attribute, value) {
    	if (value == null) node.removeAttribute(attribute);
    	else if (node.getAttribute(attribute) !== value) node.setAttribute(attribute, value);
    }

    /**
     * @param {Element} element
     * @returns {ChildNode[]}
     */
    function children(element) {
    	return Array.from(element.childNodes);
    }

    /**
     * @param {Text} text
     * @param {unknown} data
     * @returns {void}
     */
    function set_data(text, data) {
    	data = '' + data;
    	if (text.data === data) return;
    	text.data = /** @type {string} */ (data);
    }

    /**
     * @returns {void} */
    function set_style(node, key, value, important) {
    	if (value == null) {
    		node.style.removeProperty(key);
    	} else {
    		node.style.setProperty(key, value, important ? 'important' : '');
    	}
    }

    /**
     * @param {HTMLElement} element
     * @returns {{}}
     */
    function get_custom_elements_slots(element) {
    	const result = {};
    	element.childNodes.forEach(
    		/** @param {Element} node */ (node) => {
    			result[node.slot || 'default'] = true;
    		}
    	);
    	return result;
    }

    /**
     * @typedef {Node & {
     * 	claim_order?: number;
     * 	hydrate_init?: true;
     * 	actual_end_child?: NodeEx;
     * 	childNodes: NodeListOf<NodeEx>;
     * }} NodeEx
     */

    /** @typedef {ChildNode & NodeEx} ChildNodeEx */

    /** @typedef {NodeEx & { claim_order: number }} NodeEx2 */

    /**
     * @typedef {ChildNodeEx[] & {
     * 	claim_info?: {
     * 		last_index: number;
     * 		total_claimed: number;
     * 	};
     * }} ChildNodeArray
     */

    let current_component;

    /** @returns {void} */
    function set_current_component(component) {
    	current_component = component;
    }

    function get_current_component() {
    	if (!current_component) throw new Error('Function called outside component initialization');
    	return current_component;
    }

    /**
     * The `onMount` function schedules a callback to run as soon as the component has been mounted to the DOM.
     * It must be called during the component's initialisation (but doesn't need to live *inside* the component;
     * it can be called from an external module).
     *
     * If a function is returned _synchronously_ from `onMount`, it will be called when the component is unmounted.
     *
     * `onMount` does not run inside a [server-side component](/docs#run-time-server-side-component-api).
     *
     * https://svelte.dev/docs/svelte#onmount
     * @template T
     * @param {() => import('./private.js').NotFunction<T> | Promise<import('./private.js').NotFunction<T>> | (() => any)} fn
     * @returns {void}
     */
    function onMount(fn) {
    	get_current_component().$$.on_mount.push(fn);
    }

    const dirty_components = [];
    const binding_callbacks = [];

    let render_callbacks = [];

    const flush_callbacks = [];

    const resolved_promise = /* @__PURE__ */ Promise.resolve();

    let update_scheduled = false;

    /** @returns {void} */
    function schedule_update() {
    	if (!update_scheduled) {
    		update_scheduled = true;
    		resolved_promise.then(flush);
    	}
    }

    /** @returns {void} */
    function add_render_callback(fn) {
    	render_callbacks.push(fn);
    }

    // flush() calls callbacks in this order:
    // 1. All beforeUpdate callbacks, in order: parents before children
    // 2. All bind:this callbacks, in reverse order: children before parents.
    // 3. All afterUpdate callbacks, in order: parents before children. EXCEPT
    //    for afterUpdates called during the initial onMount, which are called in
    //    reverse order: children before parents.
    // Since callbacks might update component values, which could trigger another
    // call to flush(), the following steps guard against this:
    // 1. During beforeUpdate, any updated components will be added to the
    //    dirty_components array and will cause a reentrant call to flush(). Because
    //    the flush index is kept outside the function, the reentrant call will pick
    //    up where the earlier call left off and go through all dirty components. The
    //    current_component value is saved and restored so that the reentrant call will
    //    not interfere with the "parent" flush() call.
    // 2. bind:this callbacks cannot trigger new flush() calls.
    // 3. During afterUpdate, any updated components will NOT have their afterUpdate
    //    callback called a second time; the seen_callbacks set, outside the flush()
    //    function, guarantees this behavior.
    const seen_callbacks = new Set();

    let flushidx = 0; // Do *not* move this inside the flush() function

    /** @returns {void} */
    function flush() {
    	// Do not reenter flush while dirty components are updated, as this can
    	// result in an infinite loop. Instead, let the inner flush handle it.
    	// Reentrancy is ok afterwards for bindings etc.
    	if (flushidx !== 0) {
    		return;
    	}
    	const saved_component = current_component;
    	do {
    		// first, call beforeUpdate functions
    		// and update components
    		try {
    			while (flushidx < dirty_components.length) {
    				const component = dirty_components[flushidx];
    				flushidx++;
    				set_current_component(component);
    				update(component.$$);
    			}
    		} catch (e) {
    			// reset dirty state to not end up in a deadlocked state and then rethrow
    			dirty_components.length = 0;
    			flushidx = 0;
    			throw e;
    		}
    		set_current_component(null);
    		dirty_components.length = 0;
    		flushidx = 0;
    		while (binding_callbacks.length) binding_callbacks.pop()();
    		// then, once components are updated, call
    		// afterUpdate functions. This may cause
    		// subsequent updates...
    		for (let i = 0; i < render_callbacks.length; i += 1) {
    			const callback = render_callbacks[i];
    			if (!seen_callbacks.has(callback)) {
    				// ...so guard against infinite loops
    				seen_callbacks.add(callback);
    				callback();
    			}
    		}
    		render_callbacks.length = 0;
    	} while (dirty_components.length);
    	while (flush_callbacks.length) {
    		flush_callbacks.pop()();
    	}
    	update_scheduled = false;
    	seen_callbacks.clear();
    	set_current_component(saved_component);
    }

    /** @returns {void} */
    function update($$) {
    	if ($$.fragment !== null) {
    		$$.update();
    		run_all($$.before_update);
    		const dirty = $$.dirty;
    		$$.dirty = [-1];
    		$$.fragment && $$.fragment.p($$.ctx, dirty);
    		$$.after_update.forEach(add_render_callback);
    	}
    }

    /**
     * Useful for example to execute remaining `afterUpdate` callbacks before executing `destroy`.
     * @param {Function[]} fns
     * @returns {void}
     */
    function flush_render_callbacks(fns) {
    	const filtered = [];
    	const targets = [];
    	render_callbacks.forEach((c) => (fns.indexOf(c) === -1 ? filtered.push(c) : targets.push(c)));
    	targets.forEach((c) => c());
    	render_callbacks = filtered;
    }

    const outroing = new Set();

    /**
     * @type {Outro}
     */
    let outros;

    /**
     * @param {import('./private.js').Fragment} block
     * @param {0 | 1} [local]
     * @returns {void}
     */
    function transition_in(block, local) {
    	if (block && block.i) {
    		outroing.delete(block);
    		block.i(local);
    	}
    }

    /**
     * @param {import('./private.js').Fragment} block
     * @param {0 | 1} local
     * @param {0 | 1} [detach]
     * @param {() => void} [callback]
     * @returns {void}
     */
    function transition_out(block, local, detach, callback) {
    	if (block && block.o) {
    		if (outroing.has(block)) return;
    		outroing.add(block);
    		outros.c.push(() => {
    			outroing.delete(block);
    			if (callback) {
    				if (detach) block.d(1);
    				callback();
    			}
    		});
    		block.o(local);
    	} else if (callback) {
    		callback();
    	}
    }

    /** @typedef {1} INTRO */
    /** @typedef {0} OUTRO */
    /** @typedef {{ direction: 'in' | 'out' | 'both' }} TransitionOptions */
    /** @typedef {(node: Element, params: any, options: TransitionOptions) => import('../transition/public.js').TransitionConfig} TransitionFn */

    /**
     * @typedef {Object} Outro
     * @property {number} r
     * @property {Function[]} c
     * @property {Object} p
     */

    /**
     * @typedef {Object} PendingProgram
     * @property {number} start
     * @property {INTRO|OUTRO} b
     * @property {Outro} [group]
     */

    /**
     * @typedef {Object} Program
     * @property {number} a
     * @property {INTRO|OUTRO} b
     * @property {1|-1} d
     * @property {number} duration
     * @property {number} start
     * @property {number} end
     * @property {Outro} [group]
     */

    // general each functions:

    function ensure_array_like(array_like_or_iterator) {
    	return array_like_or_iterator?.length !== undefined
    		? array_like_or_iterator
    		: Array.from(array_like_or_iterator);
    }

    // keyed each functions:

    /** @returns {void} */
    function destroy_block(block, lookup) {
    	block.d(1);
    	lookup.delete(block.key);
    }

    /** @returns {any[]} */
    function update_keyed_each(
    	old_blocks,
    	dirty,
    	get_key,
    	dynamic,
    	ctx,
    	list,
    	lookup,
    	node,
    	destroy,
    	create_each_block,
    	next,
    	get_context
    ) {
    	let o = old_blocks.length;
    	let n = list.length;
    	let i = o;
    	const old_indexes = {};
    	while (i--) old_indexes[old_blocks[i].key] = i;
    	const new_blocks = [];
    	const new_lookup = new Map();
    	const deltas = new Map();
    	const updates = [];
    	i = n;
    	while (i--) {
    		const child_ctx = get_context(ctx, list, i);
    		const key = get_key(child_ctx);
    		let block = lookup.get(key);
    		if (!block) {
    			block = create_each_block(key, child_ctx);
    			block.c();
    		} else if (dynamic) {
    			// defer updates until all the DOM shuffling is done
    			updates.push(() => block.p(child_ctx, dirty));
    		}
    		new_lookup.set(key, (new_blocks[i] = block));
    		if (key in old_indexes) deltas.set(key, Math.abs(i - old_indexes[key]));
    	}
    	const will_move = new Set();
    	const did_move = new Set();
    	/** @returns {void} */
    	function insert(block) {
    		transition_in(block, 1);
    		block.m(node, next);
    		lookup.set(block.key, block);
    		next = block.first;
    		n--;
    	}
    	while (o && n) {
    		const new_block = new_blocks[n - 1];
    		const old_block = old_blocks[o - 1];
    		const new_key = new_block.key;
    		const old_key = old_block.key;
    		if (new_block === old_block) {
    			// do nothing
    			next = new_block.first;
    			o--;
    			n--;
    		} else if (!new_lookup.has(old_key)) {
    			// remove old block
    			destroy(old_block, lookup);
    			o--;
    		} else if (!lookup.has(new_key) || will_move.has(new_key)) {
    			insert(new_block);
    		} else if (did_move.has(old_key)) {
    			o--;
    		} else if (deltas.get(new_key) > deltas.get(old_key)) {
    			did_move.add(new_key);
    			insert(new_block);
    		} else {
    			will_move.add(old_key);
    			o--;
    		}
    	}
    	while (o--) {
    		const old_block = old_blocks[o];
    		if (!new_lookup.has(old_block.key)) destroy(old_block, lookup);
    	}
    	while (n) insert(new_blocks[n - 1]);
    	run_all(updates);
    	return new_blocks;
    }

    /** @returns {void} */
    function create_component(block) {
    	block && block.c();
    }

    /** @returns {void} */
    function mount_component(component, target, anchor) {
    	const { fragment, after_update } = component.$$;
    	fragment && fragment.m(target, anchor);
    	// onMount happens before the initial afterUpdate
    	add_render_callback(() => {
    		const new_on_destroy = component.$$.on_mount.map(run).filter(is_function);
    		// if the component was destroyed immediately
    		// it will update the `$$.on_destroy` reference to `null`.
    		// the destructured on_destroy may still reference to the old array
    		if (component.$$.on_destroy) {
    			component.$$.on_destroy.push(...new_on_destroy);
    		} else {
    			// Edge case - component was destroyed immediately,
    			// most likely as a result of a binding initialising
    			run_all(new_on_destroy);
    		}
    		component.$$.on_mount = [];
    	});
    	after_update.forEach(add_render_callback);
    }

    /** @returns {void} */
    function destroy_component(component, detaching) {
    	const $$ = component.$$;
    	if ($$.fragment !== null) {
    		flush_render_callbacks($$.after_update);
    		run_all($$.on_destroy);
    		$$.fragment && $$.fragment.d(detaching);
    		// TODO null out other refs, including component.$$ (but need to
    		// preserve final state?)
    		$$.on_destroy = $$.fragment = null;
    		$$.ctx = [];
    	}
    }

    /** @returns {void} */
    function make_dirty(component, i) {
    	if (component.$$.dirty[0] === -1) {
    		dirty_components.push(component);
    		schedule_update();
    		component.$$.dirty.fill(0);
    	}
    	component.$$.dirty[(i / 31) | 0] |= 1 << i % 31;
    }

    /** @returns {void} */
    function init$1(
    	component,
    	options,
    	instance,
    	create_fragment,
    	not_equal,
    	props,
    	append_styles,
    	dirty = [-1]
    ) {
    	const parent_component = current_component;
    	set_current_component(component);
    	/** @type {import('./private.js').T$$} */
    	const $$ = (component.$$ = {
    		fragment: null,
    		ctx: [],
    		// state
    		props,
    		update: noop,
    		not_equal,
    		bound: blank_object(),
    		// lifecycle
    		on_mount: [],
    		on_destroy: [],
    		on_disconnect: [],
    		before_update: [],
    		after_update: [],
    		context: new Map(options.context || (parent_component ? parent_component.$$.context : [])),
    		// everything else
    		callbacks: blank_object(),
    		dirty,
    		skip_bound: false,
    		root: options.target || parent_component.$$.root
    	});
    	append_styles && append_styles($$.root);
    	let ready = false;
    	$$.ctx = instance
    		? instance(component, options.props || {}, (i, ret, ...rest) => {
    				const value = rest.length ? rest[0] : ret;
    				if ($$.ctx && not_equal($$.ctx[i], ($$.ctx[i] = value))) {
    					if (!$$.skip_bound && $$.bound[i]) $$.bound[i](value);
    					if (ready) make_dirty(component, i);
    				}
    				return ret;
    		  })
    		: [];
    	$$.update();
    	ready = true;
    	run_all($$.before_update);
    	// `false` as a special case of no DOM component
    	$$.fragment = create_fragment ? create_fragment($$.ctx) : false;
    	if (options.target) {
    		if (options.hydrate) {
    			const nodes = children(options.target);
    			// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    			$$.fragment && $$.fragment.l(nodes);
    			nodes.forEach(detach);
    		} else {
    			// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    			$$.fragment && $$.fragment.c();
    		}
    		if (options.intro) transition_in(component.$$.fragment);
    		mount_component(component, options.target, options.anchor);
    		flush();
    	}
    	set_current_component(parent_component);
    }

    let SvelteElement;

    if (typeof HTMLElement === 'function') {
    	SvelteElement = class extends HTMLElement {
    		/** The Svelte component constructor */
    		$$ctor;
    		/** Slots */
    		$$s;
    		/** The Svelte component instance */
    		$$c;
    		/** Whether or not the custom element is connected */
    		$$cn = false;
    		/** Component props data */
    		$$d = {};
    		/** `true` if currently in the process of reflecting component props back to attributes */
    		$$r = false;
    		/** @type {Record<string, CustomElementPropDefinition>} Props definition (name, reflected, type etc) */
    		$$p_d = {};
    		/** @type {Record<string, Function[]>} Event listeners */
    		$$l = {};
    		/** @type {Map<Function, Function>} Event listener unsubscribe functions */
    		$$l_u = new Map();

    		constructor($$componentCtor, $$slots, use_shadow_dom) {
    			super();
    			this.$$ctor = $$componentCtor;
    			this.$$s = $$slots;
    			if (use_shadow_dom) {
    				this.attachShadow({ mode: 'open' });
    			}
    		}

    		addEventListener(type, listener, options) {
    			// We can't determine upfront if the event is a custom event or not, so we have to
    			// listen to both. If someone uses a custom event with the same name as a regular
    			// browser event, this fires twice - we can't avoid that.
    			this.$$l[type] = this.$$l[type] || [];
    			this.$$l[type].push(listener);
    			if (this.$$c) {
    				const unsub = this.$$c.$on(type, listener);
    				this.$$l_u.set(listener, unsub);
    			}
    			super.addEventListener(type, listener, options);
    		}

    		removeEventListener(type, listener, options) {
    			super.removeEventListener(type, listener, options);
    			if (this.$$c) {
    				const unsub = this.$$l_u.get(listener);
    				if (unsub) {
    					unsub();
    					this.$$l_u.delete(listener);
    				}
    			}
    		}

    		async connectedCallback() {
    			this.$$cn = true;
    			if (!this.$$c) {
    				// We wait one tick to let possible child slot elements be created/mounted
    				await Promise.resolve();
    				if (!this.$$cn) {
    					return;
    				}
    				function create_slot(name) {
    					return () => {
    						let node;
    						const obj = {
    							c: function create() {
    								node = element('slot');
    								if (name !== 'default') {
    									attr(node, 'name', name);
    								}
    							},
    							/**
    							 * @param {HTMLElement} target
    							 * @param {HTMLElement} [anchor]
    							 */
    							m: function mount(target, anchor) {
    								insert(target, node, anchor);
    							},
    							d: function destroy(detaching) {
    								if (detaching) {
    									detach(node);
    								}
    							}
    						};
    						return obj;
    					};
    				}
    				const $$slots = {};
    				const existing_slots = get_custom_elements_slots(this);
    				for (const name of this.$$s) {
    					if (name in existing_slots) {
    						$$slots[name] = [create_slot(name)];
    					}
    				}
    				for (const attribute of this.attributes) {
    					// this.$$data takes precedence over this.attributes
    					const name = this.$$g_p(attribute.name);
    					if (!(name in this.$$d)) {
    						this.$$d[name] = get_custom_element_value(name, attribute.value, this.$$p_d, 'toProp');
    					}
    				}
    				this.$$c = new this.$$ctor({
    					target: this.shadowRoot || this,
    					props: {
    						...this.$$d,
    						$$slots,
    						$$scope: {
    							ctx: []
    						}
    					}
    				});

    				// Reflect component props as attributes
    				const reflect_attributes = () => {
    					this.$$r = true;
    					for (const key in this.$$p_d) {
    						this.$$d[key] = this.$$c.$$.ctx[this.$$c.$$.props[key]];
    						if (this.$$p_d[key].reflect) {
    							const attribute_value = get_custom_element_value(
    								key,
    								this.$$d[key],
    								this.$$p_d,
    								'toAttribute'
    							);
    							if (attribute_value == null) {
    								this.removeAttribute(key);
    							} else {
    								this.setAttribute(this.$$p_d[key].attribute || key, attribute_value);
    							}
    						}
    					}
    					this.$$r = false;
    				};
    				this.$$c.$$.after_update.push(reflect_attributes);
    				reflect_attributes(); // once initially because after_update is added too late for first render

    				for (const type in this.$$l) {
    					for (const listener of this.$$l[type]) {
    						const unsub = this.$$c.$on(type, listener);
    						this.$$l_u.set(listener, unsub);
    					}
    				}
    				this.$$l = {};
    			}
    		}

    		// We don't need this when working within Svelte code, but for compatibility of people using this outside of Svelte
    		// and setting attributes through setAttribute etc, this is helpful
    		attributeChangedCallback(attr, _oldValue, newValue) {
    			if (this.$$r) return;
    			attr = this.$$g_p(attr);
    			this.$$d[attr] = get_custom_element_value(attr, newValue, this.$$p_d, 'toProp');
    			this.$$c?.$set({ [attr]: this.$$d[attr] });
    		}

    		disconnectedCallback() {
    			this.$$cn = false;
    			// In a microtask, because this could be a move within the DOM
    			Promise.resolve().then(() => {
    				if (!this.$$cn) {
    					this.$$c.$destroy();
    					this.$$c = undefined;
    				}
    			});
    		}

    		$$g_p(attribute_name) {
    			return (
    				Object.keys(this.$$p_d).find(
    					(key) =>
    						this.$$p_d[key].attribute === attribute_name ||
    						(!this.$$p_d[key].attribute && key.toLowerCase() === attribute_name)
    				) || attribute_name
    			);
    		}
    	};
    }

    /**
     * @param {string} prop
     * @param {any} value
     * @param {Record<string, CustomElementPropDefinition>} props_definition
     * @param {'toAttribute' | 'toProp'} [transform]
     */
    function get_custom_element_value(prop, value, props_definition, transform) {
    	const type = props_definition[prop]?.type;
    	value = type === 'Boolean' && typeof value !== 'boolean' ? value != null : value;
    	if (!transform || !props_definition[prop]) {
    		return value;
    	} else if (transform === 'toAttribute') {
    		switch (type) {
    			case 'Object':
    			case 'Array':
    				return value == null ? null : JSON.stringify(value);
    			case 'Boolean':
    				return value ? '' : null;
    			case 'Number':
    				return value == null ? null : value;
    			default:
    				return value;
    		}
    	} else {
    		switch (type) {
    			case 'Object':
    			case 'Array':
    				return value && JSON.parse(value);
    			case 'Boolean':
    				return value; // conversion already handled above
    			case 'Number':
    				return value != null ? +value : value;
    			default:
    				return value;
    		}
    	}
    }

    /**
     * @internal
     *
     * Turn a Svelte component into a custom element.
     * @param {import('./public.js').ComponentType} Component  A Svelte component constructor
     * @param {Record<string, CustomElementPropDefinition>} props_definition  The props to observe
     * @param {string[]} slots  The slots to create
     * @param {string[]} accessors  Other accessors besides the ones for props the component has
     * @param {boolean} use_shadow_dom  Whether to use shadow DOM
     */
    function create_custom_element(
    	Component,
    	props_definition,
    	slots,
    	accessors,
    	use_shadow_dom
    ) {
    	const Class = class extends SvelteElement {
    		constructor() {
    			super(Component, slots, use_shadow_dom);
    			this.$$p_d = props_definition;
    		}
    		static get observedAttributes() {
    			return Object.keys(props_definition).map((key) =>
    				(props_definition[key].attribute || key).toLowerCase()
    			);
    		}
    	};
    	Object.keys(props_definition).forEach((prop) => {
    		Object.defineProperty(Class.prototype, prop, {
    			get() {
    				return this.$$c && prop in this.$$c ? this.$$c[prop] : this.$$d[prop];
    			},
    			set(value) {
    				value = get_custom_element_value(prop, value, props_definition);
    				this.$$d[prop] = value;
    				this.$$c?.$set({ [prop]: value });
    			}
    		});
    	});
    	accessors.forEach((accessor) => {
    		Object.defineProperty(Class.prototype, accessor, {
    			get() {
    				return this.$$c?.[accessor];
    			}
    		});
    	});
    	Component.element = /** @type {any} */ (Class);
    	return Class;
    }

    /**
     * Base class for Svelte components. Used when dev=false.
     *
     * @template {Record<string, any>} [Props=any]
     * @template {Record<string, any>} [Events=any]
     */
    class SvelteComponent {
    	/**
    	 * ### PRIVATE API
    	 *
    	 * Do not use, may change at any time
    	 *
    	 * @type {any}
    	 */
    	$$ = undefined;
    	/**
    	 * ### PRIVATE API
    	 *
    	 * Do not use, may change at any time
    	 *
    	 * @type {any}
    	 */
    	$$set = undefined;

    	/** @returns {void} */
    	$destroy() {
    		destroy_component(this, 1);
    		this.$destroy = noop;
    	}

    	/**
    	 * @template {Extract<keyof Events, string>} K
    	 * @param {K} type
    	 * @param {((e: Events[K]) => void) | null | undefined} callback
    	 * @returns {() => void}
    	 */
    	$on(type, callback) {
    		if (!is_function(callback)) {
    			return noop;
    		}
    		const callbacks = this.$$.callbacks[type] || (this.$$.callbacks[type] = []);
    		callbacks.push(callback);
    		return () => {
    			const index = callbacks.indexOf(callback);
    			if (index !== -1) callbacks.splice(index, 1);
    		};
    	}

    	/**
    	 * @param {Partial<Props>} props
    	 * @returns {void}
    	 */
    	$set(props) {
    		if (this.$$set && !is_empty(props)) {
    			this.$$.skip_bound = true;
    			this.$$set(props);
    			this.$$.skip_bound = false;
    		}
    	}
    }

    /**
     * @typedef {Object} CustomElementPropDefinition
     * @property {string} [attribute]
     * @property {boolean} [reflect]
     * @property {'String'|'Boolean'|'Number'|'Array'|'Object'} [type]
     */

    // generated during release, do not modify

    const PUBLIC_VERSION = '4';

    if (typeof window !== 'undefined')
    	// @ts-ignore
    	(window.__svelte || (window.__svelte = { v: new Set() })).v.add(PUBLIC_VERSION);

    let CONSTANTS = {
        baseURL: "https://configurator-api.daiku.click",
        s3BaseURL: "https://testnet-configurator-uploads.s3.eu-central-1.amazonaws.com",
        endpoints: {
            fetchProductConfig: "catalogs/{catalogId}/configurations",
            fetchProductPrice: "catalogs/price-calculation",
            addOrUpdateModifiedProductConfig: "carts",
            fetchModifiedProductConfig: "carts/{configurationId}",
            fetchSnapshots: "projects/snapshots",
            fetchTranslations: "translations/language",
        },
        theme: {
            cssVariables: [
                "backgroundColor",
                "baseColor",
                "primaryColor",
                "primaryTextColor",
                "secondaryColor",
                "secondaryTextColor",
            ],
        },
        regexUUIDPattern: "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx",
        configurations: {
            xAxis: "xAxis",
            yAxis: "yAxis",
            zAxis: "zAxis",
        },
        localstorage: {
            locale: "configurator-sdk-locale",
            apiKey: "configurator-sdk-api-key",
        },
    };

    const subscriber_queue = [];

    /**
     * Creates a `Readable` store that allows reading by subscription.
     *
     * https://svelte.dev/docs/svelte-store#readable
     * @template T
     * @param {T} [value] initial value
     * @param {import('./public.js').StartStopNotifier<T>} [start]
     * @returns {import('./public.js').Readable<T>}
     */
    function readable(value, start) {
    	return {
    		subscribe: writable(value, start).subscribe
    	};
    }

    /**
     * Create a `Writable` store that allows both updating and reading by subscription.
     *
     * https://svelte.dev/docs/svelte-store#writable
     * @template T
     * @param {T} [value] initial value
     * @param {import('./public.js').StartStopNotifier<T>} [start]
     * @returns {import('./public.js').Writable<T>}
     */
    function writable(value, start = noop) {
    	/** @type {import('./public.js').Unsubscriber} */
    	let stop;
    	/** @type {Set<import('./private.js').SubscribeInvalidateTuple<T>>} */
    	const subscribers = new Set();
    	/** @param {T} new_value
    	 * @returns {void}
    	 */
    	function set(new_value) {
    		if (safe_not_equal(value, new_value)) {
    			value = new_value;
    			if (stop) {
    				// store is ready
    				const run_queue = !subscriber_queue.length;
    				for (const subscriber of subscribers) {
    					subscriber[1]();
    					subscriber_queue.push(subscriber, value);
    				}
    				if (run_queue) {
    					for (let i = 0; i < subscriber_queue.length; i += 2) {
    						subscriber_queue[i][0](subscriber_queue[i + 1]);
    					}
    					subscriber_queue.length = 0;
    				}
    			}
    		}
    	}

    	/**
    	 * @param {import('./public.js').Updater<T>} fn
    	 * @returns {void}
    	 */
    	function update(fn) {
    		set(fn(value));
    	}

    	/**
    	 * @param {import('./public.js').Subscriber<T>} run
    	 * @param {import('./private.js').Invalidator<T>} [invalidate]
    	 * @returns {import('./public.js').Unsubscriber}
    	 */
    	function subscribe(run, invalidate = noop) {
    		/** @type {import('./private.js').SubscribeInvalidateTuple<T>} */
    		const subscriber = [run, invalidate];
    		subscribers.add(subscriber);
    		if (subscribers.size === 1) {
    			stop = start(set, update) || noop;
    		}
    		run(value);
    		return () => {
    			subscribers.delete(subscriber);
    			if (subscribers.size === 0 && stop) {
    				stop();
    				stop = null;
    			}
    		};
    	}
    	return { set, update, subscribe };
    }

    /**
     * Derived value store by synchronizing one or more readable stores and
     * applying an aggregation function over its input values.
     *
     * https://svelte.dev/docs/svelte-store#derived
     * @template {import('./private.js').Stores} S
     * @template T
     * @overload
     * @param {S} stores - input stores
     * @param {(values: import('./private.js').StoresValues<S>, set: (value: T) => void, update: (fn: import('./public.js').Updater<T>) => void) => import('./public.js').Unsubscriber | void} fn - function callback that aggregates the values
     * @param {T} [initial_value] - initial value
     * @returns {import('./public.js').Readable<T>}
     */

    /**
     * Derived value store by synchronizing one or more readable stores and
     * applying an aggregation function over its input values.
     *
     * https://svelte.dev/docs/svelte-store#derived
     * @template {import('./private.js').Stores} S
     * @template T
     * @overload
     * @param {S} stores - input stores
     * @param {(values: import('./private.js').StoresValues<S>) => T} fn - function callback that aggregates the values
     * @param {T} [initial_value] - initial value
     * @returns {import('./public.js').Readable<T>}
     */

    /**
     * @template {import('./private.js').Stores} S
     * @template T
     * @param {S} stores
     * @param {Function} fn
     * @param {T} [initial_value]
     * @returns {import('./public.js').Readable<T>}
     */
    function derived(stores, fn, initial_value) {
    	const single = !Array.isArray(stores);
    	/** @type {Array<import('./public.js').Readable<any>>} */
    	const stores_array = single ? [stores] : stores;
    	if (!stores_array.every(Boolean)) {
    		throw new Error('derived() expects stores as input, got a falsy value');
    	}
    	const auto = fn.length < 2;
    	return readable(initial_value, (set, update) => {
    		let started = false;
    		const values = [];
    		let pending = 0;
    		let cleanup = noop;
    		const sync = () => {
    			if (pending) {
    				return;
    			}
    			cleanup();
    			const result = fn(single ? values[0] : values, set, update);
    			if (auto) {
    				set(result);
    			} else {
    				cleanup = is_function(result) ? result : noop;
    			}
    		};
    		const unsubscribers = stores_array.map((store, i) =>
    			subscribe(
    				store,
    				(value) => {
    					values[i] = value;
    					pending &= ~(1 << i);
    					if (started) {
    						sync();
    					}
    				},
    				() => {
    					pending |= 1 << i;
    				}
    			)
    		);
    		started = true;
    		sync();
    		return function stop() {
    			run_all(unsubscribers);
    			cleanup();
    			// We need to set this to false because callbacks can still happen despite having unsubscribed:
    			// Callbacks might already be placed in the queue which doesn't know it should no longer
    			// invoke this derived store.
    			started = false;
    		};
    	});
    }

    const height = writable(0);
    const width = writable(0);
    const productPrice = writable({});
    const activeRules = writable([]);
    const modifiedConfigId = writable("");
    const productInfo = writable({});
    const uiElements = writable({});
    const uiConfig = writable({});
    const isDataLoaded = writable(false);
    const visibleAssets = writable({});
    const currentUIConfig = writable({});
    const theme = writable({});
    const meshesToShowForLayout = writable([]);
    const meshesToHideForLayout = writable([]);
    const translations = writable([]);
    const repetitiveShowMeshes = writable([]);
    const repetitiveHideMeshes = writable([]);
    const currentHoveredMeshOrGroup = writable([]);
    const currentAtomGroup = writable("");
    const excludeRule = writable(false);
    const excludeTextureRule = writable(false);
    const getLocalizedOrDefaultText = derived(translations, ($translations) => {
        return (translationKey, defaultValue) => {
            var _a;
            const t = $translations.find((translation) => translation.translationKey.key === translationKey);
            return ((_a = t === null || t === void 0 ? void 0 : t.translationValue) === null || _a === void 0 ? void 0 : _a.value) || defaultValue;
        };
    });

    var ConfigKind;
    (function (ConfigKind) {
        ConfigKind["Original"] = "original";
        ConfigKind["Modified"] = "modified";
    })(ConfigKind || (ConfigKind = {}));
    var UIElementKind;
    (function (UIElementKind) {
        UIElementKind["backpanel"] = "backpanel";
        UIElementKind["layout"] = "layout";
        UIElementKind["hideItems"] = "hideItems";
        UIElementKind["height"] = "height";
        UIElementKind["width"] = "width";
        UIElementKind["depth"] = "depth";
        UIElementKind["color"] = "color";
        UIElementKind["density"] = "density";
        UIElementKind["layoutIcon"] = "layoutIcon";
        UIElementKind["additionalStorage"] = "additionalStorage";
        UIElementKind["rowHeight"] = "rowHeight";
        UIElementKind["drawers"] = "drawers";
        UIElementKind["doors"] = "doors";
        UIElementKind["hideLayout"] = "hideLayout";
        UIElementKind["texture"] = "texture";
    })(UIElementKind || (UIElementKind = {}));
    var ConfigRuleKind;
    (function (ConfigRuleKind) {
        ConfigRuleKind["backpanel"] = "backpanelRule";
        ConfigRuleKind["layout"] = "layoutRule";
        ConfigRuleKind["height"] = "heightRule";
        ConfigRuleKind["width"] = "widthRule";
        ConfigRuleKind["color"] = "colorRule";
    })(ConfigRuleKind || (ConfigRuleKind = {}));
    var AttributeKind;
    (function (AttributeKind) {
        AttributeKind["color"] = "Color";
        AttributeKind["string"] = "String";
        AttributeKind["number"] = "Number";
        AttributeKind["boolean"] = "Boolean";
        AttributeKind["texture"] = "Texture";
    })(AttributeKind || (AttributeKind = {}));
    var AttributeKeyKind;
    (function (AttributeKeyKind) {
        AttributeKeyKind["backpanel"] = "backpanel";
        AttributeKeyKind["layout"] = "layout";
        AttributeKeyKind["width"] = "width";
        AttributeKeyKind["height"] = "height";
        AttributeKeyKind["widthPercent"] = "widthPercent";
        AttributeKeyKind["heightPercent"] = "heightPercent";
        AttributeKeyKind["color"] = "color";
        AttributeKeyKind["additionalStorage"] = "additionalStorage";
        AttributeKeyKind["drawers"] = "drawers";
        AttributeKeyKind["rowHeight"] = "rowHeight";
        AttributeKeyKind["doors"] = "doors";
        AttributeKeyKind["texture"] = "texture";
        AttributeKeyKind["setDefaultDimension"] = "setDefaultDimension";
    })(AttributeKeyKind || (AttributeKeyKind = {}));
    var translationKeyKind;
    (function (translationKeyKind) {
        translationKeyKind["width"] = "_WIDTH_";
        translationKeyKind["height"] = "_HEIGHT_";
        translationKeyKind["depth"] = "_DEPTH_";
        translationKeyKind["density"] = "_DENSITY_";
        translationKeyKind["backpanel"] = "_BACKPANEL_";
        translationKeyKind["color"] = "_COLOR_";
        translationKeyKind["styles"] = "_STYLES_";
        translationKeyKind["tax"] = "_TAX_";
    })(translationKeyKind || (translationKeyKind = {}));

    function addModifiedConfig({ configurationId, configuration, visibleAssets, catalogId, prices, }) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield fetch(`${CONSTANTS.baseURL}/${CONSTANTS.endpoints.addOrUpdateModifiedProductConfig}`, {
                    method: "POST",
                    body: JSON.stringify({
                        catalogId,
                        configurationId,
                        visibleAssets,
                        prices,
                        configuration,
                    }),
                    headers: {
                        "Content-Type": "application/json",
                    },
                });
                if (!response.ok) {
                    throw new Error(`Network response was not ok, status: ${response.status}`);
                }
                return yield response.json();
            }
            catch (error) {
                console.error("Error while creating modified configuration:", error.message);
                throw error;
            }
        });
    }
    function calculateProductPrice({ catalogId, visibleAssets, dimensions, }) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield fetch(`${CONSTANTS.baseURL}/${CONSTANTS.endpoints.fetchProductPrice}`, {
                    method: "POST",
                    body: JSON.stringify({
                        catalogId,
                        visibleAssets,
                        dimensions,
                    }),
                    headers: {
                        "Content-Type": "application/json",
                    },
                });
                if (!response.ok) {
                    throw new Error(`Network response was not ok, status: ${response.status}`);
                }
                return yield response.json();
            }
            catch (error) {
                console.error("Error calculating product price:", error.message);
                throw error;
            }
        });
    }
    function getProductInfo$1(productId, configKind) {
        return __awaiter(this, void 0, void 0, function* () {
            let path;
            if (configKind && configKind == "modified") {
                path = CONSTANTS.endpoints.fetchModifiedProductConfig.replace("{configurationId}", productId);
            }
            else {
                path = CONSTANTS.endpoints.fetchProductConfig.replace("{catalogId}", productId);
            }
            try {
                const response = yield fetch(`${CONSTANTS.baseURL}/${path}`);
                if (!response.ok) {
                    throw new Error(`Network response was not ok, status: ${response.status}`);
                }
                return yield response.json();
            }
            catch (error) {
                console.error("Error fetching product info:", error.message);
                throw error;
            }
        });
    }
    function getProductSnapshots(projectId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield fetch(`${CONSTANTS.baseURL}/${CONSTANTS.endpoints.fetchSnapshots}?projectId=${projectId}`, {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                    },
                });
                if (!response.ok) {
                    throw new Error(`Network response was not ok, status: ${response.status}`);
                }
                return yield response.json();
            }
            catch (error) {
                console.error("Error while fetching the product images", error.message);
                throw error;
            }
        });
    }
    function getTranslations(organizationId, language) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const queryParams = new URLSearchParams({
                    organizationId: organizationId,
                    language: language,
                });
                const url = new URL(`${CONSTANTS.baseURL}/${CONSTANTS.endpoints.fetchTranslations}`);
                url.search = queryParams.toString();
                const response = yield fetch(url, {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                    },
                });
                if (!response.ok) {
                    throw new Error(`Network response was not ok, status: ${response.status}`);
                }
                return yield response.json();
            }
            catch (error) {
                console.error("Error while fetching translations", error.message);
                throw error;
            }
        });
    }

    function formattedPrice(prices) {
        return Object.entries(prices).map(([key, value]) => ({
            key: key,
            value: value,
        }));
    }
    function formatProductPriceForUI(price) {
        if (Object.keys(price).length === 0)
            return;
        const [currency, value] = Object.entries(price)[0];
        return `${OSREC__namespace.format(value, { currency: currency })}`;
    }
    function generateUUID() {
        return CONSTANTS.regexUUIDPattern.replace(/[xy]/g, (c) => {
            const r = (Math.random() * 16) | 0;
            const v = c === "x" ? r : (r & 0x3) | 0x8;
            return v.toString(16);
        });
    }
    function getPercentageFromActualValue(calculatePercentParams) {
        const { actualValue, minValue, maxValue } = calculatePercentParams;
        return Math.round(((actualValue - minValue) / (maxValue - minValue)) * 100);
    }
    function handleButtonSelection(root, targetButton, querySelector, className) {
        const buttons = root.querySelectorAll(querySelector);
        buttons.forEach((button) => {
            if (button.value !== targetButton.value) {
                button.classList.remove(className);
            }
            else {
                button.classList.add(className);
            }
        });
        targetButton.classList.add(className);
    }
    function symmetricDifference(arr1, arr2) {
        const diff1 = arr1.filter((item) => !arr2.includes(item));
        const diff2 = arr2.filter((item) => !arr1.includes(item));
        return diff1.concat(diff2);
    }
    function getAnimatePositionForAtoms(atom, transformKind) {
        return Object.fromEntries(Object.entries(atom.animation.animationTransforms.find((transform) => transform.transformKind == transformKind))
            .filter(([key]) => [
            CONSTANTS.configurations.xAxis,
            CONSTANTS.configurations.yAxis,
            CONSTANTS.configurations.zAxis,
        ].includes(key))
            .map(([key, value]) => [
            key.charAt(0).toLowerCase() + key.slice(1, -4),
            Number(value),
        ])
            .concat([["speed", Number(atom.animation.animationSpeed)]]));
    }
    const operatorFn = {
        ">": (a, b) => a > b,
        "<": (a, b) => a < b,
        ">=": (a, b) => a >= b,
        "<=": (a, b) => a <= b,
        "=": (a, b) => a == b,
    };

    function updateUIElements(uiElementKind, visible = false) {
        uiElements.update((value) => (Object.assign(Object.assign({}, value), { [uiElementKind]: visible })));
    }
    function updateUIConfig(attribute) {
        const { name } = attribute, attr = __rest(attribute, ["name"]);
        if (attr.attributeKind == "Boolean")
            attr.defaultAttributeValue = JSON.parse(attr.defaultAttributeValue);
        uiConfig.update((config) => (Object.assign(Object.assign({}, config), { [name]: attr })));
        updateUIElements(UIElementKind[name], true);
        setCurrentUIConfig(UIElementKind[name], attr.defaultAttributeValue);
    }
    function addOrUpdateActiveRules(rule, ruleName) {
        let existingRuleIndex = get_store_value(activeRules).findIndex((rule) => rule.name === ruleName);
        if (existingRuleIndex === -1) {
            activeRules.update((arr) => [...arr, rule]);
        }
        else {
            activeRules.update((arr) => arr.map((existingRule, index) => index === existingRuleIndex ? rule : existingRule));
        }
    }
    function setCurrentUIConfig(attributeKind, value) {
        currentUIConfig.update((config) => (Object.assign(Object.assign({}, config), { [attributeKind]: { value: value } })));
    }
    function setValueToAttribute(configuration) {
        const { attribute } = configuration;
        const attr = __rest(attribute, ["defaultAttributeValue"]);
        let config = get_store_value(currentUIConfig);
        configuration.defaultAttributeValue = config[attribute.name].value;
        configuration.attribute = attr;
        return configuration;
    }
    function initializeColors() {
        CONSTANTS.theme.cssVariables.map((cssVariable) => {
            document.documentElement.style.setProperty(`--${cssVariable}`, get_store_value(theme)[cssVariable]);
        });
    }
    function fetchDefaultConfigAndTheme() {
        var _a, _b;
        get_store_value(productInfo).configurations.configurations.forEach((configuration) => {
            const { attribute, defaultAttributeValue } = configuration;
            attribute.defaultAttributeValue = defaultAttributeValue;
            updateUIConfig(attribute);
        });
        if ((_a = get_store_value(productInfo)) === null || _a === void 0 ? void 0 : _a.defaultTheme) {
            theme.set((_b = get_store_value(productInfo)) === null || _b === void 0 ? void 0 : _b.defaultTheme);
            initializeColors();
        }
    }
    function getMeshesFromRule(rule, attributeName) {
        return rule.atomActions.flatMap((action) => {
            var _a, _b, _c, _d;
            if (action.attribute.name == attributeName) {
                let atoms = ((_a = action === null || action === void 0 ? void 0 : action.atom) === null || _a === void 0 ? void 0 : _a.id)
                    ? [action === null || action === void 0 ? void 0 : action.atom]
                    : ((_c = (_b = action === null || action === void 0 ? void 0 : action.dna) === null || _b === void 0 ? void 0 : _b.atoms) === null || _c === void 0 ? void 0 : _c.length)
                        ? [...(_d = action === null || action === void 0 ? void 0 : action.dna) === null || _d === void 0 ? void 0 : _d.atoms]
                        : [];
                return atoms.map((atom) => atom.name);
            }
            return [];
        });
    }
    function updateMeshesToShowStore(store, element) {
        store.update((array) => {
            if (!array.includes(element)) {
                return [...array, element];
            }
            return array;
        });
    }
    function addModifiedProductWithActiveRules() {
        return __awaiter(this, void 0, void 0, function* () {
            let modifiedConfigId = generateUUID(), configuration = get_store_value(productInfo);
            configuration["activeRules"] = get_store_value(activeRules);
            configuration.configurations.configurations =
                configuration.configurations.configurations.map(setValueToAttribute);
            let response = yield addModifiedConfig({
                catalogId: configuration.catalog.id,
                configurationId: modifiedConfigId,
                visibleAssets: get_store_value(visibleAssets).filter((mesh) => mesh.visible === true),
                prices: formattedPrice(get_store_value(productPrice)),
                configuration: configuration,
            });
            return response;
        });
    }
    function initProductInfoAndUIElements({ productId, configKind, }) {
        var _a, _b, _c;
        return __awaiter(this, void 0, void 0, function* () {
            productInfo.set(yield getProductInfo$1(productId, configKind));
            if (((_a = get_store_value(productInfo)) === null || _a === void 0 ? void 0 : _a.configurations) &&
                ((_b = get_store_value(productInfo).configurations) === null || _b === void 0 ? void 0 : _b.activeRules))
                productInfo.set(get_store_value(productInfo).configurations);
            translations.set(yield getTranslations(get_store_value(productInfo).organizationId, localStorage.getItem(CONSTANTS.localstorage.locale) || "en"));
            isDataLoaded.set(true);
            if (((_c = get_store_value(productInfo)) === null || _c === void 0 ? void 0 : _c.configurations) &&
                !get_store_value(productInfo).configurations.configurations.length)
                return;
            fetchDefaultConfigAndTheme();
        });
    }
    function setAnimationAndPopupOptions(configuratorLib) {
        var _a;
        let popupOptions = {}, popupAtoms = {};
        (_a = get_store_value(productInfo)) === null || _a === void 0 ? void 0 : _a.project.atoms.filter((atom) => atom.name != "Camera").map((atom) => {
            if (atom.rightAtoms.length) {
                let options = {};
                atom.atomOptions.map((atomOption) => {
                    if (atomOption.attribute.name == "rowHeight") {
                        options["rowHeight"] = get_store_value(currentUIConfig).rowHeight.value;
                        if (!(options === null || options === void 0 ? void 0 : options.url))
                            options.url =
                                atomOption.option.imageS3Key != ""
                                    ? `${CONSTANTS.s3BaseURL}/${atomOption.option.imageS3Key}`
                                    : `${CONSTANTS.s3BaseURL}/${atomOption.option.value[0].imageS3Key}`;
                    }
                    if (atomOption.attribute.name == "drawers") {
                        options["drawers"] = get_store_value(currentUIConfig).drawers.value;
                        if (!(options === null || options === void 0 ? void 0 : options.url))
                            options.url =
                                atomOption.option.imageS3Key != ""
                                    ? `${CONSTANTS.s3BaseURL}/${atomOption.option.imageS3Key}`
                                    : `${CONSTANTS.s3BaseURL}/${atomOption.option.value[0].imageS3Key}`;
                    }
                });
                popupAtoms[atom.name] = options;
                setCurrentUIConfig("popupOptions", popupAtoms);
                atom.rightAtoms.map((rightAtom) => {
                    let position = getAnimatePositionForAtoms(atom, "Location");
                    position.groupedAtoms = atom.rightAtoms.flatMap((rightAtom) => [
                        rightAtom.name,
                    ]);
                    let rotationPosition = getAnimatePositionForAtoms(atom, "Rotation");
                    position.groupedAtoms = atom.rightAtoms.flatMap((rightAtom) => [
                        rightAtom.name,
                    ]);
                    let obj = configuratorLib.scene.getObjectByName(rightAtom.name);
                    obj.userData.animatePosition = position;
                    obj.userData.animateRotationPosition = rotationPosition;
                    obj.userData.atomOptions = atom.atomOptions;
                    popupOptions[`${atom.name}`] = atom.atomOptions;
                    obj.userData.groupedAtomName = atom.name;
                });
            }
            else {
                let obj = configuratorLib.scene.getObjectByName(atom.name);
                obj.userData.animatePosition = getAnimatePositionForAtoms(atom, "Location");
                obj.userData.animateRotationPosition = getAnimatePositionForAtoms(atom, "Rotation");
                obj.userData.atomOptions = atom.atomOptions;
            }
        });
        uiConfig.update((config) => (Object.assign(Object.assign({}, config), { popupOptions: popupOptions })));
    }

    function fetchAndExecuteRules(configuratorLib, shouldExecuteProductPrice = true) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function* () {
            if (((_a = get_store_value(productInfo)) === null || _a === void 0 ? void 0 : _a.activeRules) &&
                ((_b = get_store_value(productInfo)) === null || _b === void 0 ? void 0 : _b.activeRules.length) != 0) {
                for (const rule of get_store_value(productInfo).activeRules) {
                    yield executeRules(rule, configuratorLib);
                }
            }
            else {
                for (const rule of get_store_value(productInfo).project.rules) {
                    yield executeRules(rule, configuratorLib);
                }
                let diffMeshes = get_store_value(meshesToHideForLayout).filter((item) => !get_store_value(meshesToShowForLayout).includes(item));
                let rpMeshes = get_store_value(repetitiveShowMeshes).filter((item) => !get_store_value(meshesToShowForLayout).includes(item));
                yield rpMeshes.map((mesh) => {
                    updateMeshesToShowStore(meshesToShowForLayout, mesh);
                });
                let showMeshes = symmetricDifference(get_store_value(meshesToShowForLayout), get_store_value(repetitiveHideMeshes));
                yield configuratorLib.scene.traverse((mesh) => {
                    if (mesh.isMesh) {
                        if (diffMeshes.includes(mesh.name)) {
                            mesh.visible = false;
                            configuratorLib.setVisibleBoolen(mesh.name, false);
                        }
                        else if (showMeshes.includes(mesh.name)) {
                            mesh.visible = true;
                            configuratorLib.setVisibleBoolen(mesh.name, true);
                        }
                        else {
                            mesh.visible = false;
                            configuratorLib.setVisibleBoolen(mesh.name, false);
                        }
                    }
                });
                meshesToShowForLayout.set([]);
                meshesToHideForLayout.set([]);
                repetitiveHideMeshes.set([]);
                repetitiveShowMeshes.set([]);
            }
            if (shouldExecuteProductPrice) {
                getProductPrice$1(configuratorLib);
            }
        });
    }
    function executeRules(rule, configuratorLib) {
        return __awaiter(this, void 0, void 0, function* () {
            let shouldExecuteWidthRule = [];
            let shouldExecuteHeightRule = [];
            yield Promise.all(rule.conditions.map((condition) => __awaiter(this, void 0, void 0, function* () {
                var _a, _b, _c, _d, _e, _f, _g, _h;
                switch (condition.attribute.name) {
                    case AttributeKeyKind.backpanel:
                        yield configuratorLib.executeBackPanelRule(rule, JSON.parse(get_store_value(currentUIConfig).backpanel.value));
                        break;
                    case AttributeKeyKind.layout:
                        yield configuratorLib.executeLayoutRule(rule, get_store_value(currentUIConfig).layout.value, condition);
                        break;
                    case AttributeKeyKind.color:
                        if (condition.value == get_store_value(currentUIConfig).color.value) {
                            yield configuratorLib.executeColorRule(rule, get_store_value(currentUIConfig).color.value);
                        }
                        break;
                    case AttributeKeyKind.widthPercent:
                        const widthPercentage = getPercentageFromActualValue({
                            actualValue: configuratorLib.totalWidth,
                            minValue: (_b = (_a = get_store_value(uiConfig)) === null || _a === void 0 ? void 0 : _a.width) === null || _b === void 0 ? void 0 : _b.minValue,
                            maxValue: (_d = (_c = get_store_value(uiConfig)) === null || _c === void 0 ? void 0 : _c.width) === null || _d === void 0 ? void 0 : _d.maxValue,
                        });
                        let widthConditionFn = operatorFn[condition.operator];
                        if (widthConditionFn(widthPercentage, condition.value)) {
                            shouldExecuteWidthRule.push(true);
                        }
                        else {
                            shouldExecuteWidthRule.push(false);
                        }
                        break;
                    case AttributeKeyKind.heightPercent:
                        const heightPercentage = getPercentageFromActualValue({
                            actualValue: configuratorLib.totalHeight,
                            minValue: (_f = (_e = get_store_value(uiConfig)) === null || _e === void 0 ? void 0 : _e.height) === null || _f === void 0 ? void 0 : _f.minValue,
                            maxValue: (_h = (_g = get_store_value(uiConfig)) === null || _g === void 0 ? void 0 : _g.height) === null || _h === void 0 ? void 0 : _h.maxValue,
                        });
                        let heightConditionFn = operatorFn[condition.operator];
                        if (heightConditionFn(heightPercentage, condition.value)) {
                            shouldExecuteHeightRule.push(true);
                        }
                        else {
                            shouldExecuteHeightRule.push(false);
                        }
                        break;
                    // case AttributeKeyKind.additionalStorage:
                    //   await configuratorLib.executeAdditionalStorageRule(
                    //     rule,
                    //     JSON.parse(get(currentUIConfig)?.additionalStorage?.value)
                    //   );
                    //   break;
                    case AttributeKeyKind.drawers:
                        yield configuratorLib.executeDrawersRule(rule, get_store_value(currentUIConfig).drawers.value, condition);
                        break;
                    case AttributeKeyKind.rowHeight:
                        yield configuratorLib.executeRowHeightRule(rule, get_store_value(currentUIConfig).rowHeight.value, condition);
                        break;
                    case AttributeKeyKind.setDefaultDimension:
                        if (!get_store_value(excludeRule)) {
                            yield setDefaultDimension(rule, configuratorLib);
                        }
                        // await configuratorLib.executeSetDefaultDimensionRule(
                        //   rule
                        // );
                        break;
                }
            })));
            // excludeRule.set(true);
            if (shouldExecuteWidthRule.length &&
                shouldExecuteWidthRule.every((element) => element === true)) {
                yield configuratorLib.executeWidthRule(rule, get_store_value(currentUIConfig).width.value);
            }
            if (shouldExecuteHeightRule.length &&
                shouldExecuteHeightRule.every((element) => element === true)) {
                yield configuratorLib.executeHeightRule(rule, get_store_value(currentUIConfig).height.value);
            }
        });
    }
    let dimensions = [];
    function getProductPrice$1(configuratorLib) {
        return __awaiter(this, void 0, void 0, function* () {
            yield setDimensionsForFormula(configuratorLib);
            let productPriceResp = yield calculateProductPrice({
                catalogId: get_store_value(productInfo).catalog.id,
                visibleAssets: configuratorLib
                    .getVisibleMeshes()
                    .map((item) => item.id),
                dimensions: dimensions.map((item) => ({
                    assetItemId: item.assetItem.id,
                    attributeId: item.attribute.id,
                    value: item.value,
                })),
            });
            productPrice.set(productPriceResp === null || productPriceResp === void 0 ? void 0 : productPriceResp.totalPriceByCurrency);
            dimensions = [];
        });
    }
    function setDimensionsForFormula(configuratorLib) {
        return __awaiter(this, void 0, void 0, function* () {
            get_store_value(productInfo).pricings.pricings.map((pricing) => __awaiter(this, void 0, void 0, function* () {
                let filteredFormula = pricing.formula.filter((formulaType) => formulaType.key == "Asset Item");
                yield Promise.all(filteredFormula.map((item) => __awaiter(this, void 0, void 0, function* () {
                    const assetItem = {
                        assetItem: {
                            id: item.value.id,
                            name: item.value.name,
                        },
                        attribute: {
                            id: item.value.attribute.id,
                        },
                        value: Number(yield getCurrentDimensionValue(item, item.value.name, configuratorLib)),
                    };
                    dimensions.push(assetItem);
                })));
            }));
        });
    }
    function getCurrentDimensionValue(item, meshName, configuratorLib) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            let getAttribute = (_a = get_store_value(productInfo).project) === null || _a === void 0 ? void 0 : _a.attributes.filter((attr) => attr.id == item.value.attribute.id);
            if (!getAttribute.length)
                return;
            return configuratorLib.meshes.filter((mesh) => mesh.name == meshName)[0]
                .dimensions[item.value.attribute.name.toLowerCase()];
        });
    }
    function setDefaultDimension(rule, configuratorLib) {
        return __awaiter(this, void 0, void 0, function* () {
            rule.atomActions.forEach((atomAction) => {
                configDimensionsToMesh(atomAction, configuratorLib);
            });
        });
    }
    function configDimensionsToMesh(atomAction, configuratorLib) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            if ((atomAction === null || atomAction === void 0 ? void 0 : atomAction.attribute.name) != "widthDimension" &&
                (atomAction === null || atomAction === void 0 ? void 0 : atomAction.attribute.name) != "heightDimension")
                return;
            let meshes = atomAction.dna.atoms.map((atom) => atom.name);
            let params = {};
            params[`${(_a = atomAction.attribute.name.split("Dimension")[0]) === null || _a === void 0 ? void 0 : _a.toLowerCase()}`] = atomAction.stringValue;
            for (let mesh in meshes) {
                configuratorLib.setDimensions(meshes[mesh], params);
            }
        });
    }

    function loadEventListeners(root, configuratorLib) {
        const canvasElement = root.querySelector("#webgl");
        const popUpElement = root.querySelector(".layer-options");
        const boundMouseMove = configuratorLib.onMouseMove.bind(configuratorLib);
        canvasElement.addEventListener("mousemove", (event) => {
            boundMouseMove(event, root);
        });
        canvasElement.addEventListener("mouseleave", (event) => {
            configuratorLib.onMouseOut(event, root);
        });
        popUpElement.addEventListener("mouseenter", () => {
            popUpElement.style.display = "block";
        });
        popUpElement.addEventListener("mouseleave", () => {
            popUpElement.style.display = "none";
        });
    }
    function popupModalEventListener(root, configuratorLib) {
        let popupModalElement = root.querySelector(".layer-options");
        popupModalElement.addEventListener("click", function (event) {
            return __awaiter(this, void 0, void 0, function* () {
                if (event.target.classList.contains("row-height-button")) {
                    let current = popupModalElement.querySelectorAll(".row-height-button");
                    for (let i = 0; i < current.length; i++) {
                        current[i].classList.remove("active");
                    }
                    event.target.classList.add("active");
                    setCurrentUIConfig("rowHeight", event.target.value);
                    yield fetchAndExecuteRules(configuratorLib);
                }
                if (event.target.classList.contains("drawers-button")) {
                    let current = popupModalElement.querySelectorAll(".drawers-button");
                    for (let i = 0; i < current.length; i++) {
                        current[i].classList.remove("active");
                    }
                    event.target.classList.add("active");
                    setCurrentUIConfig("drawers", event.target.value);
                    yield fetchAndExecuteRules(configuratorLib);
                }
            });
        });
    }

    /* src/lib/components/Navbar.svelte generated by Svelte v4.0.3 */

    function get_each_context$4(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[6] = list[i];
    	return child_ctx;
    }

    function get_each_context_1$4(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[6] = list[i];
    	return child_ctx;
    }

    // (8:2) {#if !$isDataLoaded}
    function create_if_block_2$5(ctx) {
    	let div0;
    	let t;
    	let div1;

    	return {
    		c() {
    			div0 = element("div");
    			t = space();
    			div1 = element("div");
    			attr(div0, "class", "w-4/5 h-7 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div1, "class", "w-4/5 h-7 mx-2 bg-gray-300 animate-pulse rounded");
    		},
    		m(target, anchor) {
    			insert(target, div0, anchor);
    			insert(target, t, anchor);
    			insert(target, div1, anchor);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div0);
    				detach(t);
    				detach(div1);
    			}
    		}
    	};
    }

    // (13:2) {#if $uiElements.color}
    function create_if_block_1$5(ctx) {
    	let div;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_value_1 = ensure_array_like(/*$uiConfig*/ ctx[3].color.options);
    	const get_key = ctx => /*option*/ ctx[6];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		let child_ctx = get_each_context_1$4(ctx, each_value_1, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block_1$4(key, child_ctx));
    	}

    	return {
    		c() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div, "class", "flex gap-2 color-variants items-center");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				if (each_blocks[i]) {
    					each_blocks[i].m(div, null);
    				}
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$uiConfig*/ 8) {
    				each_value_1 = ensure_array_like(/*$uiConfig*/ ctx[3].color.options);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value_1, each_1_lookup, div, destroy_block, create_each_block_1$4, null, get_each_context_1$4);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};
    }

    // (15:6) {#each $uiConfig.color.options as option (option)}
    function create_each_block_1$4(key_1, ctx) {
    	let div1;
    	let div0;
    	let t;
    	let div1_class_value;
    	let div1_data_color_value;

    	return {
    		key: key_1,
    		first: null,
    		c() {
    			div1 = element("div");
    			div0 = element("div");
    			t = space();
    			attr(div0, "class", "rounded-full h-6 w-6");
    			set_style(div0, "background-color", /*option*/ ctx[6]);

    			attr(div1, "class", div1_class_value = "variants rounded-full p-1.5 border border-[#F2F2F2] cursor-pointer " + (/*option*/ ctx[6] === /*$uiConfig*/ ctx[3].color.defaultAttributeValue
    			? 'selectedColor'
    			: ''));

    			attr(div1, "data-color", div1_data_color_value = /*option*/ ctx[6]);
    			this.first = div1;
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, div0);
    			append(div1, t);
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (dirty & /*$uiConfig*/ 8) {
    				set_style(div0, "background-color", /*option*/ ctx[6]);
    			}

    			if (dirty & /*$uiConfig*/ 8 && div1_class_value !== (div1_class_value = "variants rounded-full p-1.5 border border-[#F2F2F2] cursor-pointer " + (/*option*/ ctx[6] === /*$uiConfig*/ ctx[3].color.defaultAttributeValue
    			? 'selectedColor'
    			: ''))) {
    				attr(div1, "class", div1_class_value);
    			}

    			if (dirty & /*$uiConfig*/ 8 && div1_data_color_value !== (div1_data_color_value = /*option*/ ctx[6])) {
    				attr(div1, "data-color", div1_data_color_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}
    		}
    	};
    }

    // (32:2) {#if $uiElements.depth}
    function create_if_block$5(ctx) {
    	let div1;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[4](translationKeyKind.depth, "Depth") + "";
    	let t0;
    	let t1;
    	let div0;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_value = ensure_array_like(/*$uiConfig*/ ctx[3].depth.options);
    	const get_key = ctx => /*option*/ ctx[6];

    	for (let i = 0; i < each_value.length; i += 1) {
    		let child_ctx = get_each_context$4(ctx, each_value, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block$4(key, child_ctx));
    	}

    	return {
    		c() {
    			div1 = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div0, "class", "options flex items-center gap-6 text-base");
    			attr(div1, "class", "flex items-center gap-6 text-base");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, t0);
    			append(div1, t1);
    			append(div1, div0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				if (each_blocks[i]) {
    					each_blocks[i].m(div0, null);
    				}
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 16 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[4](translationKeyKind.depth, "Depth") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig, handleButtonSelection, root*/ 9) {
    				each_value = ensure_array_like(/*$uiConfig*/ ctx[3].depth.options);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value, each_1_lookup, div0, destroy_block, create_each_block$4, null, get_each_context$4);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};
    }

    // (36:8) {#each $uiConfig.depth.options as option (option)}
    function create_each_block$4(key_1, ctx) {
    	let button;
    	let t0_value = /*option*/ ctx[6] + "";
    	let t0;
    	let t1;
    	let button_class_value;
    	let button_value_value;
    	let mounted;
    	let dispose;

    	return {
    		key: key_1,
    		first: null,
    		c() {
    			button = element("button");
    			t0 = text(t0_value);
    			t1 = text(" cm\n          ");

    			attr(button, "class", button_class_value = "depth-button cursor-pointer rounded-[29px] px-4 py-2.5 w-24 border " + (/*option*/ ctx[6] === /*$uiConfig*/ ctx[3].depth.defaultAttributeValue
    			? 'selected'
    			: ''));

    			button.value = button_value_value = /*option*/ ctx[6];
    			this.first = button;
    		},
    		m(target, anchor) {
    			insert(target, button, anchor);
    			append(button, t0);
    			append(button, t1);

    			if (!mounted) {
    				dispose = listen(button, "click", /*click_handler*/ ctx[5]);
    				mounted = true;
    			}
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty & /*$uiConfig*/ 8 && t0_value !== (t0_value = /*option*/ ctx[6] + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig*/ 8 && button_class_value !== (button_class_value = "depth-button cursor-pointer rounded-[29px] px-4 py-2.5 w-24 border " + (/*option*/ ctx[6] === /*$uiConfig*/ ctx[3].depth.defaultAttributeValue
    			? 'selected'
    			: ''))) {
    				attr(button, "class", button_class_value);
    			}

    			if (dirty & /*$uiConfig*/ 8 && button_value_value !== (button_value_value = /*option*/ ctx[6])) {
    				button.value = button_value_value;
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(button);
    			}

    			mounted = false;
    			dispose();
    		}
    	};
    }

    function create_fragment$6(ctx) {
    	let div;
    	let t0;
    	let t1;
    	let if_block0 = !/*$isDataLoaded*/ ctx[1] && create_if_block_2$5();
    	let if_block1 = /*$uiElements*/ ctx[2].color && create_if_block_1$5(ctx);
    	let if_block2 = /*$uiElements*/ ctx[2].depth && create_if_block$5(ctx);

    	return {
    		c() {
    			div = element("div");
    			if (if_block0) if_block0.c();
    			t0 = space();
    			if (if_block1) if_block1.c();
    			t1 = space();
    			if (if_block2) if_block2.c();
    			attr(div, "class", "hidden xl:flex justify-between w-4/5 mx-auto");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			if (if_block0) if_block0.m(div, null);
    			append(div, t0);
    			if (if_block1) if_block1.m(div, null);
    			append(div, t1);
    			if (if_block2) if_block2.m(div, null);
    		},
    		p(ctx, [dirty]) {
    			if (!/*$isDataLoaded*/ ctx[1]) {
    				if (if_block0) ; else {
    					if_block0 = create_if_block_2$5();
    					if_block0.c();
    					if_block0.m(div, t0);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (/*$uiElements*/ ctx[2].color) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    				} else {
    					if_block1 = create_if_block_1$5(ctx);
    					if_block1.c();
    					if_block1.m(div, t1);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}

    			if (/*$uiElements*/ ctx[2].depth) {
    				if (if_block2) {
    					if_block2.p(ctx, dirty);
    				} else {
    					if_block2 = create_if_block$5(ctx);
    					if_block2.c();
    					if_block2.m(div, null);
    				}
    			} else if (if_block2) {
    				if_block2.d(1);
    				if_block2 = null;
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}

    			if (if_block0) if_block0.d();
    			if (if_block1) if_block1.d();
    			if (if_block2) if_block2.d();
    		}
    	};
    }

    function instance$6($$self, $$props, $$invalidate) {
    	let $isDataLoaded;
    	let $uiElements;
    	let $uiConfig;
    	let $getLocalizedOrDefaultText;
    	component_subscribe($$self, isDataLoaded, $$value => $$invalidate(1, $isDataLoaded = $$value));
    	component_subscribe($$self, uiElements, $$value => $$invalidate(2, $uiElements = $$value));
    	component_subscribe($$self, uiConfig, $$value => $$invalidate(3, $uiConfig = $$value));
    	component_subscribe($$self, getLocalizedOrDefaultText, $$value => $$invalidate(4, $getLocalizedOrDefaultText = $$value));
    	let { root } = $$props;
    	const click_handler = event => handleButtonSelection(root, event.target, ".options button", "selected");

    	$$self.$$set = $$props => {
    		if ('root' in $$props) $$invalidate(0, root = $$props.root);
    	};

    	return [
    		root,
    		$isDataLoaded,
    		$uiElements,
    		$uiConfig,
    		$getLocalizedOrDefaultText,
    		click_handler
    	];
    }

    class Navbar extends SvelteComponent {
    	constructor(options) {
    		super();
    		init$1(this, options, instance$6, create_fragment$6, safe_not_equal, { root: 0 });
    	}

    	get root() {
    		return this.$$.ctx[0];
    	}

    	set root(root) {
    		this.$$set({ root });
    		flush();
    	}
    }

    create_custom_element(Navbar, {"root":{}}, [], [], true);

    /* src/lib/components/Dimension.svelte generated by Svelte v4.0.3 */

    function create_if_block_3$3(ctx) {
    	let div0;
    	let t0;
    	let div1;
    	let t1;
    	let div2;

    	return {
    		c() {
    			div0 = element("div");
    			t0 = space();
    			div1 = element("div");
    			t1 = space();
    			div2 = element("div");
    			attr(div0, "class", "w-1/2 h-4 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div1, "class", "w-1/2 h-4 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div2, "class", "w-1/2 h-4 mx-2 bg-gray-300 animate-pulse rounded");
    		},
    		m(target, anchor) {
    			insert(target, div0, anchor);
    			insert(target, t0, anchor);
    			insert(target, div1, anchor);
    			insert(target, t1, anchor);
    			insert(target, div2, anchor);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div0);
    				detach(t0);
    				detach(div1);
    				detach(t1);
    				detach(div2);
    			}
    		}
    	};
    }

    // (20:2) {#if $uiElements.height && !$uiElements.hideLayout}
    function create_if_block_2$4(ctx) {
    	let div6;
    	let div5;
    	let div1;
    	let t0;
    	let div4;
    	let t3;
    	let input;
    	let input_min_value;
    	let input_max_value;
    	let input_value_value;
    	let input_step_value;
    	let t4;
    	let p;
    	let t5_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.height, "Height") + "";
    	let t5;

    	return {
    		c() {
    			div6 = element("div");
    			div5 = element("div");
    			div1 = element("div");
    			div1.innerHTML = `<div class="range__slider-line height-range-line"></div>`;
    			t0 = space();
    			div4 = element("div");
    			div4.innerHTML = `<div class="range__value"><span class="range__value-number height-range-number">1</span> <div class="arrow-down"></div></div>`;
    			t3 = space();
    			input = element("input");
    			t4 = space();
    			p = element("p");
    			t5 = text(t5_value);
    			attr(div1, "class", "range__slider");
    			attr(div4, "class", "range__thumb height-range-thumb");
    			attr(input, "type", "range");
    			attr(input, "class", "range__input border rangeSlider slider height-range sliderRange");
    			attr(input, "min", input_min_value = /*$uiConfig*/ ctx[2].height.minValue);
    			attr(input, "max", input_max_value = /*$uiConfig*/ ctx[2].height.maxValue);
    			input.value = input_value_value = /*$uiConfig*/ ctx[2].height.defaultAttributeValue;
    			attr(input, "step", input_step_value = /*$uiConfig*/ ctx[2].height.stepValue);
    			attr(div5, "class", "range__content");
    			attr(p, "class", "");
    			attr(div6, "class", "flex flex-col justify-center items-center gap-2 slidecontainer w-1/4 text-base");
    		},
    		m(target, anchor) {
    			insert(target, div6, anchor);
    			append(div6, div5);
    			append(div5, div1);
    			append(div5, t0);
    			append(div5, div4);
    			append(div5, t3);
    			append(div5, input);
    			append(div6, t4);
    			append(div6, p);
    			append(p, t5);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$uiConfig*/ 4 && input_min_value !== (input_min_value = /*$uiConfig*/ ctx[2].height.minValue)) {
    				attr(input, "min", input_min_value);
    			}

    			if (dirty & /*$uiConfig*/ 4 && input_max_value !== (input_max_value = /*$uiConfig*/ ctx[2].height.maxValue)) {
    				attr(input, "max", input_max_value);
    			}

    			if (dirty & /*$uiConfig*/ 4 && input_value_value !== (input_value_value = /*$uiConfig*/ ctx[2].height.defaultAttributeValue)) {
    				input.value = input_value_value;
    			}

    			if (dirty & /*$uiConfig*/ 4 && input_step_value !== (input_step_value = /*$uiConfig*/ ctx[2].height.stepValue)) {
    				attr(input, "step", input_step_value);
    			}

    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t5_value !== (t5_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.height, "Height") + "")) set_data(t5, t5_value);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div6);
    			}
    		}
    	};
    }

    // (50:2) {#if $uiElements.density}
    function create_if_block_1$4(ctx) {
    	let div6;
    	let div5;
    	let div1;
    	let t0;
    	let div4;
    	let t3;
    	let input;
    	let input_min_value;
    	let input_max_value;
    	let input_value_value;
    	let input_step_value;
    	let t4;
    	let p;
    	let t5_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.density, "Density") + "";
    	let t5;

    	return {
    		c() {
    			div6 = element("div");
    			div5 = element("div");
    			div1 = element("div");
    			div1.innerHTML = `<div class="range__slider-line density-range-line"></div>`;
    			t0 = space();
    			div4 = element("div");
    			div4.innerHTML = `<div class="range__value"><span class="range__value-number density-range-number">1</span> <div class="arrow-down"></div></div>`;
    			t3 = space();
    			input = element("input");
    			t4 = space();
    			p = element("p");
    			t5 = text(t5_value);
    			attr(div1, "class", "range__slider");
    			attr(div4, "class", "range__thumb density-range-thumb");
    			attr(input, "type", "range");
    			attr(input, "class", "range__input border rangeSlider slider density-range sliderRange");
    			attr(input, "min", input_min_value = /*$uiConfig*/ ctx[2].density.minValue);
    			attr(input, "max", input_max_value = /*$uiConfig*/ ctx[2].density.maxValue);
    			input.value = input_value_value = /*$uiConfig*/ ctx[2].density.defaultAttributeValue;
    			attr(input, "step", input_step_value = /*$uiConfig*/ ctx[2].density.stepValue);
    			attr(div5, "class", "range__content");
    			attr(p, "class", "");
    			attr(div6, "class", "flex flex-col justify-center items-center gap-2 slidecontainer w-1/4 text-base");
    		},
    		m(target, anchor) {
    			insert(target, div6, anchor);
    			append(div6, div5);
    			append(div5, div1);
    			append(div5, t0);
    			append(div5, div4);
    			append(div5, t3);
    			append(div5, input);
    			append(div6, t4);
    			append(div6, p);
    			append(p, t5);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$uiConfig*/ 4 && input_min_value !== (input_min_value = /*$uiConfig*/ ctx[2].density.minValue)) {
    				attr(input, "min", input_min_value);
    			}

    			if (dirty & /*$uiConfig*/ 4 && input_max_value !== (input_max_value = /*$uiConfig*/ ctx[2].density.maxValue)) {
    				attr(input, "max", input_max_value);
    			}

    			if (dirty & /*$uiConfig*/ 4 && input_value_value !== (input_value_value = /*$uiConfig*/ ctx[2].density.defaultAttributeValue)) {
    				input.value = input_value_value;
    			}

    			if (dirty & /*$uiConfig*/ 4 && input_step_value !== (input_step_value = /*$uiConfig*/ ctx[2].density.stepValue)) {
    				attr(input, "step", input_step_value);
    			}

    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t5_value !== (t5_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.density, "Density") + "")) set_data(t5, t5_value);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div6);
    			}
    		}
    	};
    }

    // (80:2) {#if $uiElements.width}
    function create_if_block$4(ctx) {
    	let div6;
    	let div5;
    	let div1;
    	let t0;
    	let div4;
    	let t3;
    	let input;
    	let input_min_value;
    	let input_max_value;
    	let input_value_value;
    	let input_step_value;
    	let t4;
    	let p;
    	let t5_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.width, "Width") + "";
    	let t5;

    	return {
    		c() {
    			div6 = element("div");
    			div5 = element("div");
    			div1 = element("div");
    			div1.innerHTML = `<div class="range__slider-line width-range-line"></div>`;
    			t0 = space();
    			div4 = element("div");
    			div4.innerHTML = `<div class="range__value"><span class="range__value-number width-range-number">1</span> <div class="arrow-down"></div></div>`;
    			t3 = space();
    			input = element("input");
    			t4 = space();
    			p = element("p");
    			t5 = text(t5_value);
    			attr(div1, "class", "range__slider");
    			attr(div4, "class", "range__thumb width-range-thumb");
    			attr(input, "type", "range");
    			attr(input, "class", "range__input border slider rangeSlider width-range sliderRange width-slider");
    			attr(input, "min", input_min_value = /*$uiConfig*/ ctx[2].width.minValue);
    			attr(input, "max", input_max_value = /*$uiConfig*/ ctx[2].width.maxValue);
    			input.value = input_value_value = /*$uiConfig*/ ctx[2].width.defaultAttributeValue;
    			attr(input, "step", input_step_value = /*$uiConfig*/ ctx[2].width.stepValue);
    			attr(div5, "class", "range__content");
    			attr(p, "class", "");
    			attr(div6, "class", "flex flex-col justify-center items-center gap-2 slidecontainer w-1/4 text-base");
    		},
    		m(target, anchor) {
    			insert(target, div6, anchor);
    			append(div6, div5);
    			append(div5, div1);
    			append(div5, t0);
    			append(div5, div4);
    			append(div5, t3);
    			append(div5, input);
    			append(div6, t4);
    			append(div6, p);
    			append(p, t5);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$uiConfig*/ 4 && input_min_value !== (input_min_value = /*$uiConfig*/ ctx[2].width.minValue)) {
    				attr(input, "min", input_min_value);
    			}

    			if (dirty & /*$uiConfig*/ 4 && input_max_value !== (input_max_value = /*$uiConfig*/ ctx[2].width.maxValue)) {
    				attr(input, "max", input_max_value);
    			}

    			if (dirty & /*$uiConfig*/ 4 && input_value_value !== (input_value_value = /*$uiConfig*/ ctx[2].width.defaultAttributeValue)) {
    				input.value = input_value_value;
    			}

    			if (dirty & /*$uiConfig*/ 4 && input_step_value !== (input_step_value = /*$uiConfig*/ ctx[2].width.stepValue)) {
    				attr(input, "step", input_step_value);
    			}

    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t5_value !== (t5_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.width, "Width") + "")) set_data(t5, t5_value);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div6);
    			}
    		}
    	};
    }

    function create_fragment$5(ctx) {
    	let div;
    	let t0;
    	let t1;
    	let t2;
    	let if_block0 = !/*$isDataLoaded*/ ctx[0] && create_if_block_3$3();
    	let if_block1 = /*$uiElements*/ ctx[1].height && !/*$uiElements*/ ctx[1].hideLayout && create_if_block_2$4(ctx);
    	let if_block2 = /*$uiElements*/ ctx[1].density && create_if_block_1$4(ctx);
    	let if_block3 = /*$uiElements*/ ctx[1].width && create_if_block$4(ctx);

    	return {
    		c() {
    			div = element("div");
    			if (if_block0) if_block0.c();
    			t0 = space();
    			if (if_block1) if_block1.c();
    			t1 = space();
    			if (if_block2) if_block2.c();
    			t2 = space();
    			if (if_block3) if_block3.c();
    			attr(div, "class", "w-5/6 hidden xl:flex justify-between items-center mx-auto py-2 mt-20");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			if (if_block0) if_block0.m(div, null);
    			append(div, t0);
    			if (if_block1) if_block1.m(div, null);
    			append(div, t1);
    			if (if_block2) if_block2.m(div, null);
    			append(div, t2);
    			if (if_block3) if_block3.m(div, null);
    		},
    		p(ctx, [dirty]) {
    			if (!/*$isDataLoaded*/ ctx[0]) {
    				if (if_block0) ; else {
    					if_block0 = create_if_block_3$3();
    					if_block0.c();
    					if_block0.m(div, t0);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (/*$uiElements*/ ctx[1].height && !/*$uiElements*/ ctx[1].hideLayout) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    				} else {
    					if_block1 = create_if_block_2$4(ctx);
    					if_block1.c();
    					if_block1.m(div, t1);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}

    			if (/*$uiElements*/ ctx[1].density) {
    				if (if_block2) {
    					if_block2.p(ctx, dirty);
    				} else {
    					if_block2 = create_if_block_1$4(ctx);
    					if_block2.c();
    					if_block2.m(div, t2);
    				}
    			} else if (if_block2) {
    				if_block2.d(1);
    				if_block2 = null;
    			}

    			if (/*$uiElements*/ ctx[1].width) {
    				if (if_block3) {
    					if_block3.p(ctx, dirty);
    				} else {
    					if_block3 = create_if_block$4(ctx);
    					if_block3.c();
    					if_block3.m(div, null);
    				}
    			} else if (if_block3) {
    				if_block3.d(1);
    				if_block3 = null;
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}

    			if (if_block0) if_block0.d();
    			if (if_block1) if_block1.d();
    			if (if_block2) if_block2.d();
    			if (if_block3) if_block3.d();
    		}
    	};
    }

    function instance$5($$self, $$props, $$invalidate) {
    	let $isDataLoaded;
    	let $uiElements;
    	let $uiConfig;
    	let $getLocalizedOrDefaultText;
    	component_subscribe($$self, isDataLoaded, $$value => $$invalidate(0, $isDataLoaded = $$value));
    	component_subscribe($$self, uiElements, $$value => $$invalidate(1, $uiElements = $$value));
    	component_subscribe($$self, uiConfig, $$value => $$invalidate(2, $uiConfig = $$value));
    	component_subscribe($$self, getLocalizedOrDefaultText, $$value => $$invalidate(3, $getLocalizedOrDefaultText = $$value));
    	return [$isDataLoaded, $uiElements, $uiConfig, $getLocalizedOrDefaultText];
    }

    class Dimension extends SvelteComponent {
    	constructor(options) {
    		super();
    		init$1(this, options, instance$5, create_fragment$5, safe_not_equal, {});
    	}
    }

    create_custom_element(Dimension, {}, [], [], true);

    var checkIcon = 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTUiIGhlaWdodD0iMTUiIHZpZXdCb3g9IjAgMCAxNSAxNSIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTcuNSAxLjQwNjI1QzQuMTMzNzkgMS40MDYyNSAxLjQwNjI1IDQuMTMzNzkgMS40MDYyNSA3LjVDMS40MDYyNSAxMC44NjYyIDQuMTMzNzkgMTMuNTkzOCA3LjUgMTMuNTkzOEMxMC44NjYyIDEzLjU5MzggMTMuNTkzOCAxMC44NjYyIDEzLjU5MzggNy41QzEzLjU5MzggNC4xMzM3OSAxMC44NjYyIDEuNDA2MjUgNy41IDEuNDA2MjVaTTYuNTU5NTcgOS42NTkxOEM2LjQ4OTI2IDkuNzI5NDkgNi4zODk2NSA5Ljc4ODA5IDYuMzAxNzYgOS43ODgwOUM2LjIxMzg3IDkuNzg4MDkgNi4xMTQyNiA5LjcyNjU2IDYuMDQxMDIgOS42NTYyNUw0LjQwMDM5IDguMDE1NjJMNC45MjE4OCA3LjQ5NDE0TDYuMzA0NjkgOC44NzY5NUw5Ljk2MDk0IDUuMTk0MzRMMTAuNDczNiA1LjcyNDYxTDYuNTU5NTcgOS42NTkxOFoiIGZpbGw9IiM2NTY1NkMiLz4KPC9zdmc+';

    /* src/lib/components/TabPanel.svelte generated by Svelte v4.0.3 */

    function get_each_context$3(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[7] = list[i];
    	child_ctx[9] = i;
    	return child_ctx;
    }

    function get_each_context_1$3(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[7] = list[i];
    	child_ctx[9] = i;
    	return child_ctx;
    }

    // (24:2) {:else}
    function create_else_block$2(ctx) {
    	let span0;
    	let t0_value = /*$productInfo*/ ctx[1].catalog.name + "";
    	let t0;
    	let t1;
    	let span1;
    	let t2_value = (formatProductPriceForUI(/*$productPrice*/ ctx[2]) || "") + "";
    	let t2;
    	let t3;
    	let div;
    	let img;
    	let img_src_value;
    	let t4;
    	let span2;
    	let t5_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.tax, "Taxes included") + "";
    	let t5;

    	return {
    		c() {
    			span0 = element("span");
    			t0 = text(t0_value);
    			t1 = space();
    			span1 = element("span");
    			t2 = text(t2_value);
    			t3 = space();
    			div = element("div");
    			img = element("img");
    			t4 = space();
    			span2 = element("span");
    			t5 = text(t5_value);
    			attr(span0, "class", "font-medium text-base");
    			attr(span1, "class", "text-2xl font-medium");
    			if (!src_url_equal(img.src, img_src_value = checkIcon)) attr(img, "src", img_src_value);
    			attr(img, "alt", "");
    			attr(span2, "class", "text-sm text-[#65656C]");
    			attr(div, "class", "flex gap-1.5");
    		},
    		m(target, anchor) {
    			insert(target, span0, anchor);
    			append(span0, t0);
    			insert(target, t1, anchor);
    			insert(target, span1, anchor);
    			append(span1, t2);
    			insert(target, t3, anchor);
    			insert(target, div, anchor);
    			append(div, img);
    			append(div, t4);
    			append(div, span2);
    			append(span2, t5);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$productInfo*/ 2 && t0_value !== (t0_value = /*$productInfo*/ ctx[1].catalog.name + "")) set_data(t0, t0_value);
    			if (dirty & /*$productPrice*/ 4 && t2_value !== (t2_value = (formatProductPriceForUI(/*$productPrice*/ ctx[2]) || "") + "")) set_data(t2, t2_value);
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t5_value !== (t5_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.tax, "Taxes included") + "")) set_data(t5, t5_value);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(span0);
    				detach(t1);
    				detach(span1);
    				detach(t3);
    				detach(div);
    			}
    		}
    	};
    }

    // (17:2) {#if !$isDataLoaded}
    function create_if_block_6$2(ctx) {
    	let div0;
    	let t0;
    	let div1;
    	let t1;
    	let div2;
    	let t2;
    	let div3;
    	let t3;
    	let div4;
    	let t4;
    	let div5;

    	return {
    		c() {
    			div0 = element("div");
    			t0 = space();
    			div1 = element("div");
    			t1 = space();
    			div2 = element("div");
    			t2 = space();
    			div3 = element("div");
    			t3 = space();
    			div4 = element("div");
    			t4 = space();
    			div5 = element("div");
    			attr(div0, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div1, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div2, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div3, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div4, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div5, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    		},
    		m(target, anchor) {
    			insert(target, div0, anchor);
    			insert(target, t0, anchor);
    			insert(target, div1, anchor);
    			insert(target, t1, anchor);
    			insert(target, div2, anchor);
    			insert(target, t2, anchor);
    			insert(target, div3, anchor);
    			insert(target, t3, anchor);
    			insert(target, div4, anchor);
    			insert(target, t4, anchor);
    			insert(target, div5, anchor);
    		},
    		p: noop,
    		d(detaching) {
    			if (detaching) {
    				detach(div0);
    				detach(t0);
    				detach(div1);
    				detach(t1);
    				detach(div2);
    				detach(t2);
    				detach(div3);
    				detach(t3);
    				detach(div4);
    				detach(t4);
    				detach(div5);
    			}
    		}
    	};
    }

    // (41:4) {#if $uiElements.showDimensions}
    function create_if_block_5$2(ctx) {
    	let div1;

    	return {
    		c() {
    			div1 = element("div");
    			div1.innerHTML = `<div class="checkbox-wrapper-48"><label><input type="checkbox" id="toggle-dimensions-checkbox" name="cb" class="cursor-pointer toggle-dimensions-checkbox"/></label></div> <p class="">Show dimensions</p>`;
    			attr(div1, "class", "flex items-center gap-3");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}
    		}
    	};
    }

    // (57:4) {#if $uiElements.hideItems}
    function create_if_block_4$2(ctx) {
    	let div1;

    	return {
    		c() {
    			div1 = element("div");
    			div1.innerHTML = `<div class="checkbox-wrapper-48"><label><input type="checkbox" checked="" name="cb" class="cursor-pointer hide-itme-checkbox"/></label></div> <p class="">Hide items</p>`;
    			attr(div1, "class", "flex items-center gap-3");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}
    		}
    	};
    }

    // (73:4) {#if $uiElements.backpanel}
    function create_if_block_3$2(ctx) {
    	let div1;
    	let div0;
    	let label;
    	let input;
    	let t0;
    	let p;
    	let t1_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.backpanel, "Back panels") + "";
    	let t1;
    	let mounted;
    	let dispose;

    	return {
    		c() {
    			div1 = element("div");
    			div0 = element("div");
    			label = element("label");
    			input = element("input");
    			t0 = space();
    			p = element("p");
    			t1 = text(t1_value);
    			attr(input, "type", "checkbox");
    			attr(input, "name", "cb");
    			attr(input, "class", "cursor-pointer back-panels-checkbox");
    			attr(div0, "class", "checkbox-wrapper-48");
    			attr(p, "class", "");
    			attr(div1, "class", "flex items-center gap-3");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, div0);
    			append(div0, label);
    			append(label, input);
    			input.checked = /*$uiConfig*/ ctx[5].backpanel.defaultAttributeValue;
    			append(div1, t0);
    			append(div1, p);
    			append(p, t1);

    			if (!mounted) {
    				dispose = listen(input, "change", /*input_change_handler*/ ctx[6]);
    				mounted = true;
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$uiConfig*/ 32) {
    				input.checked = /*$uiConfig*/ ctx[5].backpanel.defaultAttributeValue;
    			}

    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t1_value !== (t1_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.backpanel, "Back panels") + "")) set_data(t1, t1_value);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (95:2) {#if $uiElements.layout && !$uiElements.hideLayout}
    function create_if_block_1$3(ctx) {
    	let div1;
    	let span;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.styles, "Styles") + "";
    	let t0;
    	let t1;
    	let div0;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_value_1 = ensure_array_like(/*$uiConfig*/ ctx[5].layout.options);
    	const get_key = ctx => /*option*/ ctx[7];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		let child_ctx = get_each_context_1$3(ctx, each_value_1, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block_1$3(key, child_ctx));
    	}

    	return {
    		c() {
    			div1 = element("div");
    			span = element("span");
    			t0 = text(t0_value);
    			t1 = space();
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(span, "class", "text-base");
    			attr(div0, "class", "flex items-center flex-wrap gap-4");
    			attr(div1, "class", "flex flex-col gap-6 style-category max-w-[180px]");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, span);
    			append(span, t0);
    			append(div1, t1);
    			append(div1, div0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				if (each_blocks[i]) {
    					each_blocks[i].m(div0, null);
    				}
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.styles, "Styles") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig, CONSTANTS*/ 32) {
    				each_value_1 = ensure_array_like(/*$uiConfig*/ ctx[5].layout.options);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value_1, each_1_lookup, div0, destroy_block, create_each_block_1$3, null, get_each_context_1$3);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};
    }

    // (102:10) {#if $uiConfig.layoutIcon.options[index]}
    function create_if_block_2$3(ctx) {
    	let div;
    	let img;
    	let img_src_value;
    	let t;
    	let div_class_value;
    	let div_data_layout_kind_value;

    	return {
    		c() {
    			div = element("div");
    			img = element("img");
    			t = space();
    			attr(img, "class", "max-w-full max-h-full");
    			if (!src_url_equal(img.src, img_src_value = `${CONSTANTS.s3BaseURL}/${/*$uiConfig*/ ctx[5].layoutIcon.options[/*index*/ ctx[9]]}`)) attr(img, "src", img_src_value);
    			attr(img, "alt", "");

    			attr(div, "class", div_class_value = "layouts p-1.5 border border-[#F2F2F2] cursor-pointer flex items-center justify-center " + (/*option*/ ctx[7] === /*$uiConfig*/ ctx[5].layout.defaultAttributeValue
    			? 'active'
    			: '') + "");

    			attr(div, "data-layout-kind", div_data_layout_kind_value = /*option*/ ctx[7]);
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, img);
    			append(div, t);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$uiConfig*/ 32 && !src_url_equal(img.src, img_src_value = `${CONSTANTS.s3BaseURL}/${/*$uiConfig*/ ctx[5].layoutIcon.options[/*index*/ ctx[9]]}`)) {
    				attr(img, "src", img_src_value);
    			}

    			if (dirty & /*$uiConfig*/ 32 && div_class_value !== (div_class_value = "layouts p-1.5 border border-[#F2F2F2] cursor-pointer flex items-center justify-center " + (/*option*/ ctx[7] === /*$uiConfig*/ ctx[5].layout.defaultAttributeValue
    			? 'active'
    			: '') + "")) {
    				attr(div, "class", div_class_value);
    			}

    			if (dirty & /*$uiConfig*/ 32 && div_data_layout_kind_value !== (div_data_layout_kind_value = /*option*/ ctx[7])) {
    				attr(div, "data-layout-kind", div_data_layout_kind_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}
    		}
    	};
    }

    // (101:8) {#each $uiConfig.layout.options as option, index (option)}
    function create_each_block_1$3(key_1, ctx) {
    	let first;
    	let if_block_anchor;
    	let if_block = /*$uiConfig*/ ctx[5].layoutIcon.options[/*index*/ ctx[9]] && create_if_block_2$3(ctx);

    	return {
    		key: key_1,
    		first: null,
    		c() {
    			first = empty();
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    			this.first = first;
    		},
    		m(target, anchor) {
    			insert(target, first, anchor);
    			if (if_block) if_block.m(target, anchor);
    			insert(target, if_block_anchor, anchor);
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (/*$uiConfig*/ ctx[5].layoutIcon.options[/*index*/ ctx[9]]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block_2$3(ctx);
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(first);
    				detach(if_block_anchor);
    			}

    			if (if_block) if_block.d(detaching);
    		}
    	};
    }

    // (122:2) {#if $uiElements.texture}
    function create_if_block$3(ctx) {
    	let div1;
    	let span;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.material, "Material") + "";
    	let t0;
    	let t1;
    	let div0;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_value = ensure_array_like(/*$uiConfig*/ ctx[5].texture.options);
    	const get_key = ctx => /*option*/ ctx[7];

    	for (let i = 0; i < each_value.length; i += 1) {
    		let child_ctx = get_each_context$3(ctx, each_value, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block$3(key, child_ctx));
    	}

    	return {
    		c() {
    			div1 = element("div");
    			span = element("span");
    			t0 = text(t0_value);
    			t1 = space();
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(span, "class", "text-base");
    			attr(div0, "class", "flex items-center flex-wrap gap-4");
    			attr(div1, "class", "flex flex-col gap-6 style-category max-w-[180px]");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, span);
    			append(span, t0);
    			append(div1, t1);
    			append(div1, div0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				if (each_blocks[i]) {
    					each_blocks[i].m(div0, null);
    				}
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.material, "Material") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig, CONSTANTS*/ 32) {
    				each_value = ensure_array_like(/*$uiConfig*/ ctx[5].texture.options);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value, each_1_lookup, div0, destroy_block, create_each_block$3, null, get_each_context$3);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};
    }

    // (128:8) {#each $uiConfig.texture.options as option, index (option)}
    function create_each_block$3(key_1, ctx) {
    	let div;
    	let img;
    	let img_src_value;
    	let img_alt_value;
    	let t;
    	let div_class_value;
    	let div_data_layout_kind_value;

    	return {
    		key: key_1,
    		first: null,
    		c() {
    			div = element("div");
    			img = element("img");
    			t = space();
    			attr(img, "class", "max-w-full max-h-full");
    			if (!src_url_equal(img.src, img_src_value = `${CONSTANTS.s3BaseURL}/${/*$uiConfig*/ ctx[5].texture.options[/*index*/ ctx[9]]}`)) attr(img, "src", img_src_value);
    			attr(img, "alt", img_alt_value = /*option*/ ctx[7]);

    			attr(div, "class", div_class_value = "materials p-1.5 border border-[#F2F2F2] cursor-pointer flex items-center justify-center " + (/*option*/ ctx[7] === /*$uiConfig*/ ctx[5].texture.defaultAttributeValue
    			? 'active'
    			: '') + "");

    			attr(div, "data-layout-kind", div_data_layout_kind_value = /*option*/ ctx[7]);
    			this.first = div;
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, img);
    			append(div, t);
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (dirty & /*$uiConfig*/ 32 && !src_url_equal(img.src, img_src_value = `${CONSTANTS.s3BaseURL}/${/*$uiConfig*/ ctx[5].texture.options[/*index*/ ctx[9]]}`)) {
    				attr(img, "src", img_src_value);
    			}

    			if (dirty & /*$uiConfig*/ 32 && img_alt_value !== (img_alt_value = /*option*/ ctx[7])) {
    				attr(img, "alt", img_alt_value);
    			}

    			if (dirty & /*$uiConfig*/ 32 && div_class_value !== (div_class_value = "materials p-1.5 border border-[#F2F2F2] cursor-pointer flex items-center justify-center " + (/*option*/ ctx[7] === /*$uiConfig*/ ctx[5].texture.defaultAttributeValue
    			? 'active'
    			: '') + "")) {
    				attr(div, "class", div_class_value);
    			}

    			if (dirty & /*$uiConfig*/ 32 && div_data_layout_kind_value !== (div_data_layout_kind_value = /*option*/ ctx[7])) {
    				attr(div, "data-layout-kind", div_data_layout_kind_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}
    		}
    	};
    }

    function create_fragment$4(ctx) {
    	let div1;
    	let t0;
    	let div0;
    	let t1;
    	let t2;
    	let t3;
    	let t4;

    	function select_block_type(ctx, dirty) {
    		if (!/*$isDataLoaded*/ ctx[0]) return create_if_block_6$2;
    		return create_else_block$2;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block0 = current_block_type(ctx);
    	let if_block1 = /*$uiElements*/ ctx[4].showDimensions && create_if_block_5$2();
    	let if_block2 = /*$uiElements*/ ctx[4].hideItems && create_if_block_4$2();
    	let if_block3 = /*$uiElements*/ ctx[4].backpanel && create_if_block_3$2(ctx);
    	let if_block4 = /*$uiElements*/ ctx[4].layout && !/*$uiElements*/ ctx[4].hideLayout && create_if_block_1$3(ctx);
    	let if_block5 = /*$uiElements*/ ctx[4].texture && create_if_block$3(ctx);

    	return {
    		c() {
    			div1 = element("div");
    			if_block0.c();
    			t0 = space();
    			div0 = element("div");
    			if (if_block1) if_block1.c();
    			t1 = space();
    			if (if_block2) if_block2.c();
    			t2 = space();
    			if (if_block3) if_block3.c();
    			t3 = space();
    			if (if_block4) if_block4.c();
    			t4 = space();
    			if (if_block5) if_block5.c();
    			attr(div0, "class", "flex flex-row xl:flex-col gap-4 md:gap-8 xl:gap-2 text-base");
    			attr(div1, "class", "hidden xl:flex flex-col gap-6 text-base p-6 w-1/4");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			if_block0.m(div1, null);
    			append(div1, t0);
    			append(div1, div0);
    			if (if_block1) if_block1.m(div0, null);
    			append(div0, t1);
    			if (if_block2) if_block2.m(div0, null);
    			append(div0, t2);
    			if (if_block3) if_block3.m(div0, null);
    			append(div1, t3);
    			if (if_block4) if_block4.m(div1, null);
    			append(div1, t4);
    			if (if_block5) if_block5.m(div1, null);
    		},
    		p(ctx, [dirty]) {
    			if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block0) {
    				if_block0.p(ctx, dirty);
    			} else {
    				if_block0.d(1);
    				if_block0 = current_block_type(ctx);

    				if (if_block0) {
    					if_block0.c();
    					if_block0.m(div1, t0);
    				}
    			}

    			if (/*$uiElements*/ ctx[4].showDimensions) {
    				if (if_block1) ; else {
    					if_block1 = create_if_block_5$2();
    					if_block1.c();
    					if_block1.m(div0, t1);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}

    			if (/*$uiElements*/ ctx[4].hideItems) {
    				if (if_block2) ; else {
    					if_block2 = create_if_block_4$2();
    					if_block2.c();
    					if_block2.m(div0, t2);
    				}
    			} else if (if_block2) {
    				if_block2.d(1);
    				if_block2 = null;
    			}

    			if (/*$uiElements*/ ctx[4].backpanel) {
    				if (if_block3) {
    					if_block3.p(ctx, dirty);
    				} else {
    					if_block3 = create_if_block_3$2(ctx);
    					if_block3.c();
    					if_block3.m(div0, null);
    				}
    			} else if (if_block3) {
    				if_block3.d(1);
    				if_block3 = null;
    			}

    			if (/*$uiElements*/ ctx[4].layout && !/*$uiElements*/ ctx[4].hideLayout) {
    				if (if_block4) {
    					if_block4.p(ctx, dirty);
    				} else {
    					if_block4 = create_if_block_1$3(ctx);
    					if_block4.c();
    					if_block4.m(div1, t4);
    				}
    			} else if (if_block4) {
    				if_block4.d(1);
    				if_block4 = null;
    			}

    			if (/*$uiElements*/ ctx[4].texture) {
    				if (if_block5) {
    					if_block5.p(ctx, dirty);
    				} else {
    					if_block5 = create_if_block$3(ctx);
    					if_block5.c();
    					if_block5.m(div1, null);
    				}
    			} else if (if_block5) {
    				if_block5.d(1);
    				if_block5 = null;
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			if_block0.d();
    			if (if_block1) if_block1.d();
    			if (if_block2) if_block2.d();
    			if (if_block3) if_block3.d();
    			if (if_block4) if_block4.d();
    			if (if_block5) if_block5.d();
    		}
    	};
    }

    function instance$4($$self, $$props, $$invalidate) {
    	let $isDataLoaded;
    	let $productInfo;
    	let $productPrice;
    	let $getLocalizedOrDefaultText;
    	let $uiElements;
    	let $uiConfig;
    	component_subscribe($$self, isDataLoaded, $$value => $$invalidate(0, $isDataLoaded = $$value));
    	component_subscribe($$self, productInfo, $$value => $$invalidate(1, $productInfo = $$value));
    	component_subscribe($$self, productPrice, $$value => $$invalidate(2, $productPrice = $$value));
    	component_subscribe($$self, getLocalizedOrDefaultText, $$value => $$invalidate(3, $getLocalizedOrDefaultText = $$value));
    	component_subscribe($$self, uiElements, $$value => $$invalidate(4, $uiElements = $$value));
    	component_subscribe($$self, uiConfig, $$value => $$invalidate(5, $uiConfig = $$value));

    	function input_change_handler() {
    		$uiConfig.backpanel.defaultAttributeValue = this.checked;
    		uiConfig.set($uiConfig);
    	}

    	return [
    		$isDataLoaded,
    		$productInfo,
    		$productPrice,
    		$getLocalizedOrDefaultText,
    		$uiElements,
    		$uiConfig,
    		input_change_handler
    	];
    }

    class TabPanel extends SvelteComponent {
    	constructor(options) {
    		super();
    		init$1(this, options, instance$4, create_fragment$4, safe_not_equal, {});
    	}
    }

    create_custom_element(TabPanel, {}, [], [], true);

    /* src/lib/components/ResponsiveScreen.svelte generated by Svelte v4.0.3 */

    function get_each_context$2(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[6] = list[i];
    	child_ctx[8] = i;
    	return child_ctx;
    }

    function get_each_context_1$2(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[6] = list[i];
    	child_ctx[8] = i;
    	return child_ctx;
    }

    function get_each_context_2$2(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[6] = list[i];
    	return child_ctx;
    }

    function get_each_context_3$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[6] = list[i];
    	return child_ctx;
    }

    // (16:2) {:else}
    function create_else_block$1(ctx) {
    	let t0;
    	let t1;
    	let t2;
    	let t3;
    	let t4;
    	let t5;
    	let if_block6_anchor;
    	let if_block0 = /*$uiElements*/ ctx[2].color && create_if_block_8$1(ctx);
    	let if_block1 = /*$uiElements*/ ctx[2].depth && create_if_block_7$1(ctx);
    	let if_block2 = /*$uiElements*/ ctx[2].layout && !/*$uiElements*/ ctx[2].hideLayout && create_if_block_5$1(ctx);
    	let if_block3 = /*$uiElements*/ ctx[2].texture && create_if_block_4$1(ctx);
    	let if_block4 = /*$uiElements*/ ctx[2].height && !/*$uiElements*/ ctx[2].hideLayout && create_if_block_3$1(ctx);
    	let if_block5 = /*$uiElements*/ ctx[2].density && create_if_block_2$2(ctx);
    	let if_block6 = /*$uiElements*/ ctx[2].width && create_if_block_1$2(ctx);

    	return {
    		c() {
    			if (if_block0) if_block0.c();
    			t0 = space();
    			if (if_block1) if_block1.c();
    			t1 = space();
    			if (if_block2) if_block2.c();
    			t2 = space();
    			if (if_block3) if_block3.c();
    			t3 = space();
    			if (if_block4) if_block4.c();
    			t4 = space();
    			if (if_block5) if_block5.c();
    			t5 = space();
    			if (if_block6) if_block6.c();
    			if_block6_anchor = empty();
    		},
    		m(target, anchor) {
    			if (if_block0) if_block0.m(target, anchor);
    			insert(target, t0, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert(target, t1, anchor);
    			if (if_block2) if_block2.m(target, anchor);
    			insert(target, t2, anchor);
    			if (if_block3) if_block3.m(target, anchor);
    			insert(target, t3, anchor);
    			if (if_block4) if_block4.m(target, anchor);
    			insert(target, t4, anchor);
    			if (if_block5) if_block5.m(target, anchor);
    			insert(target, t5, anchor);
    			if (if_block6) if_block6.m(target, anchor);
    			insert(target, if_block6_anchor, anchor);
    		},
    		p(ctx, dirty) {
    			if (/*$uiElements*/ ctx[2].color) {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);
    				} else {
    					if_block0 = create_if_block_8$1(ctx);
    					if_block0.c();
    					if_block0.m(t0.parentNode, t0);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (/*$uiElements*/ ctx[2].depth) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    				} else {
    					if_block1 = create_if_block_7$1(ctx);
    					if_block1.c();
    					if_block1.m(t1.parentNode, t1);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}

    			if (/*$uiElements*/ ctx[2].layout && !/*$uiElements*/ ctx[2].hideLayout) {
    				if (if_block2) {
    					if_block2.p(ctx, dirty);
    				} else {
    					if_block2 = create_if_block_5$1(ctx);
    					if_block2.c();
    					if_block2.m(t2.parentNode, t2);
    				}
    			} else if (if_block2) {
    				if_block2.d(1);
    				if_block2 = null;
    			}

    			if (/*$uiElements*/ ctx[2].texture) {
    				if (if_block3) {
    					if_block3.p(ctx, dirty);
    				} else {
    					if_block3 = create_if_block_4$1(ctx);
    					if_block3.c();
    					if_block3.m(t3.parentNode, t3);
    				}
    			} else if (if_block3) {
    				if_block3.d(1);
    				if_block3 = null;
    			}

    			if (/*$uiElements*/ ctx[2].height && !/*$uiElements*/ ctx[2].hideLayout) {
    				if (if_block4) {
    					if_block4.p(ctx, dirty);
    				} else {
    					if_block4 = create_if_block_3$1(ctx);
    					if_block4.c();
    					if_block4.m(t4.parentNode, t4);
    				}
    			} else if (if_block4) {
    				if_block4.d(1);
    				if_block4 = null;
    			}

    			if (/*$uiElements*/ ctx[2].density) {
    				if (if_block5) {
    					if_block5.p(ctx, dirty);
    				} else {
    					if_block5 = create_if_block_2$2(ctx);
    					if_block5.c();
    					if_block5.m(t5.parentNode, t5);
    				}
    			} else if (if_block5) {
    				if_block5.d(1);
    				if_block5 = null;
    			}

    			if (/*$uiElements*/ ctx[2].width) {
    				if (if_block6) {
    					if_block6.p(ctx, dirty);
    				} else {
    					if_block6 = create_if_block_1$2(ctx);
    					if_block6.c();
    					if_block6.m(if_block6_anchor.parentNode, if_block6_anchor);
    				}
    			} else if (if_block6) {
    				if_block6.d(1);
    				if_block6 = null;
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(t0);
    				detach(t1);
    				detach(t2);
    				detach(t3);
    				detach(t4);
    				detach(t5);
    				detach(if_block6_anchor);
    			}

    			if (if_block0) if_block0.d(detaching);
    			if (if_block1) if_block1.d(detaching);
    			if (if_block2) if_block2.d(detaching);
    			if (if_block3) if_block3.d(detaching);
    			if (if_block4) if_block4.d(detaching);
    			if (if_block5) if_block5.d(detaching);
    			if (if_block6) if_block6.d(detaching);
    		}
    	};
    }

    // (9:2) {#if !$isDataLoaded}
    function create_if_block$2(ctx) {
    	let div0;
    	let t0;
    	let div1;
    	let t1;
    	let div2;
    	let t2;
    	let div3;
    	let t3;
    	let div4;
    	let t4;
    	let div5;

    	return {
    		c() {
    			div0 = element("div");
    			t0 = space();
    			div1 = element("div");
    			t1 = space();
    			div2 = element("div");
    			t2 = space();
    			div3 = element("div");
    			t3 = space();
    			div4 = element("div");
    			t4 = space();
    			div5 = element("div");
    			attr(div0, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div1, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div2, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div3, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div4, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div5, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    		},
    		m(target, anchor) {
    			insert(target, div0, anchor);
    			insert(target, t0, anchor);
    			insert(target, div1, anchor);
    			insert(target, t1, anchor);
    			insert(target, div2, anchor);
    			insert(target, t2, anchor);
    			insert(target, div3, anchor);
    			insert(target, t3, anchor);
    			insert(target, div4, anchor);
    			insert(target, t4, anchor);
    			insert(target, div5, anchor);
    		},
    		p: noop,
    		d(detaching) {
    			if (detaching) {
    				detach(div0);
    				detach(t0);
    				detach(div1);
    				detach(t1);
    				detach(div2);
    				detach(t2);
    				detach(div3);
    				detach(t3);
    				detach(div4);
    				detach(t4);
    				detach(div5);
    			}
    		}
    	};
    }

    // (17:4) {#if $uiElements.color}
    function create_if_block_8$1(ctx) {
    	let div1;
    	let span;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.color, "Color") + "";
    	let t0;
    	let t1;
    	let div0;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_value_3 = ensure_array_like(/*$uiConfig*/ ctx[4].color.options);
    	const get_key = ctx => /*option*/ ctx[6];

    	for (let i = 0; i < each_value_3.length; i += 1) {
    		let child_ctx = get_each_context_3$1(ctx, each_value_3, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block_3$1(key, child_ctx));
    	}

    	return {
    		c() {
    			div1 = element("div");
    			span = element("span");
    			t0 = text(t0_value);
    			t1 = space();
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div0, "class", "flex items-center justify-between md:justify-start md:gap-12 color-variants w-full");
    			attr(div1, "class", "flex items-center gap-12 color-variants");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, span);
    			append(span, t0);
    			append(div1, t1);
    			append(div1, div0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				if (each_blocks[i]) {
    					each_blocks[i].m(div0, null);
    				}
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.color, "Color") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig*/ 16) {
    				each_value_3 = ensure_array_like(/*$uiConfig*/ ctx[4].color.options);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value_3, each_1_lookup, div0, destroy_block, create_each_block_3$1, null, get_each_context_3$1);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};
    }

    // (25:10) {#each $uiConfig.color.options as option (option)}
    function create_each_block_3$1(key_1, ctx) {
    	let div1;
    	let div0;
    	let t;
    	let div1_class_value;
    	let div1_data_color_value;

    	return {
    		key: key_1,
    		first: null,
    		c() {
    			div1 = element("div");
    			div0 = element("div");
    			t = space();
    			attr(div0, "class", "rounded-full h-6 w-6");
    			set_style(div0, "background-color", /*option*/ ctx[6]);

    			attr(div1, "class", div1_class_value = "variants rounded-full p-1.5 border border-[#F2F2F2] cursor-pointer " + (/*option*/ ctx[6] === /*$uiConfig*/ ctx[4].color.defaultAttributeValue
    			? 'selectedColor'
    			: ''));

    			attr(div1, "data-color", div1_data_color_value = /*option*/ ctx[6]);
    			this.first = div1;
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, div0);
    			append(div1, t);
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (dirty & /*$uiConfig*/ 16) {
    				set_style(div0, "background-color", /*option*/ ctx[6]);
    			}

    			if (dirty & /*$uiConfig*/ 16 && div1_class_value !== (div1_class_value = "variants rounded-full p-1.5 border border-[#F2F2F2] cursor-pointer " + (/*option*/ ctx[6] === /*$uiConfig*/ ctx[4].color.defaultAttributeValue
    			? 'selectedColor'
    			: ''))) {
    				attr(div1, "class", div1_class_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && div1_data_color_value !== (div1_data_color_value = /*option*/ ctx[6])) {
    				attr(div1, "data-color", div1_data_color_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}
    		}
    	};
    }

    // (43:4) {#if $uiElements.depth}
    function create_if_block_7$1(ctx) {
    	let div1;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.depth, "Depth") + "";
    	let t0;
    	let t1;
    	let div0;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_value_2 = ensure_array_like(/*$uiConfig*/ ctx[4].depth.options);
    	const get_key = ctx => /*option*/ ctx[6];

    	for (let i = 0; i < each_value_2.length; i += 1) {
    		let child_ctx = get_each_context_2$2(ctx, each_value_2, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block_2$2(key, child_ctx));
    	}

    	return {
    		c() {
    			div1 = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div0, "class", "options flex items-center gap-6 text-base");
    			attr(div1, "class", "flex items-center gap-12 text-base");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, t0);
    			append(div1, t1);
    			append(div1, div0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				if (each_blocks[i]) {
    					each_blocks[i].m(div0, null);
    				}
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.depth, "Depth") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig, handleButtonSelection, root*/ 17) {
    				each_value_2 = ensure_array_like(/*$uiConfig*/ ctx[4].depth.options);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value_2, each_1_lookup, div0, destroy_block, create_each_block_2$2, null, get_each_context_2$2);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};
    }

    // (47:10) {#each $uiConfig.depth.options as option (option)}
    function create_each_block_2$2(key_1, ctx) {
    	let button;
    	let t0_value = /*option*/ ctx[6] + "";
    	let t0;
    	let t1;
    	let button_class_value;
    	let button_value_value;
    	let mounted;
    	let dispose;

    	return {
    		key: key_1,
    		first: null,
    		c() {
    			button = element("button");
    			t0 = text(t0_value);
    			t1 = text(" cm\n            ");

    			attr(button, "class", button_class_value = "border depth-button cursor-pointer rounded-[29px] px-4 py-2.5 w-24 " + (/*option*/ ctx[6] === /*$uiConfig*/ ctx[4].depth.defaultAttributeValue
    			? 'selected'
    			: ''));

    			button.value = button_value_value = /*option*/ ctx[6];
    			this.first = button;
    		},
    		m(target, anchor) {
    			insert(target, button, anchor);
    			append(button, t0);
    			append(button, t1);

    			if (!mounted) {
    				dispose = listen(button, "click", /*click_handler*/ ctx[5]);
    				mounted = true;
    			}
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty & /*$uiConfig*/ 16 && t0_value !== (t0_value = /*option*/ ctx[6] + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig*/ 16 && button_class_value !== (button_class_value = "border depth-button cursor-pointer rounded-[29px] px-4 py-2.5 w-24 " + (/*option*/ ctx[6] === /*$uiConfig*/ ctx[4].depth.defaultAttributeValue
    			? 'selected'
    			: ''))) {
    				attr(button, "class", button_class_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && button_value_value !== (button_value_value = /*option*/ ctx[6])) {
    				button.value = button_value_value;
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(button);
    			}

    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (69:4) {#if $uiElements.layout && !$uiElements.hideLayout}
    function create_if_block_5$1(ctx) {
    	let div1;
    	let span;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.styles, "Styles") + "";
    	let t0;
    	let t1;
    	let div0;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_value_1 = ensure_array_like(/*$uiConfig*/ ctx[4].layout.options);
    	const get_key = ctx => /*option*/ ctx[6];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		let child_ctx = get_each_context_1$2(ctx, each_value_1, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block_1$2(key, child_ctx));
    	}

    	return {
    		c() {
    			div1 = element("div");
    			span = element("span");
    			t0 = text(t0_value);
    			t1 = space();
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(span, "class", "text-base");
    			attr(div0, "class", "flex items-center flex-wrap gap-4");
    			attr(div1, "class", "flex items-center gap-12 style-category");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, span);
    			append(span, t0);
    			append(div1, t1);
    			append(div1, div0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				if (each_blocks[i]) {
    					each_blocks[i].m(div0, null);
    				}
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.styles, "Styles") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig, CONSTANTS*/ 16) {
    				each_value_1 = ensure_array_like(/*$uiConfig*/ ctx[4].layout.options);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value_1, each_1_lookup, div0, destroy_block, create_each_block_1$2, null, get_each_context_1$2);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};
    }

    // (79:12) {#if $uiConfig.layoutIcon.options[index]}
    function create_if_block_6$1(ctx) {
    	let div;
    	let img;
    	let img_src_value;
    	let t;
    	let div_class_value;
    	let div_data_layout_kind_value;

    	return {
    		c() {
    			div = element("div");
    			img = element("img");
    			t = space();
    			attr(img, "class", "max-w-full max-h-full");
    			if (!src_url_equal(img.src, img_src_value = `${CONSTANTS.s3BaseURL}/${/*$uiConfig*/ ctx[4].layoutIcon.options[/*index*/ ctx[8]]}`)) attr(img, "src", img_src_value);
    			attr(img, "alt", "");

    			attr(div, "class", div_class_value = "layouts p-1.5 border border-[#F2F2F2] cursor-pointer flex items-center justify-center " + (/*option*/ ctx[6] === /*$uiConfig*/ ctx[4].layout.defaultAttributeValue
    			? 'active'
    			: ''));

    			attr(div, "data-layout-kind", div_data_layout_kind_value = /*option*/ ctx[6]);
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, img);
    			append(div, t);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$uiConfig*/ 16 && !src_url_equal(img.src, img_src_value = `${CONSTANTS.s3BaseURL}/${/*$uiConfig*/ ctx[4].layoutIcon.options[/*index*/ ctx[8]]}`)) {
    				attr(img, "src", img_src_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && div_class_value !== (div_class_value = "layouts p-1.5 border border-[#F2F2F2] cursor-pointer flex items-center justify-center " + (/*option*/ ctx[6] === /*$uiConfig*/ ctx[4].layout.defaultAttributeValue
    			? 'active'
    			: ''))) {
    				attr(div, "class", div_class_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && div_data_layout_kind_value !== (div_data_layout_kind_value = /*option*/ ctx[6])) {
    				attr(div, "data-layout-kind", div_data_layout_kind_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}
    		}
    	};
    }

    // (78:10) {#each $uiConfig.layout.options as option, index (option)}
    function create_each_block_1$2(key_1, ctx) {
    	let first;
    	let if_block_anchor;
    	let if_block = /*$uiConfig*/ ctx[4].layoutIcon.options[/*index*/ ctx[8]] && create_if_block_6$1(ctx);

    	return {
    		key: key_1,
    		first: null,
    		c() {
    			first = empty();
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    			this.first = first;
    		},
    		m(target, anchor) {
    			insert(target, first, anchor);
    			if (if_block) if_block.m(target, anchor);
    			insert(target, if_block_anchor, anchor);
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (/*$uiConfig*/ ctx[4].layoutIcon.options[/*index*/ ctx[8]]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block_6$1(ctx);
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(first);
    				detach(if_block_anchor);
    			}

    			if (if_block) if_block.d(detaching);
    		}
    	};
    }

    // (99:4) {#if $uiElements.texture}
    function create_if_block_4$1(ctx) {
    	let div1;
    	let span;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.material, "Material") + "";
    	let t0;
    	let t1;
    	let div0;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_value = ensure_array_like(/*$uiConfig*/ ctx[4].texture.options);
    	const get_key = ctx => /*option*/ ctx[6];

    	for (let i = 0; i < each_value.length; i += 1) {
    		let child_ctx = get_each_context$2(ctx, each_value, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block$2(key, child_ctx));
    	}

    	return {
    		c() {
    			div1 = element("div");
    			span = element("span");
    			t0 = text(t0_value);
    			t1 = space();
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(span, "class", "text-base");
    			attr(div0, "class", "flex items-center flex-wrap gap-4");
    			attr(div1, "class", "flex items-center gap-12 style-category");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, span);
    			append(span, t0);
    			append(div1, t1);
    			append(div1, div0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				if (each_blocks[i]) {
    					each_blocks[i].m(div0, null);
    				}
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.material, "Material") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig, CONSTANTS*/ 16) {
    				each_value = ensure_array_like(/*$uiConfig*/ ctx[4].texture.options);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value, each_1_lookup, div0, destroy_block, create_each_block$2, null, get_each_context$2);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};
    }

    // (108:10) {#each $uiConfig.texture.options as option, index (option)}
    function create_each_block$2(key_1, ctx) {
    	let div;
    	let img;
    	let img_src_value;
    	let img_alt_value;
    	let t;
    	let div_class_value;

    	return {
    		key: key_1,
    		first: null,
    		c() {
    			div = element("div");
    			img = element("img");
    			t = space();
    			attr(img, "class", "max-w-full max-h-full");
    			if (!src_url_equal(img.src, img_src_value = `${CONSTANTS.s3BaseURL}/${/*$uiConfig*/ ctx[4].texture.options[/*index*/ ctx[8]]}`)) attr(img, "src", img_src_value);
    			attr(img, "alt", img_alt_value = /*option*/ ctx[6]);

    			attr(div, "class", div_class_value = "materials p-1.5 border border-[#F2F2F2] cursor-pointer flex items-center justify-center " + (/*option*/ ctx[6] === /*$uiConfig*/ ctx[4].texture.defaultAttributeValue
    			? 'active'
    			: ''));

    			this.first = div;
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, img);
    			append(div, t);
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (dirty & /*$uiConfig*/ 16 && !src_url_equal(img.src, img_src_value = `${CONSTANTS.s3BaseURL}/${/*$uiConfig*/ ctx[4].texture.options[/*index*/ ctx[8]]}`)) {
    				attr(img, "src", img_src_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && img_alt_value !== (img_alt_value = /*option*/ ctx[6])) {
    				attr(img, "alt", img_alt_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && div_class_value !== (div_class_value = "materials p-1.5 border border-[#F2F2F2] cursor-pointer flex items-center justify-center " + (/*option*/ ctx[6] === /*$uiConfig*/ ctx[4].texture.defaultAttributeValue
    			? 'active'
    			: ''))) {
    				attr(div, "class", div_class_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}
    		}
    	};
    }

    // (126:4) {#if $uiElements.height && !$uiElements.hideLayout}
    function create_if_block_3$1(ctx) {
    	let div6;
    	let p;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.height, "Height") + "";
    	let t0;
    	let t1;
    	let div5;
    	let div1;
    	let t2;
    	let div4;
    	let t5;
    	let input;
    	let input_min_value;
    	let input_max_value;
    	let input_value_value;
    	let input_step_value;

    	return {
    		c() {
    			div6 = element("div");
    			p = element("p");
    			t0 = text(t0_value);
    			t1 = space();
    			div5 = element("div");
    			div1 = element("div");
    			div1.innerHTML = `<div class="range__slider-line height-range-line"></div>`;
    			t2 = space();
    			div4 = element("div");
    			div4.innerHTML = `<div class="range__value"><span class="range__value-number height-range-number">1</span> <div class="arrow-down"></div></div>`;
    			t5 = space();
    			input = element("input");
    			attr(p, "class", "w-20");
    			attr(div1, "class", "range__slider");
    			attr(div4, "class", "range__thumb height-range-thumb");
    			attr(input, "type", "range");
    			attr(input, "class", "range__input border rangeSlider slider height-range sliderRange width-slider");
    			attr(input, "min", input_min_value = /*$uiConfig*/ ctx[4].height.minValue);
    			attr(input, "max", input_max_value = /*$uiConfig*/ ctx[4].height.maxValue);
    			input.value = input_value_value = /*$uiConfig*/ ctx[4].height.defaultAttributeValue;
    			attr(input, "step", input_step_value = /*$uiConfig*/ ctx[4].height.stepValue);
    			attr(div5, "class", "range__content");
    			attr(div6, "class", "flex items-center gap-6 slidecontainer text-base");
    		},
    		m(target, anchor) {
    			insert(target, div6, anchor);
    			append(div6, p);
    			append(p, t0);
    			append(div6, t1);
    			append(div6, div5);
    			append(div5, div1);
    			append(div5, t2);
    			append(div5, div4);
    			append(div5, t5);
    			append(div5, input);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.height, "Height") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig*/ 16 && input_min_value !== (input_min_value = /*$uiConfig*/ ctx[4].height.minValue)) {
    				attr(input, "min", input_min_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_max_value !== (input_max_value = /*$uiConfig*/ ctx[4].height.maxValue)) {
    				attr(input, "max", input_max_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_value_value !== (input_value_value = /*$uiConfig*/ ctx[4].height.defaultAttributeValue)) {
    				input.value = input_value_value;
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_step_value !== (input_step_value = /*$uiConfig*/ ctx[4].height.stepValue)) {
    				attr(input, "step", input_step_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div6);
    			}
    		}
    	};
    }

    // (154:4) {#if $uiElements.density}
    function create_if_block_2$2(ctx) {
    	let div6;
    	let p;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.density, "Density") + "";
    	let t0;
    	let t1;
    	let div5;
    	let div1;
    	let t2;
    	let div4;
    	let t5;
    	let input;
    	let input_min_value;
    	let input_max_value;
    	let input_value_value;
    	let input_step_value;

    	return {
    		c() {
    			div6 = element("div");
    			p = element("p");
    			t0 = text(t0_value);
    			t1 = space();
    			div5 = element("div");
    			div1 = element("div");
    			div1.innerHTML = `<div class="range__slider-line density-range-line"></div>`;
    			t2 = space();
    			div4 = element("div");
    			div4.innerHTML = `<div class="range__value"><span class="range__value-number density-range-number">1</span> <div class="arrow-down"></div></div>`;
    			t5 = space();
    			input = element("input");
    			attr(p, "class", "w-20");
    			attr(div1, "class", "range__slider");
    			attr(div4, "class", "range__thumb density-range-thumb");
    			attr(input, "type", "range");
    			attr(input, "class", "range__input border rangeSlider slider density-range sliderRange");
    			attr(input, "min", input_min_value = /*$uiConfig*/ ctx[4].density.minValue);
    			attr(input, "max", input_max_value = /*$uiConfig*/ ctx[4].density.maxValue);
    			input.value = input_value_value = /*$uiConfig*/ ctx[4].density.defaultAttributeValue;
    			attr(input, "step", input_step_value = /*$uiConfig*/ ctx[4].density.stepValue);
    			attr(div5, "class", "range__content");
    			attr(div6, "class", "flex items-center gap-6 slidecontainer text-base");
    		},
    		m(target, anchor) {
    			insert(target, div6, anchor);
    			append(div6, p);
    			append(p, t0);
    			append(div6, t1);
    			append(div6, div5);
    			append(div5, div1);
    			append(div5, t2);
    			append(div5, div4);
    			append(div5, t5);
    			append(div5, input);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.density, "Density") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig*/ 16 && input_min_value !== (input_min_value = /*$uiConfig*/ ctx[4].density.minValue)) {
    				attr(input, "min", input_min_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_max_value !== (input_max_value = /*$uiConfig*/ ctx[4].density.maxValue)) {
    				attr(input, "max", input_max_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_value_value !== (input_value_value = /*$uiConfig*/ ctx[4].density.defaultAttributeValue)) {
    				input.value = input_value_value;
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_step_value !== (input_step_value = /*$uiConfig*/ ctx[4].density.stepValue)) {
    				attr(input, "step", input_step_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div6);
    			}
    		}
    	};
    }

    // (182:4) {#if $uiElements.width}
    function create_if_block_1$2(ctx) {
    	let div6;
    	let p;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.width, "Width") + "";
    	let t0;
    	let t1;
    	let div5;
    	let div1;
    	let t2;
    	let div4;
    	let t5;
    	let input;
    	let input_min_value;
    	let input_max_value;
    	let input_value_value;
    	let input_step_value;

    	return {
    		c() {
    			div6 = element("div");
    			p = element("p");
    			t0 = text(t0_value);
    			t1 = space();
    			div5 = element("div");
    			div1 = element("div");
    			div1.innerHTML = `<div class="range__slider-line width-range-line"></div>`;
    			t2 = space();
    			div4 = element("div");
    			div4.innerHTML = `<div class="range__value"><span class="range__value-number width-range-number">1</span> <div class="arrow-down"></div></div>`;
    			t5 = space();
    			input = element("input");
    			attr(p, "class", "w-20");
    			attr(div1, "class", "range__slider");
    			attr(div4, "class", "range__thumb width-range-thumb");
    			attr(input, "type", "range");
    			attr(input, "class", "range__input border slider rangeSlider width-range sliderRange width-slider");
    			attr(input, "min", input_min_value = /*$uiConfig*/ ctx[4].width.minValue);
    			attr(input, "max", input_max_value = /*$uiConfig*/ ctx[4].width.maxValue);
    			input.value = input_value_value = /*$uiConfig*/ ctx[4].width.defaultAttributeValue;
    			attr(input, "step", input_step_value = /*$uiConfig*/ ctx[4].width.stepValue);
    			attr(div5, "class", "range__content");
    			attr(div6, "class", "flex items-center gap-6 slidecontainer text-base");
    		},
    		m(target, anchor) {
    			insert(target, div6, anchor);
    			append(div6, p);
    			append(p, t0);
    			append(div6, t1);
    			append(div6, div5);
    			append(div5, div1);
    			append(div5, t2);
    			append(div5, div4);
    			append(div5, t5);
    			append(div5, input);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.width, "Width") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig*/ 16 && input_min_value !== (input_min_value = /*$uiConfig*/ ctx[4].width.minValue)) {
    				attr(input, "min", input_min_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_max_value !== (input_max_value = /*$uiConfig*/ ctx[4].width.maxValue)) {
    				attr(input, "max", input_max_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_value_value !== (input_value_value = /*$uiConfig*/ ctx[4].width.defaultAttributeValue)) {
    				input.value = input_value_value;
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_step_value !== (input_step_value = /*$uiConfig*/ ctx[4].width.stepValue)) {
    				attr(input, "step", input_step_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div6);
    			}
    		}
    	};
    }

    function create_fragment$3(ctx) {
    	let div0;
    	let t;
    	let div3;

    	function select_block_type(ctx, dirty) {
    		if (!/*$isDataLoaded*/ ctx[1]) return create_if_block$2;
    		return create_else_block$1;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block = current_block_type(ctx);

    	return {
    		c() {
    			div0 = element("div");
    			if_block.c();
    			t = space();
    			div3 = element("div");
    			div3.innerHTML = `<div class="column right-container"><div id="input-configuration"></div></div>`;
    			attr(div0, "class", "hidden sm:flex xl:hidden flex-col gap-14 p-7 md:p-16");
    			attr(div3, "class", "flex");
    		},
    		m(target, anchor) {
    			insert(target, div0, anchor);
    			if_block.m(div0, null);
    			insert(target, t, anchor);
    			insert(target, div3, anchor);
    		},
    		p(ctx, [dirty]) {
    			if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
    				if_block.p(ctx, dirty);
    			} else {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(div0, null);
    				}
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (detaching) {
    				detach(div0);
    				detach(t);
    				detach(div3);
    			}

    			if_block.d();
    		}
    	};
    }

    function instance$3($$self, $$props, $$invalidate) {
    	let $isDataLoaded;
    	let $uiElements;
    	let $getLocalizedOrDefaultText;
    	let $uiConfig;
    	component_subscribe($$self, isDataLoaded, $$value => $$invalidate(1, $isDataLoaded = $$value));
    	component_subscribe($$self, uiElements, $$value => $$invalidate(2, $uiElements = $$value));
    	component_subscribe($$self, getLocalizedOrDefaultText, $$value => $$invalidate(3, $getLocalizedOrDefaultText = $$value));
    	component_subscribe($$self, uiConfig, $$value => $$invalidate(4, $uiConfig = $$value));
    	let { root } = $$props;
    	const click_handler = event => handleButtonSelection(root, event.target, ".options button", "selected");

    	$$self.$$set = $$props => {
    		if ('root' in $$props) $$invalidate(0, root = $$props.root);
    	};

    	return [
    		root,
    		$isDataLoaded,
    		$uiElements,
    		$getLocalizedOrDefaultText,
    		$uiConfig,
    		click_handler
    	];
    }

    class ResponsiveScreen extends SvelteComponent {
    	constructor(options) {
    		super();
    		init$1(this, options, instance$3, create_fragment$3, safe_not_equal, { root: 0 });
    	}

    	get root() {
    		return this.$$.ctx[0];
    	}

    	set root(root) {
    		this.$$set({ root });
    		flush();
    	}
    }

    create_custom_element(ResponsiveScreen, {"root":{}}, [], [], true);

    /* src/lib/components/MobileScreen.svelte generated by Svelte v4.0.3 */

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[19] = list[i];
    	return child_ctx;
    }

    function get_each_context_1$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[19] = list[i];
    	return child_ctx;
    }

    function get_each_context_2$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[19] = list[i];
    	child_ctx[25] = i;
    	return child_ctx;
    }

    function get_each_context_3(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[19] = list[i];
    	child_ctx[25] = i;
    	return child_ctx;
    }

    // (38:2) {:else}
    function create_else_block_2(ctx) {
    	let t0;
    	let t1;
    	let t2;
    	let t3;
    	let t4;
    	let t5;
    	let if_block6_anchor;
    	let if_block0 = /*$uiElements*/ ctx[2].color && create_if_block_20(ctx);
    	let if_block1 = /*$uiElements*/ ctx[2].depth && create_if_block_19(ctx);
    	let if_block2 = /*$uiElements*/ ctx[2].layout && !/*$uiElements*/ ctx[2].hideLayout && create_if_block_18(ctx);
    	let if_block3 = /*$uiElements*/ ctx[2].texture && create_if_block_17(ctx);
    	let if_block4 = /*$uiElements*/ ctx[2].height && !/*$uiElements*/ ctx[2].hideLayout && create_if_block_16(ctx);
    	let if_block5 = /*$uiElements*/ ctx[2].density && create_if_block_15(ctx);
    	let if_block6 = /*$uiElements*/ ctx[2].width && create_if_block_14(ctx);

    	return {
    		c() {
    			if (if_block0) if_block0.c();
    			t0 = space();
    			if (if_block1) if_block1.c();
    			t1 = space();
    			if (if_block2) if_block2.c();
    			t2 = space();
    			if (if_block3) if_block3.c();
    			t3 = space();
    			if (if_block4) if_block4.c();
    			t4 = space();
    			if (if_block5) if_block5.c();
    			t5 = space();
    			if (if_block6) if_block6.c();
    			if_block6_anchor = empty();
    		},
    		m(target, anchor) {
    			if (if_block0) if_block0.m(target, anchor);
    			insert(target, t0, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert(target, t1, anchor);
    			if (if_block2) if_block2.m(target, anchor);
    			insert(target, t2, anchor);
    			if (if_block3) if_block3.m(target, anchor);
    			insert(target, t3, anchor);
    			if (if_block4) if_block4.m(target, anchor);
    			insert(target, t4, anchor);
    			if (if_block5) if_block5.m(target, anchor);
    			insert(target, t5, anchor);
    			if (if_block6) if_block6.m(target, anchor);
    			insert(target, if_block6_anchor, anchor);
    		},
    		p(ctx, dirty) {
    			if (/*$uiElements*/ ctx[2].color) {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);
    				} else {
    					if_block0 = create_if_block_20(ctx);
    					if_block0.c();
    					if_block0.m(t0.parentNode, t0);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (/*$uiElements*/ ctx[2].depth) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    				} else {
    					if_block1 = create_if_block_19(ctx);
    					if_block1.c();
    					if_block1.m(t1.parentNode, t1);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}

    			if (/*$uiElements*/ ctx[2].layout && !/*$uiElements*/ ctx[2].hideLayout) {
    				if (if_block2) {
    					if_block2.p(ctx, dirty);
    				} else {
    					if_block2 = create_if_block_18(ctx);
    					if_block2.c();
    					if_block2.m(t2.parentNode, t2);
    				}
    			} else if (if_block2) {
    				if_block2.d(1);
    				if_block2 = null;
    			}

    			if (/*$uiElements*/ ctx[2].texture) {
    				if (if_block3) {
    					if_block3.p(ctx, dirty);
    				} else {
    					if_block3 = create_if_block_17(ctx);
    					if_block3.c();
    					if_block3.m(t3.parentNode, t3);
    				}
    			} else if (if_block3) {
    				if_block3.d(1);
    				if_block3 = null;
    			}

    			if (/*$uiElements*/ ctx[2].height && !/*$uiElements*/ ctx[2].hideLayout) {
    				if (if_block4) {
    					if_block4.p(ctx, dirty);
    				} else {
    					if_block4 = create_if_block_16(ctx);
    					if_block4.c();
    					if_block4.m(t4.parentNode, t4);
    				}
    			} else if (if_block4) {
    				if_block4.d(1);
    				if_block4 = null;
    			}

    			if (/*$uiElements*/ ctx[2].density) {
    				if (if_block5) {
    					if_block5.p(ctx, dirty);
    				} else {
    					if_block5 = create_if_block_15(ctx);
    					if_block5.c();
    					if_block5.m(t5.parentNode, t5);
    				}
    			} else if (if_block5) {
    				if_block5.d(1);
    				if_block5 = null;
    			}

    			if (/*$uiElements*/ ctx[2].width) {
    				if (if_block6) {
    					if_block6.p(ctx, dirty);
    				} else {
    					if_block6 = create_if_block_14(ctx);
    					if_block6.c();
    					if_block6.m(if_block6_anchor.parentNode, if_block6_anchor);
    				}
    			} else if (if_block6) {
    				if_block6.d(1);
    				if_block6 = null;
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(t0);
    				detach(t1);
    				detach(t2);
    				detach(t3);
    				detach(t4);
    				detach(t5);
    				detach(if_block6_anchor);
    			}

    			if (if_block0) if_block0.d(detaching);
    			if (if_block1) if_block1.d(detaching);
    			if (if_block2) if_block2.d(detaching);
    			if (if_block3) if_block3.d(detaching);
    			if (if_block4) if_block4.d(detaching);
    			if (if_block5) if_block5.d(detaching);
    			if (if_block6) if_block6.d(detaching);
    		}
    	};
    }

    // (36:2) {#if !$isDataLoaded}
    function create_if_block_13(ctx) {
    	let div;

    	return {
    		c() {
    			div = element("div");
    			attr(div, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    		},
    		p: noop,
    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}
    		}
    	};
    }

    // (39:4) {#if $uiElements.color}
    function create_if_block_20(ctx) {
    	let button;
    	let t_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.color, "Color") + "";
    	let t;
    	let mounted;
    	let dispose;

    	return {
    		c() {
    			button = element("button");
    			t = text(t_value);
    			attr(button, "class", "pl-7");
    		},
    		m(target, anchor) {
    			insert(target, button, anchor);
    			append(button, t);

    			if (!mounted) {
    				dispose = listen(button, "click", /*click_handler*/ ctx[8]);
    				mounted = true;
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t_value !== (t_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.color, "Color") + "")) set_data(t, t_value);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(button);
    			}

    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (44:4) {#if $uiElements.depth}
    function create_if_block_19(ctx) {
    	let button;
    	let t_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.depth, "Depth") + "";
    	let t;
    	let mounted;
    	let dispose;

    	return {
    		c() {
    			button = element("button");
    			t = text(t_value);
    		},
    		m(target, anchor) {
    			insert(target, button, anchor);
    			append(button, t);

    			if (!mounted) {
    				dispose = listen(button, "click", /*click_handler_1*/ ctx[9]);
    				mounted = true;
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t_value !== (t_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.depth, "Depth") + "")) set_data(t, t_value);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(button);
    			}

    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (49:4) {#if $uiElements.layout && !$uiElements.hideLayout}
    function create_if_block_18(ctx) {
    	let button;
    	let t_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.styles, "Styles") + "";
    	let t;
    	let mounted;
    	let dispose;

    	return {
    		c() {
    			button = element("button");
    			t = text(t_value);
    		},
    		m(target, anchor) {
    			insert(target, button, anchor);
    			append(button, t);

    			if (!mounted) {
    				dispose = listen(button, "click", /*click_handler_2*/ ctx[10]);
    				mounted = true;
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t_value !== (t_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.styles, "Styles") + "")) set_data(t, t_value);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(button);
    			}

    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (57:4) {#if $uiElements.texture}
    function create_if_block_17(ctx) {
    	let button;
    	let t_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.material, "Material") + "";
    	let t;
    	let mounted;
    	let dispose;

    	return {
    		c() {
    			button = element("button");
    			t = text(t_value);
    		},
    		m(target, anchor) {
    			insert(target, button, anchor);
    			append(button, t);

    			if (!mounted) {
    				dispose = listen(button, "click", /*click_handler_3*/ ctx[11]);
    				mounted = true;
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t_value !== (t_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.material, "Material") + "")) set_data(t, t_value);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(button);
    			}

    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (65:4) {#if $uiElements.height && !$uiElements.hideLayout}
    function create_if_block_16(ctx) {
    	let button;
    	let t_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.height, "Height") + "";
    	let t;
    	let mounted;
    	let dispose;

    	return {
    		c() {
    			button = element("button");
    			t = text(t_value);
    		},
    		m(target, anchor) {
    			insert(target, button, anchor);
    			append(button, t);

    			if (!mounted) {
    				dispose = listen(button, "click", /*click_handler_4*/ ctx[12]);
    				mounted = true;
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t_value !== (t_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.height, "Height") + "")) set_data(t, t_value);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(button);
    			}

    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (73:4) {#if $uiElements.density}
    function create_if_block_15(ctx) {
    	let button;
    	let t_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.density, "Density") + "";
    	let t;
    	let mounted;
    	let dispose;

    	return {
    		c() {
    			button = element("button");
    			t = text(t_value);
    		},
    		m(target, anchor) {
    			insert(target, button, anchor);
    			append(button, t);

    			if (!mounted) {
    				dispose = listen(button, "click", /*click_handler_5*/ ctx[13]);
    				mounted = true;
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t_value !== (t_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.density, "Density") + "")) set_data(t, t_value);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(button);
    			}

    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (81:4) {#if $uiElements.width}
    function create_if_block_14(ctx) {
    	let button;
    	let t_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.width, "Width") + "";
    	let t;
    	let mounted;
    	let dispose;

    	return {
    		c() {
    			button = element("button");
    			t = text(t_value);
    			attr(button, "class", "pr-7");
    		},
    		m(target, anchor) {
    			insert(target, button, anchor);
    			append(button, t);

    			if (!mounted) {
    				dispose = listen(button, "click", /*click_handler_6*/ ctx[14]);
    				mounted = true;
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t_value !== (t_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.width, "Width") + "")) set_data(t, t_value);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(button);
    			}

    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (92:2) {:else}
    function create_else_block_1(ctx) {
    	let t0;
    	let t1;
    	let t2;
    	let t3;
    	let t4;
    	let t5;
    	let if_block6_anchor;
    	let if_block0 = /*$uiElements*/ ctx[2].density && create_if_block_12(ctx);
    	let if_block1 = /*$uiElements*/ ctx[2].height && !/*$uiElements*/ ctx[2].hideLayout && create_if_block_11(ctx);
    	let if_block2 = /*$uiElements*/ ctx[2].layout && !/*$uiElements*/ ctx[2].hideLayout && create_if_block_9(ctx);
    	let if_block3 = /*$uiElements*/ ctx[2].texture && create_if_block_8(ctx);
    	let if_block4 = /*$uiElements*/ ctx[2].depth && create_if_block_7(ctx);
    	let if_block5 = /*$uiElements*/ ctx[2].color && create_if_block_6(ctx);
    	let if_block6 = /*$uiElements*/ ctx[2].width && create_if_block_5(ctx);

    	return {
    		c() {
    			if (if_block0) if_block0.c();
    			t0 = space();
    			if (if_block1) if_block1.c();
    			t1 = space();
    			if (if_block2) if_block2.c();
    			t2 = space();
    			if (if_block3) if_block3.c();
    			t3 = space();
    			if (if_block4) if_block4.c();
    			t4 = space();
    			if (if_block5) if_block5.c();
    			t5 = space();
    			if (if_block6) if_block6.c();
    			if_block6_anchor = empty();
    		},
    		m(target, anchor) {
    			if (if_block0) if_block0.m(target, anchor);
    			insert(target, t0, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert(target, t1, anchor);
    			if (if_block2) if_block2.m(target, anchor);
    			insert(target, t2, anchor);
    			if (if_block3) if_block3.m(target, anchor);
    			insert(target, t3, anchor);
    			if (if_block4) if_block4.m(target, anchor);
    			insert(target, t4, anchor);
    			if (if_block5) if_block5.m(target, anchor);
    			insert(target, t5, anchor);
    			if (if_block6) if_block6.m(target, anchor);
    			insert(target, if_block6_anchor, anchor);
    		},
    		p(ctx, dirty) {
    			if (/*$uiElements*/ ctx[2].density) {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);
    				} else {
    					if_block0 = create_if_block_12(ctx);
    					if_block0.c();
    					if_block0.m(t0.parentNode, t0);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (/*$uiElements*/ ctx[2].height && !/*$uiElements*/ ctx[2].hideLayout) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    				} else {
    					if_block1 = create_if_block_11(ctx);
    					if_block1.c();
    					if_block1.m(t1.parentNode, t1);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}

    			if (/*$uiElements*/ ctx[2].layout && !/*$uiElements*/ ctx[2].hideLayout) {
    				if (if_block2) {
    					if_block2.p(ctx, dirty);
    				} else {
    					if_block2 = create_if_block_9(ctx);
    					if_block2.c();
    					if_block2.m(t2.parentNode, t2);
    				}
    			} else if (if_block2) {
    				if_block2.d(1);
    				if_block2 = null;
    			}

    			if (/*$uiElements*/ ctx[2].texture) {
    				if (if_block3) {
    					if_block3.p(ctx, dirty);
    				} else {
    					if_block3 = create_if_block_8(ctx);
    					if_block3.c();
    					if_block3.m(t3.parentNode, t3);
    				}
    			} else if (if_block3) {
    				if_block3.d(1);
    				if_block3 = null;
    			}

    			if (/*$uiElements*/ ctx[2].depth) {
    				if (if_block4) {
    					if_block4.p(ctx, dirty);
    				} else {
    					if_block4 = create_if_block_7(ctx);
    					if_block4.c();
    					if_block4.m(t4.parentNode, t4);
    				}
    			} else if (if_block4) {
    				if_block4.d(1);
    				if_block4 = null;
    			}

    			if (/*$uiElements*/ ctx[2].color) {
    				if (if_block5) {
    					if_block5.p(ctx, dirty);
    				} else {
    					if_block5 = create_if_block_6(ctx);
    					if_block5.c();
    					if_block5.m(t5.parentNode, t5);
    				}
    			} else if (if_block5) {
    				if_block5.d(1);
    				if_block5 = null;
    			}

    			if (/*$uiElements*/ ctx[2].width) {
    				if (if_block6) {
    					if_block6.p(ctx, dirty);
    				} else {
    					if_block6 = create_if_block_5(ctx);
    					if_block6.c();
    					if_block6.m(if_block6_anchor.parentNode, if_block6_anchor);
    				}
    			} else if (if_block6) {
    				if_block6.d(1);
    				if_block6 = null;
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(t0);
    				detach(t1);
    				detach(t2);
    				detach(t3);
    				detach(t4);
    				detach(t5);
    				detach(if_block6_anchor);
    			}

    			if (if_block0) if_block0.d(detaching);
    			if (if_block1) if_block1.d(detaching);
    			if (if_block2) if_block2.d(detaching);
    			if (if_block3) if_block3.d(detaching);
    			if (if_block4) if_block4.d(detaching);
    			if (if_block5) if_block5.d(detaching);
    			if (if_block6) if_block6.d(detaching);
    		}
    	};
    }

    // (90:2) {#if !$isDataLoaded}
    function create_if_block_4(ctx) {
    	let div;

    	return {
    		c() {
    			div = element("div");
    			attr(div, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    		},
    		p: noop,
    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}
    		}
    	};
    }

    // (93:4) {#if $uiElements.density}
    function create_if_block_12(ctx) {
    	let div6;
    	let p;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.density, "Density") + "";
    	let t0;
    	let t1;
    	let div5;
    	let div1;
    	let t2;
    	let div4;
    	let t5;
    	let input;
    	let input_min_value;
    	let input_max_value;
    	let input_value_value;
    	let input_step_value;

    	return {
    		c() {
    			div6 = element("div");
    			p = element("p");
    			t0 = text(t0_value);
    			t1 = space();
    			div5 = element("div");
    			div1 = element("div");
    			div1.innerHTML = `<div class="range__slider-line density-range-line"></div>`;
    			t2 = space();
    			div4 = element("div");
    			div4.innerHTML = `<div class="range__value"><span class="range__value-number density-range-number">1</span> <div class="arrow-down"></div></div>`;
    			t5 = space();
    			input = element("input");
    			attr(p, "class", "");
    			attr(div1, "class", "range__slider");
    			attr(div4, "class", "range__thumb density-range-thumb");
    			attr(input, "type", "range");
    			attr(input, "class", "range__input border rangeSlider slider density-range sliderRange");
    			attr(input, "min", input_min_value = /*$uiConfig*/ ctx[4].density.minValue);
    			attr(input, "max", input_max_value = /*$uiConfig*/ ctx[4].density.maxValue);
    			input.value = input_value_value = /*$uiConfig*/ ctx[4].density.defaultAttributeValue;
    			attr(input, "step", input_step_value = /*$uiConfig*/ ctx[4].density.stepValue);
    			attr(div5, "class", "range__content w-2/3");
    			attr(div6, "class", "flex justify-between items-center gap-2 slidecontainer text-base w-full my-5");
    			attr(div6, "id", "density-slider");
    			set_style(div6, "display", "none");
    		},
    		m(target, anchor) {
    			insert(target, div6, anchor);
    			append(div6, p);
    			append(p, t0);
    			append(div6, t1);
    			append(div6, div5);
    			append(div5, div1);
    			append(div5, t2);
    			append(div5, div4);
    			append(div5, t5);
    			append(div5, input);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.density, "Density") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig*/ 16 && input_min_value !== (input_min_value = /*$uiConfig*/ ctx[4].density.minValue)) {
    				attr(input, "min", input_min_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_max_value !== (input_max_value = /*$uiConfig*/ ctx[4].density.maxValue)) {
    				attr(input, "max", input_max_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_value_value !== (input_value_value = /*$uiConfig*/ ctx[4].density.defaultAttributeValue)) {
    				input.value = input_value_value;
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_step_value !== (input_step_value = /*$uiConfig*/ ctx[4].density.stepValue)) {
    				attr(input, "step", input_step_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div6);
    			}
    		}
    	};
    }

    // (125:4) {#if $uiElements.height && !$uiElements.hideLayout}
    function create_if_block_11(ctx) {
    	let div6;
    	let p;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.height, "Height") + "";
    	let t0;
    	let t1;
    	let div5;
    	let div1;
    	let t2;
    	let div4;
    	let t5;
    	let input;
    	let input_min_value;
    	let input_max_value;
    	let input_value_value;
    	let input_step_value;

    	return {
    		c() {
    			div6 = element("div");
    			p = element("p");
    			t0 = text(t0_value);
    			t1 = space();
    			div5 = element("div");
    			div1 = element("div");
    			div1.innerHTML = `<div class="range__slider-line height-range-line"></div>`;
    			t2 = space();
    			div4 = element("div");
    			div4.innerHTML = `<div class="range__value"><span class="range__value-number height-range-number">1</span> <div class="arrow-down"></div></div>`;
    			t5 = space();
    			input = element("input");
    			attr(p, "class", "");
    			attr(div1, "class", "range__slider");
    			attr(div4, "class", "range__thumb height-range-thumb");
    			attr(input, "type", "range");
    			attr(input, "class", "range__input border rangeSlider slider height-range sliderRange width-slider");
    			attr(input, "min", input_min_value = /*$uiConfig*/ ctx[4].height.minValue);
    			attr(input, "max", input_max_value = /*$uiConfig*/ ctx[4].height.maxValue);
    			input.value = input_value_value = /*$uiConfig*/ ctx[4].height.defaultAttributeValue;
    			attr(input, "step", input_step_value = /*$uiConfig*/ ctx[4].height.stepValue);
    			attr(div5, "class", "range__content w-2/3");
    			attr(div6, "class", "flex justify-between items-center gap-2 slidecontainer text-base w-full my-5");
    			attr(div6, "id", "height-slider");
    			set_style(div6, "display", "none");
    		},
    		m(target, anchor) {
    			insert(target, div6, anchor);
    			append(div6, p);
    			append(p, t0);
    			append(div6, t1);
    			append(div6, div5);
    			append(div5, div1);
    			append(div5, t2);
    			append(div5, div4);
    			append(div5, t5);
    			append(div5, input);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.height, "Height") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig*/ 16 && input_min_value !== (input_min_value = /*$uiConfig*/ ctx[4].height.minValue)) {
    				attr(input, "min", input_min_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_max_value !== (input_max_value = /*$uiConfig*/ ctx[4].height.maxValue)) {
    				attr(input, "max", input_max_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_value_value !== (input_value_value = /*$uiConfig*/ ctx[4].height.defaultAttributeValue)) {
    				input.value = input_value_value;
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_step_value !== (input_step_value = /*$uiConfig*/ ctx[4].height.stepValue)) {
    				attr(input, "step", input_step_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div6);
    			}
    		}
    	};
    }

    // (157:4) {#if $uiElements.layout && !$uiElements.hideLayout}
    function create_if_block_9(ctx) {
    	let div1;
    	let span;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.styles, "Styles") + "";
    	let t0;
    	let t1;
    	let div0;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_value_3 = ensure_array_like(/*$uiConfig*/ ctx[4].layout.options);
    	const get_key = ctx => /*option*/ ctx[19];

    	for (let i = 0; i < each_value_3.length; i += 1) {
    		let child_ctx = get_each_context_3(ctx, each_value_3, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block_3(key, child_ctx));
    	}

    	return {
    		c() {
    			div1 = element("div");
    			span = element("span");
    			t0 = text(t0_value);
    			t1 = space();
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(span, "class", "text-base");
    			attr(div0, "class", "flex items-center flex-wrap gap-4");
    			attr(div1, "class", "flex items-center justify-between gap-2 style-category w-full");
    			attr(div1, "id", "style-picker");
    			set_style(div1, "display", "none");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, span);
    			append(span, t0);
    			append(div1, t1);
    			append(div1, div0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				if (each_blocks[i]) {
    					each_blocks[i].m(div0, null);
    				}
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.styles, "Styles") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig, CONSTANTS*/ 16) {
    				each_value_3 = ensure_array_like(/*$uiConfig*/ ctx[4].layout.options);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value_3, each_1_lookup, div0, destroy_block, create_each_block_3, null, get_each_context_3);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};
    }

    // (171:12) {#if $uiConfig.layoutIcon.options[index]}
    function create_if_block_10(ctx) {
    	let div;
    	let img;
    	let img_src_value;
    	let t;
    	let div_class_value;
    	let div_data_layout_kind_value;

    	return {
    		c() {
    			div = element("div");
    			img = element("img");
    			t = space();
    			attr(img, "class", "max-w-full max-h-full");
    			if (!src_url_equal(img.src, img_src_value = `${CONSTANTS.s3BaseURL}/${/*$uiConfig*/ ctx[4].layoutIcon.options[/*index*/ ctx[25]]}`)) attr(img, "src", img_src_value);
    			attr(img, "alt", "");

    			attr(div, "class", div_class_value = "layouts p-1.5 border border-[#F2F2F2] cursor-pointer flex items-center justify-center " + (/*option*/ ctx[19] === /*$uiConfig*/ ctx[4].layout.defaultAttributeValue
    			? 'active'
    			: ''));

    			attr(div, "data-layout-kind", div_data_layout_kind_value = /*option*/ ctx[19]);
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, img);
    			append(div, t);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$uiConfig*/ 16 && !src_url_equal(img.src, img_src_value = `${CONSTANTS.s3BaseURL}/${/*$uiConfig*/ ctx[4].layoutIcon.options[/*index*/ ctx[25]]}`)) {
    				attr(img, "src", img_src_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && div_class_value !== (div_class_value = "layouts p-1.5 border border-[#F2F2F2] cursor-pointer flex items-center justify-center " + (/*option*/ ctx[19] === /*$uiConfig*/ ctx[4].layout.defaultAttributeValue
    			? 'active'
    			: ''))) {
    				attr(div, "class", div_class_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && div_data_layout_kind_value !== (div_data_layout_kind_value = /*option*/ ctx[19])) {
    				attr(div, "data-layout-kind", div_data_layout_kind_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}
    		}
    	};
    }

    // (170:10) {#each $uiConfig.layout.options as option, index (option)}
    function create_each_block_3(key_1, ctx) {
    	let first;
    	let if_block_anchor;
    	let if_block = /*$uiConfig*/ ctx[4].layoutIcon.options[/*index*/ ctx[25]] && create_if_block_10(ctx);

    	return {
    		key: key_1,
    		first: null,
    		c() {
    			first = empty();
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    			this.first = first;
    		},
    		m(target, anchor) {
    			insert(target, first, anchor);
    			if (if_block) if_block.m(target, anchor);
    			insert(target, if_block_anchor, anchor);
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (/*$uiConfig*/ ctx[4].layoutIcon.options[/*index*/ ctx[25]]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block_10(ctx);
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(first);
    				detach(if_block_anchor);
    			}

    			if (if_block) if_block.d(detaching);
    		}
    	};
    }

    // (191:4) {#if $uiElements.texture}
    function create_if_block_8(ctx) {
    	let div1;
    	let span;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.material, "Material") + "";
    	let t0;
    	let t1;
    	let div0;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_value_2 = ensure_array_like(/*$uiConfig*/ ctx[4].texture.options);
    	const get_key = ctx => /*option*/ ctx[19];

    	for (let i = 0; i < each_value_2.length; i += 1) {
    		let child_ctx = get_each_context_2$1(ctx, each_value_2, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block_2$1(key, child_ctx));
    	}

    	return {
    		c() {
    			div1 = element("div");
    			span = element("span");
    			t0 = text(t0_value);
    			t1 = space();
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(span, "class", "text-base");
    			attr(div0, "class", "flex items-center flex-wrap gap-4");
    			attr(div1, "class", "flex items-center justify-between gap-2 style-category w-full");
    			attr(div1, "id", "material-picker");
    			set_style(div1, "display", "none");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, span);
    			append(span, t0);
    			append(div1, t1);
    			append(div1, div0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				if (each_blocks[i]) {
    					each_blocks[i].m(div0, null);
    				}
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.material, "Material") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig, CONSTANTS*/ 16) {
    				each_value_2 = ensure_array_like(/*$uiConfig*/ ctx[4].texture.options);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value_2, each_1_lookup, div0, destroy_block, create_each_block_2$1, null, get_each_context_2$1);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};
    }

    // (204:10) {#each $uiConfig.texture.options as option, index (option)}
    function create_each_block_2$1(key_1, ctx) {
    	let div;
    	let img;
    	let img_src_value;
    	let t;
    	let div_class_value;

    	return {
    		key: key_1,
    		first: null,
    		c() {
    			div = element("div");
    			img = element("img");
    			t = space();
    			attr(img, "class", "max-w-full max-h-full");
    			if (!src_url_equal(img.src, img_src_value = `${CONSTANTS.s3BaseURL}/${/*$uiConfig*/ ctx[4].texture.options[/*index*/ ctx[25]]}`)) attr(img, "src", img_src_value);
    			attr(img, "alt", "");

    			attr(div, "class", div_class_value = "materials p-1.5 border border-[#F2F2F2] cursor-pointer flex items-center justify-center " + (/*option*/ ctx[19] === /*$uiConfig*/ ctx[4].texture.defaultAttributeValue
    			? 'active'
    			: ''));

    			this.first = div;
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, img);
    			append(div, t);
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (dirty & /*$uiConfig*/ 16 && !src_url_equal(img.src, img_src_value = `${CONSTANTS.s3BaseURL}/${/*$uiConfig*/ ctx[4].texture.options[/*index*/ ctx[25]]}`)) {
    				attr(img, "src", img_src_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && div_class_value !== (div_class_value = "materials p-1.5 border border-[#F2F2F2] cursor-pointer flex items-center justify-center " + (/*option*/ ctx[19] === /*$uiConfig*/ ctx[4].texture.defaultAttributeValue
    			? 'active'
    			: ''))) {
    				attr(div, "class", div_class_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}
    		}
    	};
    }

    // (222:4) {#if $uiElements.depth}
    function create_if_block_7(ctx) {
    	let div1;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.depth, "Depth") + "";
    	let t0;
    	let t1;
    	let div0;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_value_1 = ensure_array_like(/*$uiConfig*/ ctx[4].depth.options);
    	const get_key = ctx => /*option*/ ctx[19];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		let child_ctx = get_each_context_1$1(ctx, each_value_1, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block_1$1(key, child_ctx));
    	}

    	return {
    		c() {
    			div1 = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div0, "class", "options flex items-center justify-end gap-3 flex-wrap text-base");
    			attr(div1, "class", "flex items-center justify-between gap-2 text-base w-full");
    			attr(div1, "id", "depth-picker");
    			set_style(div1, "display", "none");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, t0);
    			append(div1, t1);
    			append(div1, div0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				if (each_blocks[i]) {
    					each_blocks[i].m(div0, null);
    				}
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.depth, "Depth") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig, handleButtonSelection, root*/ 17) {
    				each_value_1 = ensure_array_like(/*$uiConfig*/ ctx[4].depth.options);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value_1, each_1_lookup, div0, destroy_block, create_each_block_1$1, null, get_each_context_1$1);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};
    }

    // (232:10) {#each $uiConfig.depth.options as option (option)}
    function create_each_block_1$1(key_1, ctx) {
    	let button;
    	let t0_value = /*option*/ ctx[19] + "";
    	let t0;
    	let t1;
    	let button_class_value;
    	let button_value_value;
    	let mounted;
    	let dispose;

    	return {
    		key: key_1,
    		first: null,
    		c() {
    			button = element("button");
    			t0 = text(t0_value);
    			t1 = text(" cm\n            ");

    			attr(button, "class", button_class_value = "border depth-button density-button cursor-pointer rounded-[25px] px-2 py-0.5 " + (/*option*/ ctx[19] === /*$uiConfig*/ ctx[4].depth.defaultAttributeValue
    			? 'selected'
    			: ''));

    			button.value = button_value_value = /*option*/ ctx[19];
    			this.first = button;
    		},
    		m(target, anchor) {
    			insert(target, button, anchor);
    			append(button, t0);
    			append(button, t1);

    			if (!mounted) {
    				dispose = listen(button, "click", /*click_handler_7*/ ctx[15]);
    				mounted = true;
    			}
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty & /*$uiConfig*/ 16 && t0_value !== (t0_value = /*option*/ ctx[19] + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig*/ 16 && button_class_value !== (button_class_value = "border depth-button density-button cursor-pointer rounded-[25px] px-2 py-0.5 " + (/*option*/ ctx[19] === /*$uiConfig*/ ctx[4].depth.defaultAttributeValue
    			? 'selected'
    			: ''))) {
    				attr(button, "class", button_class_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && button_value_value !== (button_value_value = /*option*/ ctx[19])) {
    				button.value = button_value_value;
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(button);
    			}

    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (254:4) {#if $uiElements.color}
    function create_if_block_6(ctx) {
    	let div1;
    	let span;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.color, "Color") + "";
    	let t0;
    	let t1;
    	let div0;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_value = ensure_array_like(/*$uiConfig*/ ctx[4].color.options);
    	const get_key = ctx => /*option*/ ctx[19];

    	for (let i = 0; i < each_value.length; i += 1) {
    		let child_ctx = get_each_context$1(ctx, each_value, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block$1(key, child_ctx));
    	}

    	return {
    		c() {
    			div1 = element("div");
    			span = element("span");
    			t0 = text(t0_value);
    			t1 = space();
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div0, "class", "flex items-center gap-3 color-variants flex-wrap");
    			attr(div1, "class", "flex items-center justify-between gap-12 color-variants w-full");
    			attr(div1, "id", "color-picker");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, span);
    			append(span, t0);
    			append(div1, t1);
    			append(div1, div0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				if (each_blocks[i]) {
    					each_blocks[i].m(div0, null);
    				}
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.color, "Color") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig*/ 16) {
    				each_value = ensure_array_like(/*$uiConfig*/ ctx[4].color.options);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value, each_1_lookup, div0, destroy_block, create_each_block$1, null, get_each_context$1);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};
    }

    // (263:10) {#each $uiConfig.color.options as option (option)}
    function create_each_block$1(key_1, ctx) {
    	let div1;
    	let div0;
    	let t;
    	let div1_class_value;
    	let div1_data_color_value;

    	return {
    		key: key_1,
    		first: null,
    		c() {
    			div1 = element("div");
    			div0 = element("div");
    			t = space();
    			attr(div0, "class", "rounded-full h-6 w-6");
    			set_style(div0, "background-color", /*option*/ ctx[19]);

    			attr(div1, "class", div1_class_value = "variants rounded-full p-1.5 border border-[#F2F2F2] cursor-pointer " + (/*option*/ ctx[19] === /*$uiConfig*/ ctx[4].color.defaultAttributeValue
    			? 'selectedColor'
    			: ''));

    			attr(div1, "data-color", div1_data_color_value = /*option*/ ctx[19]);
    			this.first = div1;
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, div0);
    			append(div1, t);
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (dirty & /*$uiConfig*/ 16) {
    				set_style(div0, "background-color", /*option*/ ctx[19]);
    			}

    			if (dirty & /*$uiConfig*/ 16 && div1_class_value !== (div1_class_value = "variants rounded-full p-1.5 border border-[#F2F2F2] cursor-pointer " + (/*option*/ ctx[19] === /*$uiConfig*/ ctx[4].color.defaultAttributeValue
    			? 'selectedColor'
    			: ''))) {
    				attr(div1, "class", div1_class_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && div1_data_color_value !== (div1_data_color_value = /*option*/ ctx[19])) {
    				attr(div1, "data-color", div1_data_color_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}
    		}
    	};
    }

    // (281:4) {#if $uiElements.width}
    function create_if_block_5(ctx) {
    	let div6;
    	let p;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.width, "Width") + "";
    	let t0;
    	let t1;
    	let div5;
    	let div1;
    	let t2;
    	let div4;
    	let t5;
    	let input;
    	let input_min_value;
    	let input_max_value;
    	let input_value_value;
    	let input_step_value;

    	return {
    		c() {
    			div6 = element("div");
    			p = element("p");
    			t0 = text(t0_value);
    			t1 = space();
    			div5 = element("div");
    			div1 = element("div");
    			div1.innerHTML = `<div class="range__slider-line width-range-line"></div>`;
    			t2 = space();
    			div4 = element("div");
    			div4.innerHTML = `<div class="range__value"><span class="range__value-number width-range-number">1</span> <div class="arrow-down"></div></div>`;
    			t5 = space();
    			input = element("input");
    			attr(p, "class", "");
    			attr(div1, "class", "range__slider");
    			attr(div4, "class", "range__thumb width-range-thumb");
    			attr(input, "type", "range");
    			attr(input, "class", "range__input border slider rangeSlider width-range sliderRange width-slider");
    			attr(input, "min", input_min_value = /*$uiConfig*/ ctx[4].width.minValue);
    			attr(input, "max", input_max_value = /*$uiConfig*/ ctx[4].width.maxValue);
    			input.value = input_value_value = /*$uiConfig*/ ctx[4].width.defaultAttributeValue;
    			attr(input, "step", input_step_value = /*$uiConfig*/ ctx[4].width.stepValue);
    			attr(div5, "class", "range__content w-2/3");
    			attr(div6, "id", "width-slider");
    			attr(div6, "class", "flex justify-between items-center gap-2 slidecontainer text-base w-full my-5");
    			set_style(div6, "display", "none");
    		},
    		m(target, anchor) {
    			insert(target, div6, anchor);
    			append(div6, p);
    			append(p, t0);
    			append(div6, t1);
    			append(div6, div5);
    			append(div5, div1);
    			append(div5, t2);
    			append(div5, div4);
    			append(div5, t5);
    			append(div5, input);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.width, "Width") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig*/ 16 && input_min_value !== (input_min_value = /*$uiConfig*/ ctx[4].width.minValue)) {
    				attr(input, "min", input_min_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_max_value !== (input_max_value = /*$uiConfig*/ ctx[4].width.maxValue)) {
    				attr(input, "max", input_max_value);
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_value_value !== (input_value_value = /*$uiConfig*/ ctx[4].width.defaultAttributeValue)) {
    				input.value = input_value_value;
    			}

    			if (dirty & /*$uiConfig*/ 16 && input_step_value !== (input_step_value = /*$uiConfig*/ ctx[4].width.stepValue)) {
    				attr(input, "step", input_step_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div6);
    			}
    		}
    	};
    }

    // (323:2) {:else}
    function create_else_block(ctx) {
    	let span0;
    	let t0_value = /*$productInfo*/ ctx[5].catalog.name + "";
    	let t0;
    	let t1;
    	let span1;
    	let t2_value = (formatProductPriceForUI(/*$productPrice*/ ctx[6]) || "") + "";
    	let t2;
    	let t3;
    	let div0;
    	let img;
    	let img_src_value;
    	let t4;
    	let span2;
    	let t5_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.tax, "Taxes included") + "";
    	let t5;
    	let t6;
    	let div1;
    	let t7;
    	let t8;
    	let if_block0 = /*$uiElements*/ ctx[2].showDimensions && create_if_block_3();
    	let if_block1 = /*$uiElements*/ ctx[2].hideItems && create_if_block_2$1();
    	let if_block2 = /*$uiElements*/ ctx[2].backpanel && create_if_block_1$1(ctx);

    	return {
    		c() {
    			span0 = element("span");
    			t0 = text(t0_value);
    			t1 = space();
    			span1 = element("span");
    			t2 = text(t2_value);
    			t3 = space();
    			div0 = element("div");
    			img = element("img");
    			t4 = space();
    			span2 = element("span");
    			t5 = text(t5_value);
    			t6 = space();
    			div1 = element("div");
    			if (if_block0) if_block0.c();
    			t7 = space();
    			if (if_block1) if_block1.c();
    			t8 = space();
    			if (if_block2) if_block2.c();
    			attr(span0, "class", "font-medium");
    			attr(span1, "class", "text-2xl font-medium");
    			if (!src_url_equal(img.src, img_src_value = checkIcon)) attr(img, "src", img_src_value);
    			attr(img, "alt", "");
    			attr(span2, "class", "text-sm text-[#65656C]");
    			attr(div0, "class", "flex gap-1.5");
    			attr(div1, "class", "flex flex-col md:flex-row gap-4 md:gap-8 xl:gap-2 text-base");
    		},
    		m(target, anchor) {
    			insert(target, span0, anchor);
    			append(span0, t0);
    			insert(target, t1, anchor);
    			insert(target, span1, anchor);
    			append(span1, t2);
    			insert(target, t3, anchor);
    			insert(target, div0, anchor);
    			append(div0, img);
    			append(div0, t4);
    			append(div0, span2);
    			append(span2, t5);
    			insert(target, t6, anchor);
    			insert(target, div1, anchor);
    			if (if_block0) if_block0.m(div1, null);
    			append(div1, t7);
    			if (if_block1) if_block1.m(div1, null);
    			append(div1, t8);
    			if (if_block2) if_block2.m(div1, null);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$productInfo*/ 32 && t0_value !== (t0_value = /*$productInfo*/ ctx[5].catalog.name + "")) set_data(t0, t0_value);
    			if (dirty & /*$productPrice*/ 64 && t2_value !== (t2_value = (formatProductPriceForUI(/*$productPrice*/ ctx[6]) || "") + "")) set_data(t2, t2_value);
    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t5_value !== (t5_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.tax, "Taxes included") + "")) set_data(t5, t5_value);

    			if (/*$uiElements*/ ctx[2].showDimensions) {
    				if (if_block0) ; else {
    					if_block0 = create_if_block_3();
    					if_block0.c();
    					if_block0.m(div1, t7);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (/*$uiElements*/ ctx[2].hideItems) {
    				if (if_block1) ; else {
    					if_block1 = create_if_block_2$1();
    					if_block1.c();
    					if_block1.m(div1, t8);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}

    			if (/*$uiElements*/ ctx[2].backpanel) {
    				if (if_block2) {
    					if_block2.p(ctx, dirty);
    				} else {
    					if_block2 = create_if_block_1$1(ctx);
    					if_block2.c();
    					if_block2.m(div1, null);
    				}
    			} else if (if_block2) {
    				if_block2.d(1);
    				if_block2 = null;
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(span0);
    				detach(t1);
    				detach(span1);
    				detach(t3);
    				detach(div0);
    				detach(t6);
    				detach(div1);
    			}

    			if (if_block0) if_block0.d();
    			if (if_block1) if_block1.d();
    			if (if_block2) if_block2.d();
    		}
    	};
    }

    // (318:2) {#if !$isDataLoaded}
    function create_if_block$1(ctx) {
    	let div0;
    	let t0;
    	let div1;
    	let t1;
    	let div2;
    	let t2;
    	let div3;

    	return {
    		c() {
    			div0 = element("div");
    			t0 = space();
    			div1 = element("div");
    			t1 = space();
    			div2 = element("div");
    			t2 = space();
    			div3 = element("div");
    			attr(div0, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div1, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div2, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    			attr(div3, "class", "w-full h-7 mx-2 bg-gray-300 animate-pulse rounded");
    		},
    		m(target, anchor) {
    			insert(target, div0, anchor);
    			insert(target, t0, anchor);
    			insert(target, div1, anchor);
    			insert(target, t1, anchor);
    			insert(target, div2, anchor);
    			insert(target, t2, anchor);
    			insert(target, div3, anchor);
    		},
    		p: noop,
    		d(detaching) {
    			if (detaching) {
    				detach(div0);
    				detach(t0);
    				detach(div1);
    				detach(t1);
    				detach(div2);
    				detach(t2);
    				detach(div3);
    			}
    		}
    	};
    }

    // (338:6) {#if $uiElements.showDimensions}
    function create_if_block_3(ctx) {
    	let div1;

    	return {
    		c() {
    			div1 = element("div");
    			div1.innerHTML = `<div class="checkbox-wrapper-48"><label><input type="checkbox" id="toggle-dimensions-checkbox" name="cb" class="cursor-pointer toggle-dimensions-checkbox"/></label></div> <p class="">Show dimensions</p>`;
    			attr(div1, "class", "flex items-center gap-3");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}
    		}
    	};
    }

    // (354:6) {#if $uiElements.hideItems}
    function create_if_block_2$1(ctx) {
    	let div1;

    	return {
    		c() {
    			div1 = element("div");
    			div1.innerHTML = `<div class="checkbox-wrapper-48"><label><input type="checkbox" checked="" name="cb" class="cursor-pointer hide-itme-checkbox"/></label></div> <p class="">Hide items</p>`;
    			attr(div1, "class", "flex items-center gap-3");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}
    		}
    	};
    }

    // (370:6) {#if $uiElements.backpanel}
    function create_if_block_1$1(ctx) {
    	let div1;
    	let div0;
    	let label;
    	let input;
    	let t0;
    	let p;
    	let t1_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.backpanel, "Back panels") + "";
    	let t1;
    	let mounted;
    	let dispose;

    	return {
    		c() {
    			div1 = element("div");
    			div0 = element("div");
    			label = element("label");
    			input = element("input");
    			t0 = space();
    			p = element("p");
    			t1 = text(t1_value);
    			attr(input, "type", "checkbox");
    			attr(input, "name", "cb");
    			attr(input, "class", "cursor-pointer back-panels-checkbox");
    			attr(div0, "class", "checkbox-wrapper-48");
    			attr(p, "class", "");
    			attr(div1, "class", "flex items-center gap-3");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, div0);
    			append(div0, label);
    			append(label, input);
    			input.checked = /*$uiConfig*/ ctx[4].backpanel.defaultAttributeValue;
    			append(div1, t0);
    			append(div1, p);
    			append(p, t1);

    			if (!mounted) {
    				dispose = listen(input, "change", /*input_change_handler*/ ctx[17]);
    				mounted = true;
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$uiConfig*/ 16) {
    				input.checked = /*$uiConfig*/ ctx[4].backpanel.defaultAttributeValue;
    			}

    			if (dirty & /*$getLocalizedOrDefaultText*/ 8 && t1_value !== (t1_value = /*$getLocalizedOrDefaultText*/ ctx[3](translationKeyKind.backpanel, "Back panels") + "")) set_data(t1, t1_value);
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div1);
    			}

    			mounted = false;
    			dispose();
    		}
    	};
    }

    function create_fragment$2(ctx) {
    	let div0;
    	let t0;
    	let div1;
    	let t1;
    	let div2;
    	let t2;
    	let div3;

    	function select_block_type(ctx, dirty) {
    		if (!/*$isDataLoaded*/ ctx[1]) return create_if_block_13;
    		return create_else_block_2;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block0 = current_block_type(ctx);

    	function select_block_type_1(ctx, dirty) {
    		if (!/*$isDataLoaded*/ ctx[1]) return create_if_block_4;
    		return create_else_block_1;
    	}

    	let current_block_type_1 = select_block_type_1(ctx);
    	let if_block1 = current_block_type_1(ctx);

    	function select_block_type_2(ctx, dirty) {
    		if (!/*$isDataLoaded*/ ctx[1]) return create_if_block$1;
    		return create_else_block;
    	}

    	let current_block_type_2 = select_block_type_2(ctx);
    	let if_block2 = current_block_type_2(ctx);

    	return {
    		c() {
    			div0 = element("div");
    			if_block0.c();
    			t0 = space();
    			div1 = element("div");
    			if_block1.c();
    			t1 = space();
    			div2 = element("div");
    			if_block2.c();
    			t2 = space();
    			div3 = element("div");
    			attr(div0, "class", "flex sm:hidden items-center gap-8 text-base overflow-x-scroll no-scrollbar text-[#8C8C97] buttonContainer");
    			attr(div1, "class", "flex sm:hidden mx-7 border px-2 py-5 rounded-lg");
    			attr(div2, "class", "flex flex-col xl:hidden md:items-center gap-5 md:gap-2 text-base px-7 md:px-0 w-full");
    			attr(div3, "class", "flex flex-col md:flex-row xl:flex-col text-sm gap-5 md:gap-4 xl:gap-12 xl:w-40 px-7 md:px-0");
    		},
    		m(target, anchor) {
    			insert(target, div0, anchor);
    			if_block0.m(div0, null);
    			insert(target, t0, anchor);
    			insert(target, div1, anchor);
    			if_block1.m(div1, null);
    			/*div1_binding*/ ctx[16](div1);
    			insert(target, t1, anchor);
    			insert(target, div2, anchor);
    			if_block2.m(div2, null);
    			insert(target, t2, anchor);
    			insert(target, div3, anchor);
    		},
    		p(ctx, [dirty]) {
    			if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block0) {
    				if_block0.p(ctx, dirty);
    			} else {
    				if_block0.d(1);
    				if_block0 = current_block_type(ctx);

    				if (if_block0) {
    					if_block0.c();
    					if_block0.m(div0, null);
    				}
    			}

    			if (current_block_type_1 === (current_block_type_1 = select_block_type_1(ctx)) && if_block1) {
    				if_block1.p(ctx, dirty);
    			} else {
    				if_block1.d(1);
    				if_block1 = current_block_type_1(ctx);

    				if (if_block1) {
    					if_block1.c();
    					if_block1.m(div1, null);
    				}
    			}

    			if (current_block_type_2 === (current_block_type_2 = select_block_type_2(ctx)) && if_block2) {
    				if_block2.p(ctx, dirty);
    			} else {
    				if_block2.d(1);
    				if_block2 = current_block_type_2(ctx);

    				if (if_block2) {
    					if_block2.c();
    					if_block2.m(div2, null);
    				}
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (detaching) {
    				detach(div0);
    				detach(t0);
    				detach(div1);
    				detach(t1);
    				detach(div2);
    				detach(t2);
    				detach(div3);
    			}

    			if_block0.d();
    			if_block1.d();
    			/*div1_binding*/ ctx[16](null);
    			if_block2.d();
    		}
    	};
    }

    function instance$2($$self, $$props, $$invalidate) {
    	let $isDataLoaded;
    	let $uiElements;
    	let $getLocalizedOrDefaultText;
    	let $uiConfig;
    	let $productInfo;
    	let $productPrice;
    	component_subscribe($$self, isDataLoaded, $$value => $$invalidate(1, $isDataLoaded = $$value));
    	component_subscribe($$self, uiElements, $$value => $$invalidate(2, $uiElements = $$value));
    	component_subscribe($$self, getLocalizedOrDefaultText, $$value => $$invalidate(3, $getLocalizedOrDefaultText = $$value));
    	component_subscribe($$self, uiConfig, $$value => $$invalidate(4, $uiConfig = $$value));
    	component_subscribe($$self, productInfo, $$value => $$invalidate(5, $productInfo = $$value));
    	component_subscribe($$self, productPrice, $$value => $$invalidate(6, $productPrice = $$value));
    	let { root } = $$props;

    	function hideAllElements() {
    		const selectors = [
    			"#width-slider",
    			"#height-slider",
    			"#color-picker",
    			"#depth-picker",
    			"#style-picker",
    			"#density-slider",
    			"#material-picker"
    		];

    		selectors.forEach(selector => {
    			const element = root.querySelector(selector);

    			if (element) {
    				element.style.display = "none";
    			}
    		});
    	}

    	function showElement(selector) {
    		hideAllElements();
    		const targetElement = root.querySelector(selector);

    		if (targetElement) {
    			targetElement.style.display = "flex";
    		}
    	}

    	const click_handler = () => showElement("#color-picker");
    	const click_handler_1 = () => showElement("#depth-picker");
    	const click_handler_2 = () => showElement("#style-picker");
    	const click_handler_3 = () => showElement("#material-picker");
    	const click_handler_4 = () => showElement("#height-slider");
    	const click_handler_5 = () => showElement("#density-slider");
    	const click_handler_6 = () => showElement("#width-slider");
    	const click_handler_7 = event => handleButtonSelection(root, event.target, ".options button", "selected");

    	function div1_binding($$value) {
    		binding_callbacks[$$value ? 'unshift' : 'push'](() => {
    			root = $$value;
    			$$invalidate(0, root);
    		});
    	}

    	function input_change_handler() {
    		$uiConfig.backpanel.defaultAttributeValue = this.checked;
    		uiConfig.set($uiConfig);
    	}

    	$$self.$$set = $$props => {
    		if ('root' in $$props) $$invalidate(0, root = $$props.root);
    	};

    	return [
    		root,
    		$isDataLoaded,
    		$uiElements,
    		$getLocalizedOrDefaultText,
    		$uiConfig,
    		$productInfo,
    		$productPrice,
    		showElement,
    		click_handler,
    		click_handler_1,
    		click_handler_2,
    		click_handler_3,
    		click_handler_4,
    		click_handler_5,
    		click_handler_6,
    		click_handler_7,
    		div1_binding,
    		input_change_handler
    	];
    }

    class MobileScreen extends SvelteComponent {
    	constructor(options) {
    		super();
    		init$1(this, options, instance$2, create_fragment$2, safe_not_equal, { root: 0 });
    	}

    	get root() {
    		return this.$$.ctx[0];
    	}

    	set root(root) {
    		this.$$set({ root });
    		flush();
    	}
    }

    create_custom_element(MobileScreen, {"root":{}}, [], [], true);

    /* src/lib/components/PopupModal.svelte generated by Svelte v4.0.3 */

    function get_each_context(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[6] = list[i];
    	return child_ctx;
    }

    function get_each_context_1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[9] = list[i];
    	return child_ctx;
    }

    function get_each_context_2(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[9] = list[i];
    	return child_ctx;
    }

    // (13:2) {#if $uiElements.popupOptions && $currentAtomGroup}
    function create_if_block(ctx) {
    	let div;
    	let img;
    	let img_src_value;
    	let t;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_1_anchor;
    	let each_value = ensure_array_like(/*$uiConfig*/ ctx[3].popupOptions[/*$currentAtomGroup*/ ctx[1]]);
    	const get_key = ctx => /*popupOption*/ ctx[6];

    	for (let i = 0; i < each_value.length; i += 1) {
    		let child_ctx = get_each_context(ctx, each_value, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block(key, child_ctx));
    	}

    	return {
    		c() {
    			div = element("div");
    			img = element("img");
    			t = space();

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			each_1_anchor = empty();
    			attr(img, "class", "popup-options max-w-full max-h-full");
    			if (!src_url_equal(img.src, img_src_value = /*$currentUIConfig*/ ctx[2].popupOptions.value[/*$currentAtomGroup*/ ctx[1]]?.url || "")) attr(img, "src", img_src_value);
    			attr(img, "alt", "");
    			attr(div, "class", "p-1.5 cursor-pointer flex items-center justify-center");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, img);
    			insert(target, t, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				if (each_blocks[i]) {
    					each_blocks[i].m(target, anchor);
    				}
    			}

    			insert(target, each_1_anchor, anchor);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$currentUIConfig, $currentAtomGroup*/ 6 && !src_url_equal(img.src, img_src_value = /*$currentUIConfig*/ ctx[2].popupOptions.value[/*$currentAtomGroup*/ ctx[1]]?.url || "")) {
    				attr(img, "src", img_src_value);
    			}

    			if (dirty & /*$uiConfig, $currentAtomGroup, $currentUIConfig, $getLocalizedOrDefaultText, translationKeyKind, console*/ 30) {
    				each_value = ensure_array_like(/*$uiConfig*/ ctx[3].popupOptions[/*$currentAtomGroup*/ ctx[1]]);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value, each_1_lookup, each_1_anchor.parentNode, destroy_block, create_each_block, each_1_anchor, get_each_context);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div);
    				detach(t);
    				detach(each_1_anchor);
    			}

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d(detaching);
    			}
    		}
    	};
    }

    // (23:6) {#if popupOption.attribute.name == "rowHeight"}
    function create_if_block_2(ctx) {
    	let div2;
    	let div0;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[4](translationKeyKind.rowHeight, "Row Height") + "";
    	let t0;
    	let t1;
    	let div1;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_value_2 = ensure_array_like(/*popupOption*/ ctx[6].option.value);
    	const get_key = ctx => /*option*/ ctx[9];

    	for (let i = 0; i < each_value_2.length; i += 1) {
    		let child_ctx = get_each_context_2(ctx, each_value_2, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block_2(key, child_ctx));
    	}

    	return {
    		c() {
    			div2 = element("div");
    			div0 = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			div1 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div0, "class", "popup-text text-base");
    			attr(div1, "class", "options flex items-center gap-3 text-base");
    			attr(div2, "class", "flex flex-col space-y-2");
    		},
    		m(target, anchor) {
    			insert(target, div2, anchor);
    			append(div2, div0);
    			append(div0, t0);
    			append(div2, t1);
    			append(div2, div1);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				if (each_blocks[i]) {
    					each_blocks[i].m(div1, null);
    				}
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 16 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[4](translationKeyKind.rowHeight, "Row Height") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig, $currentAtomGroup, $currentUIConfig, console*/ 14) {
    				each_value_2 = ensure_array_like(/*popupOption*/ ctx[6].option.value);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value_2, each_1_lookup, div1, destroy_block, create_each_block_2, null, get_each_context_2);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div2);
    			}

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};
    }

    // (32:12) {#each popupOption.option.value as option (option)}
    function create_each_block_2(key_1, ctx) {
    	let button;
    	let t0_value = /*option*/ ctx[9].value + "";
    	let t0;
    	let t1;
    	let button_class_value;
    	let button_value_value;
    	let mounted;
    	let dispose;

    	function load_handler() {
    		return /*load_handler*/ ctx[5](/*popupOption*/ ctx[6]);
    	}

    	return {
    		key: key_1,
    		first: null,
    		c() {
    			button = element("button");
    			t0 = text(t0_value);
    			t1 = text(" cm\n              ");

    			attr(button, "class", button_class_value = "row-height-button cursor-pointer rounded-[25px] p-1.5 border " + (/*option*/ ctx[9].value === /*$currentUIConfig*/ ctx[2].popupOptions.value[/*$currentAtomGroup*/ ctx[1]].rowHeight
    			? 'active'
    			: ''));

    			button.value = button_value_value = /*option*/ ctx[9].value;
    			this.first = button;
    		},
    		m(target, anchor) {
    			insert(target, button, anchor);
    			append(button, t0);
    			append(button, t1);

    			if (!mounted) {
    				dispose = listen(button, "load", load_handler);
    				mounted = true;
    			}
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty & /*$uiConfig, $currentAtomGroup*/ 10 && t0_value !== (t0_value = /*option*/ ctx[9].value + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig, $currentAtomGroup, $currentUIConfig*/ 14 && button_class_value !== (button_class_value = "row-height-button cursor-pointer rounded-[25px] p-1.5 border " + (/*option*/ ctx[9].value === /*$currentUIConfig*/ ctx[2].popupOptions.value[/*$currentAtomGroup*/ ctx[1]].rowHeight
    			? 'active'
    			: ''))) {
    				attr(button, "class", button_class_value);
    			}

    			if (dirty & /*$uiConfig, $currentAtomGroup*/ 10 && button_value_value !== (button_value_value = /*option*/ ctx[9].value)) {
    				button.value = button_value_value;
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(button);
    			}

    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (50:6) {#if popupOption.attribute.name == "drawers"}
    function create_if_block_1(ctx) {
    	let div2;
    	let div0;
    	let t0_value = /*$getLocalizedOrDefaultText*/ ctx[4](translationKeyKind.drawers, "Drawers") + "";
    	let t0;
    	let t1;
    	let div1;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let t2;
    	let each_value_1 = ensure_array_like(/*popupOption*/ ctx[6].option.value);
    	const get_key = ctx => /*option*/ ctx[9];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		let child_ctx = get_each_context_1(ctx, each_value_1, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block_1(key, child_ctx));
    	}

    	return {
    		c() {
    			div2 = element("div");
    			div0 = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			div1 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t2 = space();
    			attr(div0, "class", "popup-text text-base");
    			attr(div1, "class", "options flex items-center gap-3 text-base");
    			attr(div2, "class", "flex flex-col space-y-2");
    		},
    		m(target, anchor) {
    			insert(target, div2, anchor);
    			append(div2, div0);
    			append(div0, t0);
    			append(div2, t1);
    			append(div2, div1);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				if (each_blocks[i]) {
    					each_blocks[i].m(div1, null);
    				}
    			}

    			append(div2, t2);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*$getLocalizedOrDefaultText*/ 16 && t0_value !== (t0_value = /*$getLocalizedOrDefaultText*/ ctx[4](translationKeyKind.drawers, "Drawers") + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig, $currentAtomGroup, $currentUIConfig*/ 14) {
    				each_value_1 = ensure_array_like(/*popupOption*/ ctx[6].option.value);
    				each_blocks = update_keyed_each(each_blocks, dirty, get_key, 1, ctx, each_value_1, each_1_lookup, div1, destroy_block, create_each_block_1, null, get_each_context_1);
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div2);
    			}

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};
    }

    // (56:12) {#each popupOption.option.value as option (option)}
    function create_each_block_1(key_1, ctx) {
    	let button;
    	let t0_value = /*option*/ ctx[9].value + "";
    	let t0;
    	let t1;
    	let button_class_value;
    	let button_value_value;

    	return {
    		key: key_1,
    		first: null,
    		c() {
    			button = element("button");
    			t0 = text(t0_value);
    			t1 = space();

    			attr(button, "class", button_class_value = "drawers-button cursor-pointer rounded-[25px] p-1.5 border capitalize " + (/*option*/ ctx[9].value === /*$currentUIConfig*/ ctx[2].popupOptions.value[/*$currentAtomGroup*/ ctx[1]].drawers
    			? 'active'
    			: ''));

    			button.value = button_value_value = /*option*/ ctx[9].value;
    			this.first = button;
    		},
    		m(target, anchor) {
    			insert(target, button, anchor);
    			append(button, t0);
    			append(button, t1);
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty & /*$uiConfig, $currentAtomGroup*/ 10 && t0_value !== (t0_value = /*option*/ ctx[9].value + "")) set_data(t0, t0_value);

    			if (dirty & /*$uiConfig, $currentAtomGroup, $currentUIConfig*/ 14 && button_class_value !== (button_class_value = "drawers-button cursor-pointer rounded-[25px] p-1.5 border capitalize " + (/*option*/ ctx[9].value === /*$currentUIConfig*/ ctx[2].popupOptions.value[/*$currentAtomGroup*/ ctx[1]].drawers
    			? 'active'
    			: ''))) {
    				attr(button, "class", button_class_value);
    			}

    			if (dirty & /*$uiConfig, $currentAtomGroup*/ 10 && button_value_value !== (button_value_value = /*option*/ ctx[9].value)) {
    				button.value = button_value_value;
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(button);
    			}
    		}
    	};
    }

    // (22:4) {#each $uiConfig.popupOptions[$currentAtomGroup] as popupOption (popupOption)}
    function create_each_block(key_1, ctx) {
    	let first;
    	let t;
    	let if_block1_anchor;
    	let if_block0 = /*popupOption*/ ctx[6].attribute.name == "rowHeight" && create_if_block_2(ctx);
    	let if_block1 = /*popupOption*/ ctx[6].attribute.name == "drawers" && create_if_block_1(ctx);

    	return {
    		key: key_1,
    		first: null,
    		c() {
    			first = empty();
    			if (if_block0) if_block0.c();
    			t = space();
    			if (if_block1) if_block1.c();
    			if_block1_anchor = empty();
    			this.first = first;
    		},
    		m(target, anchor) {
    			insert(target, first, anchor);
    			if (if_block0) if_block0.m(target, anchor);
    			insert(target, t, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert(target, if_block1_anchor, anchor);
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (/*popupOption*/ ctx[6].attribute.name == "rowHeight") {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);
    				} else {
    					if_block0 = create_if_block_2(ctx);
    					if_block0.c();
    					if_block0.m(t.parentNode, t);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (/*popupOption*/ ctx[6].attribute.name == "drawers") {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    				} else {
    					if_block1 = create_if_block_1(ctx);
    					if_block1.c();
    					if_block1.m(if_block1_anchor.parentNode, if_block1_anchor);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(first);
    				detach(t);
    				detach(if_block1_anchor);
    			}

    			if (if_block0) if_block0.d(detaching);
    			if (if_block1) if_block1.d(detaching);
    		}
    	};
    }

    function create_fragment$1(ctx) {
    	let div;
    	let if_block = /*$uiElements*/ ctx[0].popupOptions && /*$currentAtomGroup*/ ctx[1] && create_if_block(ctx);

    	return {
    		c() {
    			div = element("div");
    			if (if_block) if_block.c();
    			attr(div, "class", "layer-options arrow-left flex flex-col space-y-5");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			if (if_block) if_block.m(div, null);
    		},
    		p(ctx, [dirty]) {
    			if (/*$uiElements*/ ctx[0].popupOptions && /*$currentAtomGroup*/ ctx[1]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block(ctx);
    					if_block.c();
    					if_block.m(div, null);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (detaching) {
    				detach(div);
    			}

    			if (if_block) if_block.d();
    		}
    	};
    }

    function instance$1($$self, $$props, $$invalidate) {
    	let $uiElements;
    	let $currentAtomGroup;
    	let $currentUIConfig;
    	let $uiConfig;
    	let $getLocalizedOrDefaultText;
    	component_subscribe($$self, uiElements, $$value => $$invalidate(0, $uiElements = $$value));
    	component_subscribe($$self, currentAtomGroup, $$value => $$invalidate(1, $currentAtomGroup = $$value));
    	component_subscribe($$self, currentUIConfig, $$value => $$invalidate(2, $currentUIConfig = $$value));
    	component_subscribe($$self, uiConfig, $$value => $$invalidate(3, $uiConfig = $$value));
    	component_subscribe($$self, getLocalizedOrDefaultText, $$value => $$invalidate(4, $getLocalizedOrDefaultText = $$value));

    	const load_handler = popupOption => {
    		console.log("row height", popupOption);
    	};

    	return [
    		$uiElements,
    		$currentAtomGroup,
    		$currentUIConfig,
    		$uiConfig,
    		$getLocalizedOrDefaultText,
    		load_handler
    	];
    }

    class PopupModal extends SvelteComponent {
    	constructor(options) {
    		super();
    		init$1(this, options, instance$1, create_fragment$1, safe_not_equal, {});
    	}
    }

    create_custom_element(PopupModal, {}, [], [], true);

    /* src/lib/components/Product.svelte generated by Svelte v4.0.3 */

    function add_css(target) {
    	append_styles(target, "svelte-cks7mv", "*,::before,::after{box-sizing:border-box;border-width:0;border-style:solid;border-color:#e5e7eb}::before,::after{--tw-content:''}html{line-height:1.5;-webkit-text-size-adjust:100%;-moz-tab-size:4;-o-tab-size:4;tab-size:4;font-family:ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, \"Helvetica Neue\", Arial, \"Noto Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";font-feature-settings:normal;font-variation-settings:normal}body{margin:0;line-height:inherit}hr{height:0;color:inherit;border-top-width:1px}abbr:where([title]){-webkit-text-decoration:underline dotted;text-decoration:underline dotted}h1,h2,h3,h4,h5,h6{font-size:inherit;font-weight:inherit}a{color:inherit;text-decoration:inherit}b,strong{font-weight:bolder}code,kbd,samp,pre{font-family:ui-monospace, SFMono-Regular, Menlo, Monaco, Consolas, \"Liberation Mono\", \"Courier New\", monospace;font-size:1em}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sub{bottom:-0.25em}sup{top:-0.5em}table{text-indent:0;border-color:inherit;border-collapse:collapse}button,input,optgroup,select,textarea{font-family:inherit;font-size:100%;font-weight:inherit;line-height:inherit;color:inherit;margin:0;padding:0}button,select{text-transform:none}button,[type='button'],[type='reset'],[type='submit']{-webkit-appearance:button;background-color:transparent;background-image:none}:-moz-focusring{outline:auto}:-moz-ui-invalid{box-shadow:none}progress{vertical-align:baseline}::-webkit-inner-spin-button,::-webkit-outer-spin-button{height:auto}[type='search']{-webkit-appearance:textfield;outline-offset:-2px}::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}summary{display:list-item}blockquote,dl,dd,h1,h2,h3,h4,h5,h6,hr,figure,p,pre{margin:0}fieldset{margin:0;padding:0}legend{padding:0}ol,ul,menu{list-style:none;margin:0;padding:0}textarea{resize:vertical}input::-moz-placeholder,textarea::-moz-placeholder{opacity:1;color:#9ca3af}input::placeholder,textarea::placeholder{opacity:1;color:#9ca3af}button,[role=\"button\"]{cursor:pointer}:disabled{cursor:default}img,svg,video,canvas,audio,iframe,embed,object{display:block;vertical-align:middle}img,video{max-width:100%;height:auto}[hidden]{display:none}*,::before,::after{--tw-border-spacing-x:0;--tw-border-spacing-y:0;--tw-translate-x:0;--tw-translate-y:0;--tw-rotate:0;--tw-skew-x:0;--tw-skew-y:0;--tw-scale-x:1;--tw-scale-y:1;--tw-pan-x:  ;--tw-pan-y:  ;--tw-pinch-zoom:  ;--tw-scroll-snap-strictness:proximity;--tw-gradient-from-position:  ;--tw-gradient-via-position:  ;--tw-gradient-to-position:  ;--tw-ordinal:  ;--tw-slashed-zero:  ;--tw-numeric-figure:  ;--tw-numeric-spacing:  ;--tw-numeric-fraction:  ;--tw-ring-inset:  ;--tw-ring-offset-width:0px;--tw-ring-offset-color:#fff;--tw-ring-color:rgb(59 130 246 / 0.5);--tw-ring-offset-shadow:0 0 #0000;--tw-ring-shadow:0 0 #0000;--tw-shadow:0 0 #0000;--tw-shadow-colored:0 0 #0000;--tw-blur:  ;--tw-brightness:  ;--tw-contrast:  ;--tw-grayscale:  ;--tw-hue-rotate:  ;--tw-invert:  ;--tw-saturate:  ;--tw-sepia:  ;--tw-drop-shadow:  ;--tw-backdrop-blur:  ;--tw-backdrop-brightness:  ;--tw-backdrop-contrast:  ;--tw-backdrop-grayscale:  ;--tw-backdrop-hue-rotate:  ;--tw-backdrop-invert:  ;--tw-backdrop-opacity:  ;--tw-backdrop-saturate:  ;--tw-backdrop-sepia:  }::backdrop{--tw-border-spacing-x:0;--tw-border-spacing-y:0;--tw-translate-x:0;--tw-translate-y:0;--tw-rotate:0;--tw-skew-x:0;--tw-skew-y:0;--tw-scale-x:1;--tw-scale-y:1;--tw-pan-x:  ;--tw-pan-y:  ;--tw-pinch-zoom:  ;--tw-scroll-snap-strictness:proximity;--tw-gradient-from-position:  ;--tw-gradient-via-position:  ;--tw-gradient-to-position:  ;--tw-ordinal:  ;--tw-slashed-zero:  ;--tw-numeric-figure:  ;--tw-numeric-spacing:  ;--tw-numeric-fraction:  ;--tw-ring-inset:  ;--tw-ring-offset-width:0px;--tw-ring-offset-color:#fff;--tw-ring-color:rgb(59 130 246 / 0.5);--tw-ring-offset-shadow:0 0 #0000;--tw-ring-shadow:0 0 #0000;--tw-shadow:0 0 #0000;--tw-shadow-colored:0 0 #0000;--tw-blur:  ;--tw-brightness:  ;--tw-contrast:  ;--tw-grayscale:  ;--tw-hue-rotate:  ;--tw-invert:  ;--tw-saturate:  ;--tw-sepia:  ;--tw-drop-shadow:  ;--tw-backdrop-blur:  ;--tw-backdrop-brightness:  ;--tw-backdrop-contrast:  ;--tw-backdrop-grayscale:  ;--tw-backdrop-hue-rotate:  ;--tw-backdrop-invert:  ;--tw-backdrop-opacity:  ;--tw-backdrop-saturate:  ;--tw-backdrop-sepia:  }.container{width:100%}@media(min-width: 640px){.container{max-width:640px}}@media(min-width: 768px){.container{max-width:768px}}@media(min-width: 1024px){.container{max-width:1024px}}@media(min-width: 1280px){.container{max-width:1280px}}@media(min-width: 1536px){.container{max-width:1536px}}.visible{visibility:visible}.absolute{position:absolute}.relative{position:relative}.mx-2{margin-left:0.5rem;margin-right:0.5rem}.mx-7{margin-left:1.75rem;margin-right:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.my-5{margin-top:1.25rem;margin-bottom:1.25rem}.mt-20{margin-top:5rem}.block{display:block}.flex{display:flex}.table{display:table}.grid{display:grid}.hidden{display:none}.h-4{height:1rem}.h-6{height:1.5rem}.h-7{height:1.75rem}.max-h-full{max-height:100%}.w-1\\/2{width:50%}.w-1\\/4{width:25%}.w-2\\/3{width:66.666667%}.w-20{width:5rem}.w-24{width:6rem}.w-4\\/5{width:80%}.w-5\\/6{width:83.333333%}.w-6{width:1.5rem}.w-full{width:100%}.max-w-\\[180px\\]{max-width:180px}.max-w-full{max-width:100%}.transform{transform:translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y))}@keyframes pulse{50%{opacity:.5}}.animate-pulse{animation:pulse 2s cubic-bezier(0.4, 0, 0.6, 1) infinite}.cursor-pointer{cursor:pointer}.flex-row{flex-direction:row}.flex-col{flex-direction:column}.flex-wrap{flex-wrap:wrap}.items-center{align-items:center}.justify-end{justify-content:flex-end}.justify-center{justify-content:center}.justify-between{justify-content:space-between}.gap-1{gap:0.25rem}.gap-1\\.5{gap:0.375rem}.gap-12{gap:3rem}.gap-14{gap:3.5rem}.gap-2{gap:0.5rem}.gap-3{gap:0.75rem}.gap-4{gap:1rem}.gap-5{gap:1.25rem}.gap-6{gap:1.5rem}.gap-8{gap:2rem}.space-y-2>:not([hidden])~:not([hidden]){--tw-space-y-reverse:0;margin-top:calc(0.5rem * calc(1 - var(--tw-space-y-reverse)));margin-bottom:calc(0.5rem * var(--tw-space-y-reverse))}.space-y-5>:not([hidden])~:not([hidden]){--tw-space-y-reverse:0;margin-top:calc(1.25rem * calc(1 - var(--tw-space-y-reverse)));margin-bottom:calc(1.25rem * var(--tw-space-y-reverse))}.overflow-x-scroll{overflow-x:scroll}.rounded{border-radius:0.25rem}.rounded-\\[25px\\]{border-radius:25px}.rounded-\\[29px\\]{border-radius:29px}.rounded-full{border-radius:9999px}.rounded-lg{border-radius:0.5rem}.border{border-width:1px}.border-\\[\\#F2F2F2\\]{--tw-border-opacity:1;border-color:rgb(242 242 242 / var(--tw-border-opacity))}.bg-\\[\\#F2F2F2\\]{--tw-bg-opacity:1;background-color:rgb(242 242 242 / var(--tw-bg-opacity))}.bg-gray-300{--tw-bg-opacity:1;background-color:rgb(209 213 219 / var(--tw-bg-opacity))}.p-1{padding:0.25rem}.p-1\\.5{padding:0.375rem}.p-6{padding:1.5rem}.p-7{padding:1.75rem}.px-2{padding-left:0.5rem;padding-right:0.5rem}.px-4{padding-left:1rem;padding-right:1rem}.px-7{padding-left:1.75rem;padding-right:1.75rem}.py-0{padding-top:0px;padding-bottom:0px}.py-0\\.5{padding-top:0.125rem;padding-bottom:0.125rem}.py-10{padding-top:2.5rem;padding-bottom:2.5rem}.py-2{padding-top:0.5rem;padding-bottom:0.5rem}.py-2\\.5{padding-top:0.625rem;padding-bottom:0.625rem}.py-5{padding-top:1.25rem;padding-bottom:1.25rem}.pl-7{padding-left:1.75rem}.pr-7{padding-right:1.75rem}.text-2xl{font-size:1.5rem;line-height:2rem}.text-3xl{font-size:1.875rem;line-height:2.25rem}.text-base{font-size:1rem;line-height:1.5rem}.text-sm{font-size:0.875rem;line-height:1.25rem}.font-bold{font-weight:700}.font-medium{font-weight:500}.capitalize{text-transform:capitalize}.text-\\[\\#65656C\\]{--tw-text-opacity:1;color:rgb(101 101 108 / var(--tw-text-opacity))}.text-\\[\\#8C8C97\\]{--tw-text-opacity:1;color:rgb(140 140 151 / var(--tw-text-opacity))}.underline{text-decoration-line:underline}.filter{filter:var(--tw-blur) var(--tw-brightness) var(--tw-contrast) var(--tw-grayscale) var(--tw-hue-rotate) var(--tw-invert) var(--tw-saturate) var(--tw-sepia) var(--tw-drop-shadow)}.transition{transition-property:color, background-color, border-color, text-decoration-color, fill, stroke, opacity, box-shadow, transform, filter, -webkit-backdrop-filter;transition-property:color, background-color, border-color, text-decoration-color, fill, stroke, opacity, box-shadow, transform, filter, backdrop-filter;transition-property:color, background-color, border-color, text-decoration-color, fill, stroke, opacity, box-shadow, transform, filter, backdrop-filter, -webkit-backdrop-filter;transition-timing-function:cubic-bezier(0.4, 0, 0.2, 1);transition-duration:150ms}:root{--backgroundColor:#f6efef;--baseColor:#f6f5ff;--primaryColor:#e8c899;--primaryTextColor:#000000;--secondaryColor:#e9eb9d;--secondaryTextColor:#6e6b6b}.config-container{color:var(--primaryTextColor);background-color:var(--backgroundColor);font-family:\"Montserrat\", sans-serif}*{box-sizing:border-box}.column{width:100%;float:left;padding:10px}.left-container{width:70%}.right-container{width:30%}.form-text{font-size:12px;font-weight:bold}div.variants:hover{opacity:1;border:1px solid var(--primaryColor);box-shadow:0 8px 8px -4px var(--secondaryColor)}.checkbox-slider{position:absolute;cursor:pointer;top:0;left:0;right:0;bottom:0;background-color:#ccc;transition:0.4s}.checkbox-slider:before{position:absolute;content:\"\";height:20px;width:20px;left:1px;bottom:3px;background-color:white;transition:0.4s}input:checked+.checkbox-slider{background-color:#d6c0a6}input:focus+.checkbox-slider{box-shadow:0 0 1px #d6c0a6}input:checked+.checkbox-slider:before{transform:translateX(26px)}.checkbox-slider.round{border-radius:25px}.checkbox-slider.round:before{border-radius:50%}.layouts>img{cursor:pointer}.active{opacity:1;border:3px solid var(--primaryColor) !important;box-shadow:0 8px 8px -4px var(--secondaryColor)}.dimensions-circle{width:30px;height:30px;border-radius:50%;background-color:grey;display:flex;justify-content:center;align-items:center;color:white}img{vertical-align:middle}.container{position:relative}.cursor{cursor:pointer}.prev,.next{cursor:pointer;position:absolute;top:40%;width:auto;padding:16px;margin-top:-50px;color:white;font-weight:bold;font-size:20px;border-radius:0 3px 3px 0;-moz-user-select:none;user-select:none;-webkit-user-select:none}.next{right:0;border-radius:3px 0 0 3px}.prev:hover,.next:hover{background-color:rgba(0, 0, 0, 0.8)}.numbertext{color:#f2f2f2;font-size:12px;padding:8px 12px;position:absolute;top:0}.real-time-image-row:after{content:\"\";display:table;clear:both}.real-time-image-column{float:left;width:16.66%;padding:5px}.demo{opacity:0.6;border-radius:10px}.active,.demo:hover{opacity:1}.mySlides.active-slide{display:block}.mySlides>img{border-radius:10px}.checkbox-wrapper-48 label{font-size:1.35em}.checkbox-wrapper-48 input{-webkit-appearance:none;-moz-appearance:none;appearance:none;width:1em;height:1em;font:inherit;border:0.1em solid var(--primaryColor);margin-bottom:-0.125em}.checkbox-wrapper-48 input[type=\"checkbox\"]{border-radius:0.25em}.checkbox-wrapper-48 input:checked{border-color:transparent;background:var(--primaryColor);box-shadow:0 0 0 0.1em inset #fff}.checkbox-wrapper-48 input:not(:checked):hover{border-color:transparent;background:var(--primaryColor);border-color:var(--secondaryColor)}.bgImage{background-color:var(--baseColor)}.options button.selected{border:1px solid var(--primaryColor)}.selectedColor{border:3px solid var(--primaryColor) !important}.no-scrollbar::-webkit-scrollbar{display:none}.no-scrollbar{-ms-overflow-style:none;scrollbar-width:none}h1{font-size:1.5rem}.range{height:64px;width:50%;border:1px solid black;max-width:332px;background-color:var(--primaryColor);border-radius:4rem;padding-inline:2.5rem;display:grid}.range__content{position:relative;width:100%;display:grid;place-items:center}.range__slider{width:100%;height:3px;background-color:#c6c6c6;border-radius:4rem;overflow:hidden}.range__slider-line{height:100%;background:var(--primaryColor)}.range__thumb{width:15px;height:15px;background-color:var(--primaryColor);border-radius:50%;box-shadow:0 0 12px #0a0c1580;position:absolute}.range__value{background:var(--primaryColor);position:absolute;top:-50px;left:-45px;padding:6px 20px 6px 20px;border-radius:25px;display:grid;place-items:center;box-shadow:0 0 12px #0a0c1580}.range__value-number{text-align:center;width:65px;color:var(--primaryTextColor)}.range__input{-webkit-appearance:none;-moz-appearance:none;appearance:none;width:100%;height:16px;position:absolute;opacity:0}.range__input::-webkit-slider-thumb{-webkit-appearance:none;appearance:none;width:10px;height:10px}.range__input::-webkit-slider-thumb:hover{cursor:pointer}.arrow-down{width:0;height:0;border-left:10px solid transparent;border-right:10px solid transparent;margin-bottom:-15px;border-top:10px solid var(--primaryColor)}.materials,.layouts{border-radius:4px;border:1px;width:48px;height:48px;background-color:var(--secondaryColor)}.layouts:hover{opacity:1;box-shadow:0 8px 8px -4px var(--secondaryColor)}.variants{background-color:var(--secondaryColor)}.layer-options{display:none;position:absolute;width:211px;height:273px;background-color:#FFFFFF;border-radius:10px;padding:10px}.layer-options.arrow-left:after{content:\" \";position:absolute;left:-15px;top:15px;border-top:15px solid transparent;border-right:15px solid #FFFFFF;border-left:none;border-bottom:15px solid transparent}.popup-text,.popup-button,.drawers-button,.row-height-button{font-size:12px;font-weight:400;line-height:14.5px}.popup-options{height:50px;width:120px}@media(min-width: 640px){.sm\\:flex{display:flex}.sm\\:hidden{display:none}}@media(min-width: 768px){.md\\:flex-row{flex-direction:row}.md\\:items-center{align-items:center}.md\\:justify-start{justify-content:flex-start}.md\\:gap-12{gap:3rem}.md\\:gap-2{gap:0.5rem}.md\\:gap-4{gap:1rem}.md\\:gap-8{gap:2rem}.md\\:p-16{padding:4rem}.md\\:px-0{padding-left:0px;padding-right:0px}}@media(min-width: 1280px){.xl\\:bottom-0{bottom:0px}.xl\\:flex{display:flex}.xl\\:hidden{display:none}.xl\\:w-40{width:10rem}.xl\\:w-fit{width:-moz-fit-content;width:fit-content}.xl\\:flex-row{flex-direction:row}.xl\\:flex-col{flex-direction:column}.xl\\:gap-12{gap:3rem}.xl\\:gap-2{gap:0.5rem}.xl\\:bg-transparent{background-color:transparent}.xl\\:py-0{padding-top:0px;padding-bottom:0px}.xl\\:pt-10{padding-top:2.5rem}}");
    }

    function create_fragment(ctx) {
    	let div3;
    	let div2;
    	let navbar;
    	let t0;
    	let div1;
    	let tabpanel;
    	let t1;
    	let div0;
    	let canvas;
    	let canvas_class_value;
    	let t2;
    	let popupmodal;
    	let t3;
    	let mobilescreen;
    	let t4;
    	let dimension;
    	let t5;
    	let responsivescreen;
    	let current;
    	navbar = new Navbar({ props: { root: /*root*/ ctx[0] } });
    	tabpanel = new TabPanel({});
    	popupmodal = new PopupModal({});
    	mobilescreen = new MobileScreen({ props: { root: /*root*/ ctx[0] } });
    	dimension = new Dimension({});
    	responsivescreen = new ResponsiveScreen({ props: { root: /*root*/ ctx[0] } });

    	return {
    		c() {
    			div3 = element("div");
    			div2 = element("div");
    			create_component(navbar.$$.fragment);
    			t0 = space();
    			div1 = element("div");
    			create_component(tabpanel.$$.fragment);
    			t1 = space();
    			div0 = element("div");
    			canvas = element("canvas");
    			t2 = space();
    			create_component(popupmodal.$$.fragment);
    			t3 = space();
    			create_component(mobilescreen.$$.fragment);
    			t4 = space();
    			create_component(dimension.$$.fragment);
    			t5 = space();
    			create_component(responsivescreen.$$.fragment);
    			attr(canvas, "id", "webgl");

    			attr(canvas, "class", canvas_class_value = "rounded-lg flex relative " + (!/*$isDataLoaded*/ ctx[1]
    			? 'w-full bg-gray-300 rounded animate-pulse'
    			: ''));

    			attr(div0, "class", "flex justify-center bg-[#F2F2F2] xl:bg-transparent bgImage w-full xl:w-fit py-10 xl:py-0 relative xl:bottom-0");
    			attr(div0, "id", "canvas-container");
    			attr(div1, "class", "flex flex-col xl:flex-row md:items-center justify-center gap-5 xl:pt-10");
    			attr(div2, "class", "bgImage xl:pt-10");
    			attr(div3, "class", "config-container");
    		},
    		m(target, anchor) {
    			insert(target, div3, anchor);
    			append(div3, div2);
    			mount_component(navbar, div2, null);
    			append(div2, t0);
    			append(div2, div1);
    			mount_component(tabpanel, div1, null);
    			append(div1, t1);
    			append(div1, div0);
    			append(div0, canvas);
    			append(div0, t2);
    			mount_component(popupmodal, div0, null);
    			append(div1, t3);
    			mount_component(mobilescreen, div1, null);
    			append(div3, t4);
    			mount_component(dimension, div3, null);
    			append(div3, t5);
    			mount_component(responsivescreen, div3, null);
    			/*div3_binding*/ ctx[4](div3);
    			current = true;
    		},
    		p(ctx, [dirty]) {
    			const navbar_changes = {};
    			if (dirty & /*root*/ 1) navbar_changes.root = /*root*/ ctx[0];
    			navbar.$set(navbar_changes);

    			if (!current || dirty & /*$isDataLoaded*/ 2 && canvas_class_value !== (canvas_class_value = "rounded-lg flex relative " + (!/*$isDataLoaded*/ ctx[1]
    			? 'w-full bg-gray-300 rounded animate-pulse'
    			: ''))) {
    				attr(canvas, "class", canvas_class_value);
    			}

    			const mobilescreen_changes = {};
    			if (dirty & /*root*/ 1) mobilescreen_changes.root = /*root*/ ctx[0];
    			mobilescreen.$set(mobilescreen_changes);
    			const responsivescreen_changes = {};
    			if (dirty & /*root*/ 1) responsivescreen_changes.root = /*root*/ ctx[0];
    			responsivescreen.$set(responsivescreen_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(navbar.$$.fragment, local);
    			transition_in(tabpanel.$$.fragment, local);
    			transition_in(popupmodal.$$.fragment, local);
    			transition_in(mobilescreen.$$.fragment, local);
    			transition_in(dimension.$$.fragment, local);
    			transition_in(responsivescreen.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(navbar.$$.fragment, local);
    			transition_out(tabpanel.$$.fragment, local);
    			transition_out(popupmodal.$$.fragment, local);
    			transition_out(mobilescreen.$$.fragment, local);
    			transition_out(dimension.$$.fragment, local);
    			transition_out(responsivescreen.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			if (detaching) {
    				detach(div3);
    			}

    			destroy_component(navbar);
    			destroy_component(tabpanel);
    			destroy_component(popupmodal);
    			destroy_component(mobilescreen);
    			destroy_component(dimension);
    			destroy_component(responsivescreen);
    			/*div3_binding*/ ctx[4](null);
    		}
    	};
    }

    function instance($$self, $$props, $$invalidate) {
    	let $currentUIConfig;
    	let $productInfo;
    	let $uiConfig;
    	let $isDataLoaded;
    	component_subscribe($$self, currentUIConfig, $$value => $$invalidate(5, $currentUIConfig = $$value));
    	component_subscribe($$self, productInfo, $$value => $$invalidate(6, $productInfo = $$value));
    	component_subscribe($$self, uiConfig, $$value => $$invalidate(7, $uiConfig = $$value));
    	component_subscribe($$self, isDataLoaded, $$value => $$invalidate(1, $isDataLoaded = $$value));

    	var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
    		function adopt(value) {
    			return value instanceof P
    			? value
    			: new P(function (resolve) {
    						resolve(value);
    					});
    		}

    		return new (P || (P = Promise))(function (resolve, reject) {
    				function fulfilled(value) {
    					try {
    						step(generator.next(value));
    					} catch(e) {
    						reject(e);
    					}
    				}

    				function rejected(value) {
    					try {
    						step(generator["throw"](value));
    					} catch(e) {
    						reject(e);
    					}
    				}

    				function step(result) {
    					result.done
    					? resolve(result.value)
    					: adopt(result.value).then(fulfilled, rejected);
    				}

    				step((generator = generator.apply(thisArg, _arguments || [])).next());
    			});
    	};

    	let { productId } = $$props;
    	let { configKind } = $$props;
    	let root;

    	onMount(() => __awaiter(void 0, void 0, void 0, function* () {
    		var _a;
    		yield initProductInfoAndUIElements({ productId, configKind });
    		let canvas = root.querySelector("#webgl");
    		const { ConfiguratorLib } = yield Promise.resolve().then(function () { return configuratorLib; });
    		const configuratorLib$1 = new ConfiguratorLib({ renderElement: canvas });

    		yield configuratorLib$1.addGLTFLoader(
    			`${CONSTANTS.s3BaseURL}/${(_a = $productInfo === null || $productInfo === void 0
			? void 0
			: $productInfo.project) === null || _a === void 0
			? void 0
			: _a.mediaS3Key}`,
    			$uiConfig,
    			loadAndApplyRules
    		);

    		function loadAndApplyRules() {
    			var _a;

    			return __awaiter(this, void 0, void 0, function* () {
    				(_a = $productInfo === null || $productInfo === void 0
    				? void 0
    				: $productInfo.project) === null || _a === void 0
    				? void 0
    				: _a.assetItems.forEach(assetItem => configuratorLib$1.setVisibleMesh(assetItem));

    				excludeRule.set(false);
    				setAnimationAndPopupOptions(configuratorLib$1);
    				yield fetchAndExecuteRules(configuratorLib$1);
    				excludeRule.set(true);
    				excludeTextureRule.set(true);
    				loadEventListeners(root, configuratorLib$1);

    				canvas.addEventListener("click", function (event) {
    					var _a;

    					return __awaiter(this, void 0, void 0, function* () {
    						setCurrentUIConfig("additionalStorage", !JSON.parse((_a = $currentUIConfig === null || $currentUIConfig === void 0
    						? void 0
    						: $currentUIConfig.additionalStorage) === null || _a === void 0
    						? void 0
    						: _a.value));

    						configuratorLib$1.onMouseClick.bind(configuratorLib$1);
    						yield fetchAndExecuteRules(configuratorLib$1);
    					});
    				});

    				configuratorLib$1.animate();
    			});
    		}

    		let dimensionMetrics = root.getElementsByClassName("toggle-dimensions-checkbox");

    		for (let i = 0; i < dimensionMetrics.length; i++) {
    			dimensionMetrics[i].addEventListener(
    				"click",
    				function () {
    					configuratorLib$1.toggleDimensionMetrics(this);
    				},
    				false
    			);
    		}

    		let backPanels = root.getElementsByClassName("back-panels-checkbox");

    		for (let i = 0; i < backPanels.length; i++) {
    			backPanels[i].addEventListener(
    				"click",
    				function () {
    					return __awaiter(this, void 0, void 0, function* () {
    						setCurrentUIConfig("backpanel", backPanels[i].checked);
    						yield fetchAndExecuteRules(configuratorLib$1);
    					});
    				},
    				false
    			);
    		}

    		let variants = root.getElementsByClassName("variants");

    		for (let i = 0; i < variants.length; i++) {
    			variants[i].addEventListener(
    				"click",
    				function () {
    					return __awaiter(this, void 0, void 0, function* () {
    						let color = this.getAttribute("data-color");
    						setCurrentUIConfig("color", color);
    						yield fetchAndExecuteRules(configuratorLib$1, false);

    						for (var i = 0; i < variants.length; i++) {
    							variants[i].classList.remove("selectedColor");
    						}

    						this.classList.add("selectedColor");
    					});
    				},
    				false
    			);
    		}

    		let widthSlider = root.getElementsByClassName("width-range");

    		for (let i = 0; i < widthSlider.length; i++) {
    			widthSlider[i].addEventListener("input", function () {
    				return __awaiter(this, void 0, void 0, function* () {
    					configuratorLib$1.totalWidth = widthSlider[i].value;

    					// configuratorLib.updateWidth(this);
    					setCurrentUIConfig("width", widthSlider[i].value);

    					yield fetchAndExecuteRules(configuratorLib$1);
    				});
    			});
    		}

    		const rangeThumbs = root.getElementsByClassName("width-range-thumb");
    		const rangeNumbers = root.getElementsByClassName("width-range-number");
    		const rangeLines = root.getElementsByClassName("width-range-line");
    		const rangeInputs = root.getElementsByClassName("width-range");

    		const updateRangeValue = () => {
    			for (let i = 0; i < rangeInputs.length; i++) {
    				rangeNumbers[i].textContent = configuratorLib$1.totalWidth + " cm";
    			}
    		};

    		const updateRangeVisuals = () => {
    			for (let i = 0; i < rangeInputs.length; i++) {
    				const thumbPosition = (rangeInputs[i].value - rangeInputs[i].min) / (rangeInputs[i].max - rangeInputs[i].min) * 100;
    				const space = rangeInputs[i].offsetWidth - rangeThumbs[i].offsetWidth;
    				rangeThumbs[i].style.left = thumbPosition * space / 100 + "px";
    				rangeLines[i].style.width = thumbPosition + "%";
    			}
    		};

    		for (let i = 0; i < rangeInputs.length; i++) {
    			rangeInputs[i].addEventListener("input", () => {
    				updateRangeValue();
    				updateRangeVisuals();
    			});
    		}

    		const initializeRangeSlider = () => {
    			updateRangeValue();
    			updateRangeVisuals();
    		};

    		let heightSlider = root.getElementsByClassName("height-range");

    		for (let i = 0; i < heightSlider.length; i++) {
    			heightSlider[i].addEventListener("input", function () {
    				return __awaiter(this, void 0, void 0, function* () {
    					// configuratorLib.updateHeight(this);
    					configuratorLib$1.totalHeight = heightSlider[i].value;

    					setCurrentUIConfig("height", heightSlider[i].value);
    					yield fetchAndExecuteRules(configuratorLib$1);
    				});
    			});
    		}

    		const heightRangeThumbs = root.getElementsByClassName("height-range-thumb");
    		const heightRangeNumbers = root.getElementsByClassName("height-range-number");
    		const heightRangeLines = root.getElementsByClassName("height-range-line");
    		const heightRangeInputs = root.getElementsByClassName("height-range");

    		const updateHeightRangeValue = () => {
    			for (let i = 0; i < heightRangeInputs.length; i++) {
    				heightRangeNumbers[i].textContent = configuratorLib$1.totalHeight + " cm";
    			}
    		};

    		const updateHeightRangeVisuals = () => {
    			for (let i = 0; i < heightRangeInputs.length; i++) {
    				const thumbPosition = (heightRangeInputs[i].value - heightRangeInputs[i].min) / (heightRangeInputs[i].max - heightRangeInputs[i].min) * 100;
    				const space = heightRangeInputs[i].offsetWidth - heightRangeThumbs[i].offsetWidth;
    				heightRangeThumbs[i].style.left = thumbPosition * space / 100 + "px";
    				heightRangeLines[i].style.width = thumbPosition + "%";
    			}
    		};

    		for (let i = 0; i < heightRangeInputs.length; i++) {
    			heightRangeInputs[i].addEventListener("input", () => {
    				updateHeightRangeValue();
    				updateHeightRangeVisuals();
    			});
    		}

    		const initializeHeightRangeSlider = () => {
    			updateHeightRangeValue();
    			updateHeightRangeVisuals();
    		};

    		root.getElementsByClassName("density-range");

    		// for (let i = 0; i < densitySlider.length; i++) {
    		//   densitySlider[i].addEventListener("input", function () {
    		//     changeDensityAndLoadAsset(this);
    		//   });
    		// }
    		const densityRangeThumbs = root.getElementsByClassName("density-range-thumb");

    		const densityRangeNumbers = root.getElementsByClassName("density-range-number");
    		const densityRangeLines = root.getElementsByClassName("density-range-line");
    		const densityRangeInputs = root.getElementsByClassName("density-range");

    		const updateDensityRangeValue = () => {
    			for (let i = 0; i < densityRangeInputs.length; i++) {
    				densityRangeNumbers[i].textContent = Number(densityRangeInputs[i].value) + 1 + "0 cm";
    			}
    		};

    		const updateDensityRangeVisuals = () => {
    			for (let i = 0; i < densityRangeInputs.length; i++) {
    				const thumbPosition = (densityRangeInputs[i].value - densityRangeInputs[i].min) / (densityRangeInputs[i].max - densityRangeInputs[i].min) * 100;
    				const space = densityRangeInputs[i].offsetWidth - densityRangeThumbs[i].offsetWidth;
    				densityRangeThumbs[i].style.left = thumbPosition * space + "px";
    				densityRangeLines[i].style.width = densityRangeInputs[i].value * 33.33 + "%";
    			}
    		};

    		for (let i = 0; i < densityRangeInputs.length; i++) {
    			densityRangeInputs[i].addEventListener("input", () => {
    				updateDensityRangeValue();
    				updateDensityRangeVisuals();
    			});
    		}

    		const initializeDensityRangeSlider = () => {
    			updateDensityRangeValue();
    			updateDensityRangeVisuals();
    		};

    		initializeDensityRangeSlider();
    		let depthButton = root.getElementsByClassName("depth-button");

    		for (let i = 0; i < depthButton.length; i++) {
    			depthButton[i].addEventListener("click", function () {
    				configuratorLib$1.updateDepth(this);
    			});
    		}

    		let layouts = root.getElementsByClassName("layouts");

    		for (let i = 0; i < layouts.length; i++) {
    			layouts[i].addEventListener(
    				"click",
    				function () {
    					return __awaiter(this, void 0, void 0, function* () {
    						var current = layouts;

    						for (var i = 0; i < current.length; i++) {
    							current[i].classList.remove("active");
    						}

    						this.classList.add("active");
    						let layoutKind = this.getAttribute("data-layout-kind");
    						setCurrentUIConfig("layout", layoutKind);
    						yield fetchAndExecuteRules(configuratorLib$1);
    					});
    				},
    				false
    			);
    		}

    		let materials = root.getElementsByClassName("materials");

    		for (let i = 0; i < materials.length; i++) {
    			materials[i].addEventListener(
    				"click",
    				function () {
    					return __awaiter(this, void 0, void 0, function* () {
    						var current = materials;

    						for (var i = 0; i < current.length; i++) {
    							current[i].classList.remove("active");
    						}

    						this.classList.add("active");
    						setCurrentUIConfig("material", this.getAttribute("alt"));
    						excludeTextureRule.set(false);
    						yield fetchAndExecuteRules(configuratorLib$1);
    						excludeTextureRule.set(true);
    					});
    				},
    				false
    			);
    		}

    		popupModalEventListener(root, configuratorLib$1);

    		height.subscribe(value => {
    			initializeHeightRangeSlider();
    		});

    		width.subscribe(value => {
    			initializeRangeSlider();
    		});
    	}));

    	function div3_binding($$value) {
    		binding_callbacks[$$value ? 'unshift' : 'push'](() => {
    			root = $$value;
    			$$invalidate(0, root);
    		});
    	}

    	$$self.$$set = $$props => {
    		if ('productId' in $$props) $$invalidate(2, productId = $$props.productId);
    		if ('configKind' in $$props) $$invalidate(3, configKind = $$props.configKind);
    	};

    	return [root, $isDataLoaded, productId, configKind, div3_binding];
    }

    class Product extends SvelteComponent {
    	constructor(options) {
    		super();
    		init$1(this, options, instance, create_fragment, safe_not_equal, { productId: 2, configKind: 3 }, add_css);
    	}

    	get productId() {
    		return this.$$.ctx[2];
    	}

    	set productId(productId) {
    		this.$$set({ productId });
    		flush();
    	}

    	get configKind() {
    		return this.$$.ctx[3];
    	}

    	set configKind(configKind) {
    		this.$$set({ configKind });
    		flush();
    	}
    }

    customElements.define("product-element", create_custom_element(Product, {"productId":{},"configKind":{}}, [], [], true));

    const init = ({ apiKey }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!apiKey)
            throw new Error("API KEY is required to initialize");
        localStorage.setItem("configurator-sdk-api-key", apiKey);
        if (!localStorage.getItem("configurator-sdk-locale")) {
            localStorage.setItem("configurator-sdk-locale", "en");
        }
        console.log(`Initialized Configurator SDK`);
    });
    const setLocale = (locale) => __awaiter(void 0, void 0, void 0, function* () {
        if (locale == "")
            return;
        localStorage.setItem("configurator-sdk-locale", locale);
        let organizationId = get_store_value(productInfo).organizationId;
        translations.set(yield getTranslations(organizationId, locale));
    });
    const getProductPrice = () => __awaiter(void 0, void 0, void 0, function* () {
        return { price: get_store_value(productPrice) };
    });
    const getModifiedProductId = () => __awaiter(void 0, void 0, void 0, function* () {
        return { configId: get_store_value(modifiedConfigId), configKind: "modified" };
    });
    const getProductInfo = () => __awaiter(void 0, void 0, void 0, function* () {
        return { productInfo: get_store_value(productInfo) };
    });
    const getProductImages = () => __awaiter(void 0, void 0, void 0, function* () {
        var _a, _b;
        let response = yield getProductSnapshots((_b = (_a = get_store_value(productInfo)) === null || _a === void 0 ? void 0 : _a.project) === null || _b === void 0 ? void 0 : _b.id);
        return { productImages: response === null || response === void 0 ? void 0 : response.result };
    });
    const createModifiedProduct = () => __awaiter(void 0, void 0, void 0, function* () {
        let response = yield addModifiedProductWithActiveRules();
        return { response };
    });

    // OrbitControls performs orbiting, dollying (zooming), and panning.
    // Unlike TrackballControls, it maintains the "up" direction object.up (+Y by default).
    //
    //    Orbit - left mouse / touch: one-finger move
    //    Zoom - middle mouse, or mousewheel / touch: two-finger spread or squish
    //    Pan - right mouse, or left mouse + ctrl/meta/shiftKey, or arrow keys / touch: two-finger move

    const _changeEvent = { type: 'change' };
    const _startEvent = { type: 'start' };
    const _endEvent = { type: 'end' };

    class OrbitControls extends THREE.EventDispatcher {

    	constructor( object, domElement ) {

    		super();

    		this.object = object;
    		this.domElement = domElement;
    		this.domElement.style.touchAction = 'none'; // disable touch scroll

    		// Set to false to disable this control
    		this.enabled = true;

    		// "target" sets the location of focus, where the object orbits around
    		this.target = new THREE.Vector3();

    		// How far you can dolly in and out ( PerspectiveCamera only )
    		this.minDistance = 0;
    		this.maxDistance = Infinity;

    		// How far you can zoom in and out ( OrthographicCamera only )
    		this.minZoom = 0;
    		this.maxZoom = Infinity;

    		// How far you can orbit vertically, upper and lower limits.
    		// Range is 0 to Math.PI radians.
    		this.minPolarAngle = 0; // radians
    		this.maxPolarAngle = Math.PI; // radians

    		// How far you can orbit horizontally, upper and lower limits.
    		// If set, the interval [ min, max ] must be a sub-interval of [ - 2 PI, 2 PI ], with ( max - min < 2 PI )
    		this.minAzimuthAngle = - Infinity; // radians
    		this.maxAzimuthAngle = Infinity; // radians

    		// Set to true to enable damping (inertia)
    		// If damping is enabled, you must call controls.update() in your animation loop
    		this.enableDamping = false;
    		this.dampingFactor = 0.05;

    		// This option actually enables dollying in and out; left as "zoom" for backwards compatibility.
    		// Set to false to disable zooming
    		this.enableZoom = true;
    		this.zoomSpeed = 1.0;

    		// Set to false to disable rotating
    		this.enableRotate = true;
    		this.rotateSpeed = 1.0;

    		// Set to false to disable panning
    		this.enablePan = true;
    		this.panSpeed = 1.0;
    		this.screenSpacePanning = true; // if false, pan orthogonal to world-space direction camera.up
    		this.keyPanSpeed = 7.0;	// pixels moved per arrow key push

    		// Set to true to automatically rotate around the target
    		// If auto-rotate is enabled, you must call controls.update() in your animation loop
    		this.autoRotate = false;
    		this.autoRotateSpeed = 2.0; // 30 seconds per orbit when fps is 60

    		// The four arrow keys
    		this.keys = { LEFT: 'ArrowLeft', UP: 'ArrowUp', RIGHT: 'ArrowRight', BOTTOM: 'ArrowDown' };

    		// Mouse buttons
    		this.mouseButtons = { LEFT: THREE.MOUSE.ROTATE, MIDDLE: THREE.MOUSE.DOLLY, RIGHT: THREE.MOUSE.PAN };

    		// Touch fingers
    		this.touches = { ONE: THREE.TOUCH.ROTATE, TWO: THREE.TOUCH.DOLLY_PAN };

    		// for reset
    		this.target0 = this.target.clone();
    		this.position0 = this.object.position.clone();
    		this.zoom0 = this.object.zoom;

    		// the target DOM element for key events
    		this._domElementKeyEvents = null;

    		//
    		// public methods
    		//

    		this.getPolarAngle = function () {

    			return spherical.phi;

    		};

    		this.getAzimuthalAngle = function () {

    			return spherical.theta;

    		};

    		this.getDistance = function () {

    			return this.object.position.distanceTo( this.target );

    		};

    		this.listenToKeyEvents = function ( domElement ) {

    			domElement.addEventListener( 'keydown', onKeyDown );
    			this._domElementKeyEvents = domElement;

    		};

    		this.stopListenToKeyEvents = function () {

    			this._domElementKeyEvents.removeEventListener( 'keydown', onKeyDown );
    			this._domElementKeyEvents = null;

    		};

    		this.saveState = function () {

    			scope.target0.copy( scope.target );
    			scope.position0.copy( scope.object.position );
    			scope.zoom0 = scope.object.zoom;

    		};

    		this.reset = function () {

    			scope.target.copy( scope.target0 );
    			scope.object.position.copy( scope.position0 );
    			scope.object.zoom = scope.zoom0;

    			scope.object.updateProjectionMatrix();
    			scope.dispatchEvent( _changeEvent );

    			scope.update();

    			state = STATE.NONE;

    		};

    		// this method is exposed, but perhaps it would be better if we can make it private...
    		this.update = function () {

    			const offset = new THREE.Vector3();

    			// so camera.up is the orbit axis
    			const quat = new THREE.Quaternion().setFromUnitVectors( object.up, new THREE.Vector3( 0, 1, 0 ) );
    			const quatInverse = quat.clone().invert();

    			const lastPosition = new THREE.Vector3();
    			const lastQuaternion = new THREE.Quaternion();
    			const lastTargetPosition = new THREE.Vector3();

    			const twoPI = 2 * Math.PI;

    			return function update() {

    				const position = scope.object.position;

    				offset.copy( position ).sub( scope.target );

    				// rotate offset to "y-axis-is-up" space
    				offset.applyQuaternion( quat );

    				// angle from z-axis around y-axis
    				spherical.setFromVector3( offset );

    				if ( scope.autoRotate && state === STATE.NONE ) {

    					rotateLeft( getAutoRotationAngle() );

    				}

    				if ( scope.enableDamping ) {

    					spherical.theta += sphericalDelta.theta * scope.dampingFactor;
    					spherical.phi += sphericalDelta.phi * scope.dampingFactor;

    				} else {

    					spherical.theta += sphericalDelta.theta;
    					spherical.phi += sphericalDelta.phi;

    				}

    				// restrict theta to be between desired limits

    				let min = scope.minAzimuthAngle;
    				let max = scope.maxAzimuthAngle;

    				if ( isFinite( min ) && isFinite( max ) ) {

    					if ( min < - Math.PI ) min += twoPI; else if ( min > Math.PI ) min -= twoPI;

    					if ( max < - Math.PI ) max += twoPI; else if ( max > Math.PI ) max -= twoPI;

    					if ( min <= max ) {

    						spherical.theta = Math.max( min, Math.min( max, spherical.theta ) );

    					} else {

    						spherical.theta = ( spherical.theta > ( min + max ) / 2 ) ?
    							Math.max( min, spherical.theta ) :
    							Math.min( max, spherical.theta );

    					}

    				}

    				// restrict phi to be between desired limits
    				spherical.phi = Math.max( scope.minPolarAngle, Math.min( scope.maxPolarAngle, spherical.phi ) );

    				spherical.makeSafe();


    				spherical.radius *= scale;

    				// restrict radius to be between desired limits
    				spherical.radius = Math.max( scope.minDistance, Math.min( scope.maxDistance, spherical.radius ) );

    				// move target to panned location

    				if ( scope.enableDamping === true ) {

    					scope.target.addScaledVector( panOffset, scope.dampingFactor );

    				} else {

    					scope.target.add( panOffset );

    				}

    				offset.setFromSpherical( spherical );

    				// rotate offset back to "camera-up-vector-is-up" space
    				offset.applyQuaternion( quatInverse );

    				position.copy( scope.target ).add( offset );

    				scope.object.lookAt( scope.target );

    				if ( scope.enableDamping === true ) {

    					sphericalDelta.theta *= ( 1 - scope.dampingFactor );
    					sphericalDelta.phi *= ( 1 - scope.dampingFactor );

    					panOffset.multiplyScalar( 1 - scope.dampingFactor );

    				} else {

    					sphericalDelta.set( 0, 0, 0 );

    					panOffset.set( 0, 0, 0 );

    				}

    				scale = 1;

    				// update condition is:
    				// min(camera displacement, camera rotation in radians)^2 > EPS
    				// using small-angle approximation cos(x/2) = 1 - x^2 / 8

    				if ( zoomChanged ||
    					lastPosition.distanceToSquared( scope.object.position ) > EPS ||
    					8 * ( 1 - lastQuaternion.dot( scope.object.quaternion ) ) > EPS ||
    					lastTargetPosition.distanceToSquared( scope.target ) > 0 ) {

    					scope.dispatchEvent( _changeEvent );

    					lastPosition.copy( scope.object.position );
    					lastQuaternion.copy( scope.object.quaternion );
    					lastTargetPosition.copy( scope.target );

    					zoomChanged = false;

    					return true;

    				}

    				return false;

    			};

    		}();

    		this.dispose = function () {

    			scope.domElement.removeEventListener( 'contextmenu', onContextMenu );

    			scope.domElement.removeEventListener( 'pointerdown', onPointerDown );
    			scope.domElement.removeEventListener( 'pointercancel', onPointerUp );
    			scope.domElement.removeEventListener( 'wheel', onMouseWheel );

    			scope.domElement.removeEventListener( 'pointermove', onPointerMove );
    			scope.domElement.removeEventListener( 'pointerup', onPointerUp );


    			if ( scope._domElementKeyEvents !== null ) {

    				scope._domElementKeyEvents.removeEventListener( 'keydown', onKeyDown );
    				scope._domElementKeyEvents = null;

    			}

    			//scope.dispatchEvent( { type: 'dispose' } ); // should this be added here?

    		};

    		//
    		// internals
    		//

    		const scope = this;

    		const STATE = {
    			NONE: - 1,
    			ROTATE: 0,
    			DOLLY: 1,
    			PAN: 2,
    			TOUCH_ROTATE: 3,
    			TOUCH_PAN: 4,
    			TOUCH_DOLLY_PAN: 5,
    			TOUCH_DOLLY_ROTATE: 6
    		};

    		let state = STATE.NONE;

    		const EPS = 0.000001;

    		// current position in spherical coordinates
    		const spherical = new THREE.Spherical();
    		const sphericalDelta = new THREE.Spherical();

    		let scale = 1;
    		const panOffset = new THREE.Vector3();
    		let zoomChanged = false;

    		const rotateStart = new THREE.Vector2();
    		const rotateEnd = new THREE.Vector2();
    		const rotateDelta = new THREE.Vector2();

    		const panStart = new THREE.Vector2();
    		const panEnd = new THREE.Vector2();
    		const panDelta = new THREE.Vector2();

    		const dollyStart = new THREE.Vector2();
    		const dollyEnd = new THREE.Vector2();
    		const dollyDelta = new THREE.Vector2();

    		const pointers = [];
    		const pointerPositions = {};

    		function getAutoRotationAngle() {

    			return 2 * Math.PI / 60 / 60 * scope.autoRotateSpeed;

    		}

    		function getZoomScale() {

    			return Math.pow( 0.95, scope.zoomSpeed );

    		}

    		function rotateLeft( angle ) {

    			sphericalDelta.theta -= angle;

    		}

    		function rotateUp( angle ) {

    			sphericalDelta.phi -= angle;

    		}

    		const panLeft = function () {

    			const v = new THREE.Vector3();

    			return function panLeft( distance, objectMatrix ) {

    				v.setFromMatrixColumn( objectMatrix, 0 ); // get X column of objectMatrix
    				v.multiplyScalar( - distance );

    				panOffset.add( v );

    			};

    		}();

    		const panUp = function () {

    			const v = new THREE.Vector3();

    			return function panUp( distance, objectMatrix ) {

    				if ( scope.screenSpacePanning === true ) {

    					v.setFromMatrixColumn( objectMatrix, 1 );

    				} else {

    					v.setFromMatrixColumn( objectMatrix, 0 );
    					v.crossVectors( scope.object.up, v );

    				}

    				v.multiplyScalar( distance );

    				panOffset.add( v );

    			};

    		}();

    		// deltaX and deltaY are in pixels; right and down are positive
    		const pan = function () {

    			const offset = new THREE.Vector3();

    			return function pan( deltaX, deltaY ) {

    				const element = scope.domElement;

    				if ( scope.object.isPerspectiveCamera ) {

    					// perspective
    					const position = scope.object.position;
    					offset.copy( position ).sub( scope.target );
    					let targetDistance = offset.length();

    					// half of the fov is center to top of screen
    					targetDistance *= Math.tan( ( scope.object.fov / 2 ) * Math.PI / 180.0 );

    					// we use only clientHeight here so aspect ratio does not distort speed
    					panLeft( 2 * deltaX * targetDistance / element.clientHeight, scope.object.matrix );
    					panUp( 2 * deltaY * targetDistance / element.clientHeight, scope.object.matrix );

    				} else if ( scope.object.isOrthographicCamera ) {

    					// orthographic
    					panLeft( deltaX * ( scope.object.right - scope.object.left ) / scope.object.zoom / element.clientWidth, scope.object.matrix );
    					panUp( deltaY * ( scope.object.top - scope.object.bottom ) / scope.object.zoom / element.clientHeight, scope.object.matrix );

    				} else {

    					// camera neither orthographic nor perspective
    					console.warn( 'WARNING: OrbitControls.js encountered an unknown camera type - pan disabled.' );
    					scope.enablePan = false;

    				}

    			};

    		}();

    		function dollyOut( dollyScale ) {

    			if ( scope.object.isPerspectiveCamera ) {

    				scale /= dollyScale;

    			} else if ( scope.object.isOrthographicCamera ) {

    				scope.object.zoom = Math.max( scope.minZoom, Math.min( scope.maxZoom, scope.object.zoom * dollyScale ) );
    				scope.object.updateProjectionMatrix();
    				zoomChanged = true;

    			} else {

    				console.warn( 'WARNING: OrbitControls.js encountered an unknown camera type - dolly/zoom disabled.' );
    				scope.enableZoom = false;

    			}

    		}

    		function dollyIn( dollyScale ) {

    			if ( scope.object.isPerspectiveCamera ) {

    				scale *= dollyScale;

    			} else if ( scope.object.isOrthographicCamera ) {

    				scope.object.zoom = Math.max( scope.minZoom, Math.min( scope.maxZoom, scope.object.zoom / dollyScale ) );
    				scope.object.updateProjectionMatrix();
    				zoomChanged = true;

    			} else {

    				console.warn( 'WARNING: OrbitControls.js encountered an unknown camera type - dolly/zoom disabled.' );
    				scope.enableZoom = false;

    			}

    		}

    		//
    		// event callbacks - update the object state
    		//

    		function handleMouseDownRotate( event ) {

    			rotateStart.set( event.clientX, event.clientY );

    		}

    		function handleMouseDownDolly( event ) {

    			dollyStart.set( event.clientX, event.clientY );

    		}

    		function handleMouseDownPan( event ) {

    			panStart.set( event.clientX, event.clientY );

    		}

    		function handleMouseMoveRotate( event ) {

    			rotateEnd.set( event.clientX, event.clientY );

    			rotateDelta.subVectors( rotateEnd, rotateStart ).multiplyScalar( scope.rotateSpeed );

    			const element = scope.domElement;

    			rotateLeft( 2 * Math.PI * rotateDelta.x / element.clientHeight ); // yes, height

    			rotateUp( 2 * Math.PI * rotateDelta.y / element.clientHeight );

    			rotateStart.copy( rotateEnd );

    			scope.update();

    		}

    		function handleMouseMoveDolly( event ) {

    			dollyEnd.set( event.clientX, event.clientY );

    			dollyDelta.subVectors( dollyEnd, dollyStart );

    			if ( dollyDelta.y > 0 ) {

    				dollyOut( getZoomScale() );

    			} else if ( dollyDelta.y < 0 ) {

    				dollyIn( getZoomScale() );

    			}

    			dollyStart.copy( dollyEnd );

    			scope.update();

    		}

    		function handleMouseMovePan( event ) {

    			panEnd.set( event.clientX, event.clientY );

    			panDelta.subVectors( panEnd, panStart ).multiplyScalar( scope.panSpeed );

    			pan( panDelta.x, panDelta.y );

    			panStart.copy( panEnd );

    			scope.update();

    		}

    		function handleMouseWheel( event ) {

    			if ( event.deltaY < 0 ) {

    				dollyIn( getZoomScale() );

    			} else if ( event.deltaY > 0 ) {

    				dollyOut( getZoomScale() );

    			}

    			scope.update();

    		}

    		function handleKeyDown( event ) {

    			let needsUpdate = false;

    			switch ( event.code ) {

    				case scope.keys.UP:

    					if ( event.ctrlKey || event.metaKey || event.shiftKey ) {

    						rotateUp( 2 * Math.PI * scope.rotateSpeed / scope.domElement.clientHeight );

    					} else {

    						pan( 0, scope.keyPanSpeed );

    					}

    					needsUpdate = true;
    					break;

    				case scope.keys.BOTTOM:

    					if ( event.ctrlKey || event.metaKey || event.shiftKey ) {

    						rotateUp( - 2 * Math.PI * scope.rotateSpeed / scope.domElement.clientHeight );

    					} else {

    						pan( 0, - scope.keyPanSpeed );

    					}

    					needsUpdate = true;
    					break;

    				case scope.keys.LEFT:

    					if ( event.ctrlKey || event.metaKey || event.shiftKey ) {

    						rotateLeft( 2 * Math.PI * scope.rotateSpeed / scope.domElement.clientHeight );

    					} else {

    						pan( scope.keyPanSpeed, 0 );

    					}

    					needsUpdate = true;
    					break;

    				case scope.keys.RIGHT:

    					if ( event.ctrlKey || event.metaKey || event.shiftKey ) {

    						rotateLeft( - 2 * Math.PI * scope.rotateSpeed / scope.domElement.clientHeight );

    					} else {

    						pan( - scope.keyPanSpeed, 0 );

    					}

    					needsUpdate = true;
    					break;

    			}

    			if ( needsUpdate ) {

    				// prevent the browser from scrolling on cursor keys
    				event.preventDefault();

    				scope.update();

    			}


    		}

    		function handleTouchStartRotate() {

    			if ( pointers.length === 1 ) {

    				rotateStart.set( pointers[ 0 ].pageX, pointers[ 0 ].pageY );

    			} else {

    				const x = 0.5 * ( pointers[ 0 ].pageX + pointers[ 1 ].pageX );
    				const y = 0.5 * ( pointers[ 0 ].pageY + pointers[ 1 ].pageY );

    				rotateStart.set( x, y );

    			}

    		}

    		function handleTouchStartPan() {

    			if ( pointers.length === 1 ) {

    				panStart.set( pointers[ 0 ].pageX, pointers[ 0 ].pageY );

    			} else {

    				const x = 0.5 * ( pointers[ 0 ].pageX + pointers[ 1 ].pageX );
    				const y = 0.5 * ( pointers[ 0 ].pageY + pointers[ 1 ].pageY );

    				panStart.set( x, y );

    			}

    		}

    		function handleTouchStartDolly() {

    			const dx = pointers[ 0 ].pageX - pointers[ 1 ].pageX;
    			const dy = pointers[ 0 ].pageY - pointers[ 1 ].pageY;

    			const distance = Math.sqrt( dx * dx + dy * dy );

    			dollyStart.set( 0, distance );

    		}

    		function handleTouchStartDollyPan() {

    			if ( scope.enableZoom ) handleTouchStartDolly();

    			if ( scope.enablePan ) handleTouchStartPan();

    		}

    		function handleTouchStartDollyRotate() {

    			if ( scope.enableZoom ) handleTouchStartDolly();

    			if ( scope.enableRotate ) handleTouchStartRotate();

    		}

    		function handleTouchMoveRotate( event ) {

    			if ( pointers.length == 1 ) {

    				rotateEnd.set( event.pageX, event.pageY );

    			} else {

    				const position = getSecondPointerPosition( event );

    				const x = 0.5 * ( event.pageX + position.x );
    				const y = 0.5 * ( event.pageY + position.y );

    				rotateEnd.set( x, y );

    			}

    			rotateDelta.subVectors( rotateEnd, rotateStart ).multiplyScalar( scope.rotateSpeed );

    			const element = scope.domElement;

    			rotateLeft( 2 * Math.PI * rotateDelta.x / element.clientHeight ); // yes, height

    			rotateUp( 2 * Math.PI * rotateDelta.y / element.clientHeight );

    			rotateStart.copy( rotateEnd );

    		}

    		function handleTouchMovePan( event ) {

    			if ( pointers.length === 1 ) {

    				panEnd.set( event.pageX, event.pageY );

    			} else {

    				const position = getSecondPointerPosition( event );

    				const x = 0.5 * ( event.pageX + position.x );
    				const y = 0.5 * ( event.pageY + position.y );

    				panEnd.set( x, y );

    			}

    			panDelta.subVectors( panEnd, panStart ).multiplyScalar( scope.panSpeed );

    			pan( panDelta.x, panDelta.y );

    			panStart.copy( panEnd );

    		}

    		function handleTouchMoveDolly( event ) {

    			const position = getSecondPointerPosition( event );

    			const dx = event.pageX - position.x;
    			const dy = event.pageY - position.y;

    			const distance = Math.sqrt( dx * dx + dy * dy );

    			dollyEnd.set( 0, distance );

    			dollyDelta.set( 0, Math.pow( dollyEnd.y / dollyStart.y, scope.zoomSpeed ) );

    			dollyOut( dollyDelta.y );

    			dollyStart.copy( dollyEnd );

    		}

    		function handleTouchMoveDollyPan( event ) {

    			if ( scope.enableZoom ) handleTouchMoveDolly( event );

    			if ( scope.enablePan ) handleTouchMovePan( event );

    		}

    		function handleTouchMoveDollyRotate( event ) {

    			if ( scope.enableZoom ) handleTouchMoveDolly( event );

    			if ( scope.enableRotate ) handleTouchMoveRotate( event );

    		}

    		//
    		// event handlers - FSM: listen for events and reset state
    		//

    		function onPointerDown( event ) {

    			if ( scope.enabled === false ) return;

    			if ( pointers.length === 0 ) {

    				scope.domElement.setPointerCapture( event.pointerId );

    				scope.domElement.addEventListener( 'pointermove', onPointerMove );
    				scope.domElement.addEventListener( 'pointerup', onPointerUp );

    			}

    			//

    			addPointer( event );

    			if ( event.pointerType === 'touch' ) {

    				onTouchStart( event );

    			} else {

    				onMouseDown( event );

    			}

    		}

    		function onPointerMove( event ) {

    			if ( scope.enabled === false ) return;

    			if ( event.pointerType === 'touch' ) {

    				onTouchMove( event );

    			} else {

    				onMouseMove( event );

    			}

    		}

    		function onPointerUp( event ) {

    			removePointer( event );

    			if ( pointers.length === 0 ) {

    				scope.domElement.releasePointerCapture( event.pointerId );

    				scope.domElement.removeEventListener( 'pointermove', onPointerMove );
    				scope.domElement.removeEventListener( 'pointerup', onPointerUp );

    			}

    			scope.dispatchEvent( _endEvent );

    			state = STATE.NONE;

    		}

    		function onMouseDown( event ) {

    			let mouseAction;

    			switch ( event.button ) {

    				case 0:

    					mouseAction = scope.mouseButtons.LEFT;
    					break;

    				case 1:

    					mouseAction = scope.mouseButtons.MIDDLE;
    					break;

    				case 2:

    					mouseAction = scope.mouseButtons.RIGHT;
    					break;

    				default:

    					mouseAction = - 1;

    			}

    			switch ( mouseAction ) {

    				case THREE.MOUSE.DOLLY:

    					if ( scope.enableZoom === false ) return;

    					handleMouseDownDolly( event );

    					state = STATE.DOLLY;

    					break;

    				case THREE.MOUSE.ROTATE:

    					if ( event.ctrlKey || event.metaKey || event.shiftKey ) {

    						if ( scope.enablePan === false ) return;

    						handleMouseDownPan( event );

    						state = STATE.PAN;

    					} else {

    						if ( scope.enableRotate === false ) return;

    						handleMouseDownRotate( event );

    						state = STATE.ROTATE;

    					}

    					break;

    				case THREE.MOUSE.PAN:

    					if ( event.ctrlKey || event.metaKey || event.shiftKey ) {

    						if ( scope.enableRotate === false ) return;

    						handleMouseDownRotate( event );

    						state = STATE.ROTATE;

    					} else {

    						if ( scope.enablePan === false ) return;

    						handleMouseDownPan( event );

    						state = STATE.PAN;

    					}

    					break;

    				default:

    					state = STATE.NONE;

    			}

    			if ( state !== STATE.NONE ) {

    				scope.dispatchEvent( _startEvent );

    			}

    		}

    		function onMouseMove( event ) {

    			switch ( state ) {

    				case STATE.ROTATE:

    					if ( scope.enableRotate === false ) return;

    					handleMouseMoveRotate( event );

    					break;

    				case STATE.DOLLY:

    					if ( scope.enableZoom === false ) return;

    					handleMouseMoveDolly( event );

    					break;

    				case STATE.PAN:

    					if ( scope.enablePan === false ) return;

    					handleMouseMovePan( event );

    					break;

    			}

    		}

    		function onMouseWheel( event ) {

    			if ( scope.enabled === false || scope.enableZoom === false || state !== STATE.NONE ) return;

    			event.preventDefault();

    			scope.dispatchEvent( _startEvent );

    			handleMouseWheel( event );

    			scope.dispatchEvent( _endEvent );

    		}

    		function onKeyDown( event ) {

    			if ( scope.enabled === false || scope.enablePan === false ) return;

    			handleKeyDown( event );

    		}

    		function onTouchStart( event ) {

    			trackPointer( event );

    			switch ( pointers.length ) {

    				case 1:

    					switch ( scope.touches.ONE ) {

    						case THREE.TOUCH.ROTATE:

    							if ( scope.enableRotate === false ) return;

    							handleTouchStartRotate();

    							state = STATE.TOUCH_ROTATE;

    							break;

    						case THREE.TOUCH.PAN:

    							if ( scope.enablePan === false ) return;

    							handleTouchStartPan();

    							state = STATE.TOUCH_PAN;

    							break;

    						default:

    							state = STATE.NONE;

    					}

    					break;

    				case 2:

    					switch ( scope.touches.TWO ) {

    						case THREE.TOUCH.DOLLY_PAN:

    							if ( scope.enableZoom === false && scope.enablePan === false ) return;

    							handleTouchStartDollyPan();

    							state = STATE.TOUCH_DOLLY_PAN;

    							break;

    						case THREE.TOUCH.DOLLY_ROTATE:

    							if ( scope.enableZoom === false && scope.enableRotate === false ) return;

    							handleTouchStartDollyRotate();

    							state = STATE.TOUCH_DOLLY_ROTATE;

    							break;

    						default:

    							state = STATE.NONE;

    					}

    					break;

    				default:

    					state = STATE.NONE;

    			}

    			if ( state !== STATE.NONE ) {

    				scope.dispatchEvent( _startEvent );

    			}

    		}

    		function onTouchMove( event ) {

    			trackPointer( event );

    			switch ( state ) {

    				case STATE.TOUCH_ROTATE:

    					if ( scope.enableRotate === false ) return;

    					handleTouchMoveRotate( event );

    					scope.update();

    					break;

    				case STATE.TOUCH_PAN:

    					if ( scope.enablePan === false ) return;

    					handleTouchMovePan( event );

    					scope.update();

    					break;

    				case STATE.TOUCH_DOLLY_PAN:

    					if ( scope.enableZoom === false && scope.enablePan === false ) return;

    					handleTouchMoveDollyPan( event );

    					scope.update();

    					break;

    				case STATE.TOUCH_DOLLY_ROTATE:

    					if ( scope.enableZoom === false && scope.enableRotate === false ) return;

    					handleTouchMoveDollyRotate( event );

    					scope.update();

    					break;

    				default:

    					state = STATE.NONE;

    			}

    		}

    		function onContextMenu( event ) {

    			if ( scope.enabled === false ) return;

    			event.preventDefault();

    		}

    		function addPointer( event ) {

    			pointers.push( event );

    		}

    		function removePointer( event ) {

    			delete pointerPositions[ event.pointerId ];

    			for ( let i = 0; i < pointers.length; i ++ ) {

    				if ( pointers[ i ].pointerId == event.pointerId ) {

    					pointers.splice( i, 1 );
    					return;

    				}

    			}

    		}

    		function trackPointer( event ) {

    			let position = pointerPositions[ event.pointerId ];

    			if ( position === undefined ) {

    				position = new THREE.Vector2();
    				pointerPositions[ event.pointerId ] = position;

    			}

    			position.set( event.pageX, event.pageY );

    		}

    		function getSecondPointerPosition( event ) {

    			const pointer = ( event.pointerId === pointers[ 0 ].pointerId ) ? pointers[ 1 ] : pointers[ 0 ];

    			return pointerPositions[ pointer.pointerId ];

    		}

    		//

    		scope.domElement.addEventListener( 'contextmenu', onContextMenu );

    		scope.domElement.addEventListener( 'pointerdown', onPointerDown );
    		scope.domElement.addEventListener( 'pointercancel', onPointerUp );
    		scope.domElement.addEventListener( 'wheel', onMouseWheel, { passive: false } );

    		// force an update at start

    		this.update();

    	}

    }

    /**
     * @param {BufferGeometry} geometry
     * @param {number} drawMode
     * @return {BufferGeometry}
     */
    function toTrianglesDrawMode( geometry, drawMode ) {

    	if ( drawMode === THREE.TrianglesDrawMode ) {

    		console.warn( 'THREE.BufferGeometryUtils.toTrianglesDrawMode(): Geometry already defined as triangles.' );
    		return geometry;

    	}

    	if ( drawMode === THREE.TriangleFanDrawMode || drawMode === THREE.TriangleStripDrawMode ) {

    		let index = geometry.getIndex();

    		// generate index if not present

    		if ( index === null ) {

    			const indices = [];

    			const position = geometry.getAttribute( 'position' );

    			if ( position !== undefined ) {

    				for ( let i = 0; i < position.count; i ++ ) {

    					indices.push( i );

    				}

    				geometry.setIndex( indices );
    				index = geometry.getIndex();

    			} else {

    				console.error( 'THREE.BufferGeometryUtils.toTrianglesDrawMode(): Undefined position attribute. Processing not possible.' );
    				return geometry;

    			}

    		}

    		//

    		const numberOfTriangles = index.count - 2;
    		const newIndices = [];

    		if ( drawMode === THREE.TriangleFanDrawMode ) {

    			// gl.TRIANGLE_FAN

    			for ( let i = 1; i <= numberOfTriangles; i ++ ) {

    				newIndices.push( index.getX( 0 ) );
    				newIndices.push( index.getX( i ) );
    				newIndices.push( index.getX( i + 1 ) );

    			}

    		} else {

    			// gl.TRIANGLE_STRIP

    			for ( let i = 0; i < numberOfTriangles; i ++ ) {

    				if ( i % 2 === 0 ) {

    					newIndices.push( index.getX( i ) );
    					newIndices.push( index.getX( i + 1 ) );
    					newIndices.push( index.getX( i + 2 ) );

    				} else {

    					newIndices.push( index.getX( i + 2 ) );
    					newIndices.push( index.getX( i + 1 ) );
    					newIndices.push( index.getX( i ) );

    				}

    			}

    		}

    		if ( ( newIndices.length / 3 ) !== numberOfTriangles ) {

    			console.error( 'THREE.BufferGeometryUtils.toTrianglesDrawMode(): Unable to generate correct amount of triangles.' );

    		}

    		// build final geometry

    		const newGeometry = geometry.clone();
    		newGeometry.setIndex( newIndices );
    		newGeometry.clearGroups();

    		return newGeometry;

    	} else {

    		console.error( 'THREE.BufferGeometryUtils.toTrianglesDrawMode(): Unknown draw mode:', drawMode );
    		return geometry;

    	}

    }

    class GLTFLoader extends THREE.Loader {

    	constructor( manager ) {

    		super( manager );

    		this.dracoLoader = null;
    		this.ktx2Loader = null;
    		this.meshoptDecoder = null;

    		this.pluginCallbacks = [];

    		this.register( function ( parser ) {

    			return new GLTFMaterialsClearcoatExtension( parser );

    		} );

    		this.register( function ( parser ) {

    			return new GLTFTextureBasisUExtension( parser );

    		} );

    		this.register( function ( parser ) {

    			return new GLTFTextureWebPExtension( parser );

    		} );

    		this.register( function ( parser ) {

    			return new GLTFTextureAVIFExtension( parser );

    		} );

    		this.register( function ( parser ) {

    			return new GLTFMaterialsSheenExtension( parser );

    		} );

    		this.register( function ( parser ) {

    			return new GLTFMaterialsTransmissionExtension( parser );

    		} );

    		this.register( function ( parser ) {

    			return new GLTFMaterialsVolumeExtension( parser );

    		} );

    		this.register( function ( parser ) {

    			return new GLTFMaterialsIorExtension( parser );

    		} );

    		this.register( function ( parser ) {

    			return new GLTFMaterialsEmissiveStrengthExtension( parser );

    		} );

    		this.register( function ( parser ) {

    			return new GLTFMaterialsSpecularExtension( parser );

    		} );

    		this.register( function ( parser ) {

    			return new GLTFMaterialsIridescenceExtension( parser );

    		} );

    		this.register( function ( parser ) {

    			return new GLTFMaterialsAnisotropyExtension( parser );

    		} );

    		this.register( function ( parser ) {

    			return new GLTFLightsExtension( parser );

    		} );

    		this.register( function ( parser ) {

    			return new GLTFMeshoptCompression( parser );

    		} );

    		this.register( function ( parser ) {

    			return new GLTFMeshGpuInstancing( parser );

    		} );

    	}

    	load( url, onLoad, onProgress, onError ) {

    		const scope = this;

    		let resourcePath;

    		if ( this.resourcePath !== '' ) {

    			resourcePath = this.resourcePath;

    		} else if ( this.path !== '' ) {

    			resourcePath = this.path;

    		} else {

    			resourcePath = THREE.LoaderUtils.extractUrlBase( url );

    		}

    		// Tells the LoadingManager to track an extra item, which resolves after
    		// the model is fully loaded. This means the count of items loaded will
    		// be incorrect, but ensures manager.onLoad() does not fire early.
    		this.manager.itemStart( url );

    		const _onError = function ( e ) {

    			if ( onError ) {

    				onError( e );

    			} else {

    				console.error( e );

    			}

    			scope.manager.itemError( url );
    			scope.manager.itemEnd( url );

    		};

    		const loader = new THREE.FileLoader( this.manager );

    		loader.setPath( this.path );
    		loader.setResponseType( 'arraybuffer' );
    		loader.setRequestHeader( this.requestHeader );
    		loader.setWithCredentials( this.withCredentials );

    		loader.load( url, function ( data ) {

    			try {

    				scope.parse( data, resourcePath, function ( gltf ) {

    					onLoad( gltf );

    					scope.manager.itemEnd( url );

    				}, _onError );

    			} catch ( e ) {

    				_onError( e );

    			}

    		}, onProgress, _onError );

    	}

    	setDRACOLoader( dracoLoader ) {

    		this.dracoLoader = dracoLoader;
    		return this;

    	}

    	setDDSLoader() {

    		throw new Error(

    			'THREE.GLTFLoader: "MSFT_texture_dds" no longer supported. Please update to "KHR_texture_basisu".'

    		);

    	}

    	setKTX2Loader( ktx2Loader ) {

    		this.ktx2Loader = ktx2Loader;
    		return this;

    	}

    	setMeshoptDecoder( meshoptDecoder ) {

    		this.meshoptDecoder = meshoptDecoder;
    		return this;

    	}

    	register( callback ) {

    		if ( this.pluginCallbacks.indexOf( callback ) === - 1 ) {

    			this.pluginCallbacks.push( callback );

    		}

    		return this;

    	}

    	unregister( callback ) {

    		if ( this.pluginCallbacks.indexOf( callback ) !== - 1 ) {

    			this.pluginCallbacks.splice( this.pluginCallbacks.indexOf( callback ), 1 );

    		}

    		return this;

    	}

    	parse( data, path, onLoad, onError ) {

    		let json;
    		const extensions = {};
    		const plugins = {};
    		const textDecoder = new TextDecoder();

    		if ( typeof data === 'string' ) {

    			json = JSON.parse( data );

    		} else if ( data instanceof ArrayBuffer ) {

    			const magic = textDecoder.decode( new Uint8Array( data, 0, 4 ) );

    			if ( magic === BINARY_EXTENSION_HEADER_MAGIC ) {

    				try {

    					extensions[ EXTENSIONS.KHR_BINARY_GLTF ] = new GLTFBinaryExtension( data );

    				} catch ( error ) {

    					if ( onError ) onError( error );
    					return;

    				}

    				json = JSON.parse( extensions[ EXTENSIONS.KHR_BINARY_GLTF ].content );

    			} else {

    				json = JSON.parse( textDecoder.decode( data ) );

    			}

    		} else {

    			json = data;

    		}

    		if ( json.asset === undefined || json.asset.version[ 0 ] < 2 ) {

    			if ( onError ) onError( new Error( 'THREE.GLTFLoader: Unsupported asset. glTF versions >=2.0 are supported.' ) );
    			return;

    		}

    		const parser = new GLTFParser( json, {

    			path: path || this.resourcePath || '',
    			crossOrigin: this.crossOrigin,
    			requestHeader: this.requestHeader,
    			manager: this.manager,
    			ktx2Loader: this.ktx2Loader,
    			meshoptDecoder: this.meshoptDecoder

    		} );

    		parser.fileLoader.setRequestHeader( this.requestHeader );

    		for ( let i = 0; i < this.pluginCallbacks.length; i ++ ) {

    			const plugin = this.pluginCallbacks[ i ]( parser );
    			plugins[ plugin.name ] = plugin;

    			// Workaround to avoid determining as unknown extension
    			// in addUnknownExtensionsToUserData().
    			// Remove this workaround if we move all the existing
    			// extension handlers to plugin system
    			extensions[ plugin.name ] = true;

    		}

    		if ( json.extensionsUsed ) {

    			for ( let i = 0; i < json.extensionsUsed.length; ++ i ) {

    				const extensionName = json.extensionsUsed[ i ];
    				const extensionsRequired = json.extensionsRequired || [];

    				switch ( extensionName ) {

    					case EXTENSIONS.KHR_MATERIALS_UNLIT:
    						extensions[ extensionName ] = new GLTFMaterialsUnlitExtension();
    						break;

    					case EXTENSIONS.KHR_DRACO_MESH_COMPRESSION:
    						extensions[ extensionName ] = new GLTFDracoMeshCompressionExtension( json, this.dracoLoader );
    						break;

    					case EXTENSIONS.KHR_TEXTURE_TRANSFORM:
    						extensions[ extensionName ] = new GLTFTextureTransformExtension();
    						break;

    					case EXTENSIONS.KHR_MESH_QUANTIZATION:
    						extensions[ extensionName ] = new GLTFMeshQuantizationExtension();
    						break;

    					default:

    						if ( extensionsRequired.indexOf( extensionName ) >= 0 && plugins[ extensionName ] === undefined ) {

    							console.warn( 'THREE.GLTFLoader: Unknown extension "' + extensionName + '".' );

    						}

    				}

    			}

    		}

    		parser.setExtensions( extensions );
    		parser.setPlugins( plugins );
    		parser.parse( onLoad, onError );

    	}

    	parseAsync( data, path ) {

    		const scope = this;

    		return new Promise( function ( resolve, reject ) {

    			scope.parse( data, path, resolve, reject );

    		} );

    	}

    }

    /* GLTFREGISTRY */

    function GLTFRegistry() {

    	let objects = {};

    	return	{

    		get: function ( key ) {

    			return objects[ key ];

    		},

    		add: function ( key, object ) {

    			objects[ key ] = object;

    		},

    		remove: function ( key ) {

    			delete objects[ key ];

    		},

    		removeAll: function () {

    			objects = {};

    		}

    	};

    }

    /*********************************/
    /********** EXTENSIONS ***********/
    /*********************************/

    const EXTENSIONS = {
    	KHR_BINARY_GLTF: 'KHR_binary_glTF',
    	KHR_DRACO_MESH_COMPRESSION: 'KHR_draco_mesh_compression',
    	KHR_LIGHTS_PUNCTUAL: 'KHR_lights_punctual',
    	KHR_MATERIALS_CLEARCOAT: 'KHR_materials_clearcoat',
    	KHR_MATERIALS_IOR: 'KHR_materials_ior',
    	KHR_MATERIALS_SHEEN: 'KHR_materials_sheen',
    	KHR_MATERIALS_SPECULAR: 'KHR_materials_specular',
    	KHR_MATERIALS_TRANSMISSION: 'KHR_materials_transmission',
    	KHR_MATERIALS_IRIDESCENCE: 'KHR_materials_iridescence',
    	KHR_MATERIALS_ANISOTROPY: 'KHR_materials_anisotropy',
    	KHR_MATERIALS_UNLIT: 'KHR_materials_unlit',
    	KHR_MATERIALS_VOLUME: 'KHR_materials_volume',
    	KHR_TEXTURE_BASISU: 'KHR_texture_basisu',
    	KHR_TEXTURE_TRANSFORM: 'KHR_texture_transform',
    	KHR_MESH_QUANTIZATION: 'KHR_mesh_quantization',
    	KHR_MATERIALS_EMISSIVE_STRENGTH: 'KHR_materials_emissive_strength',
    	EXT_TEXTURE_WEBP: 'EXT_texture_webp',
    	EXT_TEXTURE_AVIF: 'EXT_texture_avif',
    	EXT_MESHOPT_COMPRESSION: 'EXT_meshopt_compression',
    	EXT_MESH_GPU_INSTANCING: 'EXT_mesh_gpu_instancing'
    };

    /**
     * Punctual Lights Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_lights_punctual
     */
    class GLTFLightsExtension {

    	constructor( parser ) {

    		this.parser = parser;
    		this.name = EXTENSIONS.KHR_LIGHTS_PUNCTUAL;

    		// Object3D instance caches
    		this.cache = { refs: {}, uses: {} };

    	}

    	_markDefs() {

    		const parser = this.parser;
    		const nodeDefs = this.parser.json.nodes || [];

    		for ( let nodeIndex = 0, nodeLength = nodeDefs.length; nodeIndex < nodeLength; nodeIndex ++ ) {

    			const nodeDef = nodeDefs[ nodeIndex ];

    			if ( nodeDef.extensions
    					&& nodeDef.extensions[ this.name ]
    					&& nodeDef.extensions[ this.name ].light !== undefined ) {

    				parser._addNodeRef( this.cache, nodeDef.extensions[ this.name ].light );

    			}

    		}

    	}

    	_loadLight( lightIndex ) {

    		const parser = this.parser;
    		const cacheKey = 'light:' + lightIndex;
    		let dependency = parser.cache.get( cacheKey );

    		if ( dependency ) return dependency;

    		const json = parser.json;
    		const extensions = ( json.extensions && json.extensions[ this.name ] ) || {};
    		const lightDefs = extensions.lights || [];
    		const lightDef = lightDefs[ lightIndex ];
    		let lightNode;

    		const color = new THREE.Color( 0xffffff );

    		if ( lightDef.color !== undefined ) color.fromArray( lightDef.color );

    		const range = lightDef.range !== undefined ? lightDef.range : 0;

    		switch ( lightDef.type ) {

    			case 'directional':
    				lightNode = new THREE.DirectionalLight( color );
    				lightNode.target.position.set( 0, 0, - 1 );
    				lightNode.add( lightNode.target );
    				break;

    			case 'point':
    				lightNode = new THREE.PointLight( color );
    				lightNode.distance = range;
    				break;

    			case 'spot':
    				lightNode = new THREE.SpotLight( color );
    				lightNode.distance = range;
    				// Handle spotlight properties.
    				lightDef.spot = lightDef.spot || {};
    				lightDef.spot.innerConeAngle = lightDef.spot.innerConeAngle !== undefined ? lightDef.spot.innerConeAngle : 0;
    				lightDef.spot.outerConeAngle = lightDef.spot.outerConeAngle !== undefined ? lightDef.spot.outerConeAngle : Math.PI / 4.0;
    				lightNode.angle = lightDef.spot.outerConeAngle;
    				lightNode.penumbra = 1.0 - lightDef.spot.innerConeAngle / lightDef.spot.outerConeAngle;
    				lightNode.target.position.set( 0, 0, - 1 );
    				lightNode.add( lightNode.target );
    				break;

    			default:
    				throw new Error( 'THREE.GLTFLoader: Unexpected light type: ' + lightDef.type );

    		}

    		// Some lights (e.g. spot) default to a position other than the origin. Reset the position
    		// here, because node-level parsing will only override position if explicitly specified.
    		lightNode.position.set( 0, 0, 0 );

    		lightNode.decay = 2;

    		assignExtrasToUserData( lightNode, lightDef );

    		if ( lightDef.intensity !== undefined ) lightNode.intensity = lightDef.intensity;

    		lightNode.name = parser.createUniqueName( lightDef.name || ( 'light_' + lightIndex ) );

    		dependency = Promise.resolve( lightNode );

    		parser.cache.add( cacheKey, dependency );

    		return dependency;

    	}

    	getDependency( type, index ) {

    		if ( type !== 'light' ) return;

    		return this._loadLight( index );

    	}

    	createNodeAttachment( nodeIndex ) {

    		const self = this;
    		const parser = this.parser;
    		const json = parser.json;
    		const nodeDef = json.nodes[ nodeIndex ];
    		const lightDef = ( nodeDef.extensions && nodeDef.extensions[ this.name ] ) || {};
    		const lightIndex = lightDef.light;

    		if ( lightIndex === undefined ) return null;

    		return this._loadLight( lightIndex ).then( function ( light ) {

    			return parser._getNodeRef( self.cache, lightIndex, light );

    		} );

    	}

    }

    /**
     * Unlit Materials Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_materials_unlit
     */
    class GLTFMaterialsUnlitExtension {

    	constructor() {

    		this.name = EXTENSIONS.KHR_MATERIALS_UNLIT;

    	}

    	getMaterialType() {

    		return THREE.MeshBasicMaterial;

    	}

    	extendParams( materialParams, materialDef, parser ) {

    		const pending = [];

    		materialParams.color = new THREE.Color( 1.0, 1.0, 1.0 );
    		materialParams.opacity = 1.0;

    		const metallicRoughness = materialDef.pbrMetallicRoughness;

    		if ( metallicRoughness ) {

    			if ( Array.isArray( metallicRoughness.baseColorFactor ) ) {

    				const array = metallicRoughness.baseColorFactor;

    				materialParams.color.fromArray( array );
    				materialParams.opacity = array[ 3 ];

    			}

    			if ( metallicRoughness.baseColorTexture !== undefined ) {

    				pending.push( parser.assignTexture( materialParams, 'map', metallicRoughness.baseColorTexture, THREE.SRGBColorSpace ) );

    			}

    		}

    		return Promise.all( pending );

    	}

    }

    /**
     * Materials Emissive Strength Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/blob/5768b3ce0ef32bc39cdf1bef10b948586635ead3/extensions/2.0/Khronos/KHR_materials_emissive_strength/README.md
     */
    class GLTFMaterialsEmissiveStrengthExtension {

    	constructor( parser ) {

    		this.parser = parser;
    		this.name = EXTENSIONS.KHR_MATERIALS_EMISSIVE_STRENGTH;

    	}

    	extendMaterialParams( materialIndex, materialParams ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) {

    			return Promise.resolve();

    		}

    		const emissiveStrength = materialDef.extensions[ this.name ].emissiveStrength;

    		if ( emissiveStrength !== undefined ) {

    			materialParams.emissiveIntensity = emissiveStrength;

    		}

    		return Promise.resolve();

    	}

    }

    /**
     * Clearcoat Materials Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_materials_clearcoat
     */
    class GLTFMaterialsClearcoatExtension {

    	constructor( parser ) {

    		this.parser = parser;
    		this.name = EXTENSIONS.KHR_MATERIALS_CLEARCOAT;

    	}

    	getMaterialType( materialIndex ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) return null;

    		return THREE.MeshPhysicalMaterial;

    	}

    	extendMaterialParams( materialIndex, materialParams ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) {

    			return Promise.resolve();

    		}

    		const pending = [];

    		const extension = materialDef.extensions[ this.name ];

    		if ( extension.clearcoatFactor !== undefined ) {

    			materialParams.clearcoat = extension.clearcoatFactor;

    		}

    		if ( extension.clearcoatTexture !== undefined ) {

    			pending.push( parser.assignTexture( materialParams, 'clearcoatMap', extension.clearcoatTexture ) );

    		}

    		if ( extension.clearcoatRoughnessFactor !== undefined ) {

    			materialParams.clearcoatRoughness = extension.clearcoatRoughnessFactor;

    		}

    		if ( extension.clearcoatRoughnessTexture !== undefined ) {

    			pending.push( parser.assignTexture( materialParams, 'clearcoatRoughnessMap', extension.clearcoatRoughnessTexture ) );

    		}

    		if ( extension.clearcoatNormalTexture !== undefined ) {

    			pending.push( parser.assignTexture( materialParams, 'clearcoatNormalMap', extension.clearcoatNormalTexture ) );

    			if ( extension.clearcoatNormalTexture.scale !== undefined ) {

    				const scale = extension.clearcoatNormalTexture.scale;

    				materialParams.clearcoatNormalScale = new THREE.Vector2( scale, scale );

    			}

    		}

    		return Promise.all( pending );

    	}

    }

    /**
     * Iridescence Materials Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_materials_iridescence
     */
    class GLTFMaterialsIridescenceExtension {

    	constructor( parser ) {

    		this.parser = parser;
    		this.name = EXTENSIONS.KHR_MATERIALS_IRIDESCENCE;

    	}

    	getMaterialType( materialIndex ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) return null;

    		return THREE.MeshPhysicalMaterial;

    	}

    	extendMaterialParams( materialIndex, materialParams ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) {

    			return Promise.resolve();

    		}

    		const pending = [];

    		const extension = materialDef.extensions[ this.name ];

    		if ( extension.iridescenceFactor !== undefined ) {

    			materialParams.iridescence = extension.iridescenceFactor;

    		}

    		if ( extension.iridescenceTexture !== undefined ) {

    			pending.push( parser.assignTexture( materialParams, 'iridescenceMap', extension.iridescenceTexture ) );

    		}

    		if ( extension.iridescenceIor !== undefined ) {

    			materialParams.iridescenceIOR = extension.iridescenceIor;

    		}

    		if ( materialParams.iridescenceThicknessRange === undefined ) {

    			materialParams.iridescenceThicknessRange = [ 100, 400 ];

    		}

    		if ( extension.iridescenceThicknessMinimum !== undefined ) {

    			materialParams.iridescenceThicknessRange[ 0 ] = extension.iridescenceThicknessMinimum;

    		}

    		if ( extension.iridescenceThicknessMaximum !== undefined ) {

    			materialParams.iridescenceThicknessRange[ 1 ] = extension.iridescenceThicknessMaximum;

    		}

    		if ( extension.iridescenceThicknessTexture !== undefined ) {

    			pending.push( parser.assignTexture( materialParams, 'iridescenceThicknessMap', extension.iridescenceThicknessTexture ) );

    		}

    		return Promise.all( pending );

    	}

    }

    /**
     * Sheen Materials Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/main/extensions/2.0/Khronos/KHR_materials_sheen
     */
    class GLTFMaterialsSheenExtension {

    	constructor( parser ) {

    		this.parser = parser;
    		this.name = EXTENSIONS.KHR_MATERIALS_SHEEN;

    	}

    	getMaterialType( materialIndex ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) return null;

    		return THREE.MeshPhysicalMaterial;

    	}

    	extendMaterialParams( materialIndex, materialParams ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) {

    			return Promise.resolve();

    		}

    		const pending = [];

    		materialParams.sheenColor = new THREE.Color( 0, 0, 0 );
    		materialParams.sheenRoughness = 0;
    		materialParams.sheen = 1;

    		const extension = materialDef.extensions[ this.name ];

    		if ( extension.sheenColorFactor !== undefined ) {

    			materialParams.sheenColor.fromArray( extension.sheenColorFactor );

    		}

    		if ( extension.sheenRoughnessFactor !== undefined ) {

    			materialParams.sheenRoughness = extension.sheenRoughnessFactor;

    		}

    		if ( extension.sheenColorTexture !== undefined ) {

    			pending.push( parser.assignTexture( materialParams, 'sheenColorMap', extension.sheenColorTexture, THREE.SRGBColorSpace ) );

    		}

    		if ( extension.sheenRoughnessTexture !== undefined ) {

    			pending.push( parser.assignTexture( materialParams, 'sheenRoughnessMap', extension.sheenRoughnessTexture ) );

    		}

    		return Promise.all( pending );

    	}

    }

    /**
     * Transmission Materials Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_materials_transmission
     * Draft: https://github.com/KhronosGroup/glTF/pull/1698
     */
    class GLTFMaterialsTransmissionExtension {

    	constructor( parser ) {

    		this.parser = parser;
    		this.name = EXTENSIONS.KHR_MATERIALS_TRANSMISSION;

    	}

    	getMaterialType( materialIndex ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) return null;

    		return THREE.MeshPhysicalMaterial;

    	}

    	extendMaterialParams( materialIndex, materialParams ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) {

    			return Promise.resolve();

    		}

    		const pending = [];

    		const extension = materialDef.extensions[ this.name ];

    		if ( extension.transmissionFactor !== undefined ) {

    			materialParams.transmission = extension.transmissionFactor;

    		}

    		if ( extension.transmissionTexture !== undefined ) {

    			pending.push( parser.assignTexture( materialParams, 'transmissionMap', extension.transmissionTexture ) );

    		}

    		return Promise.all( pending );

    	}

    }

    /**
     * Materials Volume Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_materials_volume
     */
    class GLTFMaterialsVolumeExtension {

    	constructor( parser ) {

    		this.parser = parser;
    		this.name = EXTENSIONS.KHR_MATERIALS_VOLUME;

    	}

    	getMaterialType( materialIndex ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) return null;

    		return THREE.MeshPhysicalMaterial;

    	}

    	extendMaterialParams( materialIndex, materialParams ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) {

    			return Promise.resolve();

    		}

    		const pending = [];

    		const extension = materialDef.extensions[ this.name ];

    		materialParams.thickness = extension.thicknessFactor !== undefined ? extension.thicknessFactor : 0;

    		if ( extension.thicknessTexture !== undefined ) {

    			pending.push( parser.assignTexture( materialParams, 'thicknessMap', extension.thicknessTexture ) );

    		}

    		materialParams.attenuationDistance = extension.attenuationDistance || Infinity;

    		const colorArray = extension.attenuationColor || [ 1, 1, 1 ];
    		materialParams.attenuationColor = new THREE.Color( colorArray[ 0 ], colorArray[ 1 ], colorArray[ 2 ] );

    		return Promise.all( pending );

    	}

    }

    /**
     * Materials ior Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_materials_ior
     */
    class GLTFMaterialsIorExtension {

    	constructor( parser ) {

    		this.parser = parser;
    		this.name = EXTENSIONS.KHR_MATERIALS_IOR;

    	}

    	getMaterialType( materialIndex ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) return null;

    		return THREE.MeshPhysicalMaterial;

    	}

    	extendMaterialParams( materialIndex, materialParams ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) {

    			return Promise.resolve();

    		}

    		const extension = materialDef.extensions[ this.name ];

    		materialParams.ior = extension.ior !== undefined ? extension.ior : 1.5;

    		return Promise.resolve();

    	}

    }

    /**
     * Materials specular Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_materials_specular
     */
    class GLTFMaterialsSpecularExtension {

    	constructor( parser ) {

    		this.parser = parser;
    		this.name = EXTENSIONS.KHR_MATERIALS_SPECULAR;

    	}

    	getMaterialType( materialIndex ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) return null;

    		return THREE.MeshPhysicalMaterial;

    	}

    	extendMaterialParams( materialIndex, materialParams ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) {

    			return Promise.resolve();

    		}

    		const pending = [];

    		const extension = materialDef.extensions[ this.name ];

    		materialParams.specularIntensity = extension.specularFactor !== undefined ? extension.specularFactor : 1.0;

    		if ( extension.specularTexture !== undefined ) {

    			pending.push( parser.assignTexture( materialParams, 'specularIntensityMap', extension.specularTexture ) );

    		}

    		const colorArray = extension.specularColorFactor || [ 1, 1, 1 ];
    		materialParams.specularColor = new THREE.Color( colorArray[ 0 ], colorArray[ 1 ], colorArray[ 2 ] );

    		if ( extension.specularColorTexture !== undefined ) {

    			pending.push( parser.assignTexture( materialParams, 'specularColorMap', extension.specularColorTexture, THREE.SRGBColorSpace ) );

    		}

    		return Promise.all( pending );

    	}

    }

    /**
     * Materials anisotropy Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_materials_anisotropy
     */
    class GLTFMaterialsAnisotropyExtension {

    	constructor( parser ) {

    		this.parser = parser;
    		this.name = EXTENSIONS.KHR_MATERIALS_ANISOTROPY;

    	}

    	getMaterialType( materialIndex ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) return null;

    		return THREE.MeshPhysicalMaterial;

    	}

    	extendMaterialParams( materialIndex, materialParams ) {

    		const parser = this.parser;
    		const materialDef = parser.json.materials[ materialIndex ];

    		if ( ! materialDef.extensions || ! materialDef.extensions[ this.name ] ) {

    			return Promise.resolve();

    		}

    		const pending = [];

    		const extension = materialDef.extensions[ this.name ];

    		if ( extension.anisotropyStrength !== undefined ) {

    			materialParams.anisotropy = extension.anisotropyStrength;

    		}

    		if ( extension.anisotropyRotation !== undefined ) {

    			materialParams.anisotropyRotation = extension.anisotropyRotation;

    		}

    		if ( extension.anisotropyTexture !== undefined ) {

    			pending.push( parser.assignTexture( materialParams, 'anisotropyMap', extension.anisotropyTexture ) );

    		}

    		return Promise.all( pending );

    	}

    }

    /**
     * BasisU Texture Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_texture_basisu
     */
    class GLTFTextureBasisUExtension {

    	constructor( parser ) {

    		this.parser = parser;
    		this.name = EXTENSIONS.KHR_TEXTURE_BASISU;

    	}

    	loadTexture( textureIndex ) {

    		const parser = this.parser;
    		const json = parser.json;

    		const textureDef = json.textures[ textureIndex ];

    		if ( ! textureDef.extensions || ! textureDef.extensions[ this.name ] ) {

    			return null;

    		}

    		const extension = textureDef.extensions[ this.name ];
    		const loader = parser.options.ktx2Loader;

    		if ( ! loader ) {

    			if ( json.extensionsRequired && json.extensionsRequired.indexOf( this.name ) >= 0 ) {

    				throw new Error( 'THREE.GLTFLoader: setKTX2Loader must be called before loading KTX2 textures' );

    			} else {

    				// Assumes that the extension is optional and that a fallback texture is present
    				return null;

    			}

    		}

    		return parser.loadTextureImage( textureIndex, extension.source, loader );

    	}

    }

    /**
     * WebP Texture Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Vendor/EXT_texture_webp
     */
    class GLTFTextureWebPExtension {

    	constructor( parser ) {

    		this.parser = parser;
    		this.name = EXTENSIONS.EXT_TEXTURE_WEBP;
    		this.isSupported = null;

    	}

    	loadTexture( textureIndex ) {

    		const name = this.name;
    		const parser = this.parser;
    		const json = parser.json;

    		const textureDef = json.textures[ textureIndex ];

    		if ( ! textureDef.extensions || ! textureDef.extensions[ name ] ) {

    			return null;

    		}

    		const extension = textureDef.extensions[ name ];
    		const source = json.images[ extension.source ];

    		let loader = parser.textureLoader;
    		if ( source.uri ) {

    			const handler = parser.options.manager.getHandler( source.uri );
    			if ( handler !== null ) loader = handler;

    		}

    		return this.detectSupport().then( function ( isSupported ) {

    			if ( isSupported ) return parser.loadTextureImage( textureIndex, extension.source, loader );

    			if ( json.extensionsRequired && json.extensionsRequired.indexOf( name ) >= 0 ) {

    				throw new Error( 'THREE.GLTFLoader: WebP required by asset but unsupported.' );

    			}

    			// Fall back to PNG or JPEG.
    			return parser.loadTexture( textureIndex );

    		} );

    	}

    	detectSupport() {

    		if ( ! this.isSupported ) {

    			this.isSupported = new Promise( function ( resolve ) {

    				const image = new Image();

    				// Lossy test image. Support for lossy images doesn't guarantee support for all
    				// WebP images, unfortunately.
    				image.src = 'data:image/webp;base64,UklGRiIAAABXRUJQVlA4IBYAAAAwAQCdASoBAAEADsD+JaQAA3AAAAAA';

    				image.onload = image.onerror = function () {

    					resolve( image.height === 1 );

    				};

    			} );

    		}

    		return this.isSupported;

    	}

    }

    /**
     * AVIF Texture Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Vendor/EXT_texture_avif
     */
    class GLTFTextureAVIFExtension {

    	constructor( parser ) {

    		this.parser = parser;
    		this.name = EXTENSIONS.EXT_TEXTURE_AVIF;
    		this.isSupported = null;

    	}

    	loadTexture( textureIndex ) {

    		const name = this.name;
    		const parser = this.parser;
    		const json = parser.json;

    		const textureDef = json.textures[ textureIndex ];

    		if ( ! textureDef.extensions || ! textureDef.extensions[ name ] ) {

    			return null;

    		}

    		const extension = textureDef.extensions[ name ];
    		const source = json.images[ extension.source ];

    		let loader = parser.textureLoader;
    		if ( source.uri ) {

    			const handler = parser.options.manager.getHandler( source.uri );
    			if ( handler !== null ) loader = handler;

    		}

    		return this.detectSupport().then( function ( isSupported ) {

    			if ( isSupported ) return parser.loadTextureImage( textureIndex, extension.source, loader );

    			if ( json.extensionsRequired && json.extensionsRequired.indexOf( name ) >= 0 ) {

    				throw new Error( 'THREE.GLTFLoader: AVIF required by asset but unsupported.' );

    			}

    			// Fall back to PNG or JPEG.
    			return parser.loadTexture( textureIndex );

    		} );

    	}

    	detectSupport() {

    		if ( ! this.isSupported ) {

    			this.isSupported = new Promise( function ( resolve ) {

    				const image = new Image();

    				// Lossy test image.
    				image.src = 'data:image/avif;base64,AAAAIGZ0eXBhdmlmAAAAAGF2aWZtaWYxbWlhZk1BMUIAAADybWV0YQAAAAAAAAAoaGRscgAAAAAAAAAAcGljdAAAAAAAAAAAAAAAAGxpYmF2aWYAAAAADnBpdG0AAAAAAAEAAAAeaWxvYwAAAABEAAABAAEAAAABAAABGgAAABcAAAAoaWluZgAAAAAAAQAAABppbmZlAgAAAAABAABhdjAxQ29sb3IAAAAAamlwcnAAAABLaXBjbwAAABRpc3BlAAAAAAAAAAEAAAABAAAAEHBpeGkAAAAAAwgICAAAAAxhdjFDgQAMAAAAABNjb2xybmNseAACAAIABoAAAAAXaXBtYQAAAAAAAAABAAEEAQKDBAAAAB9tZGF0EgAKCBgABogQEDQgMgkQAAAAB8dSLfI=';
    				image.onload = image.onerror = function () {

    					resolve( image.height === 1 );

    				};

    			} );

    		}

    		return this.isSupported;

    	}

    }

    /**
     * meshopt BufferView Compression Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Vendor/EXT_meshopt_compression
     */
    class GLTFMeshoptCompression {

    	constructor( parser ) {

    		this.name = EXTENSIONS.EXT_MESHOPT_COMPRESSION;
    		this.parser = parser;

    	}

    	loadBufferView( index ) {

    		const json = this.parser.json;
    		const bufferView = json.bufferViews[ index ];

    		if ( bufferView.extensions && bufferView.extensions[ this.name ] ) {

    			const extensionDef = bufferView.extensions[ this.name ];

    			const buffer = this.parser.getDependency( 'buffer', extensionDef.buffer );
    			const decoder = this.parser.options.meshoptDecoder;

    			if ( ! decoder || ! decoder.supported ) {

    				if ( json.extensionsRequired && json.extensionsRequired.indexOf( this.name ) >= 0 ) {

    					throw new Error( 'THREE.GLTFLoader: setMeshoptDecoder must be called before loading compressed files' );

    				} else {

    					// Assumes that the extension is optional and that fallback buffer data is present
    					return null;

    				}

    			}

    			return buffer.then( function ( res ) {

    				const byteOffset = extensionDef.byteOffset || 0;
    				const byteLength = extensionDef.byteLength || 0;

    				const count = extensionDef.count;
    				const stride = extensionDef.byteStride;

    				const source = new Uint8Array( res, byteOffset, byteLength );

    				if ( decoder.decodeGltfBufferAsync ) {

    					return decoder.decodeGltfBufferAsync( count, stride, source, extensionDef.mode, extensionDef.filter ).then( function ( res ) {

    						return res.buffer;

    					} );

    				} else {

    					// Support for MeshoptDecoder 0.18 or earlier, without decodeGltfBufferAsync
    					return decoder.ready.then( function () {

    						const result = new ArrayBuffer( count * stride );
    						decoder.decodeGltfBuffer( new Uint8Array( result ), count, stride, source, extensionDef.mode, extensionDef.filter );
    						return result;

    					} );

    				}

    			} );

    		} else {

    			return null;

    		}

    	}

    }

    /**
     * GPU Instancing Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Vendor/EXT_mesh_gpu_instancing
     *
     */
    class GLTFMeshGpuInstancing {

    	constructor( parser ) {

    		this.name = EXTENSIONS.EXT_MESH_GPU_INSTANCING;
    		this.parser = parser;

    	}

    	createNodeMesh( nodeIndex ) {

    		const json = this.parser.json;
    		const nodeDef = json.nodes[ nodeIndex ];

    		if ( ! nodeDef.extensions || ! nodeDef.extensions[ this.name ] ||
    			nodeDef.mesh === undefined ) {

    			return null;

    		}

    		const meshDef = json.meshes[ nodeDef.mesh ];

    		// No Points or Lines + Instancing support yet

    		for ( const primitive of meshDef.primitives ) {

    			if ( primitive.mode !== WEBGL_CONSTANTS.TRIANGLES &&
    				 primitive.mode !== WEBGL_CONSTANTS.TRIANGLE_STRIP &&
    				 primitive.mode !== WEBGL_CONSTANTS.TRIANGLE_FAN &&
    				 primitive.mode !== undefined ) {

    				return null;

    			}

    		}

    		const extensionDef = nodeDef.extensions[ this.name ];
    		const attributesDef = extensionDef.attributes;

    		// @TODO: Can we support InstancedMesh + SkinnedMesh?

    		const pending = [];
    		const attributes = {};

    		for ( const key in attributesDef ) {

    			pending.push( this.parser.getDependency( 'accessor', attributesDef[ key ] ).then( accessor => {

    				attributes[ key ] = accessor;
    				return attributes[ key ];

    			} ) );

    		}

    		if ( pending.length < 1 ) {

    			return null;

    		}

    		pending.push( this.parser.createNodeMesh( nodeIndex ) );

    		return Promise.all( pending ).then( results => {

    			const nodeObject = results.pop();
    			const meshes = nodeObject.isGroup ? nodeObject.children : [ nodeObject ];
    			const count = results[ 0 ].count; // All attribute counts should be same
    			const instancedMeshes = [];

    			for ( const mesh of meshes ) {

    				// Temporal variables
    				const m = new THREE.Matrix4();
    				const p = new THREE.Vector3();
    				const q = new THREE.Quaternion();
    				const s = new THREE.Vector3( 1, 1, 1 );

    				const instancedMesh = new THREE.InstancedMesh( mesh.geometry, mesh.material, count );

    				for ( let i = 0; i < count; i ++ ) {

    					if ( attributes.TRANSLATION ) {

    						p.fromBufferAttribute( attributes.TRANSLATION, i );

    					}

    					if ( attributes.ROTATION ) {

    						q.fromBufferAttribute( attributes.ROTATION, i );

    					}

    					if ( attributes.SCALE ) {

    						s.fromBufferAttribute( attributes.SCALE, i );

    					}

    					instancedMesh.setMatrixAt( i, m.compose( p, q, s ) );

    				}

    				// Add instance attributes to the geometry, excluding TRS.
    				for ( const attributeName in attributes ) {

    					if ( attributeName !== 'TRANSLATION' &&
    						 attributeName !== 'ROTATION' &&
    						 attributeName !== 'SCALE' ) {

    						mesh.geometry.setAttribute( attributeName, attributes[ attributeName ] );

    					}

    				}

    				// Just in case
    				THREE.Object3D.prototype.copy.call( instancedMesh, mesh );

    				this.parser.assignFinalMaterial( instancedMesh );

    				instancedMeshes.push( instancedMesh );

    			}

    			if ( nodeObject.isGroup ) {

    				nodeObject.clear();

    				nodeObject.add( ... instancedMeshes );

    				return nodeObject;

    			}

    			return instancedMeshes[ 0 ];

    		} );

    	}

    }

    /* BINARY EXTENSION */
    const BINARY_EXTENSION_HEADER_MAGIC = 'glTF';
    const BINARY_EXTENSION_HEADER_LENGTH = 12;
    const BINARY_EXTENSION_CHUNK_TYPES = { JSON: 0x4E4F534A, BIN: 0x004E4942 };

    class GLTFBinaryExtension {

    	constructor( data ) {

    		this.name = EXTENSIONS.KHR_BINARY_GLTF;
    		this.content = null;
    		this.body = null;

    		const headerView = new DataView( data, 0, BINARY_EXTENSION_HEADER_LENGTH );
    		const textDecoder = new TextDecoder();

    		this.header = {
    			magic: textDecoder.decode( new Uint8Array( data.slice( 0, 4 ) ) ),
    			version: headerView.getUint32( 4, true ),
    			length: headerView.getUint32( 8, true )
    		};

    		if ( this.header.magic !== BINARY_EXTENSION_HEADER_MAGIC ) {

    			throw new Error( 'THREE.GLTFLoader: Unsupported glTF-Binary header.' );

    		} else if ( this.header.version < 2.0 ) {

    			throw new Error( 'THREE.GLTFLoader: Legacy binary file detected.' );

    		}

    		const chunkContentsLength = this.header.length - BINARY_EXTENSION_HEADER_LENGTH;
    		const chunkView = new DataView( data, BINARY_EXTENSION_HEADER_LENGTH );
    		let chunkIndex = 0;

    		while ( chunkIndex < chunkContentsLength ) {

    			const chunkLength = chunkView.getUint32( chunkIndex, true );
    			chunkIndex += 4;

    			const chunkType = chunkView.getUint32( chunkIndex, true );
    			chunkIndex += 4;

    			if ( chunkType === BINARY_EXTENSION_CHUNK_TYPES.JSON ) {

    				const contentArray = new Uint8Array( data, BINARY_EXTENSION_HEADER_LENGTH + chunkIndex, chunkLength );
    				this.content = textDecoder.decode( contentArray );

    			} else if ( chunkType === BINARY_EXTENSION_CHUNK_TYPES.BIN ) {

    				const byteOffset = BINARY_EXTENSION_HEADER_LENGTH + chunkIndex;
    				this.body = data.slice( byteOffset, byteOffset + chunkLength );

    			}

    			// Clients must ignore chunks with unknown types.

    			chunkIndex += chunkLength;

    		}

    		if ( this.content === null ) {

    			throw new Error( 'THREE.GLTFLoader: JSON content not found.' );

    		}

    	}

    }

    /**
     * DRACO Mesh Compression Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_draco_mesh_compression
     */
    class GLTFDracoMeshCompressionExtension {

    	constructor( json, dracoLoader ) {

    		if ( ! dracoLoader ) {

    			throw new Error( 'THREE.GLTFLoader: No DRACOLoader instance provided.' );

    		}

    		this.name = EXTENSIONS.KHR_DRACO_MESH_COMPRESSION;
    		this.json = json;
    		this.dracoLoader = dracoLoader;
    		this.dracoLoader.preload();

    	}

    	decodePrimitive( primitive, parser ) {

    		const json = this.json;
    		const dracoLoader = this.dracoLoader;
    		const bufferViewIndex = primitive.extensions[ this.name ].bufferView;
    		const gltfAttributeMap = primitive.extensions[ this.name ].attributes;
    		const threeAttributeMap = {};
    		const attributeNormalizedMap = {};
    		const attributeTypeMap = {};

    		for ( const attributeName in gltfAttributeMap ) {

    			const threeAttributeName = ATTRIBUTES[ attributeName ] || attributeName.toLowerCase();

    			threeAttributeMap[ threeAttributeName ] = gltfAttributeMap[ attributeName ];

    		}

    		for ( const attributeName in primitive.attributes ) {

    			const threeAttributeName = ATTRIBUTES[ attributeName ] || attributeName.toLowerCase();

    			if ( gltfAttributeMap[ attributeName ] !== undefined ) {

    				const accessorDef = json.accessors[ primitive.attributes[ attributeName ] ];
    				const componentType = WEBGL_COMPONENT_TYPES[ accessorDef.componentType ];

    				attributeTypeMap[ threeAttributeName ] = componentType.name;
    				attributeNormalizedMap[ threeAttributeName ] = accessorDef.normalized === true;

    			}

    		}

    		return parser.getDependency( 'bufferView', bufferViewIndex ).then( function ( bufferView ) {

    			return new Promise( function ( resolve ) {

    				dracoLoader.decodeDracoFile( bufferView, function ( geometry ) {

    					for ( const attributeName in geometry.attributes ) {

    						const attribute = geometry.attributes[ attributeName ];
    						const normalized = attributeNormalizedMap[ attributeName ];

    						if ( normalized !== undefined ) attribute.normalized = normalized;

    					}

    					resolve( geometry );

    				}, threeAttributeMap, attributeTypeMap );

    			} );

    		} );

    	}

    }

    /**
     * Texture Transform Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_texture_transform
     */
    class GLTFTextureTransformExtension {

    	constructor() {

    		this.name = EXTENSIONS.KHR_TEXTURE_TRANSFORM;

    	}

    	extendTexture( texture, transform ) {

    		if ( ( transform.texCoord === undefined || transform.texCoord === texture.channel )
    			&& transform.offset === undefined
    			&& transform.rotation === undefined
    			&& transform.scale === undefined ) {

    			// See https://github.com/mrdoob/three.js/issues/21819.
    			return texture;

    		}

    		texture = texture.clone();

    		if ( transform.texCoord !== undefined ) {

    			texture.channel = transform.texCoord;

    		}

    		if ( transform.offset !== undefined ) {

    			texture.offset.fromArray( transform.offset );

    		}

    		if ( transform.rotation !== undefined ) {

    			texture.rotation = transform.rotation;

    		}

    		if ( transform.scale !== undefined ) {

    			texture.repeat.fromArray( transform.scale );

    		}

    		texture.needsUpdate = true;

    		return texture;

    	}

    }

    /**
     * Mesh Quantization Extension
     *
     * Specification: https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_mesh_quantization
     */
    class GLTFMeshQuantizationExtension {

    	constructor() {

    		this.name = EXTENSIONS.KHR_MESH_QUANTIZATION;

    	}

    }

    /*********************************/
    /********** INTERPOLATION ********/
    /*********************************/

    // Spline Interpolation
    // Specification: https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#appendix-c-spline-interpolation
    class GLTFCubicSplineInterpolant extends THREE.Interpolant {

    	constructor( parameterPositions, sampleValues, sampleSize, resultBuffer ) {

    		super( parameterPositions, sampleValues, sampleSize, resultBuffer );

    	}

    	copySampleValue_( index ) {

    		// Copies a sample value to the result buffer. See description of glTF
    		// CUBICSPLINE values layout in interpolate_() function below.

    		const result = this.resultBuffer,
    			values = this.sampleValues,
    			valueSize = this.valueSize,
    			offset = index * valueSize * 3 + valueSize;

    		for ( let i = 0; i !== valueSize; i ++ ) {

    			result[ i ] = values[ offset + i ];

    		}

    		return result;

    	}

    	interpolate_( i1, t0, t, t1 ) {

    		const result = this.resultBuffer;
    		const values = this.sampleValues;
    		const stride = this.valueSize;

    		const stride2 = stride * 2;
    		const stride3 = stride * 3;

    		const td = t1 - t0;

    		const p = ( t - t0 ) / td;
    		const pp = p * p;
    		const ppp = pp * p;

    		const offset1 = i1 * stride3;
    		const offset0 = offset1 - stride3;

    		const s2 = - 2 * ppp + 3 * pp;
    		const s3 = ppp - pp;
    		const s0 = 1 - s2;
    		const s1 = s3 - pp + p;

    		// Layout of keyframe output values for CUBICSPLINE animations:
    		//   [ inTangent_1, splineVertex_1, outTangent_1, inTangent_2, splineVertex_2, ... ]
    		for ( let i = 0; i !== stride; i ++ ) {

    			const p0 = values[ offset0 + i + stride ]; // splineVertex_k
    			const m0 = values[ offset0 + i + stride2 ] * td; // outTangent_k * (t_k+1 - t_k)
    			const p1 = values[ offset1 + i + stride ]; // splineVertex_k+1
    			const m1 = values[ offset1 + i ] * td; // inTangent_k+1 * (t_k+1 - t_k)

    			result[ i ] = s0 * p0 + s1 * m0 + s2 * p1 + s3 * m1;

    		}

    		return result;

    	}

    }

    const _q = new THREE.Quaternion();

    class GLTFCubicSplineQuaternionInterpolant extends GLTFCubicSplineInterpolant {

    	interpolate_( i1, t0, t, t1 ) {

    		const result = super.interpolate_( i1, t0, t, t1 );

    		_q.fromArray( result ).normalize().toArray( result );

    		return result;

    	}

    }


    /*********************************/
    /********** INTERNALS ************/
    /*********************************/

    /* CONSTANTS */

    const WEBGL_CONSTANTS = {
    	FLOAT: 5126,
    	//FLOAT_MAT2: 35674,
    	FLOAT_MAT3: 35675,
    	FLOAT_MAT4: 35676,
    	FLOAT_VEC2: 35664,
    	FLOAT_VEC3: 35665,
    	FLOAT_VEC4: 35666,
    	LINEAR: 9729,
    	REPEAT: 10497,
    	SAMPLER_2D: 35678,
    	POINTS: 0,
    	LINES: 1,
    	LINE_LOOP: 2,
    	LINE_STRIP: 3,
    	TRIANGLES: 4,
    	TRIANGLE_STRIP: 5,
    	TRIANGLE_FAN: 6,
    	UNSIGNED_BYTE: 5121,
    	UNSIGNED_SHORT: 5123
    };

    const WEBGL_COMPONENT_TYPES = {
    	5120: Int8Array,
    	5121: Uint8Array,
    	5122: Int16Array,
    	5123: Uint16Array,
    	5125: Uint32Array,
    	5126: Float32Array
    };

    const WEBGL_FILTERS = {
    	9728: THREE.NearestFilter,
    	9729: THREE.LinearFilter,
    	9984: THREE.NearestMipmapNearestFilter,
    	9985: THREE.LinearMipmapNearestFilter,
    	9986: THREE.NearestMipmapLinearFilter,
    	9987: THREE.LinearMipmapLinearFilter
    };

    const WEBGL_WRAPPINGS = {
    	33071: THREE.ClampToEdgeWrapping,
    	33648: THREE.MirroredRepeatWrapping,
    	10497: THREE.RepeatWrapping
    };

    const WEBGL_TYPE_SIZES = {
    	'SCALAR': 1,
    	'VEC2': 2,
    	'VEC3': 3,
    	'VEC4': 4,
    	'MAT2': 4,
    	'MAT3': 9,
    	'MAT4': 16
    };

    const ATTRIBUTES = {
    	POSITION: 'position',
    	NORMAL: 'normal',
    	TANGENT: 'tangent',
    	TEXCOORD_0: 'uv',
    	TEXCOORD_1: 'uv1',
    	TEXCOORD_2: 'uv2',
    	TEXCOORD_3: 'uv3',
    	COLOR_0: 'color',
    	WEIGHTS_0: 'skinWeight',
    	JOINTS_0: 'skinIndex',
    };

    const PATH_PROPERTIES = {
    	scale: 'scale',
    	translation: 'position',
    	rotation: 'quaternion',
    	weights: 'morphTargetInfluences'
    };

    const INTERPOLATION = {
    	CUBICSPLINE: undefined, // We use a custom interpolant (GLTFCubicSplineInterpolation) for CUBICSPLINE tracks. Each
    		                        // keyframe track will be initialized with a default interpolation type, then modified.
    	LINEAR: THREE.InterpolateLinear,
    	STEP: THREE.InterpolateDiscrete
    };

    const ALPHA_MODES = {
    	OPAQUE: 'OPAQUE',
    	MASK: 'MASK',
    	BLEND: 'BLEND'
    };

    /**
     * Specification: https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#default-material
     */
    function createDefaultMaterial( cache ) {

    	if ( cache[ 'DefaultMaterial' ] === undefined ) {

    		cache[ 'DefaultMaterial' ] = new THREE.MeshStandardMaterial( {
    			color: 0xFFFFFF,
    			emissive: 0x000000,
    			metalness: 1,
    			roughness: 1,
    			transparent: false,
    			depthTest: true,
    			side: THREE.FrontSide
    		} );

    	}

    	return cache[ 'DefaultMaterial' ];

    }

    function addUnknownExtensionsToUserData( knownExtensions, object, objectDef ) {

    	// Add unknown glTF extensions to an object's userData.

    	for ( const name in objectDef.extensions ) {

    		if ( knownExtensions[ name ] === undefined ) {

    			object.userData.gltfExtensions = object.userData.gltfExtensions || {};
    			object.userData.gltfExtensions[ name ] = objectDef.extensions[ name ];

    		}

    	}

    }

    /**
     * @param {Object3D|Material|BufferGeometry} object
     * @param {GLTF.definition} gltfDef
     */
    function assignExtrasToUserData( object, gltfDef ) {

    	if ( gltfDef.extras !== undefined ) {

    		if ( typeof gltfDef.extras === 'object' ) {

    			Object.assign( object.userData, gltfDef.extras );

    		} else {

    			console.warn( 'THREE.GLTFLoader: Ignoring primitive type .extras, ' + gltfDef.extras );

    		}

    	}

    }

    /**
     * Specification: https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#morph-targets
     *
     * @param {BufferGeometry} geometry
     * @param {Array<GLTF.Target>} targets
     * @param {GLTFParser} parser
     * @return {Promise<BufferGeometry>}
     */
    function addMorphTargets( geometry, targets, parser ) {

    	let hasMorphPosition = false;
    	let hasMorphNormal = false;
    	let hasMorphColor = false;

    	for ( let i = 0, il = targets.length; i < il; i ++ ) {

    		const target = targets[ i ];

    		if ( target.POSITION !== undefined ) hasMorphPosition = true;
    		if ( target.NORMAL !== undefined ) hasMorphNormal = true;
    		if ( target.COLOR_0 !== undefined ) hasMorphColor = true;

    		if ( hasMorphPosition && hasMorphNormal && hasMorphColor ) break;

    	}

    	if ( ! hasMorphPosition && ! hasMorphNormal && ! hasMorphColor ) return Promise.resolve( geometry );

    	const pendingPositionAccessors = [];
    	const pendingNormalAccessors = [];
    	const pendingColorAccessors = [];

    	for ( let i = 0, il = targets.length; i < il; i ++ ) {

    		const target = targets[ i ];

    		if ( hasMorphPosition ) {

    			const pendingAccessor = target.POSITION !== undefined
    				? parser.getDependency( 'accessor', target.POSITION )
    				: geometry.attributes.position;

    			pendingPositionAccessors.push( pendingAccessor );

    		}

    		if ( hasMorphNormal ) {

    			const pendingAccessor = target.NORMAL !== undefined
    				? parser.getDependency( 'accessor', target.NORMAL )
    				: geometry.attributes.normal;

    			pendingNormalAccessors.push( pendingAccessor );

    		}

    		if ( hasMorphColor ) {

    			const pendingAccessor = target.COLOR_0 !== undefined
    				? parser.getDependency( 'accessor', target.COLOR_0 )
    				: geometry.attributes.color;

    			pendingColorAccessors.push( pendingAccessor );

    		}

    	}

    	return Promise.all( [
    		Promise.all( pendingPositionAccessors ),
    		Promise.all( pendingNormalAccessors ),
    		Promise.all( pendingColorAccessors )
    	] ).then( function ( accessors ) {

    		const morphPositions = accessors[ 0 ];
    		const morphNormals = accessors[ 1 ];
    		const morphColors = accessors[ 2 ];

    		if ( hasMorphPosition ) geometry.morphAttributes.position = morphPositions;
    		if ( hasMorphNormal ) geometry.morphAttributes.normal = morphNormals;
    		if ( hasMorphColor ) geometry.morphAttributes.color = morphColors;
    		geometry.morphTargetsRelative = true;

    		return geometry;

    	} );

    }

    /**
     * @param {Mesh} mesh
     * @param {GLTF.Mesh} meshDef
     */
    function updateMorphTargets( mesh, meshDef ) {

    	mesh.updateMorphTargets();

    	if ( meshDef.weights !== undefined ) {

    		for ( let i = 0, il = meshDef.weights.length; i < il; i ++ ) {

    			mesh.morphTargetInfluences[ i ] = meshDef.weights[ i ];

    		}

    	}

    	// .extras has user-defined data, so check that .extras.targetNames is an array.
    	if ( meshDef.extras && Array.isArray( meshDef.extras.targetNames ) ) {

    		const targetNames = meshDef.extras.targetNames;

    		if ( mesh.morphTargetInfluences.length === targetNames.length ) {

    			mesh.morphTargetDictionary = {};

    			for ( let i = 0, il = targetNames.length; i < il; i ++ ) {

    				mesh.morphTargetDictionary[ targetNames[ i ] ] = i;

    			}

    		} else {

    			console.warn( 'THREE.GLTFLoader: Invalid extras.targetNames length. Ignoring names.' );

    		}

    	}

    }

    function createPrimitiveKey( primitiveDef ) {

    	let geometryKey;

    	const dracoExtension = primitiveDef.extensions && primitiveDef.extensions[ EXTENSIONS.KHR_DRACO_MESH_COMPRESSION ];

    	if ( dracoExtension ) {

    		geometryKey = 'draco:' + dracoExtension.bufferView
    				+ ':' + dracoExtension.indices
    				+ ':' + createAttributesKey( dracoExtension.attributes );

    	} else {

    		geometryKey = primitiveDef.indices + ':' + createAttributesKey( primitiveDef.attributes ) + ':' + primitiveDef.mode;

    	}

    	if ( primitiveDef.targets !== undefined ) {

    		for ( let i = 0, il = primitiveDef.targets.length; i < il; i ++ ) {

    			geometryKey += ':' + createAttributesKey( primitiveDef.targets[ i ] );

    		}

    	}

    	return geometryKey;

    }

    function createAttributesKey( attributes ) {

    	let attributesKey = '';

    	const keys = Object.keys( attributes ).sort();

    	for ( let i = 0, il = keys.length; i < il; i ++ ) {

    		attributesKey += keys[ i ] + ':' + attributes[ keys[ i ] ] + ';';

    	}

    	return attributesKey;

    }

    function getNormalizedComponentScale( constructor ) {

    	// Reference:
    	// https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_mesh_quantization#encoding-quantized-data

    	switch ( constructor ) {

    		case Int8Array:
    			return 1 / 127;

    		case Uint8Array:
    			return 1 / 255;

    		case Int16Array:
    			return 1 / 32767;

    		case Uint16Array:
    			return 1 / 65535;

    		default:
    			throw new Error( 'THREE.GLTFLoader: Unsupported normalized accessor component type.' );

    	}

    }

    function getImageURIMimeType( uri ) {

    	if ( uri.search( /\.jpe?g($|\?)/i ) > 0 || uri.search( /^data\:image\/jpeg/ ) === 0 ) return 'image/jpeg';
    	if ( uri.search( /\.webp($|\?)/i ) > 0 || uri.search( /^data\:image\/webp/ ) === 0 ) return 'image/webp';

    	return 'image/png';

    }

    const _identityMatrix = new THREE.Matrix4();

    /* GLTF PARSER */

    class GLTFParser {

    	constructor( json = {}, options = {} ) {

    		this.json = json;
    		this.extensions = {};
    		this.plugins = {};
    		this.options = options;

    		// loader object cache
    		this.cache = new GLTFRegistry();

    		// associations between Three.js objects and glTF elements
    		this.associations = new Map();

    		// BufferGeometry caching
    		this.primitiveCache = {};

    		// Node cache
    		this.nodeCache = {};

    		// Object3D instance caches
    		this.meshCache = { refs: {}, uses: {} };
    		this.cameraCache = { refs: {}, uses: {} };
    		this.lightCache = { refs: {}, uses: {} };

    		this.sourceCache = {};
    		this.textureCache = {};

    		// Track node names, to ensure no duplicates
    		this.nodeNamesUsed = {};

    		// Use an ImageBitmapLoader if imageBitmaps are supported. Moves much of the
    		// expensive work of uploading a texture to the GPU off the main thread.

    		let isSafari = false;
    		let isFirefox = false;
    		let firefoxVersion = - 1;

    		if ( typeof navigator !== 'undefined' ) {

    			isSafari = /^((?!chrome|android).)*safari/i.test( navigator.userAgent ) === true;
    			isFirefox = navigator.userAgent.indexOf( 'Firefox' ) > - 1;
    			firefoxVersion = isFirefox ? navigator.userAgent.match( /Firefox\/([0-9]+)\./ )[ 1 ] : - 1;

    		}

    		if ( typeof createImageBitmap === 'undefined' || isSafari || ( isFirefox && firefoxVersion < 98 ) ) {

    			this.textureLoader = new THREE.TextureLoader( this.options.manager );

    		} else {

    			this.textureLoader = new THREE.ImageBitmapLoader( this.options.manager );

    		}

    		this.textureLoader.setCrossOrigin( this.options.crossOrigin );
    		this.textureLoader.setRequestHeader( this.options.requestHeader );

    		this.fileLoader = new THREE.FileLoader( this.options.manager );
    		this.fileLoader.setResponseType( 'arraybuffer' );

    		if ( this.options.crossOrigin === 'use-credentials' ) {

    			this.fileLoader.setWithCredentials( true );

    		}

    	}

    	setExtensions( extensions ) {

    		this.extensions = extensions;

    	}

    	setPlugins( plugins ) {

    		this.plugins = plugins;

    	}

    	parse( onLoad, onError ) {

    		const parser = this;
    		const json = this.json;
    		const extensions = this.extensions;

    		// Clear the loader cache
    		this.cache.removeAll();
    		this.nodeCache = {};

    		// Mark the special nodes/meshes in json for efficient parse
    		this._invokeAll( function ( ext ) {

    			return ext._markDefs && ext._markDefs();

    		} );

    		Promise.all( this._invokeAll( function ( ext ) {

    			return ext.beforeRoot && ext.beforeRoot();

    		} ) ).then( function () {

    			return Promise.all( [

    				parser.getDependencies( 'scene' ),
    				parser.getDependencies( 'animation' ),
    				parser.getDependencies( 'camera' ),

    			] );

    		} ).then( function ( dependencies ) {

    			const result = {
    				scene: dependencies[ 0 ][ json.scene || 0 ],
    				scenes: dependencies[ 0 ],
    				animations: dependencies[ 1 ],
    				cameras: dependencies[ 2 ],
    				asset: json.asset,
    				parser: parser,
    				userData: {}
    			};

    			addUnknownExtensionsToUserData( extensions, result, json );

    			assignExtrasToUserData( result, json );

    			Promise.all( parser._invokeAll( function ( ext ) {

    				return ext.afterRoot && ext.afterRoot( result );

    			} ) ).then( function () {

    				onLoad( result );

    			} );

    		} ).catch( onError );

    	}

    	/**
    	 * Marks the special nodes/meshes in json for efficient parse.
    	 */
    	_markDefs() {

    		const nodeDefs = this.json.nodes || [];
    		const skinDefs = this.json.skins || [];
    		const meshDefs = this.json.meshes || [];

    		// Nothing in the node definition indicates whether it is a Bone or an
    		// Object3D. Use the skins' joint references to mark bones.
    		for ( let skinIndex = 0, skinLength = skinDefs.length; skinIndex < skinLength; skinIndex ++ ) {

    			const joints = skinDefs[ skinIndex ].joints;

    			for ( let i = 0, il = joints.length; i < il; i ++ ) {

    				nodeDefs[ joints[ i ] ].isBone = true;

    			}

    		}

    		// Iterate over all nodes, marking references to shared resources,
    		// as well as skeleton joints.
    		for ( let nodeIndex = 0, nodeLength = nodeDefs.length; nodeIndex < nodeLength; nodeIndex ++ ) {

    			const nodeDef = nodeDefs[ nodeIndex ];

    			if ( nodeDef.mesh !== undefined ) {

    				this._addNodeRef( this.meshCache, nodeDef.mesh );

    				// Nothing in the mesh definition indicates whether it is
    				// a SkinnedMesh or Mesh. Use the node's mesh reference
    				// to mark SkinnedMesh if node has skin.
    				if ( nodeDef.skin !== undefined ) {

    					meshDefs[ nodeDef.mesh ].isSkinnedMesh = true;

    				}

    			}

    			if ( nodeDef.camera !== undefined ) {

    				this._addNodeRef( this.cameraCache, nodeDef.camera );

    			}

    		}

    	}

    	/**
    	 * Counts references to shared node / Object3D resources. These resources
    	 * can be reused, or "instantiated", at multiple nodes in the scene
    	 * hierarchy. Mesh, Camera, and Light instances are instantiated and must
    	 * be marked. Non-scenegraph resources (like Materials, Geometries, and
    	 * Textures) can be reused directly and are not marked here.
    	 *
    	 * Example: CesiumMilkTruck sample model reuses "Wheel" meshes.
    	 */
    	_addNodeRef( cache, index ) {

    		if ( index === undefined ) return;

    		if ( cache.refs[ index ] === undefined ) {

    			cache.refs[ index ] = cache.uses[ index ] = 0;

    		}

    		cache.refs[ index ] ++;

    	}

    	/** Returns a reference to a shared resource, cloning it if necessary. */
    	_getNodeRef( cache, index, object ) {

    		if ( cache.refs[ index ] <= 1 ) return object;

    		const ref = object.clone();

    		// Propagates mappings to the cloned object, prevents mappings on the
    		// original object from being lost.
    		const updateMappings = ( original, clone ) => {

    			const mappings = this.associations.get( original );
    			if ( mappings != null ) {

    				this.associations.set( clone, mappings );

    			}

    			for ( const [ i, child ] of original.children.entries() ) {

    				updateMappings( child, clone.children[ i ] );

    			}

    		};

    		updateMappings( object, ref );

    		ref.name += '_instance_' + ( cache.uses[ index ] ++ );

    		return ref;

    	}

    	_invokeOne( func ) {

    		const extensions = Object.values( this.plugins );
    		extensions.push( this );

    		for ( let i = 0; i < extensions.length; i ++ ) {

    			const result = func( extensions[ i ] );

    			if ( result ) return result;

    		}

    		return null;

    	}

    	_invokeAll( func ) {

    		const extensions = Object.values( this.plugins );
    		extensions.unshift( this );

    		const pending = [];

    		for ( let i = 0; i < extensions.length; i ++ ) {

    			const result = func( extensions[ i ] );

    			if ( result ) pending.push( result );

    		}

    		return pending;

    	}

    	/**
    	 * Requests the specified dependency asynchronously, with caching.
    	 * @param {string} type
    	 * @param {number} index
    	 * @return {Promise<Object3D|Material|THREE.Texture|AnimationClip|ArrayBuffer|Object>}
    	 */
    	getDependency( type, index ) {

    		const cacheKey = type + ':' + index;
    		let dependency = this.cache.get( cacheKey );

    		if ( ! dependency ) {

    			switch ( type ) {

    				case 'scene':
    					dependency = this.loadScene( index );
    					break;

    				case 'node':
    					dependency = this._invokeOne( function ( ext ) {

    						return ext.loadNode && ext.loadNode( index );

    					} );
    					break;

    				case 'mesh':
    					dependency = this._invokeOne( function ( ext ) {

    						return ext.loadMesh && ext.loadMesh( index );

    					} );
    					break;

    				case 'accessor':
    					dependency = this.loadAccessor( index );
    					break;

    				case 'bufferView':
    					dependency = this._invokeOne( function ( ext ) {

    						return ext.loadBufferView && ext.loadBufferView( index );

    					} );
    					break;

    				case 'buffer':
    					dependency = this.loadBuffer( index );
    					break;

    				case 'material':
    					dependency = this._invokeOne( function ( ext ) {

    						return ext.loadMaterial && ext.loadMaterial( index );

    					} );
    					break;

    				case 'texture':
    					dependency = this._invokeOne( function ( ext ) {

    						return ext.loadTexture && ext.loadTexture( index );

    					} );
    					break;

    				case 'skin':
    					dependency = this.loadSkin( index );
    					break;

    				case 'animation':
    					dependency = this._invokeOne( function ( ext ) {

    						return ext.loadAnimation && ext.loadAnimation( index );

    					} );
    					break;

    				case 'camera':
    					dependency = this.loadCamera( index );
    					break;

    				default:
    					dependency = this._invokeOne( function ( ext ) {

    						return ext != this && ext.getDependency && ext.getDependency( type, index );

    					} );

    					if ( ! dependency ) {

    						throw new Error( 'Unknown type: ' + type );

    					}

    					break;

    			}

    			this.cache.add( cacheKey, dependency );

    		}

    		return dependency;

    	}

    	/**
    	 * Requests all dependencies of the specified type asynchronously, with caching.
    	 * @param {string} type
    	 * @return {Promise<Array<Object>>}
    	 */
    	getDependencies( type ) {

    		let dependencies = this.cache.get( type );

    		if ( ! dependencies ) {

    			const parser = this;
    			const defs = this.json[ type + ( type === 'mesh' ? 'es' : 's' ) ] || [];

    			dependencies = Promise.all( defs.map( function ( def, index ) {

    				return parser.getDependency( type, index );

    			} ) );

    			this.cache.add( type, dependencies );

    		}

    		return dependencies;

    	}

    	/**
    	 * Specification: https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#buffers-and-buffer-views
    	 * @param {number} bufferIndex
    	 * @return {Promise<ArrayBuffer>}
    	 */
    	loadBuffer( bufferIndex ) {

    		const bufferDef = this.json.buffers[ bufferIndex ];
    		const loader = this.fileLoader;

    		if ( bufferDef.type && bufferDef.type !== 'arraybuffer' ) {

    			throw new Error( 'THREE.GLTFLoader: ' + bufferDef.type + ' buffer type is not supported.' );

    		}

    		// If present, GLB container is required to be the first buffer.
    		if ( bufferDef.uri === undefined && bufferIndex === 0 ) {

    			return Promise.resolve( this.extensions[ EXTENSIONS.KHR_BINARY_GLTF ].body );

    		}

    		const options = this.options;

    		return new Promise( function ( resolve, reject ) {

    			loader.load( THREE.LoaderUtils.resolveURL( bufferDef.uri, options.path ), resolve, undefined, function () {

    				reject( new Error( 'THREE.GLTFLoader: Failed to load buffer "' + bufferDef.uri + '".' ) );

    			} );

    		} );

    	}

    	/**
    	 * Specification: https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#buffers-and-buffer-views
    	 * @param {number} bufferViewIndex
    	 * @return {Promise<ArrayBuffer>}
    	 */
    	loadBufferView( bufferViewIndex ) {

    		const bufferViewDef = this.json.bufferViews[ bufferViewIndex ];

    		return this.getDependency( 'buffer', bufferViewDef.buffer ).then( function ( buffer ) {

    			const byteLength = bufferViewDef.byteLength || 0;
    			const byteOffset = bufferViewDef.byteOffset || 0;
    			return buffer.slice( byteOffset, byteOffset + byteLength );

    		} );

    	}

    	/**
    	 * Specification: https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#accessors
    	 * @param {number} accessorIndex
    	 * @return {Promise<BufferAttribute|InterleavedBufferAttribute>}
    	 */
    	loadAccessor( accessorIndex ) {

    		const parser = this;
    		const json = this.json;

    		const accessorDef = this.json.accessors[ accessorIndex ];

    		if ( accessorDef.bufferView === undefined && accessorDef.sparse === undefined ) {

    			const itemSize = WEBGL_TYPE_SIZES[ accessorDef.type ];
    			const TypedArray = WEBGL_COMPONENT_TYPES[ accessorDef.componentType ];
    			const normalized = accessorDef.normalized === true;

    			const array = new TypedArray( accessorDef.count * itemSize );
    			return Promise.resolve( new THREE.BufferAttribute( array, itemSize, normalized ) );

    		}

    		const pendingBufferViews = [];

    		if ( accessorDef.bufferView !== undefined ) {

    			pendingBufferViews.push( this.getDependency( 'bufferView', accessorDef.bufferView ) );

    		} else {

    			pendingBufferViews.push( null );

    		}

    		if ( accessorDef.sparse !== undefined ) {

    			pendingBufferViews.push( this.getDependency( 'bufferView', accessorDef.sparse.indices.bufferView ) );
    			pendingBufferViews.push( this.getDependency( 'bufferView', accessorDef.sparse.values.bufferView ) );

    		}

    		return Promise.all( pendingBufferViews ).then( function ( bufferViews ) {

    			const bufferView = bufferViews[ 0 ];

    			const itemSize = WEBGL_TYPE_SIZES[ accessorDef.type ];
    			const TypedArray = WEBGL_COMPONENT_TYPES[ accessorDef.componentType ];

    			// For VEC3: itemSize is 3, elementBytes is 4, itemBytes is 12.
    			const elementBytes = TypedArray.BYTES_PER_ELEMENT;
    			const itemBytes = elementBytes * itemSize;
    			const byteOffset = accessorDef.byteOffset || 0;
    			const byteStride = accessorDef.bufferView !== undefined ? json.bufferViews[ accessorDef.bufferView ].byteStride : undefined;
    			const normalized = accessorDef.normalized === true;
    			let array, bufferAttribute;

    			// The buffer is not interleaved if the stride is the item size in bytes.
    			if ( byteStride && byteStride !== itemBytes ) {

    				// Each "slice" of the buffer, as defined by 'count' elements of 'byteStride' bytes, gets its own InterleavedBuffer
    				// This makes sure that IBA.count reflects accessor.count properly
    				const ibSlice = Math.floor( byteOffset / byteStride );
    				const ibCacheKey = 'InterleavedBuffer:' + accessorDef.bufferView + ':' + accessorDef.componentType + ':' + ibSlice + ':' + accessorDef.count;
    				let ib = parser.cache.get( ibCacheKey );

    				if ( ! ib ) {

    					array = new TypedArray( bufferView, ibSlice * byteStride, accessorDef.count * byteStride / elementBytes );

    					// Integer parameters to IB/IBA are in array elements, not bytes.
    					ib = new THREE.InterleavedBuffer( array, byteStride / elementBytes );

    					parser.cache.add( ibCacheKey, ib );

    				}

    				bufferAttribute = new THREE.InterleavedBufferAttribute( ib, itemSize, ( byteOffset % byteStride ) / elementBytes, normalized );

    			} else {

    				if ( bufferView === null ) {

    					array = new TypedArray( accessorDef.count * itemSize );

    				} else {

    					array = new TypedArray( bufferView, byteOffset, accessorDef.count * itemSize );

    				}

    				bufferAttribute = new THREE.BufferAttribute( array, itemSize, normalized );

    			}

    			// https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#sparse-accessors
    			if ( accessorDef.sparse !== undefined ) {

    				const itemSizeIndices = WEBGL_TYPE_SIZES.SCALAR;
    				const TypedArrayIndices = WEBGL_COMPONENT_TYPES[ accessorDef.sparse.indices.componentType ];

    				const byteOffsetIndices = accessorDef.sparse.indices.byteOffset || 0;
    				const byteOffsetValues = accessorDef.sparse.values.byteOffset || 0;

    				const sparseIndices = new TypedArrayIndices( bufferViews[ 1 ], byteOffsetIndices, accessorDef.sparse.count * itemSizeIndices );
    				const sparseValues = new TypedArray( bufferViews[ 2 ], byteOffsetValues, accessorDef.sparse.count * itemSize );

    				if ( bufferView !== null ) {

    					// Avoid modifying the original ArrayBuffer, if the bufferView wasn't initialized with zeroes.
    					bufferAttribute = new THREE.BufferAttribute( bufferAttribute.array.slice(), bufferAttribute.itemSize, bufferAttribute.normalized );

    				}

    				for ( let i = 0, il = sparseIndices.length; i < il; i ++ ) {

    					const index = sparseIndices[ i ];

    					bufferAttribute.setX( index, sparseValues[ i * itemSize ] );
    					if ( itemSize >= 2 ) bufferAttribute.setY( index, sparseValues[ i * itemSize + 1 ] );
    					if ( itemSize >= 3 ) bufferAttribute.setZ( index, sparseValues[ i * itemSize + 2 ] );
    					if ( itemSize >= 4 ) bufferAttribute.setW( index, sparseValues[ i * itemSize + 3 ] );
    					if ( itemSize >= 5 ) throw new Error( 'THREE.GLTFLoader: Unsupported itemSize in sparse BufferAttribute.' );

    				}

    			}

    			return bufferAttribute;

    		} );

    	}

    	/**
    	 * Specification: https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#textures
    	 * @param {number} textureIndex
    	 * @return {Promise<THREE.Texture|null>}
    	 */
    	loadTexture( textureIndex ) {

    		const json = this.json;
    		const options = this.options;
    		const textureDef = json.textures[ textureIndex ];
    		const sourceIndex = textureDef.source;
    		const sourceDef = json.images[ sourceIndex ];

    		let loader = this.textureLoader;

    		if ( sourceDef.uri ) {

    			const handler = options.manager.getHandler( sourceDef.uri );
    			if ( handler !== null ) loader = handler;

    		}

    		return this.loadTextureImage( textureIndex, sourceIndex, loader );

    	}

    	loadTextureImage( textureIndex, sourceIndex, loader ) {

    		const parser = this;
    		const json = this.json;

    		const textureDef = json.textures[ textureIndex ];
    		const sourceDef = json.images[ sourceIndex ];

    		const cacheKey = ( sourceDef.uri || sourceDef.bufferView ) + ':' + textureDef.sampler;

    		if ( this.textureCache[ cacheKey ] ) {

    			// See https://github.com/mrdoob/three.js/issues/21559.
    			return this.textureCache[ cacheKey ];

    		}

    		const promise = this.loadImageSource( sourceIndex, loader ).then( function ( texture ) {

    			texture.flipY = false;

    			texture.name = textureDef.name || sourceDef.name || '';

    			if ( texture.name === '' && typeof sourceDef.uri === 'string' && sourceDef.uri.startsWith( 'data:image/' ) === false ) {

    				texture.name = sourceDef.uri;

    			}

    			const samplers = json.samplers || {};
    			const sampler = samplers[ textureDef.sampler ] || {};

    			texture.magFilter = WEBGL_FILTERS[ sampler.magFilter ] || THREE.LinearFilter;
    			texture.minFilter = WEBGL_FILTERS[ sampler.minFilter ] || THREE.LinearMipmapLinearFilter;
    			texture.wrapS = WEBGL_WRAPPINGS[ sampler.wrapS ] || THREE.RepeatWrapping;
    			texture.wrapT = WEBGL_WRAPPINGS[ sampler.wrapT ] || THREE.RepeatWrapping;

    			parser.associations.set( texture, { textures: textureIndex } );

    			return texture;

    		} ).catch( function () {

    			return null;

    		} );

    		this.textureCache[ cacheKey ] = promise;

    		return promise;

    	}

    	loadImageSource( sourceIndex, loader ) {

    		const parser = this;
    		const json = this.json;
    		const options = this.options;

    		if ( this.sourceCache[ sourceIndex ] !== undefined ) {

    			return this.sourceCache[ sourceIndex ].then( ( texture ) => texture.clone() );

    		}

    		const sourceDef = json.images[ sourceIndex ];

    		const URL = self.URL || self.webkitURL;

    		let sourceURI = sourceDef.uri || '';
    		let isObjectURL = false;

    		if ( sourceDef.bufferView !== undefined ) {

    			// Load binary image data from bufferView, if provided.

    			sourceURI = parser.getDependency( 'bufferView', sourceDef.bufferView ).then( function ( bufferView ) {

    				isObjectURL = true;
    				const blob = new Blob( [ bufferView ], { type: sourceDef.mimeType } );
    				sourceURI = URL.createObjectURL( blob );
    				return sourceURI;

    			} );

    		} else if ( sourceDef.uri === undefined ) {

    			throw new Error( 'THREE.GLTFLoader: Image ' + sourceIndex + ' is missing URI and bufferView' );

    		}

    		const promise = Promise.resolve( sourceURI ).then( function ( sourceURI ) {

    			return new Promise( function ( resolve, reject ) {

    				let onLoad = resolve;

    				if ( loader.isImageBitmapLoader === true ) {

    					onLoad = function ( imageBitmap ) {

    						const texture = new THREE.Texture( imageBitmap );
    						texture.needsUpdate = true;

    						resolve( texture );

    					};

    				}

    				loader.load( THREE.LoaderUtils.resolveURL( sourceURI, options.path ), onLoad, undefined, reject );

    			} );

    		} ).then( function ( texture ) {

    			// Clean up resources and configure Texture.

    			if ( isObjectURL === true ) {

    				URL.revokeObjectURL( sourceURI );

    			}

    			texture.userData.mimeType = sourceDef.mimeType || getImageURIMimeType( sourceDef.uri );

    			return texture;

    		} ).catch( function ( error ) {

    			console.error( 'THREE.GLTFLoader: Couldn\'t load texture', sourceURI );
    			throw error;

    		} );

    		this.sourceCache[ sourceIndex ] = promise;
    		return promise;

    	}

    	/**
    	 * Asynchronously assigns a texture to the given material parameters.
    	 * @param {Object} materialParams
    	 * @param {string} mapName
    	 * @param {Object} mapDef
    	 * @return {Promise<Texture>}
    	 */
    	assignTexture( materialParams, mapName, mapDef, colorSpace ) {

    		const parser = this;

    		return this.getDependency( 'texture', mapDef.index ).then( function ( texture ) {

    			if ( ! texture ) return null;

    			if ( mapDef.texCoord !== undefined && mapDef.texCoord > 0 ) {

    				texture = texture.clone();
    				texture.channel = mapDef.texCoord;

    			}

    			if ( parser.extensions[ EXTENSIONS.KHR_TEXTURE_TRANSFORM ] ) {

    				const transform = mapDef.extensions !== undefined ? mapDef.extensions[ EXTENSIONS.KHR_TEXTURE_TRANSFORM ] : undefined;

    				if ( transform ) {

    					const gltfReference = parser.associations.get( texture );
    					texture = parser.extensions[ EXTENSIONS.KHR_TEXTURE_TRANSFORM ].extendTexture( texture, transform );
    					parser.associations.set( texture, gltfReference );

    				}

    			}

    			if ( colorSpace !== undefined ) {

    				texture.colorSpace = colorSpace;

    			}

    			materialParams[ mapName ] = texture;

    			return texture;

    		} );

    	}

    	/**
    	 * Assigns final material to a Mesh, Line, or Points instance. The instance
    	 * already has a material (generated from the glTF material options alone)
    	 * but reuse of the same glTF material may require multiple threejs materials
    	 * to accommodate different primitive types, defines, etc. New materials will
    	 * be created if necessary, and reused from a cache.
    	 * @param  {Object3D} mesh Mesh, Line, or Points instance.
    	 */
    	assignFinalMaterial( mesh ) {

    		const geometry = mesh.geometry;
    		let material = mesh.material;

    		const useDerivativeTangents = geometry.attributes.tangent === undefined;
    		const useVertexColors = geometry.attributes.color !== undefined;
    		const useFlatShading = geometry.attributes.normal === undefined;

    		if ( mesh.isPoints ) {

    			const cacheKey = 'PointsMaterial:' + material.uuid;

    			let pointsMaterial = this.cache.get( cacheKey );

    			if ( ! pointsMaterial ) {

    				pointsMaterial = new THREE.PointsMaterial();
    				THREE.Material.prototype.copy.call( pointsMaterial, material );
    				pointsMaterial.color.copy( material.color );
    				pointsMaterial.map = material.map;
    				pointsMaterial.sizeAttenuation = false; // glTF spec says points should be 1px

    				this.cache.add( cacheKey, pointsMaterial );

    			}

    			material = pointsMaterial;

    		} else if ( mesh.isLine ) {

    			const cacheKey = 'LineBasicMaterial:' + material.uuid;

    			let lineMaterial = this.cache.get( cacheKey );

    			if ( ! lineMaterial ) {

    				lineMaterial = new THREE.LineBasicMaterial();
    				THREE.Material.prototype.copy.call( lineMaterial, material );
    				lineMaterial.color.copy( material.color );
    				lineMaterial.map = material.map;

    				this.cache.add( cacheKey, lineMaterial );

    			}

    			material = lineMaterial;

    		}

    		// Clone the material if it will be modified
    		if ( useDerivativeTangents || useVertexColors || useFlatShading ) {

    			let cacheKey = 'ClonedMaterial:' + material.uuid + ':';

    			if ( useDerivativeTangents ) cacheKey += 'derivative-tangents:';
    			if ( useVertexColors ) cacheKey += 'vertex-colors:';
    			if ( useFlatShading ) cacheKey += 'flat-shading:';

    			let cachedMaterial = this.cache.get( cacheKey );

    			if ( ! cachedMaterial ) {

    				cachedMaterial = material.clone();

    				if ( useVertexColors ) cachedMaterial.vertexColors = true;
    				if ( useFlatShading ) cachedMaterial.flatShading = true;

    				if ( useDerivativeTangents ) {

    					// https://github.com/mrdoob/three.js/issues/11438#issuecomment-507003995
    					if ( cachedMaterial.normalScale ) cachedMaterial.normalScale.y *= - 1;
    					if ( cachedMaterial.clearcoatNormalScale ) cachedMaterial.clearcoatNormalScale.y *= - 1;

    				}

    				this.cache.add( cacheKey, cachedMaterial );

    				this.associations.set( cachedMaterial, this.associations.get( material ) );

    			}

    			material = cachedMaterial;

    		}

    		mesh.material = material;

    	}

    	getMaterialType( /* materialIndex */ ) {

    		return THREE.MeshStandardMaterial;

    	}

    	/**
    	 * Specification: https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#materials
    	 * @param {number} materialIndex
    	 * @return {Promise<Material>}
    	 */
    	loadMaterial( materialIndex ) {

    		const parser = this;
    		const json = this.json;
    		const extensions = this.extensions;
    		const materialDef = json.materials[ materialIndex ];

    		let materialType;
    		const materialParams = {};
    		const materialExtensions = materialDef.extensions || {};

    		const pending = [];

    		if ( materialExtensions[ EXTENSIONS.KHR_MATERIALS_UNLIT ] ) {

    			const kmuExtension = extensions[ EXTENSIONS.KHR_MATERIALS_UNLIT ];
    			materialType = kmuExtension.getMaterialType();
    			pending.push( kmuExtension.extendParams( materialParams, materialDef, parser ) );

    		} else {

    			// Specification:
    			// https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#metallic-roughness-material

    			const metallicRoughness = materialDef.pbrMetallicRoughness || {};

    			materialParams.color = new THREE.Color( 1.0, 1.0, 1.0 );
    			materialParams.opacity = 1.0;

    			if ( Array.isArray( metallicRoughness.baseColorFactor ) ) {

    				const array = metallicRoughness.baseColorFactor;

    				materialParams.color.fromArray( array );
    				materialParams.opacity = array[ 3 ];

    			}

    			if ( metallicRoughness.baseColorTexture !== undefined ) {

    				pending.push( parser.assignTexture( materialParams, 'map', metallicRoughness.baseColorTexture, THREE.SRGBColorSpace ) );

    			}

    			materialParams.metalness = metallicRoughness.metallicFactor !== undefined ? metallicRoughness.metallicFactor : 1.0;
    			materialParams.roughness = metallicRoughness.roughnessFactor !== undefined ? metallicRoughness.roughnessFactor : 1.0;

    			if ( metallicRoughness.metallicRoughnessTexture !== undefined ) {

    				pending.push( parser.assignTexture( materialParams, 'metalnessMap', metallicRoughness.metallicRoughnessTexture ) );
    				pending.push( parser.assignTexture( materialParams, 'roughnessMap', metallicRoughness.metallicRoughnessTexture ) );

    			}

    			materialType = this._invokeOne( function ( ext ) {

    				return ext.getMaterialType && ext.getMaterialType( materialIndex );

    			} );

    			pending.push( Promise.all( this._invokeAll( function ( ext ) {

    				return ext.extendMaterialParams && ext.extendMaterialParams( materialIndex, materialParams );

    			} ) ) );

    		}

    		if ( materialDef.doubleSided === true ) {

    			materialParams.side = THREE.DoubleSide;

    		}

    		const alphaMode = materialDef.alphaMode || ALPHA_MODES.OPAQUE;

    		if ( alphaMode === ALPHA_MODES.BLEND ) {

    			materialParams.transparent = true;

    			// See: https://github.com/mrdoob/three.js/issues/17706
    			materialParams.depthWrite = false;

    		} else {

    			materialParams.transparent = false;

    			if ( alphaMode === ALPHA_MODES.MASK ) {

    				materialParams.alphaTest = materialDef.alphaCutoff !== undefined ? materialDef.alphaCutoff : 0.5;

    			}

    		}

    		if ( materialDef.normalTexture !== undefined && materialType !== THREE.MeshBasicMaterial ) {

    			pending.push( parser.assignTexture( materialParams, 'normalMap', materialDef.normalTexture ) );

    			materialParams.normalScale = new THREE.Vector2( 1, 1 );

    			if ( materialDef.normalTexture.scale !== undefined ) {

    				const scale = materialDef.normalTexture.scale;

    				materialParams.normalScale.set( scale, scale );

    			}

    		}

    		if ( materialDef.occlusionTexture !== undefined && materialType !== THREE.MeshBasicMaterial ) {

    			pending.push( parser.assignTexture( materialParams, 'aoMap', materialDef.occlusionTexture ) );

    			if ( materialDef.occlusionTexture.strength !== undefined ) {

    				materialParams.aoMapIntensity = materialDef.occlusionTexture.strength;

    			}

    		}

    		if ( materialDef.emissiveFactor !== undefined && materialType !== THREE.MeshBasicMaterial ) {

    			materialParams.emissive = new THREE.Color().fromArray( materialDef.emissiveFactor );

    		}

    		if ( materialDef.emissiveTexture !== undefined && materialType !== THREE.MeshBasicMaterial ) {

    			pending.push( parser.assignTexture( materialParams, 'emissiveMap', materialDef.emissiveTexture, THREE.SRGBColorSpace ) );

    		}

    		return Promise.all( pending ).then( function () {

    			const material = new materialType( materialParams );

    			if ( materialDef.name ) material.name = materialDef.name;

    			assignExtrasToUserData( material, materialDef );

    			parser.associations.set( material, { materials: materialIndex } );

    			if ( materialDef.extensions ) addUnknownExtensionsToUserData( extensions, material, materialDef );

    			return material;

    		} );

    	}

    	/** When Object3D instances are targeted by animation, they need unique names. */
    	createUniqueName( originalName ) {

    		const sanitizedName = THREE.PropertyBinding.sanitizeNodeName( originalName || '' );

    		if ( sanitizedName in this.nodeNamesUsed ) {

    			return sanitizedName + '_' + ( ++ this.nodeNamesUsed[ sanitizedName ] );

    		} else {

    			this.nodeNamesUsed[ sanitizedName ] = 0;

    			return sanitizedName;

    		}

    	}

    	/**
    	 * Specification: https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#geometry
    	 *
    	 * Creates BufferGeometries from primitives.
    	 *
    	 * @param {Array<GLTF.Primitive>} primitives
    	 * @return {Promise<Array<BufferGeometry>>}
    	 */
    	loadGeometries( primitives ) {

    		const parser = this;
    		const extensions = this.extensions;
    		const cache = this.primitiveCache;

    		function createDracoPrimitive( primitive ) {

    			return extensions[ EXTENSIONS.KHR_DRACO_MESH_COMPRESSION ]
    				.decodePrimitive( primitive, parser )
    				.then( function ( geometry ) {

    					return addPrimitiveAttributes( geometry, primitive, parser );

    				} );

    		}

    		const pending = [];

    		for ( let i = 0, il = primitives.length; i < il; i ++ ) {

    			const primitive = primitives[ i ];
    			const cacheKey = createPrimitiveKey( primitive );

    			// See if we've already created this geometry
    			const cached = cache[ cacheKey ];

    			if ( cached ) {

    				// Use the cached geometry if it exists
    				pending.push( cached.promise );

    			} else {

    				let geometryPromise;

    				if ( primitive.extensions && primitive.extensions[ EXTENSIONS.KHR_DRACO_MESH_COMPRESSION ] ) {

    					// Use DRACO geometry if available
    					geometryPromise = createDracoPrimitive( primitive );

    				} else {

    					// Otherwise create a new geometry
    					geometryPromise = addPrimitiveAttributes( new THREE.BufferGeometry(), primitive, parser );

    				}

    				// Cache this geometry
    				cache[ cacheKey ] = { primitive: primitive, promise: geometryPromise };

    				pending.push( geometryPromise );

    			}

    		}

    		return Promise.all( pending );

    	}

    	/**
    	 * Specification: https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#meshes
    	 * @param {number} meshIndex
    	 * @return {Promise<Group|Mesh|SkinnedMesh>}
    	 */
    	loadMesh( meshIndex ) {

    		const parser = this;
    		const json = this.json;
    		const extensions = this.extensions;

    		const meshDef = json.meshes[ meshIndex ];
    		const primitives = meshDef.primitives;

    		const pending = [];

    		for ( let i = 0, il = primitives.length; i < il; i ++ ) {

    			const material = primitives[ i ].material === undefined
    				? createDefaultMaterial( this.cache )
    				: this.getDependency( 'material', primitives[ i ].material );

    			pending.push( material );

    		}

    		pending.push( parser.loadGeometries( primitives ) );

    		return Promise.all( pending ).then( function ( results ) {

    			const materials = results.slice( 0, results.length - 1 );
    			const geometries = results[ results.length - 1 ];

    			const meshes = [];

    			for ( let i = 0, il = geometries.length; i < il; i ++ ) {

    				const geometry = geometries[ i ];
    				const primitive = primitives[ i ];

    				// 1. create Mesh

    				let mesh;

    				const material = materials[ i ];

    				if ( primitive.mode === WEBGL_CONSTANTS.TRIANGLES ||
    						primitive.mode === WEBGL_CONSTANTS.TRIANGLE_STRIP ||
    						primitive.mode === WEBGL_CONSTANTS.TRIANGLE_FAN ||
    						primitive.mode === undefined ) {

    					// .isSkinnedMesh isn't in glTF spec. See ._markDefs()
    					mesh = meshDef.isSkinnedMesh === true
    						? new THREE.SkinnedMesh( geometry, material )
    						: new THREE.Mesh( geometry, material );

    					if ( mesh.isSkinnedMesh === true ) {

    						// normalize skin weights to fix malformed assets (see #15319)
    						mesh.normalizeSkinWeights();

    					}

    					if ( primitive.mode === WEBGL_CONSTANTS.TRIANGLE_STRIP ) {

    						mesh.geometry = toTrianglesDrawMode( mesh.geometry, THREE.TriangleStripDrawMode );

    					} else if ( primitive.mode === WEBGL_CONSTANTS.TRIANGLE_FAN ) {

    						mesh.geometry = toTrianglesDrawMode( mesh.geometry, THREE.TriangleFanDrawMode );

    					}

    				} else if ( primitive.mode === WEBGL_CONSTANTS.LINES ) {

    					mesh = new THREE.LineSegments( geometry, material );

    				} else if ( primitive.mode === WEBGL_CONSTANTS.LINE_STRIP ) {

    					mesh = new THREE.Line( geometry, material );

    				} else if ( primitive.mode === WEBGL_CONSTANTS.LINE_LOOP ) {

    					mesh = new THREE.LineLoop( geometry, material );

    				} else if ( primitive.mode === WEBGL_CONSTANTS.POINTS ) {

    					mesh = new THREE.Points( geometry, material );

    				} else {

    					throw new Error( 'THREE.GLTFLoader: Primitive mode unsupported: ' + primitive.mode );

    				}

    				if ( Object.keys( mesh.geometry.morphAttributes ).length > 0 ) {

    					updateMorphTargets( mesh, meshDef );

    				}

    				mesh.name = parser.createUniqueName( meshDef.name || ( 'mesh_' + meshIndex ) );

    				assignExtrasToUserData( mesh, meshDef );

    				if ( primitive.extensions ) addUnknownExtensionsToUserData( extensions, mesh, primitive );

    				parser.assignFinalMaterial( mesh );

    				meshes.push( mesh );

    			}

    			for ( let i = 0, il = meshes.length; i < il; i ++ ) {

    				parser.associations.set( meshes[ i ], {
    					meshes: meshIndex,
    					primitives: i
    				} );

    			}

    			if ( meshes.length === 1 ) {

    				if ( meshDef.extensions ) addUnknownExtensionsToUserData( extensions, meshes[ 0 ], meshDef );

    				return meshes[ 0 ];

    			}

    			const group = new THREE.Group();

    			if ( meshDef.extensions ) addUnknownExtensionsToUserData( extensions, group, meshDef );

    			parser.associations.set( group, { meshes: meshIndex } );

    			for ( let i = 0, il = meshes.length; i < il; i ++ ) {

    				group.add( meshes[ i ] );

    			}

    			return group;

    		} );

    	}

    	/**
    	 * Specification: https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#cameras
    	 * @param {number} cameraIndex
    	 * @return {Promise<THREE.Camera>}
    	 */
    	loadCamera( cameraIndex ) {

    		let camera;
    		const cameraDef = this.json.cameras[ cameraIndex ];
    		const params = cameraDef[ cameraDef.type ];

    		if ( ! params ) {

    			console.warn( 'THREE.GLTFLoader: Missing camera parameters.' );
    			return;

    		}

    		if ( cameraDef.type === 'perspective' ) {

    			camera = new THREE.PerspectiveCamera( THREE.MathUtils.radToDeg( params.yfov ), params.aspectRatio || 1, params.znear || 1, params.zfar || 2e6 );

    		} else if ( cameraDef.type === 'orthographic' ) {

    			camera = new THREE.OrthographicCamera( - params.xmag, params.xmag, params.ymag, - params.ymag, params.znear, params.zfar );

    		}

    		if ( cameraDef.name ) camera.name = this.createUniqueName( cameraDef.name );

    		assignExtrasToUserData( camera, cameraDef );

    		return Promise.resolve( camera );

    	}

    	/**
    	 * Specification: https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#skins
    	 * @param {number} skinIndex
    	 * @return {Promise<Skeleton>}
    	 */
    	loadSkin( skinIndex ) {

    		const skinDef = this.json.skins[ skinIndex ];

    		const pending = [];

    		for ( let i = 0, il = skinDef.joints.length; i < il; i ++ ) {

    			pending.push( this._loadNodeShallow( skinDef.joints[ i ] ) );

    		}

    		if ( skinDef.inverseBindMatrices !== undefined ) {

    			pending.push( this.getDependency( 'accessor', skinDef.inverseBindMatrices ) );

    		} else {

    			pending.push( null );

    		}

    		return Promise.all( pending ).then( function ( results ) {

    			const inverseBindMatrices = results.pop();
    			const jointNodes = results;

    			// Note that bones (joint nodes) may or may not be in the
    			// scene graph at this time.

    			const bones = [];
    			const boneInverses = [];

    			for ( let i = 0, il = jointNodes.length; i < il; i ++ ) {

    				const jointNode = jointNodes[ i ];

    				if ( jointNode ) {

    					bones.push( jointNode );

    					const mat = new THREE.Matrix4();

    					if ( inverseBindMatrices !== null ) {

    						mat.fromArray( inverseBindMatrices.array, i * 16 );

    					}

    					boneInverses.push( mat );

    				} else {

    					console.warn( 'THREE.GLTFLoader: Joint "%s" could not be found.', skinDef.joints[ i ] );

    				}

    			}

    			return new THREE.Skeleton( bones, boneInverses );

    		} );

    	}

    	/**
    	 * Specification: https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#animations
    	 * @param {number} animationIndex
    	 * @return {Promise<AnimationClip>}
    	 */
    	loadAnimation( animationIndex ) {

    		const json = this.json;
    		const parser = this;

    		const animationDef = json.animations[ animationIndex ];
    		const animationName = animationDef.name ? animationDef.name : 'animation_' + animationIndex;

    		const pendingNodes = [];
    		const pendingInputAccessors = [];
    		const pendingOutputAccessors = [];
    		const pendingSamplers = [];
    		const pendingTargets = [];

    		for ( let i = 0, il = animationDef.channels.length; i < il; i ++ ) {

    			const channel = animationDef.channels[ i ];
    			const sampler = animationDef.samplers[ channel.sampler ];
    			const target = channel.target;
    			const name = target.node;
    			const input = animationDef.parameters !== undefined ? animationDef.parameters[ sampler.input ] : sampler.input;
    			const output = animationDef.parameters !== undefined ? animationDef.parameters[ sampler.output ] : sampler.output;

    			if ( target.node === undefined ) continue;

    			pendingNodes.push( this.getDependency( 'node', name ) );
    			pendingInputAccessors.push( this.getDependency( 'accessor', input ) );
    			pendingOutputAccessors.push( this.getDependency( 'accessor', output ) );
    			pendingSamplers.push( sampler );
    			pendingTargets.push( target );

    		}

    		return Promise.all( [

    			Promise.all( pendingNodes ),
    			Promise.all( pendingInputAccessors ),
    			Promise.all( pendingOutputAccessors ),
    			Promise.all( pendingSamplers ),
    			Promise.all( pendingTargets )

    		] ).then( function ( dependencies ) {

    			const nodes = dependencies[ 0 ];
    			const inputAccessors = dependencies[ 1 ];
    			const outputAccessors = dependencies[ 2 ];
    			const samplers = dependencies[ 3 ];
    			const targets = dependencies[ 4 ];

    			const tracks = [];

    			for ( let i = 0, il = nodes.length; i < il; i ++ ) {

    				const node = nodes[ i ];
    				const inputAccessor = inputAccessors[ i ];
    				const outputAccessor = outputAccessors[ i ];
    				const sampler = samplers[ i ];
    				const target = targets[ i ];

    				if ( node === undefined ) continue;

    				if ( node.updateMatrix ) {

    					node.updateMatrix();
    					node.matrixAutoUpdate = true;

    				}

    				const createdTracks = parser._createAnimationTracks( node, inputAccessor, outputAccessor, sampler, target );

    				if ( createdTracks ) {

    					for ( let k = 0; k < createdTracks.length; k ++ ) {

    						tracks.push( createdTracks[ k ] );

    					}

    				}

    			}

    			return new THREE.AnimationClip( animationName, undefined, tracks );

    		} );

    	}

    	createNodeMesh( nodeIndex ) {

    		const json = this.json;
    		const parser = this;
    		const nodeDef = json.nodes[ nodeIndex ];

    		if ( nodeDef.mesh === undefined ) return null;

    		return parser.getDependency( 'mesh', nodeDef.mesh ).then( function ( mesh ) {

    			const node = parser._getNodeRef( parser.meshCache, nodeDef.mesh, mesh );

    			// if weights are provided on the node, override weights on the mesh.
    			if ( nodeDef.weights !== undefined ) {

    				node.traverse( function ( o ) {

    					if ( ! o.isMesh ) return;

    					for ( let i = 0, il = nodeDef.weights.length; i < il; i ++ ) {

    						o.morphTargetInfluences[ i ] = nodeDef.weights[ i ];

    					}

    				} );

    			}

    			return node;

    		} );

    	}

    	/**
    	 * Specification: https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#nodes-and-hierarchy
    	 * @param {number} nodeIndex
    	 * @return {Promise<Object3D>}
    	 */
    	loadNode( nodeIndex ) {

    		const json = this.json;
    		const parser = this;

    		const nodeDef = json.nodes[ nodeIndex ];

    		const nodePending = parser._loadNodeShallow( nodeIndex );

    		const childPending = [];
    		const childrenDef = nodeDef.children || [];

    		for ( let i = 0, il = childrenDef.length; i < il; i ++ ) {

    			childPending.push( parser.getDependency( 'node', childrenDef[ i ] ) );

    		}

    		const skeletonPending = nodeDef.skin === undefined
    			? Promise.resolve( null )
    			: parser.getDependency( 'skin', nodeDef.skin );

    		return Promise.all( [
    			nodePending,
    			Promise.all( childPending ),
    			skeletonPending
    		] ).then( function ( results ) {

    			const node = results[ 0 ];
    			const children = results[ 1 ];
    			const skeleton = results[ 2 ];

    			if ( skeleton !== null ) {

    				// This full traverse should be fine because
    				// child glTF nodes have not been added to this node yet.
    				node.traverse( function ( mesh ) {

    					if ( ! mesh.isSkinnedMesh ) return;

    					mesh.bind( skeleton, _identityMatrix );

    				} );

    			}

    			for ( let i = 0, il = children.length; i < il; i ++ ) {

    				node.add( children[ i ] );

    			}

    			return node;

    		} );

    	}

    	// ._loadNodeShallow() parses a single node.
    	// skin and child nodes are created and added in .loadNode() (no '_' prefix).
    	_loadNodeShallow( nodeIndex ) {

    		const json = this.json;
    		const extensions = this.extensions;
    		const parser = this;

    		// This method is called from .loadNode() and .loadSkin().
    		// Cache a node to avoid duplication.

    		if ( this.nodeCache[ nodeIndex ] !== undefined ) {

    			return this.nodeCache[ nodeIndex ];

    		}

    		const nodeDef = json.nodes[ nodeIndex ];

    		// reserve node's name before its dependencies, so the root has the intended name.
    		const nodeName = nodeDef.name ? parser.createUniqueName( nodeDef.name ) : '';

    		const pending = [];

    		const meshPromise = parser._invokeOne( function ( ext ) {

    			return ext.createNodeMesh && ext.createNodeMesh( nodeIndex );

    		} );

    		if ( meshPromise ) {

    			pending.push( meshPromise );

    		}

    		if ( nodeDef.camera !== undefined ) {

    			pending.push( parser.getDependency( 'camera', nodeDef.camera ).then( function ( camera ) {

    				return parser._getNodeRef( parser.cameraCache, nodeDef.camera, camera );

    			} ) );

    		}

    		parser._invokeAll( function ( ext ) {

    			return ext.createNodeAttachment && ext.createNodeAttachment( nodeIndex );

    		} ).forEach( function ( promise ) {

    			pending.push( promise );

    		} );

    		this.nodeCache[ nodeIndex ] = Promise.all( pending ).then( function ( objects ) {

    			let node;

    			// .isBone isn't in glTF spec. See ._markDefs
    			if ( nodeDef.isBone === true ) {

    				node = new THREE.Bone();

    			} else if ( objects.length > 1 ) {

    				node = new THREE.Group();

    			} else if ( objects.length === 1 ) {

    				node = objects[ 0 ];

    			} else {

    				node = new THREE.Object3D();

    			}

    			if ( node !== objects[ 0 ] ) {

    				for ( let i = 0, il = objects.length; i < il; i ++ ) {

    					node.add( objects[ i ] );

    				}

    			}

    			if ( nodeDef.name ) {

    				node.userData.name = nodeDef.name;
    				node.name = nodeName;

    			}

    			assignExtrasToUserData( node, nodeDef );

    			if ( nodeDef.extensions ) addUnknownExtensionsToUserData( extensions, node, nodeDef );

    			if ( nodeDef.matrix !== undefined ) {

    				const matrix = new THREE.Matrix4();
    				matrix.fromArray( nodeDef.matrix );
    				node.applyMatrix4( matrix );

    			} else {

    				if ( nodeDef.translation !== undefined ) {

    					node.position.fromArray( nodeDef.translation );

    				}

    				if ( nodeDef.rotation !== undefined ) {

    					node.quaternion.fromArray( nodeDef.rotation );

    				}

    				if ( nodeDef.scale !== undefined ) {

    					node.scale.fromArray( nodeDef.scale );

    				}

    			}

    			if ( ! parser.associations.has( node ) ) {

    				parser.associations.set( node, {} );

    			}

    			parser.associations.get( node ).nodes = nodeIndex;

    			return node;

    		} );

    		return this.nodeCache[ nodeIndex ];

    	}

    	/**
    	 * Specification: https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#scenes
    	 * @param {number} sceneIndex
    	 * @return {Promise<Group>}
    	 */
    	loadScene( sceneIndex ) {

    		const extensions = this.extensions;
    		const sceneDef = this.json.scenes[ sceneIndex ];
    		const parser = this;

    		// Loader returns Group, not Scene.
    		// See: https://github.com/mrdoob/three.js/issues/18342#issuecomment-578981172
    		const scene = new THREE.Group();
    		if ( sceneDef.name ) scene.name = parser.createUniqueName( sceneDef.name );

    		assignExtrasToUserData( scene, sceneDef );

    		if ( sceneDef.extensions ) addUnknownExtensionsToUserData( extensions, scene, sceneDef );

    		const nodeIds = sceneDef.nodes || [];

    		const pending = [];

    		for ( let i = 0, il = nodeIds.length; i < il; i ++ ) {

    			pending.push( parser.getDependency( 'node', nodeIds[ i ] ) );

    		}

    		return Promise.all( pending ).then( function ( nodes ) {

    			for ( let i = 0, il = nodes.length; i < il; i ++ ) {

    				scene.add( nodes[ i ] );

    			}

    			// Removes dangling associations, associations that reference a node that
    			// didn't make it into the scene.
    			const reduceAssociations = ( node ) => {

    				const reducedAssociations = new Map();

    				for ( const [ key, value ] of parser.associations ) {

    					if ( key instanceof THREE.Material || key instanceof THREE.Texture ) {

    						reducedAssociations.set( key, value );

    					}

    				}

    				node.traverse( ( node ) => {

    					const mappings = parser.associations.get( node );

    					if ( mappings != null ) {

    						reducedAssociations.set( node, mappings );

    					}

    				} );

    				return reducedAssociations;

    			};

    			parser.associations = reduceAssociations( scene );

    			return scene;

    		} );

    	}

    	_createAnimationTracks( node, inputAccessor, outputAccessor, sampler, target ) {

    		const tracks = [];

    		const targetName = node.name ? node.name : node.uuid;

    		const targetNames = [];

    		if ( PATH_PROPERTIES[ target.path ] === PATH_PROPERTIES.weights ) {

    			node.traverse( function ( object ) {

    				if ( object.morphTargetInfluences ) {

    					targetNames.push( object.name ? object.name : object.uuid );

    				}

    			} );

    		} else {

    			targetNames.push( targetName );

    		}

    		let TypedKeyframeTrack;

    		switch ( PATH_PROPERTIES[ target.path ] ) {

    			case PATH_PROPERTIES.weights:

    				TypedKeyframeTrack = THREE.NumberKeyframeTrack;
    				break;

    			case PATH_PROPERTIES.rotation:

    				TypedKeyframeTrack = THREE.QuaternionKeyframeTrack;
    				break;

    			case PATH_PROPERTIES.position:
    			case PATH_PROPERTIES.scale:
    			default:
    				switch ( outputAccessor.itemSize ) {

    					case 1:
    						TypedKeyframeTrack = THREE.NumberKeyframeTrack;
    						break;
    					case 2:
    					case 3:
    						TypedKeyframeTrack = THREE.VectorKeyframeTrack;
    						break;

    				}

    				break;

    		}

    		const interpolation = sampler.interpolation !== undefined ? INTERPOLATION[ sampler.interpolation ] : THREE.InterpolateLinear;

    		const outputArray = this._getArrayFromAccessor( outputAccessor );

    		for ( let j = 0, jl = targetNames.length; j < jl; j ++ ) {

    			const track = new TypedKeyframeTrack(
    				targetNames[ j ] + '.' + PATH_PROPERTIES[ target.path ],
    				inputAccessor.array,
    				outputArray,
    				interpolation
    			);

    			// Override interpolation with custom factory method.
    			if ( interpolation === 'CUBICSPLINE' ) {

    				this._createCubicSplineTrackInterpolant( track );

    			}

    			tracks.push( track );

    		}

    		return tracks;

    	}

    	_getArrayFromAccessor( accessor ) {

    		let outputArray = accessor.array;

    		if ( accessor.normalized ) {

    			const scale = getNormalizedComponentScale( outputArray.constructor );
    			const scaled = new Float32Array( outputArray.length );

    			for ( let j = 0, jl = outputArray.length; j < jl; j ++ ) {

    				scaled[ j ] = outputArray[ j ] * scale;

    			}

    			outputArray = scaled;

    		}

    		return outputArray;

    	}

    	_createCubicSplineTrackInterpolant( track ) {

    		track.createInterpolant = function InterpolantFactoryMethodGLTFCubicSpline( result ) {

    			// A CUBICSPLINE keyframe in glTF has three output values for each input value,
    			// representing inTangent, splineVertex, and outTangent. As a result, track.getValueSize()
    			// must be divided by three to get the interpolant's sampleSize argument.

    			const interpolantType = ( this instanceof THREE.QuaternionKeyframeTrack ) ? GLTFCubicSplineQuaternionInterpolant : GLTFCubicSplineInterpolant;

    			return new interpolantType( this.times, this.values, this.getValueSize() / 3, result );

    		};

    		// Mark as CUBICSPLINE. `track.getInterpolation()` doesn't support custom interpolants.
    		track.createInterpolant.isInterpolantFactoryMethodGLTFCubicSpline = true;

    	}

    }

    /**
     * @param {BufferGeometry} geometry
     * @param {GLTF.Primitive} primitiveDef
     * @param {GLTFParser} parser
     */
    function computeBounds( geometry, primitiveDef, parser ) {

    	const attributes = primitiveDef.attributes;

    	const box = new THREE.Box3();

    	if ( attributes.POSITION !== undefined ) {

    		const accessor = parser.json.accessors[ attributes.POSITION ];

    		const min = accessor.min;
    		const max = accessor.max;

    		// glTF requires 'min' and 'max', but VRM (which extends glTF) currently ignores that requirement.

    		if ( min !== undefined && max !== undefined ) {

    			box.set(
    				new THREE.Vector3( min[ 0 ], min[ 1 ], min[ 2 ] ),
    				new THREE.Vector3( max[ 0 ], max[ 1 ], max[ 2 ] )
    			);

    			if ( accessor.normalized ) {

    				const boxScale = getNormalizedComponentScale( WEBGL_COMPONENT_TYPES[ accessor.componentType ] );
    				box.min.multiplyScalar( boxScale );
    				box.max.multiplyScalar( boxScale );

    			}

    		} else {

    			console.warn( 'THREE.GLTFLoader: Missing min/max properties for accessor POSITION.' );

    			return;

    		}

    	} else {

    		return;

    	}

    	const targets = primitiveDef.targets;

    	if ( targets !== undefined ) {

    		const maxDisplacement = new THREE.Vector3();
    		const vector = new THREE.Vector3();

    		for ( let i = 0, il = targets.length; i < il; i ++ ) {

    			const target = targets[ i ];

    			if ( target.POSITION !== undefined ) {

    				const accessor = parser.json.accessors[ target.POSITION ];
    				const min = accessor.min;
    				const max = accessor.max;

    				// glTF requires 'min' and 'max', but VRM (which extends glTF) currently ignores that requirement.

    				if ( min !== undefined && max !== undefined ) {

    					// we need to get max of absolute components because target weight is [-1,1]
    					vector.setX( Math.max( Math.abs( min[ 0 ] ), Math.abs( max[ 0 ] ) ) );
    					vector.setY( Math.max( Math.abs( min[ 1 ] ), Math.abs( max[ 1 ] ) ) );
    					vector.setZ( Math.max( Math.abs( min[ 2 ] ), Math.abs( max[ 2 ] ) ) );


    					if ( accessor.normalized ) {

    						const boxScale = getNormalizedComponentScale( WEBGL_COMPONENT_TYPES[ accessor.componentType ] );
    						vector.multiplyScalar( boxScale );

    					}

    					// Note: this assumes that the sum of all weights is at most 1. This isn't quite correct - it's more conservative
    					// to assume that each target can have a max weight of 1. However, for some use cases - notably, when morph targets
    					// are used to implement key-frame animations and as such only two are active at a time - this results in very large
    					// boxes. So for now we make a box that's sometimes a touch too small but is hopefully mostly of reasonable size.
    					maxDisplacement.max( vector );

    				} else {

    					console.warn( 'THREE.GLTFLoader: Missing min/max properties for accessor POSITION.' );

    				}

    			}

    		}

    		// As per comment above this box isn't conservative, but has a reasonable size for a very large number of morph targets.
    		box.expandByVector( maxDisplacement );

    	}

    	geometry.boundingBox = box;

    	const sphere = new THREE.Sphere();

    	box.getCenter( sphere.center );
    	sphere.radius = box.min.distanceTo( box.max ) / 2;

    	geometry.boundingSphere = sphere;

    }

    /**
     * @param {BufferGeometry} geometry
     * @param {GLTF.Primitive} primitiveDef
     * @param {GLTFParser} parser
     * @return {Promise<BufferGeometry>}
     */
    function addPrimitiveAttributes( geometry, primitiveDef, parser ) {

    	const attributes = primitiveDef.attributes;

    	const pending = [];

    	function assignAttributeAccessor( accessorIndex, attributeName ) {

    		return parser.getDependency( 'accessor', accessorIndex )
    			.then( function ( accessor ) {

    				geometry.setAttribute( attributeName, accessor );

    			} );

    	}

    	for ( const gltfAttributeName in attributes ) {

    		const threeAttributeName = ATTRIBUTES[ gltfAttributeName ] || gltfAttributeName.toLowerCase();

    		// Skip attributes already provided by e.g. Draco extension.
    		if ( threeAttributeName in geometry.attributes ) continue;

    		pending.push( assignAttributeAccessor( attributes[ gltfAttributeName ], threeAttributeName ) );

    	}

    	if ( primitiveDef.indices !== undefined && ! geometry.index ) {

    		const accessor = parser.getDependency( 'accessor', primitiveDef.indices ).then( function ( accessor ) {

    			geometry.setIndex( accessor );

    		} );

    		pending.push( accessor );

    	}

    	assignExtrasToUserData( geometry, primitiveDef );

    	computeBounds( geometry, primitiveDef, parser );

    	return Promise.all( pending ).then( function () {

    		return primitiveDef.targets !== undefined
    			? addMorphTargets( geometry, primitiveDef.targets, parser )
    			: geometry;

    	} );

    }

    class FontLoader extends THREE.Loader {

    	constructor( manager ) {

    		super( manager );

    	}

    	load( url, onLoad, onProgress, onError ) {

    		const scope = this;

    		const loader = new THREE.FileLoader( this.manager );
    		loader.setPath( this.path );
    		loader.setRequestHeader( this.requestHeader );
    		loader.setWithCredentials( this.withCredentials );
    		loader.load( url, function ( text ) {

    			const font = scope.parse( JSON.parse( text ) );

    			if ( onLoad ) onLoad( font );

    		}, onProgress, onError );

    	}

    	parse( json ) {

    		return new Font( json );

    	}

    }

    //

    class Font {

    	constructor( data ) {

    		this.isFont = true;

    		this.type = 'Font';

    		this.data = data;

    	}

    	generateShapes( text, size = 100 ) {

    		const shapes = [];
    		const paths = createPaths( text, size, this.data );

    		for ( let p = 0, pl = paths.length; p < pl; p ++ ) {

    			shapes.push( ...paths[ p ].toShapes() );

    		}

    		return shapes;

    	}

    }

    function createPaths( text, size, data ) {

    	const chars = Array.from( text );
    	const scale = size / data.resolution;
    	const line_height = ( data.boundingBox.yMax - data.boundingBox.yMin + data.underlineThickness ) * scale;

    	const paths = [];

    	let offsetX = 0, offsetY = 0;

    	for ( let i = 0; i < chars.length; i ++ ) {

    		const char = chars[ i ];

    		if ( char === '\n' ) {

    			offsetX = 0;
    			offsetY -= line_height;

    		} else {

    			const ret = createPath( char, scale, offsetX, offsetY, data );
    			offsetX += ret.offsetX;
    			paths.push( ret.path );

    		}

    	}

    	return paths;

    }

    function createPath( char, scale, offsetX, offsetY, data ) {

    	const glyph = data.glyphs[ char ] || data.glyphs[ '?' ];

    	if ( ! glyph ) {

    		console.error( 'THREE.Font: character "' + char + '" does not exists in font family ' + data.familyName + '.' );

    		return;

    	}

    	const path = new THREE.ShapePath();

    	let x, y, cpx, cpy, cpx1, cpy1, cpx2, cpy2;

    	if ( glyph.o ) {

    		const outline = glyph._cachedOutline || ( glyph._cachedOutline = glyph.o.split( ' ' ) );

    		for ( let i = 0, l = outline.length; i < l; ) {

    			const action = outline[ i ++ ];

    			switch ( action ) {

    				case 'm': // moveTo

    					x = outline[ i ++ ] * scale + offsetX;
    					y = outline[ i ++ ] * scale + offsetY;

    					path.moveTo( x, y );

    					break;

    				case 'l': // lineTo

    					x = outline[ i ++ ] * scale + offsetX;
    					y = outline[ i ++ ] * scale + offsetY;

    					path.lineTo( x, y );

    					break;

    				case 'q': // quadraticCurveTo

    					cpx = outline[ i ++ ] * scale + offsetX;
    					cpy = outline[ i ++ ] * scale + offsetY;
    					cpx1 = outline[ i ++ ] * scale + offsetX;
    					cpy1 = outline[ i ++ ] * scale + offsetY;

    					path.quadraticCurveTo( cpx1, cpy1, cpx, cpy );

    					break;

    				case 'b': // bezierCurveTo

    					cpx = outline[ i ++ ] * scale + offsetX;
    					cpy = outline[ i ++ ] * scale + offsetY;
    					cpx1 = outline[ i ++ ] * scale + offsetX;
    					cpy1 = outline[ i ++ ] * scale + offsetY;
    					cpx2 = outline[ i ++ ] * scale + offsetX;
    					cpy2 = outline[ i ++ ] * scale + offsetY;

    					path.bezierCurveTo( cpx1, cpy1, cpx2, cpy2, cpx, cpy );

    					break;

    			}

    		}

    	}

    	return { offsetX: glyph.ha * scale, path: path };

    }

    /**
     * Text = 3D Text
     *
     * parameters = {
     *  font: <THREE.Font>, // font
     *
     *  size: <float>, // size of the text
     *  height: <float>, // thickness to extrude text
     *  curveSegments: <int>, // number of points on the curves
     *
     *  bevelEnabled: <bool>, // turn on bevel
     *  bevelThickness: <float>, // how deep into text bevel goes
     *  bevelSize: <float>, // how far from text outline (including bevelOffset) is bevel
     *  bevelOffset: <float> // how far from text outline does bevel start
     * }
     */


    class TextGeometry extends THREE.ExtrudeGeometry {

    	constructor( text, parameters = {} ) {

    		const font = parameters.font;

    		if ( font === undefined ) {

    			super(); // generate default extrude geometry

    		} else {

    			const shapes = font.generateShapes( text, parameters.size );

    			// translate parameters to ExtrudeGeometry API

    			parameters.depth = parameters.height !== undefined ? parameters.height : 50;

    			// defaults

    			if ( parameters.bevelThickness === undefined ) parameters.bevelThickness = 10;
    			if ( parameters.bevelSize === undefined ) parameters.bevelSize = 8;
    			if ( parameters.bevelEnabled === undefined ) parameters.bevelEnabled = false;

    			super( shapes, parameters );

    		}

    		this.type = 'TextGeometry';

    	}

    }

    class ConfiguratorUtils {
        constructor() {
            this.selectedLayout = "fdebce";
            this.totalWidth = 0;
            this.totalHeight = 0;
            this.showDimension = false;
            this.width = window.innerWidth;
            this.height = window.innerHeight;
            this.backPanelMeshes = [];
            this.dimensionMeshes = [];
            this.widthMetricMeshes = [];
            this.heightMetricMeshes = [];
            this.meshlayouts = {};
            this.meshes = [];
        }
        setSelectedLayout(selectedLayout) {
            this.selectedLayout = selectedLayout;
        }
        setGLBF(glbf) {
            this.glbf = glbf;
        }
        setTotalWidth(totalWidth) {
            this.totalWidth = totalWidth;
        }
        setTotalHeight(totalHeight) {
            this.totalHeight = totalHeight;
        }
        setShowDimension(showDimension) {
            this.showDimension = showDimension;
        }
        addMeshToBackPanels(mesh) {
            this.backPanelMeshes.push(mesh);
        }
        addMeshToDimensions(mesh) {
            this.dimensionMeshes.push(mesh);
        }
        addMeshToWidth(mesh) {
            this.widthMetricMeshes.push(mesh);
        }
        addMeshToHeight(mesh) {
            this.heightMetricMeshes.push(mesh);
        }
        addMeshToLayouts(key, mesh) {
            if (!this.meshlayouts.hasOwnProperty(key))
                this.meshlayouts[key] = [];
            this.meshlayouts[key].push(mesh);
        }
        addMesh(assetItem) {
            this.meshes.push(assetItem);
        }
        setVisibleBoolen(meshName, isVisible) {
            this.meshes = this.meshes.map((mesh) => {
                if (mesh.name == meshName) {
                    return Object.assign(Object.assign({}, mesh), { visible: isVisible });
                }
                return mesh;
            });
            visibleAssets.set(this.meshes);
        }
        setDimensions(meshName, metric) {
            this.meshes = this.meshes.map((mesh) => {
                if (mesh.name == meshName) {
                    if (!(mesh === null || mesh === void 0 ? void 0 : mesh.dimensions))
                        mesh.dimensions = {};
                    mesh.dimensions = Object.assign(Object.assign({}, mesh.dimensions), metric);
                }
                return mesh;
            });
        }
        updateWidthDimension(updateValue) {
            this.meshes = this.meshes.map((mesh) => {
                if (!(mesh === null || mesh === void 0 ? void 0 : mesh.dimensions))
                    return;
                mesh.dimensions.width = updateValue;
                return mesh;
            });
        }
        updateHeightDimension(updateValue) {
            this.meshes = this.meshes.map((mesh) => {
                if (!(mesh === null || mesh === void 0 ? void 0 : mesh.dimensions))
                    return;
                mesh.dimensions.height = updateValue;
                return mesh;
            });
        }
        getMesh(meshName) {
            return this.meshes.filter((mesh) => mesh.name == meshName);
        }
        getVisibleMeshes() {
            return this.meshes.filter((mesh) => mesh.visible === true);
        }
        setVisibleMesh(assetItem) {
            if ((assetItem === null || assetItem === void 0 ? void 0 : assetItem.assetItemKind) != "Mesh" || assetItem.name == "Camera")
                return;
            this.addMesh({
                id: assetItem.id,
                name: assetItem.name,
                visible: true,
            });
        }
    }

    class ThreeUtils extends ConfiguratorUtils {
        constructor({ renderElement }) {
            super();
            this.animateWithGSAP = (objectPosition, duration, transform, callbackFn) => {
                const gsapOptions = {
                    duration: duration || 1,
                    x: (transform === null || transform === void 0 ? void 0 : transform.xAxis) || transform.x,
                    y: (transform === null || transform === void 0 ? void 0 : transform.yAxis) || transform.y,
                    z: (transform === null || transform === void 0 ? void 0 : transform.zAxis) || transform.z,
                    ease: Power2.easeOut,
                };
                if (callbackFn) {
                    gsapOptions.onUpdate = () => {
                        callbackFn();
                    };
                }
                gsap.gsap.to(objectPosition, gsapOptions);
            };
            this.scene = new THREE__namespace.Scene();
            this.camera = this.initializeCamera();
            this.renderElement = renderElement;
            this.renderer = this.setRenderer();
            this.orbitControls = this.initializeOrbitControls();
            this.animate = this.animate.bind(this);
        }
        addGLTFLoader(glbFile, defaultConfig, loadAndApplyRules) {
            this.loader = new GLTFLoader();
            this.loader.load(glbFile, (gltf) => __awaiter(this, void 0, void 0, function* () {
                this.setGLBF(gltf);
                this.updateObjectAndCamera();
                this.scene.add(gltf.scene);
                this.scene.background = new THREE__namespace.Color(get_store_value(theme).baseColor);
                gltf.scene.traverse((object) => {
                    if (object.isMesh) {
                        object.userData.originalMaterial = object.material.clone();
                        object.userData.originalPosition = object.position.clone();
                        object.userData.hoverPosition = object.position.clone();
                    }
                });
                yield loadAndApplyRules();
            }), (xhr) => {
                var _a, _b, _c, _d;
                console.log(`${(xhr.loaded / xhr.total) * 100}% loaded`);
                this.setTotalWidth(Number((_a = defaultConfig === null || defaultConfig === void 0 ? void 0 : defaultConfig.width) === null || _a === void 0 ? void 0 : _a.defaultAttributeValue));
                width.set(Number((_b = defaultConfig === null || defaultConfig === void 0 ? void 0 : defaultConfig.width) === null || _b === void 0 ? void 0 : _b.defaultAttributeValue));
                this.setTotalHeight(Number((_c = defaultConfig === null || defaultConfig === void 0 ? void 0 : defaultConfig.height) === null || _c === void 0 ? void 0 : _c.defaultAttributeValue));
                height.set(Number((_d = defaultConfig === null || defaultConfig === void 0 ? void 0 : defaultConfig.height) === null || _d === void 0 ? void 0 : _d.defaultAttributeValue));
            }, (error) => {
                console.log("An error happened", error);
            });
        }
        updateObjectAndCamera() {
            const obj = this.glbf.scene;
            const box = new THREE__namespace.Box3().setFromObject(obj);
            const size = box.getSize(new THREE__namespace.Vector3()).length();
            const center = box.getCenter(new THREE__namespace.Vector3());
            obj.position.x = obj.position.x - center.x;
            obj.position.y = obj.position.y - center.y;
            obj.position.z = obj.position.z - center.z;
            this.camera.near = size / 100;
            this.camera.far = size * 100;
            this.camera.updateProjectionMatrix();
            const initialCameraPosition = new THREE__namespace.Vector3().copy(this.camera.position);
            this.camera.position.copy(center);
            this.camera.position.x = 0;
            this.camera.position.y = 0;
            this.camera.position.z = size / 1.25;
            this.camera.lookAt(center);
            this.orbitControls.maxDistance = size * 10;
            this.orbitControls.update();
            if (initialCameraPosition.z) {
                gsap.gsap.fromTo(this.camera.position, {
                    x: initialCameraPosition.x,
                    y: initialCameraPosition.y,
                    z: initialCameraPosition.z,
                }, {
                    duration: 1,
                    x: this.camera.position.x,
                    y: this.camera.position.y,
                    z: this.camera.position.z,
                    ease: Power2.easeInOut,
                });
            }
        }
        initializeCamera(fov = 75, aspect = 1, near = 0.1, far = 1000) {
            this.camera = new THREE__namespace.PerspectiveCamera(fov, aspect, near, far);
            this.scene.add(this.camera);
            return this.camera;
        }
        setCameraPosition(coordinates) {
            this.camera.position.set(...coordinates);
        }
        initializeOrbitControls() {
            this.orbitControls = new OrbitControls(this.camera, this.renderElement);
            this.orbitControls.minAzimuthAngle = -(1 / 4) * Math.PI;
            this.orbitControls.maxAzimuthAngle = (1 / 4) * Math.PI;
            this.orbitControls.minPolarAngle = -(1 / 2) * Math.PI;
            this.orbitControls.maxPolarAngle = (1 / 2) * Math.PI;
            this.orbitControls.enableZoom = false;
            this.setAnimateEffect();
            return this.orbitControls;
        }
        setOrbitControlProperties(propertyName, propertyValue) {
            this.orbitControls[propertyName] = propertyValue;
        }
        setLight({ light, position }) {
            light.position.set(...position);
            this.scene.add(light);
        }
        setRenderer() {
            this.renderer = new THREE__namespace.WebGLRenderer({
                canvas: this.renderElement,
                antialias: true,
                preserveDrawingBuffer: true,
                alpha: true,
            });
            this.width > 900
                ? (this.width = window.innerWidth - window.innerWidth / 2)
                : this.width < 380
                    ? (this.width = window.innerWidth - 70)
                    : (this.width = this.width - 150);
            this.height > 900
                ? (this.height = window.innerHeight - 400)
                : (this.height = window.innerHeight - 350);
            this.renderer.setSize(this.width, this.height);
            this.renderer.setPixelRatio(window.devicePixelRatio);
            this.renderer.setClearColor(0xffffff);
            this.renderer.shadowMap.enabled = true;
            return this.renderer;
        }
        animate() {
            requestAnimationFrame(this.animate);
            this.orbitControls.update();
            this.renderer.render(this.scene, this.camera);
        }
        setAnimateEffect() {
            let initialCameraState = {
                target: new THREE__namespace.Vector3(),
                position: new THREE__namespace.Vector3(),
                zoom: 1,
            };
            const minRotation = -4;
            const maxRotation = 4;
            this.orbitControls.addEventListener("start", () => {
                initialCameraState.target.copy(this.orbitControls.target);
                initialCameraState.position.copy(this.camera.position);
                initialCameraState.zoom = this.camera.zoom;
            });
            this.orbitControls.addEventListener("end", () => {
                initialCameraState.target
                    .clone()
                    .sub(this.orbitControls.target);
                const positionDiff = initialCameraState.position
                    .clone()
                    .sub(this.camera.position);
                initialCameraState.zoom - this.camera.zoom;
                const rotationChange = positionDiff.length();
                const azimuthAngle = Math.atan2(this.camera.position.x - this.orbitControls.target.x, this.camera.position.z - this.orbitControls.target.z).toFixed(3);
                if (azimuthAngle == this.orbitControls.minAzimuthAngle.toFixed(3) ||
                    azimuthAngle == this.orbitControls.maxAzimuthAngle.toFixed(3)) {
                    this.animateOrbitControls(initialCameraState);
                }
                else if (rotationChange >= minRotation &&
                    rotationChange <= maxRotation) ;
                else {
                    this.animateOrbitControls(initialCameraState);
                }
            });
        }
        onMouseMove(event, root) {
            const mouse = new THREE__namespace.Vector2();
            const raycaster = new THREE__namespace.Raycaster();
            // raycaster.params.Points.threshold = 0.1;
            const rect = this.renderer.domElement.getBoundingClientRect();
            mouse.x = ((event.clientX - rect.left) / rect.width) * 2 - 1;
            mouse.y = -((event.clientY - rect.top) / rect.height) * 2 + 1;
            raycaster.setFromCamera(mouse, this.camera);
            const intersects = raycaster.intersectObjects(this.glbf.scene.children, true);
            this.glbf.scene.traverse((child) => {
                if (child.isMesh) {
                    child.material = child.userData.originalMaterial;
                    this.animateWithGSAP(child.position, child.userData.animatePosition.speed, child.userData.originalPosition);
                    this.animateWithGSAP(child.rotation, child.userData.animateRotationPosition.speed, child.userData.originalPosition);
                }
            });
            const layerOptions = root.querySelector(".layer-options");
            const canvasContainer = root.querySelector("#canvas-container");
            if (intersects.length > 0) {
                const object = intersects[0].object;
                this.glbf.scene.traverse((child) => {
                    var _a, _b;
                    if (child.name == object.name) {
                        if (((_a = child.userData.animatePosition) === null || _a === void 0 ? void 0 : _a.groupedAtoms) &&
                            ((_b = child.userData.animatePosition) === null || _b === void 0 ? void 0 : _b.groupedAtoms.length) &&
                            child.userData.animatePosition.groupedAtoms.includes(child.name)) {
                            child.userData.animatePosition.groupedAtoms.map((atom) => {
                                let mesh = this.scene.getObjectByName(atom);
                                this.applyColorAndAnimationToMesh(mesh);
                            });
                            currentHoveredMeshOrGroup.set(child.userData.animatePosition.groupedAtoms);
                            currentAtomGroup.set(child.userData.groupedAtomName);
                            if (child.userData.atomOptions.length) {
                                child.userData.atomOptions.map((atomOption) => {
                                    updateUIElements("popupOptions", true);
                                    if (atomOption.attribute.name == "rowHeight") {
                                        let rowHeight = get_store_value(uiConfig)[UIElementKind.rowHeight];
                                        rowHeight.options = atomOption.option.value.map((item) => item.value);
                                        updateUIElements(UIElementKind.rowHeight, true);
                                        updateUIElements("popupOptions", true);
                                    }
                                    if (atomOption.attribute.name == "drawers") {
                                        let drawers = get_store_value(uiConfig)[UIElementKind.drawers];
                                        drawers.options = atomOption.option.value.map((item) => item.value);
                                        updateUIElements(UIElementKind.drawers, true);
                                        updateUIElements("popupOptions", true);
                                    }
                                });
                                const x = window.innerWidth > 768
                                    ? rect.right - canvasContainer.offsetWidth - 20
                                    : event.clientX - rect.left + 175;
                                const y = event.clientY - rect.top;
                                layerOptions.style.display = "block";
                                layerOptions.style.left = `${x}px`;
                                layerOptions.style.top = `${y}px`;
                                layerOptions.style.zIndex = 8;
                                canvasContainer.appendChild(layerOptions);
                            }
                            else {
                                updateUIElements(UIElementKind.rowHeight, false);
                                updateUIElements(UIElementKind.drawers, false);
                            }
                        }
                        else {
                            layerOptions.style.display = "none";
                            this.applyColorAndAnimationToMesh(child);
                            currentHoveredMeshOrGroup.set([child.name]);
                            updateUIElements(UIElementKind.rowHeight, false);
                            updateUIElements(UIElementKind.drawers, false);
                            updateUIElements("popupOptions", false);
                        }
                    }
                });
            }
        }
        onMouseHandler(event, root) {
            const layerOptions = root.querySelector(".layer-options");
            const canvasContainer = root.querySelector("#canvas-container");
            const rect = this.renderer.domElement.getBoundingClientRect();
            const x = window.innerWidth > 768
                ? rect.right - canvasContainer.offsetWidth - 10
                : event.clientX - rect.right;
            const y = event.clientY - rect.top;
            layerOptions.style.display = "block";
            layerOptions.style.left = `${x}px`;
            layerOptions.style.top = `${y}px`;
            layerOptions.style.zIndex = 8;
            canvasContainer.appendChild(layerOptions);
        }
        onMouseOut(event, root) {
            const layerOptions = root.querySelector(".layer-options");
            layerOptions.style.display = "none";
            // updateUIElements(UIElementKind.rowHeight, false);
            // updateUIElements(UIElementKind.drawers, false);
        }
        onMouseClick(event) {
            const mouse = new THREE__namespace.Vector2();
            const raycaster = new THREE__namespace.Raycaster();
            mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
            mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
            raycaster.setFromCamera(mouse, this.camera);
            const intersects = raycaster.intersectObjects(this.glbf.scene.children, true);
            if (intersects.length > 0) {
                intersects[0].object;
            }
        }
        applyColorAndAnimationToMesh(mesh) {
            let material = mesh.material.clone();
            const currentColor = material.color.clone();
            const hoverAlphaChange = 50;
            const newAlpha = Math.max(0, Math.min(255, currentColor.r + hoverAlphaChange));
            const hoverColor = currentColor.clone();
            hoverColor.r = newAlpha;
            material.color = hoverColor;
            mesh.material = material;
            this.animateWithGSAP(mesh.position, mesh.userData.animatePosition.speed, mesh.userData.animatePosition);
            this.animateWithGSAP(mesh.rotation, mesh.userData.animateRotationPosition.speed, mesh.userData.animateRotationPosition);
        }
        animateOrbitControls(initialCameraState) {
            gsap.gsap.to(this.orbitControls.target, {
                duration: 1,
                x: initialCameraState.target.x,
                y: initialCameraState.target.y,
                z: initialCameraState.target.z,
                ease: Power2.easeOut,
            });
            gsap.gsap.to(this.camera.position, {
                duration: 1,
                x: initialCameraState.position.x,
                y: initialCameraState.position.y,
                z: initialCameraState.position.z,
                ease: Power2.easeOut,
                onUpdate: () => {
                    this.orbitControls.update();
                },
            });
            gsap.gsap.to(this.camera, {
                duration: 1,
                zoom: initialCameraState.zoom,
                ease: Power2.easeOut,
                onUpdate: () => {
                    this.camera.updateProjectionMatrix();
                },
            });
        }
    }

    class ConfiguratorLib extends ThreeUtils {
        constructor({ renderElement }) {
            super({ renderElement });
            this.updateWidth = (element) => {
                this.glbf.scene.scale.x = element.value;
                // this.updateWidthDimension(element.value);
                this.setTotalWidth(Math.ceil(10 * (+element.value + 1)));
                this.updateWidthDimension(this.totalWidth);
                width.set(this.totalWidth);
                // this.glbf.scene.traverse((child: any) => {
                //   if (this.widthMetricMeshes.includes(child.name)) {
                //     this.setTotalWidth(
                //       Math.ceil(child.userData.width * (element.value))
                //     );
                //     this.changeText(
                //       child.name,
                //       `${Math.ceil(child.userData.width * element.value)}`
                //     );
                //   }
                // });
            };
            this.updateHeight = (element) => {
                this.glbf.scene.scale.y = Number(element.value);
                // this.updateHeightDimension(element.value);
                this.setTotalHeight(Math.ceil(10 * (+element.value + 1)));
                this.updateHeightDimension(this.totalHeight);
                height.set(this.totalHeight);
                // this.glbf.scene.traverse((child: any) => {
                //   if (this.heightMetricMeshes.includes(child.name)) {
                //     this.setTotalHeight(
                //       Math.ceil(child.userData.height * (+element.value + 1))
                //     );
                //     this.changeText(
                //       child.name,
                //       `${Math.ceil(child.userData.height * (+element.value + 1))}`
                //     );
                //   }
                // });
            };
            this.updateDepth = (element) => {
                this.glbf.scene.scale.z = +element.value / 40;
            };
            this.setLight({
                light: new THREE__namespace.DirectionalLight(0xffffff, 1),
                position: [30, 10, 90],
            });
            this.setLight({
                light: new THREE__namespace.AmbientLight(0xffffff, 1),
                position: [30, 10, 90],
            });
            this.initializeCamera();
            this.initializeOrbitControls();
            this.setRenderer();
        }
        executeBackPanelRule(rule, isVisible) {
            return __awaiter(this, void 0, void 0, function* () {
                let meshes = getMeshesFromRule(rule, "backpanel");
                if (isVisible) {
                    yield meshes.map((mesh) => {
                        updateMeshesToShowStore(meshesToShowForLayout, mesh);
                    });
                }
                else {
                    yield meshes.map((mesh) => {
                        updateMeshesToShowStore(meshesToHideForLayout, mesh);
                    });
                }
                addOrUpdateActiveRules(rule, ConfigRuleKind.backpanel);
                setCurrentUIConfig(UIElementKind.backpanel, isVisible);
            });
        }
        executeLayoutRule(rule, layoutKind, condition) {
            return __awaiter(this, void 0, void 0, function* () {
                let meshes = getMeshesFromRule(rule, "layout");
                if (condition.value == layoutKind) {
                    yield meshes.map((mesh) => {
                        updateMeshesToShowStore(meshesToShowForLayout, mesh);
                    });
                    addOrUpdateActiveRules(rule, ConfigRuleKind.layout);
                    setCurrentUIConfig(UIElementKind.layout, layoutKind);
                }
                else {
                    yield meshes.map((mesh) => {
                        updateMeshesToShowStore(meshesToHideForLayout, mesh);
                    });
                }
            });
        }
        executeWidthRule(rule, widthValue) {
            let meshes = getMeshesFromRule(rule, "visibility");
            if (meshes.length == 0) {
                rule.atomActions.map((action) => __awaiter(this, void 0, void 0, function* () {
                    this.scene.traverse((mesh) => {
                        if (mesh.name == action.atom.name) {
                            if (action.attribute.name == "scaleX") {
                                mesh.scale.x = Number(action.stringValue);
                                if (get_store_value(excludeRule)) {
                                    let children = [];
                                    mesh.traverse((child) => {
                                        if (child.isMesh && child.type == "Mesh") {
                                            children.push(child.name);
                                        }
                                    });
                                    children.map((child) => {
                                        let childDimension = this.getMesh(child);
                                        childDimension.width = 66 * Math.abs(Number(action.stringValue));
                                        childDimension.height = 25 * Math.abs(Number(action.stringValue));
                                        this.setDimensions(child, { height: childDimension.height, width: childDimension.width });
                                    });
                                }
                            }
                            if (action.attribute.name == "scaleY")
                                mesh.scale.y = Number(action.stringValue);
                            if (action.attribute.name == "scaleZ")
                                mesh.scale.z = Number(action.stringValue);
                            if (action.attribute.name == "positionX")
                                mesh.position.x = Number(action.stringValue);
                            if (action.attribute.name == "positionY")
                                mesh.position.y = Number(action.stringValue);
                            if (action.attribute.name == "positionZ")
                                mesh.position.z = Number(action.stringValue);
                        }
                    });
                }));
                this.updateObjectAndCamera();
            }
            rule.atomActions
                .filter((action) => action.attribute.name == "visibility")
                .map((action) => __awaiter(this, void 0, void 0, function* () {
                if (action.booleanValue) {
                    yield meshes.map((mesh) => {
                        updateMeshesToShowStore(meshesToShowForLayout, mesh);
                    });
                }
                else {
                    yield meshes.map((mesh) => {
                        updateMeshesToShowStore(meshesToHideForLayout, mesh);
                    });
                }
            }));
            addOrUpdateActiveRules(rule, ConfigRuleKind.width);
            setCurrentUIConfig(UIElementKind.width, widthValue);
        }
        executeHeightRule(rule, heightValue) {
            let meshes = getMeshesFromRule(rule, "visibility");
            if (meshes.length == 0) {
                rule.atomActions.map((action) => __awaiter(this, void 0, void 0, function* () {
                    this.scene.traverse((mesh) => {
                        if (mesh.name == action.atom.name) {
                            if (action.attribute.name == "scaleX")
                                mesh.scale.x = Number(action.stringValue);
                            if (action.attribute.name == "scaleY")
                                mesh.scale.y = Number(action.stringValue);
                            if (action.attribute.name == "scaleZ")
                                mesh.scale.z = Number(action.stringValue);
                        }
                    });
                }));
                this.updateObjectAndCamera();
            }
            rule.atomActions
                .filter((action) => action.attribute.name == "visibility")
                .map((action) => __awaiter(this, void 0, void 0, function* () {
                if (action.booleanValue) {
                    yield meshes.map((mesh) => {
                        updateMeshesToShowStore(meshesToShowForLayout, mesh);
                    });
                }
                else {
                    yield meshes.map((mesh) => {
                        updateMeshesToShowStore(meshesToHideForLayout, mesh);
                    });
                }
            }));
            addOrUpdateActiveRules(rule, ConfigRuleKind.width);
            setCurrentUIConfig(UIElementKind.width, heightValue);
        }
        executeColorRule(rule, color) {
            rule.atomActions.map((action) => {
                var _a, _b, _c, _d;
                let meshes = ((_a = action === null || action === void 0 ? void 0 : action.atom) === null || _a === void 0 ? void 0 : _a.id)
                    ? [action === null || action === void 0 ? void 0 : action.atom]
                    : ((_c = (_b = action === null || action === void 0 ? void 0 : action.dna) === null || _b === void 0 ? void 0 : _b.atoms) === null || _c === void 0 ? void 0 : _c.length)
                        ? [...(_d = action === null || action === void 0 ? void 0 : action.dna) === null || _d === void 0 ? void 0 : _d.atoms]
                        : [];
                meshes = meshes.map((atom) => atom.name);
                if (action.attribute.attributeKind == AttributeKind.color) {
                    this.scene.traverse((mesh) => {
                        if (mesh.isMesh && meshes.includes(mesh.name)) {
                            let material = mesh.material.clone();
                            material.color.set(new THREE__namespace.Color(color));
                            mesh.material = material;
                            mesh.userData.originalMaterial = mesh.material;
                        }
                    });
                }
                else if (action.attribute.attributeKind == AttributeKind.texture) {
                    this.scene.traverse((mesh) => {
                        if (mesh.isMesh && meshes.includes(mesh.name)) {
                            const textureLoader = new THREE__namespace.TextureLoader();
                            const texture = textureLoader.load(`${CONSTANTS.s3BaseURL}/${action.stringValue}`);
                            texture.wrapS = THREE__namespace.RepeatWrapping;
                            texture.wrapT = THREE__namespace.RepeatWrapping;
                            texture.repeat.set(1, 1);
                            let material = mesh.material.clone();
                            material.map = texture;
                            mesh.material = material;
                            mesh.userData.originalMaterial = mesh.material;
                        }
                    });
                }
            });
            addOrUpdateActiveRules(rule, ConfigRuleKind.color);
        }
        executeAdditionalStorageRule(rule, isVisible) {
            return __awaiter(this, void 0, void 0, function* () {
                let meshes = getMeshesFromRule(rule, "additionalStorage");
                if (isVisible) {
                    // let diffMeshes = symmetricDifference(get(meshesToHideForLayout), meshes);
                    yield meshes.map((mesh) => {
                        updateMeshesToShowStore(repetitiveShowMeshes, mesh);
                    });
                    // addOrUpdateActiveRules(rule, ConfigRuleKind.layout);
                    // setCurrentUIConfig(UIElementKind.layout, layoutKind);
                }
                else {
                    // let diffMeshes = symmetricDifference(get(meshesToShowForLayout), meshes);
                    yield meshes.map((mesh) => {
                        updateMeshesToShowStore(repetitiveHideMeshes, mesh);
                    });
                }
                // addOrUpdateActiveRules(rule, ConfigRuleKind.backpanel);
                setCurrentUIConfig(UIElementKind.additionalStorage, isVisible);
            });
        }
        executeDrawersRule(rule, drawerKind, condition) {
            return __awaiter(this, void 0, void 0, function* () {
                rule.atomActions
                    .filter((action) => action.attribute.name == "visibility")
                    .map((action) => __awaiter(this, void 0, void 0, function* () {
                    var _a, _b, _c, _d;
                    if (condition.value == drawerKind) {
                        let atoms = ((_a = action === null || action === void 0 ? void 0 : action.atom) === null || _a === void 0 ? void 0 : _a.id)
                            ? [action === null || action === void 0 ? void 0 : action.atom]
                            : ((_c = (_b = action === null || action === void 0 ? void 0 : action.dna) === null || _b === void 0 ? void 0 : _b.atoms) === null || _c === void 0 ? void 0 : _c.length)
                                ? [...(_d = action === null || action === void 0 ? void 0 : action.dna) === null || _d === void 0 ? void 0 : _d.atoms]
                                : [];
                        if (action.booleanValue) {
                            yield Promise.all(atoms.map((mesh) => {
                                updateMeshesToShowStore(meshesToShowForLayout, mesh.name);
                            }));
                        }
                        let popupOptions = get_store_value(currentUIConfig)["popupOptions"].value;
                        if (get_store_value(currentAtomGroup).length > 0) {
                            popupOptions[get_store_value(currentAtomGroup)].drawers = drawerKind;
                            setCurrentUIConfig("popupOptions", popupOptions);
                        }
                    }
                }));
                // addOrUpdateActiveRules(rule, ConfigRuleKind.backpanel);
                setCurrentUIConfig(UIElementKind.drawers, drawerKind);
            });
        }
        executeRowHeightRule(rule, currentRowHeight, condition) {
            return __awaiter(this, void 0, void 0, function* () {
                rule.atomActions.map((action) => __awaiter(this, void 0, void 0, function* () {
                    var _a, _b, _c, _d;
                    if (get_store_value(currentHoveredMeshOrGroup).includes(condition.atom) &&
                        condition.value == currentRowHeight) {
                        let atoms = ((_a = action === null || action === void 0 ? void 0 : action.atom) === null || _a === void 0 ? void 0 : _a.id)
                            ? [action === null || action === void 0 ? void 0 : action.atom]
                            : ((_c = (_b = action === null || action === void 0 ? void 0 : action.dna) === null || _b === void 0 ? void 0 : _b.atoms) === null || _c === void 0 ? void 0 : _c.length)
                                ? [...(_d = action === null || action === void 0 ? void 0 : action.dna) === null || _d === void 0 ? void 0 : _d.atoms]
                                : [];
                        atoms = atoms.map((atom) => atom.name);
                        this.scene.traverse((mesh) => {
                            if (mesh.isMesh && atoms.includes(mesh.name)) {
                                if (action.attribute.name == "positionY") {
                                    mesh.position.y = Number(action.stringValue);
                                    mesh.userData.originalPosition = mesh.position.clone();
                                    mesh.userData.animatePosition = mesh.position.clone();
                                }
                                else if (action.attribute.name == "scaleY") {
                                    mesh.scale.y = Number(action.stringValue);
                                }
                            }
                        });
                        let popupOptions = get_store_value(currentUIConfig)["popupOptions"].value;
                        if (get_store_value(currentAtomGroup).length > 0) {
                            popupOptions[get_store_value(currentAtomGroup)].rowHeight = currentRowHeight;
                            setCurrentUIConfig("popupOptions", popupOptions);
                        }
                    }
                }));
                setCurrentUIConfig(UIElementKind.rowHeight, currentRowHeight);
            });
        }
        executeMaterialRule(rule, material) {
            rule.atomActions.map((action) => {
                var _a, _b, _c, _d;
                let meshes = ((_a = action === null || action === void 0 ? void 0 : action.atom) === null || _a === void 0 ? void 0 : _a.id)
                    ? [action === null || action === void 0 ? void 0 : action.atom]
                    : ((_c = (_b = action === null || action === void 0 ? void 0 : action.dna) === null || _b === void 0 ? void 0 : _b.atoms) === null || _c === void 0 ? void 0 : _c.length)
                        ? [...(_d = action === null || action === void 0 ? void 0 : action.dna) === null || _d === void 0 ? void 0 : _d.atoms]
                        : [];
                meshes = meshes.map((atom) => atom.name);
                if (action.attribute.attributeKind == AttributeKind.texture) {
                    this.scene.traverse((mesh) => {
                        if (mesh.isMesh && meshes.includes(mesh.name)) {
                            const textureLoader = new THREE__namespace.TextureLoader();
                            textureLoader.crossOrigin = 'anonymous';
                            const texture = textureLoader.load(`${CONSTANTS.s3BaseURL}/${action.stringValue}`);
                            texture.wrapS = THREE__namespace.RepeatWrapping;
                            texture.wrapT = THREE__namespace.RepeatWrapping;
                            texture.repeat.set(1, 1);
                            let material = mesh.material.clone();
                            material.map = texture;
                            mesh.material = material;
                            mesh.userData.originalMaterial = mesh.material;
                        }
                    });
                }
            });
        }
        // public executeSetDefaultDimensionRule(rule: any){
        //   rule.atomActions.map((action: any) => {
        //     let meshes = action?.atom?.id
        //       ? [action?.atom]
        //       : action?.dna?.atoms?.length
        //       ? [...action?.dna?.atoms]
        //       : [];
        //   });
        // }
        toggleDimensionMetrics() {
            this.setShowDimension(!this.showDimension);
            this.scene.traverse((child) => {
                if (this.dimensionMeshes.includes(child.name)) {
                    child.visible = !child.visible;
                }
            });
        }
        changeText(meshName, newText) {
            const existingTextMesh = this.scene.getObjectByName(meshName);
            const fontLoader = new FontLoader();
            fontLoader.load("https://unpkg.com/three@0.152.2/examples/fonts/helvetiker_regular.typeface.json", function (font) {
                const textGeometry = new TextGeometry(newText, {
                    font: font,
                    size: 1,
                    height: 0.1,
                });
                existingTextMesh.rotation.x = Math.PI / 90; //10.95
                existingTextMesh.scale.set(0.1, 0.1, 0.1);
                existingTextMesh.geometry = textGeometry;
            });
        }
    }

    var configuratorLib = /*#__PURE__*/Object.freeze({
        __proto__: null,
        ConfiguratorLib: ConfiguratorLib
    });

    exports.createModifiedProduct = createModifiedProduct;
    exports.getModifiedProductId = getModifiedProductId;
    exports.getProductImages = getProductImages;
    exports.getProductInfo = getProductInfo;
    exports.getProductPrice = getProductPrice;
    exports.init = init;
    exports.setLocale = setLocale;

}));
//# sourceMappingURL=bundle.js.map
